# Repo for the new Cruises International Website.




# Cruises International Cruise Factory Importer



On truserv, inside `/var/www/secure_html/cruise_factory_local_files`, there is 3 bash script files that run a full import into cruises.co.za, for live and dev, and stage.

I recommend using just dev and live, i don't think stage is working right.

The files are:

1. update.live.cruises.co.za.NEW.sh 
2. update.dev.cruises.co.za.sh (This file doesn't re-download all the tables from cruise factory, there is one that does, it is called update.dev.cruises.co.za-with-DL.sh)

Look at `crontab -l` to see when the importer runs.

There is a log file that outputs all the outputs from running the importer on cron, that can be found at:

`/var/www/html/cruise_factory_local_files/logs/cruise_factory_cron_import.logs`


inside /var/www/secure_html/cruise_factory_local_files there is also all the plain text files of the tables that have been imported.

If the importer empties the website overnight, the first check to do is make sure the import feed is actually working - go and look at:

`http://www.bookacruise.co.za/getXML.php?Table=specials`

and make sure there is data there. Make sure the special the client has raised is even in the feed. If it is, then you can try re-run the importer manually, or check the import files.

If the bookacruise feed is empty or broken - you need to contact Reg:

`Reg Hutchison at reg@zerotimenow.com or 083 601 2064`


To run the importer manually, you can just go into cruise_factory_local_files, and run `bash update.live.cruises.co.za.NEW.sh`.

i would recommend running this script in a screen session, so that it can run in the background and you can log back in to check on it's progress. It takes about 30 minutes to do a full import. It's very inefficient, but there is no other way at the moment to track which records need to be deleted or updated, so it empties everything and re-imports every day.

### Screen crash course:

create a new screen called importer:

`screen -dmS importer`

open the screen session with:

`screen -r importer`

once you're inside, you can run the `bash update.live.cruises.co.za.NEW.sh` script, and to exit the screen, press:

`Ctrl A` and then `Ctrl D`







