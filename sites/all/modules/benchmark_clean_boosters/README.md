# Benchmark Clean Boosters
## Settings & Preferences for Boost, Expire, HTTPRL, all the fasty stuff



![Benchmark Logo](https://bytebucket.org/teampixelpower/benchmark_clean/raw/9b75eca946725261443dd28c61b71bcfaf098630/logo.png?token=8604e47c5f790059195b0e8c34cf42274c61173e)



### Changelog

#### March 2016
- 7.x-0.1 - Separated out from benchmark_clean_contrib_module_settings
