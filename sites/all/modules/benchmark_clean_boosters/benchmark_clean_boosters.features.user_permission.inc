<?php
/**
 * @file
 * benchmark_clean_boosters.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function benchmark_clean_boosters_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'boost flush pages'.
  $permissions['boost flush pages'] = array(
    'name' => 'boost flush pages',
    'roles' => array(
      'Editor' => 'Editor',
      'administrator' => 'administrator',
    ),
    'module' => 'boost',
  );

  return $permissions;
}
