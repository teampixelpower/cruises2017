<?php
/**
 * @file
 * shipmates_quizzes.features.inc
 */

/**
 * Implements hook_views_api().
 */
function shipmates_quizzes_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
