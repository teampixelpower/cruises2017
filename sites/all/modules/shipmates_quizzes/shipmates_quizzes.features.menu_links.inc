<?php
/**
 * @file
 * shipmates_quizzes.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function shipmates_quizzes_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-shipmates-menu_my-quiz-results:shipmates/my-quiz-results
  $menu_links['menu-shipmates-menu_my-quiz-results:shipmates/my-quiz-results'] = array(
    'menu_name' => 'menu-shipmates-menu',
    'link_path' => 'shipmates/my-quiz-results',
    'router_path' => 'shipmates/my-quiz-results',
    'link_title' => 'My Quiz Results',
    'options' => array(
      'identifier' => 'menu-shipmates-menu_my-quiz-results:shipmates/my-quiz-results',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-shipmates-menu_weekly-wednesday-quiz:shipmates/weekly-wednesday-quiz
  $menu_links['menu-shipmates-menu_weekly-wednesday-quiz:shipmates/weekly-wednesday-quiz'] = array(
    'menu_name' => 'menu-shipmates-menu',
    'link_path' => 'shipmates/weekly-wednesday-quiz',
    'router_path' => 'shipmates/weekly-wednesday-quiz',
    'link_title' => 'Weekly Wednesday Quiz',
    'options' => array(
      'identifier' => 'menu-shipmates-menu_weekly-wednesday-quiz:shipmates/weekly-wednesday-quiz',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('My Quiz Results');
  t('Weekly Wednesday Quiz');

  return $menu_links;
}
