<?php
/**
 * @file
 * benchmark_clean_contrib_module_settings.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function benchmark_clean_contrib_module_settings_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-footer_copyright'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'footer_copyright',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'benchmark_clean' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'benchmark_clean',
        'weight' => -14,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['custom_search_blocks-1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 1,
    'module' => 'custom_search_blocks',
    'node_types' => array(),
    'pages' => 'search*',
    'roles' => array(),
    'themes' => array(
      'benchmark_clean' => array(
        'region' => 'sidebar',
        'status' => 1,
        'theme' => 'benchmark_clean',
        'weight' => -14,
      ),
    ),
    'title' => 'Search',
    'visibility' => 0,
  );

  $export['sharethis-sharethis_block'] = array(
    'cache' => 4,
    'custom' => 0,
    'delta' => 'sharethis_block',
    'module' => 'sharethis',
    'node_types' => array(),
    'pages' => 'search*
user*
admin*',
    'roles' => array(),
    'themes' => array(
      'benchmark_clean' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'benchmark_clean',
        'weight' => -12,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['system-help'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'help',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'benchmark_clean' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'benchmark_clean',
        'weight' => -14,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'benchmark_clean' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'benchmark_clean',
        'weight' => -13,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'benchmark_clean' => array(
        'region' => 'navigationzone',
        'status' => 1,
        'theme' => 'benchmark_clean',
        'weight' => -14,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['system-powered-by'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'powered-by',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'benchmark_clean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'benchmark_clean',
        'weight' => -1,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
