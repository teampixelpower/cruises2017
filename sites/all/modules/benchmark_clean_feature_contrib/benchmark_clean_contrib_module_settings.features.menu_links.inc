<?php
/**
 * @file
 * benchmark_clean_contrib_module_settings.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function benchmark_clean_contrib_module_settings_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_home:<front>
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_search:search/node
  $menu_links['main-menu_search:search/node'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'search/node',
    'router_path' => 'search/node',
    'link_title' => 'Search',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_search:search/node',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Home');
  t('Search');

  return $menu_links;
}
