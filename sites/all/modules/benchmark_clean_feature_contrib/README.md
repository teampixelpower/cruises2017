# Benchmark Clean Features: Contrib
## Settings & Preferences for Drupal Third-Party Contrib Modules

## Version 1x
This is the Benchmark Digital Features Modules used to setup a blank Drupal instance. Contrib Drupal Settings.

![Benchmark Logo](https://bytebucket.org/teampixelpower/benchmark_clean/raw/9b75eca946725261443dd28c61b71bcfaf098630/logo.png?token=8604e47c5f790059195b0e8c34cf42274c61173e)



### Changelog

#### October 2015
- 7.x-1.0b - created by Ash Glover.
