<?php
/**
 * @file
 * benchmark_clean_contrib_module_settings.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function benchmark_clean_contrib_module_settings_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Footer Copyright';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'footer_copyright';
  $fe_block_boxes->body = 'All content © <?php print date(\'Y\'); ?>. All Rights Reserved. <a href="/sitemap">Site Map.</a>';

  $export['footer_copyright'] = $fe_block_boxes;

  return $export;
}
