<?php

/**
 * @file
 * This module provides easy configurable search pages for all entities.
 *
 * @author Jakob Lau Nielsen <frakke>
 *
 * @todo
 * - Remember to force enslave queries!!! If crappy Drupal Core allows this! Why
 *   oh why can Drupal be so blind some times to the needs of grown up sites.
 */

/**
 * Implements hook_help().
 */
function entity_backend_search_help($path, $arg) {
  switch ($path) {
    case 'admin/config/search/entity_backend_search':
      return '<p>' . t('The Entity Backend Search module provides a general search page for each entity type defined on your site.
        It is an easy way to provide the editorial staff of you site to search for content through specific fields
        since the administrative pages for the Drupal core entities are rather limited.') . '</p>';
  }
}

/**
 * Implements hook_permission().
 */
function entity_backend_search_permission() {
  $permissions = array(
    'administer entity backend search' => array(
      'title' => t('Administer Entity Search'),
      'description' => t('Configure the Entity Search pages.'),
    ),
  );

  foreach (entity_backend_search_get_enabled_search_bundles() as $entity_type) {
    $permissions['access entity backend search ' . $entity_type] = array(
      'title' => t('Access Entity Search page for %entity_type', array('%entity_type' => $entity_type)),
      'description' => t('Display the search page for %entity_type', array('%entity_type' => $entity_type)),
    );

    $permissions['entity backend search autocomplete access ' . $entity_type] = array(
      'title' => t('Access Entity entity autocomplete %entity_type', array('%entity_type' => $entity_type)),
      'description' => t('Give access to a default autocomplete for %entity_type', array('%entity_type' => $entity_type)),
    );

  }

  return $permissions;
}

/**
 * Implements hook_theme().
 */
function entity_backend_search_theme() {
  return array(
    'entity_backend_search_settings_fieldset' => array(
      'render element' => 'form',
      'file' => 'entity_backend_search.admin.inc',
    ),
    'entity_backend_search_settings_table_formatter_fieldset' => array(
      'render element' => 'form',
      'file' => 'entity_backend_search.admin.inc',
    ),
  );
}

/**
 * Implements hook_menu().
 */
function entity_backend_search_menu() {
  $items = array();

  $items['admin/config/search/entity_backend_search'] = array(
    'title' => 'Entity Backend Search',
    'description' => 'Adjust entity backend search settings.',
    'page callback' => 'entity_backend_search_settings_overview',
    'access arguments' => array('administer entity backend search'),
    'file' => 'entity_backend_search.admin.inc',
  );

  $entities_info = entity_get_info();
  foreach (field_info_bundles() as $entity_type => $bundle_info) {
    $items['admin/config/search/entity_backend_search/' . $entity_type] = array(
      'title' => 'Entity Backend Search ' . $entities_info[$entity_type]['label'],
      'description' => 'Adjust entity backend search settings.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('entity_backend_search_settings_form', 4),
      'access arguments' => array('administer entity backend search'),
      'file' => 'entity_backend_search.admin.inc',
    );
  }

  $enabled_search_bundles = entity_backend_search_get_enabled_search_bundles();
  if (!empty($enabled_search_bundles)) {
    $default_entity_type = current($enabled_search_bundles);
    $items['admin/content/entity_backend_search'] = array(
      'title' => 'Entity Backend Search',
      'description' => 'Search through entities.',
      'page callback' => 'entity_backend_search_search_page_overview',
      'access callback' => 'entity_backend_search_overview_access',
      'file' => 'entity_backend_search.pages.inc',
    );

    foreach ($enabled_search_bundles as $entity_type) {
      $items['admin/content/entity_backend_search/' . $entity_type] = array(
        'title' => 'Search ' . $entities_info[$entity_type]['label'],
        'description' => 'Search through entities.',
        'page callback' => 'entity_backend_search_search_page',
        'page arguments' => array(3),
        'access arguments' => array('access entity backend search ' . $entity_type),
        'file' => 'entity_backend_search.pages.inc',
        'type' => MENU_LOCAL_TASK,
      );
    }
  }

  $items['entity_backend_search/autocomplete/%'] = array(
    'title' => 'Entity backend search autocomplete',
    'page callback' => 'entity_backend_search_autocomplete',
    'page arguments' => array(2, 3),
    'access callback' => 'entity_backend_search_autocomplete_access',
    'access arguments' => array(2),
    'type' => MENU_CALLBACK,
    'file' => 'entity_backend_search.pages.inc',
  );

  return $items;
}

/**
 * Autocomplete access callback per entity type.
 *
 * @param string $entity_type
 *   Name of entity type to check access for.
 *
 * @return bool
 *   TRUE|FALSE if user has access.
 */
function entity_backend_search_autocomplete_access($entity_type) {
  return user_access('entity backend search autocomplete access ' . $entity_type);
}

/**
 * Check if the user has search access to ANY of the enabled entities.
 *
 * @return bool
 *   TRUE is the user has access.
 */
function entity_backend_search_overview_access() {
  $enabled_search_bundles = entity_backend_search_get_enabled_search_bundles();
  foreach ($enabled_search_bundles as $entity_type) {
    if (user_access('access entity backend search ' . $entity_type)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Load include files.
 */
function entity_backend_search_include_module_files() {
  module_load_include('inc', 'entity_backend_search', 'modules/addressfield.entity_backend_search');
  module_load_include('inc', 'entity_backend_search', 'modules/comment.entity_backend_search');
  module_load_include('inc', 'entity_backend_search', 'modules/entity_backend_search');
  module_load_include('inc', 'entity_backend_search', 'modules/file.entity_backend_search');
  module_load_include('inc', 'entity_backend_search', 'modules/node.entity_backend_search');
  module_load_include('inc', 'entity_backend_search', 'modules/taxonomy_term.entity_backend_search');
  module_load_include('inc', 'entity_backend_search', 'modules/user.entity_backend_search');
}

/**
 * Implements hook_form_alter().
 */
function entity_backend_search_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id != 'entity_backend_search_filters_form') {
    return;
  }

  // Unset the bundles option. The Drupal core module seems not to be compatible
  // with the bundle functionality of EntityFieldQuery...
  if ($form['entity_type']['#value'] == 'comment') {
    unset($form['entity_backend_search_filters']['bundles']);
  }
}

/**
 * Get search settings, either for all entity types or for one given.
 *
 * @param string $entity_type
 *   Name of entity type to get settings for.
 * @param bool $reset
 *   Allow use of static variable or not.
 * @param array $default
 *   Default return if no settings is found.
 *
 * @return array
 *   Array of search settings.
 */
function entity_backend_search_get_search_settings($entity_type = NULL, $reset = FALSE, $default = array()) {
  static $search_settings;

  if (!isset($search_settings) || $reset) {
    $search_settings = array();

    foreach (field_info_bundles() as $name => $field_info_bundle) {
      if ($settings = variable_get('entity_backend_search_entities_settings_' . $name, FALSE)) {
        $search_settings[$name] = variable_get('entity_backend_search_entities_settings_' . $name, FALSE);
      }
    }
  }

  if ($entity_type) {
    return isset($search_settings[$entity_type]) ? $search_settings[$entity_type] : $default;
  }

  return $search_settings;
}

/**
 * Get enabled entity search pages.
 *
 * @param bool $reset
 *   Allow use of static variable or not.
 *
 * @return array
 *   Array of enabled entity search pages identified by their entity type name.
 */
function entity_backend_search_get_enabled_search_bundles($reset = FALSE) {
  static $enabled_search_pages;

  if (isset($enabled_search_pages) && !$reset) {
    return $enabled_search_pages;
  }

  $enabled_search_pages = array();

  foreach (entity_backend_search_get_search_settings(NULL, $reset) as $entity_type => $settings) {
    if (!empty($settings['status'])) {
      $enabled_search_pages[$entity_type] = $entity_type;
    }
  }

  return $enabled_search_pages;
}

/**
 * Get base table info for entity types or for given entity type.
 *
 * @param string $entity_type
 *   Name of entity type to get base table info for.
 * @param bool $reset
 *   Allow use of static variable or not.
 *
 * @return mixed
 *   Array containg base table info either for all entity types, given entity
 *   type or FALSE if none is found.
 */
function entity_backend_search_entity_base_table($entity_type = NULL, $reset = FALSE) {
  static $entities_base_table_schemas;

  if (isset($entities_base_table_schemas[$entity_type])) {
    return $entities_base_table_schemas[$entity_type];
  }
  elseif (!isset($entities_base_table_schemas)) {
    // We can't use the drupal cached versions of the schemas as it strips out
    // the descriptions, which are needed for the help texts. So the schemas are
    // invoked from hook_schema() and added, but only if the module registers
    // any entities.
    $entities_base_table_schemas = array();

    $schemas_raw = array();
    module_load_all_includes('install');

    foreach (module_implements('schema') as $module) {
      // Cast the result of hook_schema() to an array, as a NULL return value
      // would cause array_merge() to set the $schema variable to NULL as
      // well. That would break modules which use $schema further down the
      // line.
      if (function_exists($module . '_entity_info')) {
        $current = (array) module_invoke($module, 'schema');
        $schemas_raw = array_merge($schemas_raw, $current);
      }
    }

    foreach (entity_get_info() as $entity_type_name => $entity_info) {
      if (isset($entity_info['base table']) && isset($schemas_raw[$entity_info['base table']])) {
        $entities_base_table_schemas[$entity_type_name]['fields'] = $schemas_raw[$entity_info['base table']]['fields'];
      }
    }
  }

  if (is_null($entity_type)) {
    return $entities_base_table_schemas;
  }

  if (!isset($entities_base_table_schemas[$entity_type])) {
    $entities_base_table_schemas[$entity_type] = FALSE;
  }

  return $entities_base_table_schemas[$entity_type];
}
