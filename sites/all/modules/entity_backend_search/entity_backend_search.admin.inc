<?php

/**
 * @file
 * The configuration related callbacks and helper functions.
 */

/**
 * Callback lists entity editorial search configurations.
 *
 * @return array
 *   A Drupal renderable form array.
 */
function entity_backend_search_settings_overview() {
  $build = array();

  $table_header = array(
    'label' => t('Name'),
    'operations' => t('Operations'),
    'status' => t('Status'),
    'table_formatter' => t('Search result table format'),
    'count' => t('Result count per page'),
    'pager' => t('Result pager type'),
  );

  $entities_info = entity_get_info();

  // Collect info about entity types and list info about each in a table row.
  $table_rows = array();
  $search_settings = entity_backend_search_get_search_settings();
  foreach (field_info_bundles() as $entity_type => $bundle_info) {
    $row = array();

    $settings = isset($search_settings[$entity_type]) ? $search_settings[$entity_type] : array();

    $row['label'] = array(
      'data' => check_plain($entities_info[$entity_type]['label']),
      'class' => array('label'),
    );

    $row['operations'] = array(
      'data' => l(t('Edit'), 'admin/config/search/entity_backend_search/' . $entity_type)
      . ' - ' . l(t('View'), 'admin/content/entity_backend_search/' . $entity_type),
    );

    $row['status'] = array(
      'data' => empty($settings['status']) ? t('Disabled') : t('Enabled'),
    );

    $table_formatter_status = t('Unconfigured');
    if (isset($settings['table_formatter']) && empty($settings['table_formatter'])) {
      $table_formatter_status = t('In code');
    }
    elseif (isset($settings['table_formatter']) && !empty($settings['table_formatter'])) {
      $table_formatter_status = t('Configured');
    }
    $row['table_formatter'] = array(
      'data' => $table_formatter_status,
    );

    $row['count'] = array(
      'data' => empty($settings['search_result_count']) ? t('Default') : $settings['search_result_count'],
    );

    $row['pager'] = array(
      'data' => empty($settings['search_result_pager']) ? t('Full') : ucfirst($settings['search_result_pager']),
    );

    $table_rows[] = array(
      'data' => $row,
      'class' => empty($settings['status']) ? array('disabled') : array('enabled'),
    );

  }

  asort($table_rows);

  $build[]['#markup'] = theme(
    'table',
    array(
      'attributes' => array(
        'id' => 'entity-backend-search-settings-overview',
      ),
      'header' => $table_header,
      'rows' => $table_rows,
      'empty' => t('No entites'),
    )
  );

  $build['#attached']['css'] = array(drupal_get_path('module', 'entity_backend_search') . '/styles/css/entity_backend_search.css');

  return $build;
}

/**
 * The settings form for configuring the entity backend search pages.
 *
 * We set up:
 * - A few general settings for the search page for the given entity type.
 * - Walks through the fields available on all bundles for the entity type.
 * - Walks through the properties (columns set on the base table).
 * - Some basic search result table setup. If a code implementation of the
 *   table is present, only a few settings is available.
 *   If no code implementation is found, you can configure the search result
 *   table from the properties available on the base table.
 *
 * @param array $form
 *   Drupal standard in form callbacks.
 * @param array &$form_state
 *   Drupal standard in form callbacks.
 * @param string $entity_type
 *   The entity type to configure entity backend search for.
 *
 * @return array
 *   A Drupal renderable form array.
 */
function entity_backend_search_settings_form($form, &$form_state, $entity_type) {
  entity_backend_search_include_module_files();

  // Reset the hook cache. Most of the time it is not needed, but when
  // developing, it is nice to have.
  module_implements(NULL, FALSE, TRUE);

  $form = array();

  $form['#attached']['css'] = array(drupal_get_path('module', 'entity_backend_search') . '/styles/css/entity_backend_search.css');

  // Get info used to generate the settings form for the given entity type.
  $instances = field_info_instances();
  $entity_type_bundles = field_info_bundles($entity_type);
  $entity_info = entity_get_info($entity_type);

  // Get the base table. We use this to process the properties of an entity
  // type.
  $entity_base_table = entity_backend_search_entity_base_table($entity_type);
  // Get the existing entity backend search settings for this entity type.
  $settings = entity_backend_search_get_search_settings($entity_type, TRUE);

  $enabled_search_entities = entity_backend_search_get_enabled_search_bundles(TRUE);

  $entity_type_label = (in_array($entity_type, $enabled_search_entities) ? '' : '*' . t('Disabled') . ' - ') . $entity_info['label'];
  $form['entity_type_' . $entity_type] = array(
    '#type' => 'fieldset',
    '#title' => t('@entity_type_label: Configure entity backend search page', array('@entity_type_label' => $entity_type_label)),
    '#collapsible' => FALSE,
    '#collapsed' => !in_array($entity_type, $enabled_search_entities),
    '#tree' => TRUE,
  );

  $form['entity_type_' . $entity_type]['permissions'] = array(
    '#markup' => l(t('Setup permissions'), 'admin/people/permissions', array('fragment' => 'module-entity_backend_search', 'attributes' => array('target' => '_blank'))),
  );

  if (empty($settings['status'])) {
    $search_page_link = t('Search page');
  }
  else {
    $search_page_link = l(
      t('search page'),
      'admin/content/entity_backend_search/' . $entity_type,
      array(
        'attributes' => array(
          'target' => '_blank',
        ),
      )
    );
  }
  $form['entity_type_' . $entity_type]['status'] = array(
    '#type' => 'checkbox',
    '#title' => t(
      'Enable !search_page_link for %entity_type',
      array(
        '%entity_type' => $entity_type,
        '!search_page_link' => $search_page_link,
      )
    ),
    '#default_value' => empty($settings['status']) ? array() : $settings['status'],
  );

  $options_bundles = array();
  foreach ($entity_type_bundles as $bundle_name => $bundle_info) {
    $bundle_label = empty($bundle_info['label']) ? $entity_type_name : $bundle_info['label'];
    $options_bundles[$bundle_name] = $bundle_label;
  }

  $form['entity_type_' . $entity_type]['entity_type'] = array(
    '#type' => 'hidden',
    '#value' => $entity_type,
  );

  $form['entity_type_' . $entity_type]['bundles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select which entities are available for search'),
    '#options' => $options_bundles,
    '#default_value' => empty($settings['bundles']) ? array() : $settings['bundles'],
    '#description' => t('If no types is checked, all will be available in filter'),
  );

  // -------------------------------------------------------------------------
  // Build filters.
  // Collect all fields for current bundle.
  $fields_on_bundle = array();
  if (!empty($instances[$entity_type])) {
    foreach ($instances[$entity_type] as $field_info_instances_bundle_name => $field_info_instances_bundle_info) {
      foreach ($field_info_instances_bundle_info as $field_name => $field_info) {
        // Allow for multiple instance overrides/variations.
        $fields_on_bundle[$field_info['field_name']]['label'][$entity_type] = $field_info['label'];

        $fields_on_bundle[$field_info['field_name']]['instances'][$field_info_instances_bundle_name] = $field_info_instances_bundle_name;
        $fields_on_bundle[$field_info['field_name']]['widgets'][] = $field_info['widget'];

        // Get some helt text for each field, either from the user inputted
        // description or just the name of the module which owns the field.
        if (!isset($fields_on_bundle[$field_info['field_name']]['description'])) {
          $fields_on_bundle[$field_info['field_name']]['description'] = array();
        }
        if (!empty($field_info['description'])) {
          $fields_on_bundle[$field_info['field_name']]['description'][$field_info['description']] = check_plain($field_info['description']);
        }
        elseif (empty($fields_on_bundle[$field_info['field_name']]['description']) && ($field_info_field = field_info_field($field_name)) && isset($field_info_field['module'])) {
          $fields_on_bundle[$field_info['field_name']]['description'][$field_info_field['module']] = ucfirst($field_info_field['module']);
        }
      }
    }
  }

  $form['entity_type_' . $entity_type]['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Choose fields to filter on'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#theme' => 'entity_backend_search_settings_fieldset',
    '#entity_type' => $entity_type,
    '#description' => t('Select the filter widget type to enable filters.
      <br />Drag fields to control the order in which they are displayed on the search pages.
      <br />Group names are used to group the filters in fieldsets to help keeping a more simple filter set for the end user.'),
  );

  // Ready elements and construct a table with:
  // field label(s), bundle_instance(s), filter widgets.
  foreach ($fields_on_bundle as $field_name => $field_info) {
    $form['entity_type_' . $entity_type]['fields'][$field_name] = array(
      '#type' => 'fieldset',
      '#field_name' => $field_name,
      '#field_info' => $field_info,
    );

    $form['entity_type_' . $entity_type]['fields'][$field_name]['label'] = array(
      '#markup' => implode(', ', $fields_on_bundle[$field_name]['label']),
      '#prefix' => isset($field_info['description']) ? '<span class="entity-backend-search-info" title="' . implode("\n", $field_info['description']) . '"><span>' : '',
    );

    $form['entity_type_' . $entity_type]['fields'][$field_name]['instances'] = array(
      '#markup' => implode(', ', $fields_on_bundle[$field_name]['instances']),
    );
  }

  if ($entity_base_table && isset($entity_base_table['fields'])) {
    foreach ($entity_base_table['fields'] as $field_name => $property_info) {
      $form['entity_type_' . $entity_type]['fields'][$field_name] = array(
        '#type' => 'fieldset',
        '#field_name' => $field_name,
        '#field_info' => $property_info,
      );

      $form['entity_type_' . $entity_type]['fields'][$field_name]['label'] = array(
        '#markup' => ucfirst(check_plain($field_name)),
        '#prefix' => isset($property_info['description']) ? '<span class="entity-backend-search-info" title="' . check_plain($property_info['description']) . '"><span>' : '',
      );

      $form['entity_type_' . $entity_type]['fields'][$field_name]['instances'] = array(
        '#markup' => t('All'),
      );
    }
  }

  $defaults = NULL;

  drupal_alter('entity_backend_search_filter_fields', $form['entity_type_' . $entity_type]['fields'], $entity_type);

  // Generate widget options after custom fields has been added.
  foreach (element_children($form['entity_type_' . $entity_type]['fields']) as $field_name) {
    $default_field_settings = isset($settings['fields'][$field_name]) ? $settings['fields'][$field_name] : array();

    $form['entity_type_' . $entity_type]['fields'][$field_name]['#field_name'] = $field_name;

    // Hrmmm, this might not belong here?
    $filter_widget_types_options = array(
      0 => t('Disabled'),
    );

    // Let other modules add their filter types.
    $hook = 'entity_backend_search_filter_types';
    foreach (module_implements($hook) as $module) {
      $function = $module . '_' . $hook;
      $additional_widgets = $function($entity_type, $field_name);
      if (!empty($additional_widgets)) {
        $filter_widget_types_options += $additional_widgets;
      }
    }

    $form['entity_type_' . $entity_type]['fields'][$field_name]['filter_widget_types'] = array(
      '#type' => 'radios',
      '#options' => $filter_widget_types_options,
      '#default_value' => isset($default_field_settings['filter_widget_types']) ? $default_field_settings['filter_widget_types'] : '0',
    );

    $form['entity_type_' . $entity_type]['fields'][$field_name]['#weight'] = isset($default_field_settings['weight']) ? $default_field_settings['weight'] : NULL;
    $form['entity_type_' . $entity_type]['fields'][$field_name]['weight'] = array(
      '#type' => 'select',
      '#default_value' => isset($default_field_settings['weight']) ? $default_field_settings['weight'] : NULL,
      '#options' => drupal_map_assoc(range(0, 200, 1)),
      '#title_display' => 'invisible',
      '#attributes' => array(
        'class' => array(
          'weight-' . str_replace(array(' ', '_'), '-', $entity_type),
        ),
      ),
    );

    $form['entity_type_' . $entity_type]['fields'][$field_name]['group'] = array(
      '#type' => 'textfield',
      '#default_value' => isset($default_field_settings['group']) ? $default_field_settings['group'] : NULL,
      '#title_display' => 'invisible',
    );
  }

  // ---------------------------------------------------------------------------
  // Configure search result options.
  $form['entity_type_' . $entity_type]['search_result'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search result options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['entity_type_' . $entity_type]['search_result']['search_result_count'] = array(
    '#type' => 'select',
    '#title' => t('Number of results to display on each page'),
    '#options' => drupal_map_assoc(range(10, 100, 5)),
    '#default_value' => empty($settings['search_result_count']) ? 20 : $settings['search_result_count'],
  );

  $form['entity_type_' . $entity_type]['search_result']['search_result_pager'] = array(
    '#type' => 'select',
    '#title' => t('Select pager type to use with search results'),
    '#options' => array(
      'full' => t('Full pager'),
      'simple' => t('Simple pager (Much better database performance)'),
      0 => t('Off'),
    ),
    '#default_value' => isset($settings['search_result_pager']) ? $settings['search_result_pager'] : 'full',
    '#description' => t('
      <ul>
        <li>Full pager: Normally the Full pager will be what you want. If there are a huge number of entities, though, the Full pager can be very expensive to generate.</li>
        <li>Simple pager: This pager will only know if there is one more search result than on the current page, but it will not do the expensive count query, thus being much faster then the Full pager. This pager is recommended for entity types with a very high count.</li>
        <li>Off: No pager will be displayed.</li>
      </ul>'),
  );

  // Build table formatter.
  $table_formatter_function = FALSE;
  $hook = 'entity_backend_search_format_search_result_table';
  $table_dummy_header = $table_dummy_rows = array();
  foreach (module_implements($hook) as $module) {
    $function = $module . '_' . $hook;
    // Returns true if processed, so we don't need to ask any further modules.
    if ($function($entity_type, array(), $table_dummy_header, $table_dummy_rows)) {
      $table_formatter_function = $function;
      break;
    }
  }

  // If a table formatter was found in code, don't display the table formatter
  // settings.
  // Else let the user configure the search result table as needed, based on the
  // properties for the current entity type from the base table.
  if ($table_formatter_function) {
    $form['entity_type_' . $entity_type]['search_result']['table_formatter'] = array(
      '#markup' => t('Search result table is formatted in code through %table_formatter_function', array('%table_formatter_function' => $table_formatter_function)),
    );
  }
  else {
    $table_formatter_defaults = !empty($settings['table_formatter']) ? $settings['table_formatter'] : array();

    $properties = array();

    if ($entity_base_table && isset($entity_base_table['fields'])) {
      $properties = $entity_base_table['fields'];
    }

    $form['entity_type_' . $entity_type]['search_result']['table_formatter'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configure search result table'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#theme' => 'entity_backend_search_settings_table_formatter_fieldset',
      '#entity_type' => $entity_type,
      '#description' => t('Since hook_entity_backend_search_format_search_result_table() is not implemented for this entity type, the search results table can be configured here.<br />
        In the value formatter input field, you can format the output by using the keywords listed below. If nothing is inputted, the raw field value will be displayed<br />
        Examples:<br />
        <code>&lt;a href="entity/@id/edit?@destination"&gt;Edit&lt;/a&gt;<br />
        &lt;a href="entity/@id"&gt;@subject&lt;/a&gt;</code><br />
        You can use @destination for returning users to the search page when submitting forms as shown in above in the edit link pattern.<br />
        %keywords', array('%keywords' => '@' . implode(', @', array_keys($properties)))),
    );

    $formatters_options_default = array(
      'text' => t('Text (optional value pattern)'),
    );
    foreach ($properties as $field_name => $property_info) {
      $column_input = array();

      $column_input['#weight'] = isset($table_formatter_defaults[$field_name]['weight']) ? $table_formatter_defaults[$field_name]['weight'] : NULL;

      $column_input['name'] = array(
        '#markup' => check_plain($field_name),
        '#title_display' => 'invisible',
        '#prefix' => isset($property_info['description']) ? '<span class="entity-search-info" title="' . check_plain($property_info['description']) . '"><span>' : '',
      );

      $column_input['thead'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($table_formatter_defaults[$field_name]['thead']) ? $table_formatter_defaults[$field_name]['thead'] : check_plain($field_name),
        '#title_display' => 'invisible',
      );

      $column_input['pattern'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($table_formatter_defaults[$field_name]['pattern']) ? $table_formatter_defaults[$field_name]['pattern'] : NULL,
        '#title_display' => 'invisible',
        '#maxlength' => 255,
      );

      // Get a few formatters for the table. It helps translate dates, uid etc.
      // into more readable elements in the table.
      $context = array(
        'entity_type' => $entity_type,
        'field_name' => $field_name,
        'type' => $property_info['type'],
      );
      $formatters_options = array();
      if ($property_info['type'] == 'int') {
        $formatters_options['date_format_short'] = t('Date (@format)', array('@format' => variable_get('date_format_short', 'm/d/Y - H:i')));
        $formatters_options['date_format_medium'] = t('Date (@format)', array('@format' => variable_get('date_format_medium', 'D, m/d/Y - H:i')));
        $formatters_options['date_format_long'] = t('Date (@format)', array('@format' => variable_get('date_format_long', 'l, F j, Y - H:i')));
        $formatters_options['boolean'] = t('Boolean @yes/@no', array('@yes' => t('Yes'), '@no' => t('No')));
      }
      drupal_alter('entity_backend_search_table_field_formaters_list', $formatters_options, $context, $op);

      $column_input['formatter'] = array(
        '#type' => 'select',
        '#options' => $formatters_options_default + $formatters_options,
        '#default_value' => isset($table_formatter_defaults[$field_name]['formatter']) ? $table_formatter_defaults[$field_name]['formatter'] : NULL,
        '#title_display' => 'invisible',
      );

      $column_input['weight'] = array(
        '#type' => 'select',
        '#default_value' => isset($table_formatter_defaults[$field_name]['weight']) ? $table_formatter_defaults[$field_name]['weight'] : NULL,
        '#options' => drupal_map_assoc(range(0, 200, 1)),
        '#title_display' => 'invisible',
        '#attributes' => array(
          'class' => array(
            'weight-' . str_replace(array(' ', '_'), '-', $entity_type),
          ),
        ),
      );

      $column_input['status'] = array(
        '#type' => 'checkbox',
        '#default_value' => isset($table_formatter_defaults[$field_name]['status']) ? $table_formatter_defaults[$field_name]['status'] : NULL,
        '#title_display' => 'invisible',
      );

      $column_input['sortable'] = array(
        '#type' => 'checkbox',
        '#default_value' => isset($table_formatter_defaults[$field_name]['sortable']) ? $table_formatter_defaults[$field_name]['sortable'] : NULL,
        '#title_display' => 'invisible',
      );

      $form['entity_type_' . $entity_type]['search_result']['table_formatter'][$field_name] = $column_input;
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#submit' => array(
      'entity_backend_search_settings_form_submit',
    ),
  );

  return $form;
}

/**
 * Submit handler for the entity backend search settings.
 */
function entity_backend_search_settings_form_submit($form, &$form_state) {
  // Check if any search entity types has been disabled or enabled and if so,
  // do a menu_rebuild and do a drupal_set_message on the permissions issue!
  $old_enabled_entity_types = entity_backend_search_get_enabled_search_bundles(TRUE);

  form_state_values_clean($form_state);

  // Save settings for each entity type. (Supports multiple entity type
  // configurations).
  foreach ($form_state['values'] as $entity_backend_search_settings) {
    $settings = array();
    $entity_type = $entity_backend_search_settings['entity_type'];

    $settings['bundles'] = array_filter($entity_backend_search_settings['bundles']);

    $settings['status'] = $entity_backend_search_settings['status'];
    $settings['search_result_count'] = $entity_backend_search_settings['search_result']['search_result_count'];
    $settings['search_result_pager'] = $entity_backend_search_settings['search_result']['search_result_pager'];

    $settings['fields'] = empty($entity_backend_search_settings['fields']) ? array() : $entity_backend_search_settings['fields'];
    uasort($settings['fields'], 'drupal_sort_weight');

    $settings['table_formatter'] = empty($entity_backend_search_settings['search_result']['table_formatter']) ? array() : $entity_backend_search_settings['search_result']['table_formatter'];
    uasort($settings['table_formatter'], 'drupal_sort_weight');

    variable_set('entity_backend_search_entities_settings_' . $entity_type, $settings);
  }

  // If any entity types has been enabled or disabled, we need to rebuild the
  // menu.
  $new_enabled_entity_types = entity_backend_search_get_enabled_search_bundles(TRUE);
  if (serialize($old_enabled_entity_types) != serialize($new_enabled_entity_types)) {
    menu_rebuild();
  }
}

/**
 * Render the draggable configuration table form for filters.
 *
 * @param array $variables
 *   The form values.
 *
 * @return string
 *   The rendered fieldset, containing the filters settings.
 */
function theme_entity_backend_search_settings_fieldset($variables) {
  $form = $variables['form'];

  $header = array(
    t('Field label'),
    t('Weight'),
    t('Used in'),
    t('Group name'),
    t('Filter widget type'),
  );

  $rows = array();
  foreach (element_children($form) as $key) {
    $row = array();

    $row['label']['data'] = drupal_render($form[$key]['label']) . drupal_render($form[$key]['bundle_name']);
    $row['weight']['data'] = drupal_render($form[$key]['weight']);
    $row['instances']['data'] = drupal_render($form[$key]['instances']);
    $row['group']['data'] = drupal_render($form[$key]['group']);
    $row['filter_widget_type']['data'] = drupal_render($form[$key]['filter_widget_types']);

    $rows[] = array(
      'data' => $row,
      'class' => array(
        'draggable',
        empty($form[$key]['filter_widget_types']['#default_value']) ? 'disabled' : 'enabled',
      ),
    );
  }

  $table_id = 'entity-backend-search-settings-' . $form['#entity_type'];

  drupal_add_tabledrag(
    $table_id,
    'order',
    'sibling',
    'weight-' . str_replace(array(' ', '_'), '-', $form['#entity_type'])
  );
  return theme(
    'table',
    array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(
        'id' => $table_id,
      ),
    )
  );
}

/**
 * Render the draggable configuration table form for the search result table.
 *
 * @param array $variables
 *   The form values.
 *
 * @return string
 *   The rendered fieldset, containing the table formatter settings.
 */
function theme_entity_backend_search_settings_table_formatter_fieldset($variables) {
  $form = $variables['form'];

  $header = array(
    t('Visible'),
    t('Name'),
    t('Column label'),
    t('Value formatter'),
    t('Value pattern'),
    t('Weight'),
    t('Sortable'),
  );

  $rows = array();
  foreach (element_children($form) as $key) {
    $row = array();

    $row['status']['data'] = drupal_render($form[$key]['status']);
    $row['name']['data'] = drupal_render($form[$key]['name']);
    $row['label']['data'] = drupal_render($form[$key]['thead']);
    $row['formatter']['data'] = drupal_render($form[$key]['formatter']);
    $row['pattern']['data'] = drupal_render($form[$key]['pattern']);
    $row['weight']['data'] = drupal_render($form[$key]['weight']);
    $row['sortable']['data'] = drupal_render($form[$key]['sortable']);

    $rows[] = array(
      'data' => $row,
      'class' => array(
        'draggable',
        empty($form[$key]['status']['#default_value']) ? 'disabled' : 'enabled',
      ),
    );
  }

  $table_id = 'entity-backend-search-settings-table-formatter-' . $form['#entity_type'];

  drupal_add_tabledrag(
    $table_id,
    'order',
    'sibling',
    'weight-' . str_replace(array(' ', '_'), '-', $form['#entity_type'])
  );
  return theme(
    'table',
    array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(
        'id' => $table_id,
      ),
    )
  );
}
