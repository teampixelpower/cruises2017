<?php
/**
 * @file
 * Entity Search integration for the addressfield module.
 */

/**
 * Implements hook_entity_backend_search_filter_types().
 */
function addressfield_entity_backend_search_filter_types($entity_type, $field_name) {

  // Add addressfield widget to available search filters.
  if (($field_info_field = field_info_field($field_name)) && $field_info_field['module'] == 'addressfield' && $field_info_field['type'] == 'addressfield') {
    $filters = array();
    $filters['addressfield'] = t('Address Field');
    return $filters;
  }
}

/**
 * Implements hook_entity_backend_search_field_widget().
 */
function addressfield_entity_backend_search_field_widget($entity_type, $field_name, $field_settings, $default_values) {

  // Generate the addressfield form and clean out additional processing not
  // fitting with the search functionality.
  if ($field_settings['filter_widget_types'] == 'addressfield' && ($field_info_field = field_info_field($field_name))) {
    // Put the address field in a fieldset.
    $label = ucfirst(str_replace('_', ' ', $field_info_field['field_name']));
    $field_widget = array(
      '#type' => 'fieldset',
      '#title' => check_plain($label),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      // For now, we don't support multiple address fields in same search form.
      '#tree' => FALSE,
    );

    $context = array(
      'mode' => 'form',
    );

    $address = $default_values + addressfield_default_values();
    $address['country'] = !empty($default_values['country']) ? $default_values['country'] : NULL;

    // Grap the addressfield widget settings from the first instance available.
    // The handlers control what is available in the address field form.
    $bundle = current($field_info_field['bundles']);
    $field_instance = field_info_instance($entity_type, $field_name, $bundle[0]);
    $handlers = $field_instance['widget']['settings']['format_handlers'];

    // Generate address field and modify it to make it search-friendly.
    $addressfield = addressfield_generate($address, $handlers, $context);
    unset($addressfield['country']['#ajax']);
    unset($addressfield['country']['#element_validate']);
    $addressfield['country']['#multiple'] = TRUE;

    $field_widget[$field_info_field['field_name']] = $addressfield;

    return $field_widget;
  }
}

/**
 * Implements hook_entity_backend_search_query_alter().
 */
function addressfield_entity_backend_search_query_alter(&$query, $entity_type, $search_filters) {
  $settings = entity_backend_search_get_search_settings($entity_type);

  foreach ($settings['fields'] as $field_name => $search_field) {
    if ($search_field['filter_widget_types'] != 'addressfield') {
      continue;
    }

    if (($field_info_field = field_info_field($field_name)) && $field_info_field['module'] == 'addressfield' && $field_info_field['type'] == 'addressfield') {
      foreach ($field_info_field['columns'] as $column_name => $column_info) {
        if (empty($search_filters[$column_name])) {
          continue;
        }

        if ($search_filters[$column_name] != '' && is_string($search_filters[$column_name]) || is_numeric($search_filters[$column_name])) {
          $query->fieldCondition($field_name, $column_name, $search_filters[$column_name], 'CONTAINS');
        }
        elseif (is_array($search_filters[$column_name])) {
          $query->fieldCondition($field_name, $column_name, $search_filters[$column_name], 'IN');
        }
      }
    }
  }
}
