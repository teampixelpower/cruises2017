<?php

/**
 * @file
 * File contains page related callbacks and helper functions.
 */

/**
 * List links to search pages which the user has access to.
 *
 * @return string
 *   A rendered table.
 */
function entity_backend_search_search_page_overview() {

  $enabled_search_bundles = entity_backend_search_get_enabled_search_bundles();

  $table_header = array(
    t('Your search pages'),
  );

  $entities_info = entity_get_info();

  $table_rows = array();
  foreach ($enabled_search_bundles as $entity_type) {
    if (user_access('access entity backend search ' . $entity_type)) {
      $table_rows[][] = array(
        'data' => l(
          t('Search for @entity_types', array('@entity_type' => $entities_info[$entity_type]['label'])),
          'admin/content/entity_backend_search/' . $entity_type
        ),
      );
    }
  }

  return theme('table', array(
      'header' => $table_header,
      'rows' => $table_rows,
      'empty' => t("You don't have search access to any entity types."),
    )
  );
}

/**
 * Callback for the search page.
 *
 * @param string $entity_type
 *   The name of the entity type beeing worked on.
 *
 * @return array
 *   An array of page elements to be rendered.
 */
function entity_backend_search_search_page($entity_type) {
  entity_backend_search_include_module_files();

  $enabled_search_bundles = entity_backend_search_get_enabled_search_bundles();
  if (!isset($enabled_search_bundles[$entity_type])) {
    return t('Not found');
  }

  $build = array();

  $build['entity_filter_form'] = drupal_get_form('entity_backend_search_filters_form', $entity_type);
  $build['entity_backend_search_result'] = drupal_get_form('entity_backend_search_search_result_table', $entity_type);

  return $build;
}

/**
 * The filter form on the search page.
 *
 * @param array $form
 *   Drupal standard in form callbacks.
 * @param array &$form_state
 *   Drupal standard in form callbacks.
 * @param string $entity_type
 *   The entity type to generate filter form for.
 *
 * @return array
 *   A Drupal renderable form array.
 */
function entity_backend_search_filters_form($form, &$form_state, $entity_type) {
  $form = array();

  $form['#attached']['css'] = array(drupal_get_path('module', 'entity_backend_search') . '/styles/css/entity_backend_search.css');

  $form['entity_type'] = array(
    '#type' => 'hidden',
    '#value' => $entity_type,
  );

  // The last enterend filter values.
  $default_values = _entity_backend_search_get_entity_search_filters($entity_type);

  // Remember and set paging and table sorts from previous search. Good to have
  // for editorial users.
  if (!empty($default_values['_GET'])) {
    foreach ($default_values['_GET'] as $name => $value) {
      // Don't override $_GET if it is already set.
      if (isset($_GET[$name])) {
        continue;
      }
      switch ($name) {
        case 'page':
          // Always let the pager be overridden by clicks on the same page.
          // Page 1 in the pager does not have any $_GET['page'] set, so we
          // have to assume, that unless a link contained the pager info, we
          // need to reset the pager.
          // This is only an issue, if the current entity search tab is clicked
          // again thus being the referer, but this should rarely happen...
          if (!empty($_SERVER['HTTP_REFERER']) && !empty($_SERVER['SCRIPT_URI']) && strpos($_SERVER['HTTP_REFERER'], $_SERVER['SCRIPT_URI']) === 0) {
            break;
          }

          if (is_numeric($value)) {
            $_GET['page'] = intval($value);
          }
          break;

        case 'sort':
          $_GET['sort'] = $value == 'desc' ? 'desc' : 'asc';
          break;

        case 'order':
          if (is_string($value) || is_numeric($value)) {
            $_GET['order'] = check_plain($value);
          }
          break;
      }
    }
  }
  _entity_backend_search_set_entity_search_filters($entity_type, $default_values);

  $settings = entity_backend_search_get_search_settings($entity_type);

  $form['entity_backend_search_filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filters'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Get available bundles to filter on, either only the selected at the
  // settings page or all, of none has been configured.
  $options_bundles = $settings['bundles'];
  if (empty($options_bundles)) {
    $options_bundles = array();
    foreach (field_info_bundles($entity_type) as $bundle_name => $bundle_info) {
      $bundle_label = empty($bundle_info['label']) ? $bundle_name : $bundle_info['label'];
      $options_bundles[$bundle_name] = $bundle_label;
    }
  }
  asort($options_bundles);
  if (count($options_bundles) > 1) {
    $form['entity_backend_search_filters']['bundles'] = array(
      '#type' => 'select',
      '#title' => t('@entity_type type', array('@entity_type' => ucfirst($entity_type))),
      '#multiple' => TRUE,
      '#options' => $options_bundles,
      '#default_value' => isset($default_values['bundles']) ? $default_values['bundles'] : array(),
      '#size' => count($options_bundles) < 4 ? count($options_bundles) : 4,
    );
  }

  // Generate the fields and group fieldsets.
  foreach ($settings['fields'] as $field_name => $field_settings) {
    if (empty($field_settings['filter_widget_types'])) {
      continue;
    }

    $fieldset = !empty($field_settings['group']);
    if ($fieldset && !isset($form['entity_backend_search_filters']['fieldset_' . $field_settings['group']])) {
      $fieldset = TRUE;
      $form['entity_backend_search_filters']['fieldset_' . $field_settings['group']] = array(
        '#type' => 'fieldset',
        '#title' => check_plain($field_settings['group']),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
    }

    $field_widget = FALSE;
    $hook = 'entity_backend_search_field_widget';
    foreach (module_implements($hook) as $module) {
      $function = $module . '_' . $hook;
      if ($field_widget = $function($entity_type, $field_name, $field_settings, $default_values)) {
        break 1;
      }
    }

    // Invoke hook_entity_backend_search_field_widget_alter() to let other
    // modules fit search widgets.
    $context = array(
      'entity_type' => $entity_type,
      'field_name' => $field_name,
      'field_settings' => $field_settings,
    );
    drupal_alter('entity_backend_search_field_widget', $field_widget, $context);

    if ($field_widget && !empty($field_widget['#type']) && $field_widget['#type'] == 'select' && !empty($field_widget['#multiple']) && !isset($field_widget['#size'])) {
      $field_widget['#size'] = count($field_widget['#options']) < 6 ? count($field_widget['#options']) : 6;
    }

    if ($field_widget && $fieldset) {
      $form['entity_backend_search_filters']['fieldset_' . $field_settings['group']][$field_name] = $field_widget;
    }
    elseif ($field_widget) {
      $form['entity_backend_search_filters'][$field_name] = $field_widget;
    }
  }

  $form['entity_backend_search_filters']['submit_search'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#name' => 'submit_search',
  );

  if (!empty($default_values)) {
    $form['submit_reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset search parameters'),
      '#name' => 'submit_reset',
    );
  }

  return $form;
}

/**
 * Submit handler for search form.
 *
 * The chosen filters are stored in the session variable. This can be rather
 * helpful when navigating from and to the search page.
 * The search filters are also reset here.
 */
function entity_backend_search_filters_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#name'] == 'submit_reset') {
    _entity_backend_search_set_entity_search_filters($form_state['values']['entity_type']);
    return;
  }

  form_state_values_clean($form_state);
  _entity_backend_search_set_entity_search_filters($form_state['values']['entity_type'], $form_state['values']);
}

/**
 * Get previous stored search parameters from session variable.
 *
 * @param string $entity_type
 *   The name of the entity type beeing worked on.
 * @param array $default
 *   The default return.
 *
 * @return mixed
 *   Either the previous search parameters or the $default return.
 */
function _entity_backend_search_get_entity_search_filters($entity_type = NULL, $default = array()) {
  if (!isset($_SESSION['entity_backend_search_filters'])) {
    return $default;
  }

  // Return all filter params, if no entity type was given.
  if (is_null($entity_type)) {
    return isset($_SESSION['entity_backend_search_filters']) ? $_SESSION['entity_backend_search_filters'] : $default;
  }

  // Return specific filters params, if entity type was given.
  return isset($_SESSION['entity_backend_search_filters'][$entity_type]) ? $_SESSION['entity_backend_search_filters'][$entity_type] : $default;
}

/**
 * Register chosen search filter params in session variable.
 *
 * @param string $entity_type
 *   The name of the entity type beeing worked on.
 * @param array $filters
 *   The filter params to store in session variable.
 */
function _entity_backend_search_set_entity_search_filters($entity_type = NULL, &$filters = NULL) {
  // Reset all filters.
  if (is_null($entity_type) && is_null($filters)) {
    if (isset($_SESSION['entity_backend_search_filters'])) {
      unset($_SESSION['entity_backend_search_filters']);
    }
    $filters = array();
    if (isset($_GET['page'])) {
      unset($_GET['page']);
    }
    if (isset($_GET['sort'])) {
      unset($_GET['sort']);
    }
    if (isset($_GET['order'])) {
      unset($_GET['order']);
    }
    return;
  }

  // Reset filters for specific entity type.
  if (is_null($filters)) {
    if (isset($_SESSION['entity_backend_search_filters']) && isset($_SESSION['entity_backend_search_filters'][$entity_type])) {
      unset($_SESSION['entity_backend_search_filters'][$entity_type]);
    }

    // Clean up, if no filters are stored anymore.
    if (empty($_SESSION['entity_backend_search_filters'])) {
      unset($_SESSION['entity_backend_search_filters']);
    }
    $filters = array();
    if (isset($_GET['page'])) {
      unset($_GET['page']);
    }
    if (isset($_GET['sort'])) {
      unset($_GET['sort']);
    }
    if (isset($_GET['order'])) {
      unset($_GET['order']);
    }
    return;
  }

  $filters['_GET'] = array();
  if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $filters['_GET']['page'] = $_GET['page'];
  }
  if (isset($_GET['sort']) && is_string($_GET['sort'])) {
    $filters['_GET']['sort'] = $_GET['sort'];
  }
  if (isset($_GET['order']) && is_string($_GET['order'])) {
    $filters['_GET']['order'] = $_GET['order'];
  }

  $_SESSION['entity_backend_search_filters'][$entity_type] = $filters;
}

/**
 * Get search result table on the search page.
 *
 * @param array $form
 *   Drupal standard in form callbacks.
 * @param array &$form_state
 *   Drupal standard in form callbacks.
 * @param string $entity_type
 *   The entity type to generate filter form for.
 *
 * @return array
 *   A Drupal renderable form array.
 */
function entity_backend_search_search_result_table($form, &$form_state, $entity_type) {
  $form = array();

  // Set up bulk operations for entity type, if any.
  // Permissions for bulk operations should be handled by the module supplying
  // the options.
  $bulk_options = module_invoke_all('entity_backend_search_bulk_operations_list', $entity_type);
  if (!empty($bulk_options)) {
    $form['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Update options'),
      '#attributes' => array('class' => array('container-inline')),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['options']['operation'] = array(
      '#type' => 'select',
      '#title' => t('Operation'),
      '#title_display' => 'invisible',
      '#options' => $bulk_options,
      '#default_value' => 'unblock',
    );
    $form['options']['entity_type'] = array(
      '#type' => 'value',
      '#value' => $entity_type,
    );
    $form['options']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
      '#validate' => array(
        'entity_backend_search_search_result_table_validate',
      ),
      '#submit' => array(
        'entity_backend_search_search_result_table_submit',
      ),
    );
  }

  $search_filters = _entity_backend_search_get_entity_search_filters($entity_type);

  $entities = array();

  $table_header = array();
  $table_rows = array();

  // This is a bit messy, but we need to first get table headers before we can
  // add table sorts to the actual query. So an empty set of entities are passed
  // to the hooks.
  module_implements('', FALSE, TRUE);
  entity_backend_search_format_search_result_table($table_header, $table_rows, $entity_type, $entities);

  $settings = variable_get('entity_backend_search_entities_settings_' . $entity_type, array());

  $entity_ids = entity_backend_search_get_search_result_ids($entity_type, $search_filters, $settings['search_result_count'], $table_header, $settings['search_result_pager']);

  if (!empty($entity_ids)) {
    $entities = entity_load($entity_type, $entity_ids);
  }

  // And run it again to process the actual results.
  entity_backend_search_format_search_result_table($table_header, $table_rows, $entity_type, $entities);

  $form['pager_top'] = array('#markup' => theme('pager'));
  if (empty($bulk_options)) {
    $form['results'] = array(
      '#markup' => theme(
        'table',
        array(
          'header' => $table_header,
          'rows' => $table_rows,
          'empty' => t('No results for %entity_type available.', array('%entity_type' => $entity_type)),
        )
      ),
    );
  }
  else {
    $form['results'] = array(
      '#type' => 'tableselect',
      '#header' => $table_header,
      '#options' => $table_rows,
      '#empty' => t('No results for %entity_type available.', array('%entity_type' => $entity_type)),
    );
  }
  $form['pager_bottom'] = array('#markup' => theme('pager'));

  return $form;
}


/**
 * Validate handler bulk operations.
 *
 * Check if any entities have been selected to perform the chosen bulk operation
 * on.
 */
function entity_backend_search_search_result_table_validate($form, &$form_state) {
  // Error if there are no items to select.
  if (!is_array($form_state['values']['results']) || !count(array_filter($form_state['values']['results']))) {
    form_set_error('', t('No items selected.'));
  }
}

/**
 * Submit handler bulk operations.
 */
function entity_backend_search_search_result_table_submit($form, &$form_state) {
  $entity_ids = array_filter($form_state['values']['results']);
  $operation = $form_state['values']['operation'];
  $entity_type = $form_state['values']['entity_type'];

  module_invoke_all('entity_backend_search_bulk_operations_action', $entity_type, $operation, $entity_ids);
}

/**
 * Default table format for search results.
 *
 * If a bundle does not have a table formatter, provide a simple one. It will
 * not always guess correctly, but it's a base to work from.
 *
 * @see hook_entity_backend_search_format_search_result_table()
 *
 * @param array &$table_header
 *   The table header array to be manipulated with.
 * @param array &$table_rows
 *   The table rows to display in table.
 * @param array $entity_type
 *   Name of entity type.
 * @param array $entities
 *   Array of entities found through search.
 */
function entity_backend_search_format_search_result_table(&$table_header, &$table_rows, $entity_type, $entities) {
  entity_backend_search_include_module_files();

  if (_entity_backend_search_search_result_table_in_code_format($table_header, $table_rows, $entity_type, $entities)) {
    // Noop.
  }
  elseif (_entity_backend_search_search_result_table_configured_format($table_header, $table_rows, $entity_type, $entities)) {
    // Noop.
  }
  else {
    _entity_backend_search_search_result_table_default_format($table_header, $table_rows, $entity_type, $entities);
  }

  $context = array(
    'entity_type' => $entity_type,
    'entities' => $entities,
  );
  // Let modules manipulate with the table data.
  drupal_alter('entity_backend_search_format_search_result_table', $table_header, $table_rows, $context);
}

/**
 * Look for a formatter in code.
 *
 * Table formatters are activated through
 * hook_entity_backend_search_format_search_result_table().
 *
 * @see hook_entity_backend_search_format_search_result_table()
 *
 * @param array &$table_header
 *   The table header array to be manipulated with.
 * @param array &$table_rows
 *   The table rows to display in table.
 * @param array $entity_type
 *   Name of entity type.
 * @param array $entities
 *   Array of entities found through search.
 */
function _entity_backend_search_search_result_table_in_code_format(&$table_header, &$table_rows, $entity_type, $entities) {
  $hook = 'entity_backend_search_format_search_result_table';
  foreach (module_implements($hook) as $module) {
    $function = $module . '_' . $hook;
    // Returns true if the table is being formatted in code.
    if ($function($entity_type, $entities, $table_header, $table_rows)) {
      return TRUE;
    }
  }
  return FALSE;
}


/**
 * Configured table format for search results.
 *
 * The table format can be configured at the configuration page.
 *
 * @see hook_entity_backend_search_format_search_result_table()
 *
 * @param array &$table_header
 *   The table header array to be manipulated with.
 * @param array &$table_rows
 *   The table rows to display in table.
 * @param array $entity_type
 *   Name of entity type.
 * @param array $entities
 *   Array of entities found through search.
 */
function _entity_backend_search_search_result_table_configured_format(&$table_header, &$table_rows, $entity_type, $entities) {

  $settings = entity_backend_search_get_search_settings($entity_type);

  if (empty($settings['table_formatter'])) {
    return FALSE;
  }

  $formatter_found = FALSE;
  $enabled_fields = array();
  foreach ($settings['table_formatter'] as $field_name => $column) {
    if (empty($column['status'])) {
      continue;
    }

    $enabled_fields[$field_name] = $column['pattern'];

    $formatter_found = TRUE;

    if (!empty($column['sortable'])) {
      $table_header[$field_name] = array(
        'data' => check_plain($column['thead']),
        'field' => $field_name,
        'type' => 'property',
        'specifier' => $field_name,
      );
    }
    else {
      $table_header[$field_name] = check_plain($column['thead']);
    }
  }

  if (!$formatter_found) {
    return FALSE;
  }

  foreach ($entities as $entity) {
    $tokens = NULL;
    $row = array();

    foreach ($enabled_fields as $field_name => $pattern) {
      // Make sure that the field value is ok to print.
      $value = (isset($entity->$field_name) && (ctype_digit($entity->$field_name) || is_string($entity->$field_name))) ? check_plain($entity->$field_name) : t('Unknown format');

      $formatter = empty($settings['table_formatter'][$field_name]['formatter']) ? 'None' : $settings['table_formatter'][$field_name]['formatter'];
      switch ($formatter) {
        case 'text':
          if (!empty($pattern)) {
            if (!isset($tokens)) {
              $tokens = _entity_backend_search_get_field_tokens($entity, $enabled_fields);
            }
            $value = strtr($pattern, $tokens);
          }
          break;

        case 'date_format_short':
        case 'date_format_medium':
        case 'date_format_long':
          $value = check_plain(format_date($value, $formatter));
          break;

        case 'boolean':
          $value = empty($value) ? t('No') : t('Yes');
          break;

        default:
          $context = array(
            'entity_type' => $entity_type,
            'field_name' => $field_name,
            'pattern' => $pattern,
            'formatter' => $formatter,
            'entity' => $entity,
          );
          drupal_alter('entity_backend_search_table_field_formaters_render', $value, $context);
          break;

      }
      $row[$field_name] = $value;

    }
    $table_rows[] = $row;
  }

  return $formatter_found;
}

/**
 * Get token replacements for given entity.
 *
 * @param object $entity
 *   Entity to get tokens from.
 * @param array $enabled_fields
 *   Array of field names to map to the tokens replacements.
 *
 * @return array
 *   Array of token replacements.
 */
function _entity_backend_search_get_field_tokens($entity, $enabled_fields) {
  $tokens = array();
  foreach ($enabled_fields as $field_name => $pattern) {
    $tokens['@' . $field_name] = (isset($entity->$field_name) && (ctype_digit($entity->$field_name) || is_string($entity->$field_name))) ? check_plain($entity->$field_name) : t('Unknown format');
  }

  $tokens['@destination'] = 'destination=' . (isset($_GET['destination']) ? $_GET['destination'] : $_GET['q']);

  return $tokens;
}

/**
 * Default table format for search results.
 *
 * If a bundle does not have a table formatter, provide a simple one. It will
 * not always guess correctly, but it's a base to work from.
 *
 * @see hook_entity_backend_search_format_search_result_table()
 *
 * @param array &$table_header
 *   The table header array to be manipulated with.
 * @param array &$table_rows
 *   The table rows to display in table.
 * @param array $entity_type
 *   Name of entity type.
 * @param array $entities
 *   Array of entities found through search.
 */
function _entity_backend_search_search_result_table_default_format(&$table_header, &$table_rows, $entity_type, $entities) {
  // Default table header.
  $table_header = array(
    t('Id'),
    t('Title'),
  );

  // Run through the first entity to search for the first string value and
  // assume that this is the title to display.
  // Then run through all the entities and display the id and the assumed title.
  $first_string = NULL;
  foreach ($entities as $id => $entity) {
    if (!isset($first_string)) {
      $first_string = FALSE;
      foreach ($entity as $key => $value) {
        if (!ctype_digit($value) && is_string($value)) {
          $first_string = $key;
          break 1;
        }
      }
    }
    $table_rows[] = array(
      array(
        'data' => t('Entity id: %id', array('%id' => $id)),
      ),
      array(
        'data' => $first_string ? $entity->{$first_string} : t('Unknown title'),
      ),
    );
  }
}

/**
 * Perform the search and return a list of entity ids.
 *
 * @param string $entity_type
 *   Type of entity currently beeing queried.
 * @param array $search_filters
 *   Array of search filters. These are based on the configured search filters
 *   and the input from the user.
 * @param int $count
 *   Number of results per page to return.
 * @param array $table_header
 *   Search result table header. Used for sorts.
 * @param string $pager
 *   Type of pager. Used to determine how to manipulate the search result.
 *
 * @return array
 *   An array of entity ids, indexed by the entity ids.
 */
function entity_backend_search_get_search_result_ids($entity_type, $search_filters = array(), $count = 10, $table_header = array(), $pager = 'full') {
  entity_backend_search_include_module_files();

  $ids = array();

  // Use our own custom class extending EntityFieldQuery to add support for
  // joins.
  $query = new EntityBackendSearchEntityFieldQuery();

  $query->entityCondition('entity_type', $entity_type, '=');

  // Always handle bundle filtering.
  if (!empty($search_filters['bundles'])) {
    $query->entityCondition('bundle', $search_filters['bundles'], 'IN');
  }

  if ($pager == 'full') {
    $query->pager($count);
  }
  else {
    $current_page = (isset($_GET['page']) && is_numeric($_GET['page'])) ? intval($_GET['page']) : 0;
    $offset = $current_page * $count;

    // Add one to the count to figure out if there are more pages.
    $query->range($offset, $pager == 'simple' ? $count + 1 : $count);
  }

  // Let table headers add sorts.
  $query->tablesort($table_header);

  // Let modules manipulate the filters and the query object.
  drupal_alter('entity_backend_search_query', $query, $entity_type, $search_filters);

  $result = $query->execute();

  if ($pager == 'simple' && !empty($result[$entity_type])) {
    // Add a simple pager, which only displays from current page and back.
    global $pager_page_array, $pager_total;
    $current_page = (isset($_GET['page']) && is_numeric($_GET['page'])) ? intval($_GET['page']) : 0;
    $pager_page_array = array(
      0 => $current_page,
    );
    $total_pages = $current_page + (count($result[$entity_type]) > $count ? 2 : 1);
    $pager_total = array(
      0 => $total_pages,
    );

    if (count($result[$entity_type]) > $count) {
      array_pop($result[$entity_type]);
    }
  }

  if (!empty($result[$entity_type])) {
    $ids = drupal_map_assoc(array_keys($result[$entity_type]));
  }

  return $ids;
}

/**
 * Page callback. Outputs JSON for autocomplete suggestions.
 *
 * Implements a very basic entity autocomplete functionality.
 *
 * @param string $entity_type
 *   Name of the entity type to search through.
 * @param string $string
 *   Input string to search for.
 *
 * @return string
 *   JSON for autocomplete suggestions.
 */
function entity_backend_search_autocomplete($entity_type, $string = '') {
  if (empty($string) || !is_string($string)) {
    drupal_json_output(array());
    return;
  }

  entity_backend_search_include_module_files();

  $field_info = array();
  $field_name = FALSE;
  $field_type = FALSE;
  $multiple_tags = TRUE;

  $entity_info = entity_get_info($entity_type);
  if (!$entity_info) {
    drupal_json_output(array(0 => t('Something went wrong. Contact site administrator.')));
    return;
  }

  $hook = 'entity_backend_search_autocomplete_title_field';
  foreach (module_implements($hook) as $module) {
    $function = $module . '_' . $hook;
    if ($field_info = $function($entity_type)) {
      if (is_string($field_info)) {
        $field_name = $field_info;
        break;
      }
      elseif (is_array($field_info)) {
        if (!empty($field_info['field name'])) {
          $field_name = $field_info['field name'];
        }
        if (isset($field_info['multiple tags'])) {
          $multiple_tags = $field_info['multiple tags'];
        }
        break;
      }
    }
  }

  if (empty($field_name) && !empty($entity_info['entity keys']) && !empty($entity_info['entity keys']['label']) && !empty($entity_info['schema_fields_sql']) && !empty($entity_info['schema_fields_sql']['base table'])) {
    if (in_array($entity_info['entity keys']['label'], $entity_info['schema_fields_sql']['base table'])) {
      $field_name = $entity_info['entity keys']['label'];
      $field_type = 'property';
    }

  }

  if (empty($field_name)) {
    drupal_json_output(array(0 => t('No title field configured. Contact site administrator.')));
    return;
  }

  // Figure out where the title field comes from.
  // First try the base table of the entity.
  if (!$field_type) {
    if (!empty($entity_info['schema_fields_sql']) && !empty($entity_info['schema_fields_sql']['base table'])) {
      if (in_array($field_name, $entity_info['schema_fields_sql']['base table'])) {
        $field_type = 'property';
      }
    }
  }

  // The try the fields of the entity.
  if (!$field_type) {
    if ($field = field_info_field($field_name)) {
      $field_type = 'field';
    }
  }

  if (!$field_type) {
    drupal_json_output(array(0 => t('Title field could not be identified. Contact site administrator.')));
    return;
  }

  // Use our own custom class extending EntityFieldQuery to add support for
  // joins.
  $query = new EntityBackendSearchEntityFieldQuery();

  $query->entityCondition('entity_type', $entity_type, '=');

  $matches_prefix = '';
  // Check if multiple tags are allowed. If the field allows for commas,
  // drupal_explode_tags() will break the functionality.
  if ($multiple_tags) {
    $strings_typed = drupal_explode_tags($string);
    $search_string = array_pop($strings_typed);

    // Prefix previously typed in results to search result.
    if (!empty($strings_typed)) {
      $matches_prefix = implode(', ', $strings_typed) . ', ';
    }
  }
  else {
    $search_string = $string;
  }

  if ($field_type == 'property') {
    $query->propertyCondition($field_name, $search_string, 'CONTAINS');
  }
  elseif ($field_type == 'field') {
    $query->fieldCondition($field_name, 'value', $search_string, 'CONTAINS');
  }

  $query->range(0, 10);

  // Lets sort by latest created.
  if (!empty($entity_info['entity keys']['id'])) {
    $query->propertyOrderBy($entity_info['entity keys']['id'], 'DESC');
  }

  $result = $query->execute();

  if (empty($result[$entity_type])) {
    drupal_json_output(array());
    return;
  }

  $matches = array();

  // First try the quick search. If the key is already set on the search result
  // a load of the search result entities is not needed.
  foreach ($result[$entity_type] as $entity_id => $entity_object) {
    if (isset($entity_object->$field_name)) {
      $match = $entity->$field_name . ' [' . $entity_type . ':' . $entity->{$entity_info['entity keys']['id']} . ']';
      $matches[$matches_prefix . $match] = $match;
    }
    else {
      break;
    }
  }

  // If there still is no matches, it means that the field was not on the light
  // weight search result object. Load the search result entities and try to
  // fetch their titles through that.
  if (empty($matches)) {
    $entities = entity_load($entity_type, array_keys($result[$entity_type]));
    foreach ($entities as $entity_id => $entity) {
      if (isset($entity->$field_name)) {
        $match = $entity->$field_name . ' [' . $entity_type . ':' . $entity->{$entity_info['entity keys']['id']} . ']';
        $matches[$matches_prefix . $match] = $match;
      }
    }
  }

  drupal_json_output($matches);
}
