<?php
/**
 * @file
 * cruise_factory_feature.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function cruise_factory_feature_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'cruiselines|cruiselines|default';
  $ds_fieldsetting->entity_type = 'cruiselines';
  $ds_fieldsetting->bundle = 'cruiselines';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'brief_description_formatter' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['cruiselines|cruiselines|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'cruises|cruises|cruise_deal_cruise_footer';
  $ds_fieldsetting->entity_type = 'cruises';
  $ds_fieldsetting->bundle = 'cruises';
  $ds_fieldsetting->view_mode = 'cruise_deal_cruise_footer';
  $ds_fieldsetting->settings = array(
    'cruise_info_formatted' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['cruises|cruises|cruise_deal_cruise_footer'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'cruises|cruises|cruise_deal_cruise_intro';
  $ds_fieldsetting->entity_type = 'cruises';
  $ds_fieldsetting->bundle = 'cruises';
  $ds_fieldsetting->view_mode = 'cruise_deal_cruise_intro';
  $ds_fieldsetting->settings = array(
    'cruiseline_logo' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'sailing_dates_for_cruise_teaser' => array(
      'weight' => '4',
      'label' => 'inline',
      'format' => 'default',
    ),
    'ship_name_link' => array(
      'weight' => '2',
      'label' => 'inline',
      'format' => 'default',
    ),
    'cruise_duration_with_days_' => array(
      'weight' => '9',
      'label' => 'inline',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['cruises|cruises|cruise_deal_cruise_intro'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'cruises|cruises|default';
  $ds_fieldsetting->entity_type = 'cruises';
  $ds_fieldsetting->bundle = 'cruises';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'cruiseline_logo' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'ship_name_link' => array(
      'weight' => '4',
      'label' => 'inline',
      'format' => 'default',
    ),
    'brief_description_formatted_crui' => array(
      'weight' => '8',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'cruise_duration_with_days_' => array(
      'weight' => '7',
      'label' => 'inline',
      'format' => 'default',
    ),
    'cruise_info_formatted' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'cruise_sailing_dates' => array(
      'weight' => '13',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'enquire_about_this_cruise' => array(
      'weight' => '12',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'ship_block' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h2',
        'class' => 'block-title',
      ),
    ),
  );
  $export['cruises|cruises|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'cruises|cruises|full';
  $ds_fieldsetting->entity_type = 'cruises';
  $ds_fieldsetting->bundle = 'cruises';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'brief_description_formatted_crui' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'ship_image' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['cruises|cruises|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'cruises|cruises|teaser';
  $ds_fieldsetting->entity_type = 'cruises';
  $ds_fieldsetting->bundle = 'cruises';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'cruiseline_name_text' => array(
      'weight' => '8',
      'label' => 'inline',
      'format' => 'default',
    ),
    'sailing_date' => array(
      'weight' => '6',
      'label' => 'inline',
      'format' => 'default',
    ),
    'sailing_dates_for_cruise_teaser' => array(
      'weight' => '4',
      'label' => 'inline',
      'format' => 'default',
    ),
    'sailing_date_view_block' => array(
      'weight' => '5',
      'label' => 'inline',
      'format' => 'default',
    ),
    'ship_name' => array(
      'weight' => '7',
      'label' => 'inline',
      'format' => 'default',
    ),
    'view_cruise_link' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'cruise_sailing_dates' => array(
      'weight' => '3',
      'label' => 'inline',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => '',
      ),
    ),
  );
  $export['cruises|cruises|teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'destinations|destinations|teaser';
  $ds_fieldsetting->entity_type = 'destinations';
  $ds_fieldsetting->bundle = 'destinations';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'destination_info_formatted' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['destinations|destinations|teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ports|ports|default';
  $ds_fieldsetting->entity_type = 'ports';
  $ds_fieldsetting->bundle = 'ports';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'ports_info_fromatted' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['ports|ports|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ships|ships|default';
  $ds_fieldsetting->entity_type = 'ships';
  $ds_fieldsetting->bundle = 'ships';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
  );
  $export['ships|ships|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ships|ships|teaser';
  $ds_fieldsetting->entity_type = 'ships';
  $ds_fieldsetting->bundle = 'ships';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'strong',
        'class' => '',
      ),
    ),
  );
  $export['ships|ships|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function cruise_factory_feature_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'brief_description_formatted_crui';
  $ds_field->label = 'Brief Description Formatted Cruises';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<h1>[cruises:field-cruise-brief-description]</h1>',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['brief_description_formatted_crui'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'brief_description_formatter';
  $ds_field->label = 'Brief Description Formatted Cruiselines';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'cruiselines' => 'cruiselines',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<h3>[cruiselines:field-brief-description]</h3>',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['brief_description_formatter'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'cruise_duration_with_days_';
  $ds_field->label = 'Duration';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '[cruises:field-length] days',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['cruise_duration_with_days_'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'cruise_info_formatted';
  $ds_field->label = 'Cruise Info Formatted';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<?php
$string = "[cruises:field_cruise_description]";

$replaceme = \'. \';
$replacewith = \'.<br /><br />\';
$nthtimes = 5;

$i = 1;
$substring2 = $string;
$one = ". ";
list($output, $null) = explode($replaceme, $string);
$initial_array = explode($replaceme, $substring2);

$result = array();
$final_result = "";
$i = 0;

if (count($initial_array) > 5) {
	foreach($initial_array as $value) {
	  if ($i++ % 4 == 0) {
	    $result[] = $value . ".<br /><br />";
	    $final_result .= $value . ".<br /><br />";
	  } else {
	  	$result[] = $value . ".<br />";
	    $final_result .= $value . ".<br />";
	  }
	}
} else {
	foreach($initial_array as $value) {
	 	$result[] = $value . ".<br />";
	  $final_result .= $value . ".<br />";
	}
}

$stringformatted = str_replace("Highlights of this cruise:", "<br><h3>Highlights of this cruise:</h3>", $final_result);
print $stringformatted;

?>',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['cruise_info_formatted'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'cruise_sailing_dates';
  $ds_field->label = 'page elements for cruises: sailing date block';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'block' => 'views|23332cf796fe9f51f1d04a392560f9d9',
    'block_render' => '2',
  );
  $export['cruise_sailing_dates'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'cruiseline_logo';
  $ds_field->label = 'Cruiseline Logo';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'block' => 'views|0f2e7f046b2cc62e50ae403ca055b8ed',
    'block_render' => '3',
  );
  $export['cruiseline_logo'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'cruiseline_name_text';
  $ds_field->label = 'Cruise Line';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '[cruises:field_cruiseline]',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['cruiseline_name_text'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'destination_info_formatted';
  $ds_field->label = 'Destination Info Formatted';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'destinations' => 'destinations',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<?php
$string = "[destinations:field-destination-description]";

$replaceme = \'. \';
$replacewith = \'.<br /><br />\';
$nthtimes = 5;

$i = 1;
$substring2 = $string;
$one = ". ";
list($output, $null) = explode($replaceme, $string);
$initial_array = explode($replaceme, $substring2);

$result = array();
$final_result = "";
$i = 0;

if (count($initial_array) > 4) {
	foreach($initial_array as $value) {
	  if ($i++ % 4 == 0) {
	    $result[] = $value . ".<br /><br />";
	    $final_result .= $value . ".<br /><br />";
	  } else {
	  	$result[] = $value . ".<br />";
	    $final_result .= $value . ".<br />";
	  }
	}
} else {
	foreach($initial_array as $value) {
	 	$result[] = $value . ".<br />";
	  $final_result .= $value . ".<br />";
	}
}

$finaloutput = str_replace("Highlights of this cruise:", "<br><h3>Highlights of this cruise:</h3>", $final_result);


$finaloutput = html_entity_decode($finaloutput);

$finaloutput = str_replace("<b>This region</b>", "<br><b>This region</b>", $finaloutput);
$finaloutput = str_replace(". <b>", ".<br><br><b>", $finaloutput);
$finaloutput = str_replace("<b>Photo Credit:</b>", "<br><b>Photo Credit:</b>", $finaloutput);

print $finaloutput;

?>',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['destination_info_formatted'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'enquire_about_this_cruise';
  $ds_field->label = 'Enquire about this Cruise';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'block' => 'views|enquiry_form_view-block',
    'block_render' => '3',
  );
  $export['enquire_about_this_cruise'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'ports_info_fromatted';
  $ds_field->label = 'Ports Info Fromatted';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'ports' => 'ports',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<?php
$finaloutput = str_replace("Things to see and do", "<br><h3>Things to see and do</h3>", "[ports:field_port_description]");
$finaloutput = str_replace("General information", "<br><h3> General information </h3>", $finaloutput);
$finaloutput = str_replace("Cruise Season", "<br><strong>Cruise Season</strong>", $finaloutput);
$finaloutput = str_replace("Currency", "<br><strong>Currency</strong>", $finaloutput);
$finaloutput = str_replace("Language", "<br><strong>Language</strong>", $finaloutput);
$finaloutput = str_replace("Land Area", "<br><strong>Land Area</strong>", $finaloutput);
$finaloutput = str_replace("Population", "<br><strong>Population</strong>", $finaloutput);
$finaloutput = str_replace("Electricity", "<br><strong>Electricity</strong>", $finaloutput);
$finaloutput = str_replace("Time", "<br><strong>Time</strong>", $finaloutput);
$finaloutput = str_replace("International Country Telephone Code", "<br><strong>International Country Telephone Code</strong>", $finaloutput);
$finaloutput = str_replace("Port Location", "<br><strong>Port Location</strong>", $finaloutput);
$finaloutput = str_replace("Transport Links", "<br><strong>Transport Links</strong>", $finaloutput);
$finaloutput = str_replace("* ", "<br>&nbsp;&nbsp;&bull; ", $finaloutput);
$finaloutput = html_entity_decode($finaloutput);
print $finaloutput;
?>',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['ports_info_fromatted'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'sailing_date';
  $ds_field->label = 'Sailing Date 1';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '[sailingdates:field-sailingdate]',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['sailing_date'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'sailing_date_view_block';
  $ds_field->label = 'Sailing Date';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'block' => 'views|011e67fc925c951c6853bb7cd3ed0726',
    'block_render' => '1',
  );
  $export['sailing_date_view_block'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'sailing_dates_for_cruise_teaser';
  $ds_field->label = 'Sailing Date';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'block' => 'views|8a3126663d1cbdce303864e20d78cacc',
    'block_render' => '3',
  );
  $export['sailing_dates_for_cruise_teaser'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'ship_block';
  $ds_field->label = 'Ship Block';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|e963fb6b8c3df984f001e4812d4e37e6',
    'block_render' => '3',
  );
  $export['ship_block'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'ship_image';
  $ds_field->label = 'Ship Image';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
* Ship Image Here *
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>',
      'format' => 'ds_code',
    ),
    'use_token' => 0,
  );
  $export['ship_image'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'ship_name';
  $ds_field->label = 'Ship';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '[cruises:field-ships:title]',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['ship_name'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'ship_name_link';
  $ds_field->label = 'Ship';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<a href="/ships/ships/[cruises:field-ships:id]">[cruises:field-ships:title]</a>',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['ship_name_link'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'video_iframe';
  $ds_field->label = 'Video iFrame';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'cruiselines' => 'cruiselines',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<iframe src="[cruiselines:field-video-url:url]" height="250" width="450" frameborder="0" seamless="1" seamless></iframe>',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['video_iframe'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'view_cruise_link';
  $ds_field->label = 'View Cruise Link';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'cruises' => 'cruises',
  );
  $ds_field->ui_limit = '*|*';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<a href="/cruises/cruises/[cruises:id]" class="btn btn-success">View cruise</a>',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['view_cruise_link'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function cruise_factory_feature_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'cruiselines|cruiselines|default';
  $ds_layout->entity_type = 'cruiselines';
  $ds_layout->bundle = 'cruiselines';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_logo',
        1 => 'brief_description_formatter',
        2 => 'field_company_bio',
        3 => 'field_location',
        4 => 'field_star_rating',
      ),
    ),
    'fields' => array(
      'field_logo' => 'ds_content',
      'brief_description_formatter' => 'ds_content',
      'field_company_bio' => 'ds_content',
      'field_location' => 'ds_content',
      'field_star_rating' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['cruiselines|cruiselines|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'cruiselines|cruiselines|teaser';
  $ds_layout->entity_type = 'cruiselines';
  $ds_layout->bundle = 'cruiselines';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_hi_res_logo',
        1 => 'field_hi_res_white_logo',
        2 => 'field_hi_res_1_colour_logo',
        3 => 'field_logo',
      ),
    ),
    'fields' => array(
      'field_hi_res_logo' => 'ds_content',
      'field_hi_res_white_logo' => 'ds_content',
      'field_hi_res_1_colour_logo' => 'ds_content',
      'field_logo' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['cruiselines|cruiselines|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'cruises|cruises|cruise_deal_cruise_footer';
  $ds_layout->entity_type = 'cruises';
  $ds_layout->bundle = 'cruises';
  $ds_layout->view_mode = 'cruise_deal_cruise_footer';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'cruise_info_formatted',
      ),
    ),
    'fields' => array(
      'cruise_info_formatted' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['cruises|cruises|cruise_deal_cruise_footer'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'cruises|cruises|cruise_deal_cruise_intro';
  $ds_layout->entity_type = 'cruises';
  $ds_layout->bundle = 'cruises';
  $ds_layout->view_mode = 'cruise_deal_cruise_intro';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'cruiseline_logo',
        1 => 'title',
        2 => 'ship_name_link',
        3 => 'sailing_dates_for_cruise_teaser',
        4 => 'field_cruiseline',
        5 => 'field_destination',
        6 => 'cruise_duration_with_days_',
        7 => 'field_cruise_type',
      ),
      'right' => array(
        8 => 'field_ships',
      ),
    ),
    'fields' => array(
      'cruiseline_logo' => 'left',
      'title' => 'left',
      'ship_name_link' => 'left',
      'sailing_dates_for_cruise_teaser' => 'left',
      'field_cruiseline' => 'left',
      'field_destination' => 'left',
      'cruise_duration_with_days_' => 'left',
      'field_cruise_type' => 'left',
      'field_ships' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['cruises|cruises|cruise_deal_cruise_intro'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'cruises|cruises|default';
  $ds_layout->entity_type = 'cruises';
  $ds_layout->bundle = 'cruises';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'cruiseline_logo',
        1 => 'title',
        2 => 'field_cruiseline',
        3 => 'field_destination',
        4 => 'ship_name_link',
        5 => 'field_cruise_type',
        6 => 'cruise_duration_with_days_',
        7 => 'brief_description_formatted_crui',
        8 => 'cruise_info_formatted',
      ),
      'right' => array(
        9 => 'ship_block',
        10 => 'field_photo',
        11 => 'enquire_about_this_cruise',
      ),
      'footer' => array(
        12 => 'cruise_sailing_dates',
      ),
    ),
    'fields' => array(
      'cruiseline_logo' => 'left',
      'title' => 'left',
      'field_cruiseline' => 'left',
      'field_destination' => 'left',
      'ship_name_link' => 'left',
      'field_cruise_type' => 'left',
      'cruise_duration_with_days_' => 'left',
      'brief_description_formatted_crui' => 'left',
      'cruise_info_formatted' => 'left',
      'ship_block' => 'right',
      'field_photo' => 'right',
      'enquire_about_this_cruise' => 'right',
      'cruise_sailing_dates' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['cruises|cruises|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'cruises|cruises|full';
  $ds_layout->entity_type = 'cruises';
  $ds_layout->bundle = 'cruises';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'footer' => array(
        0 => 'brief_description_formatted_crui',
        10 => 'field_cruise_description',
      ),
      'right' => array(
        1 => 'ship_image',
        8 => 'field_ships',
        9 => 'field_photo',
      ),
      'left' => array(
        2 => 'title',
        3 => 'field_cruiseline',
        4 => 'field_destination',
        5 => 'field_cruise_type',
        6 => 'field_length',
        7 => 'field_start_price',
      ),
    ),
    'fields' => array(
      'brief_description_formatted_crui' => 'footer',
      'ship_image' => 'right',
      'title' => 'left',
      'field_cruiseline' => 'left',
      'field_destination' => 'left',
      'field_cruise_type' => 'left',
      'field_length' => 'left',
      'field_start_price' => 'left',
      'field_ships' => 'right',
      'field_photo' => 'right',
      'field_cruise_description' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['cruises|cruises|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'cruises|cruises|teaser';
  $ds_layout->entity_type = 'cruises';
  $ds_layout->bundle = 'cruises';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
      ),
      'left' => array(
        1 => 'field_destination',
        2 => 'field_length',
        3 => 'cruise_sailing_dates',
        4 => 'sailing_dates_for_cruise_teaser',
        5 => 'sailing_date_view_block',
        6 => 'sailing_date',
        7 => 'ship_name',
        8 => 'cruiseline_name_text',
        9 => 'field_cruiseline',
        10 => 'view_cruise_link',
      ),
      'right' => array(
        11 => 'field_ships',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'field_destination' => 'left',
      'field_length' => 'left',
      'cruise_sailing_dates' => 'left',
      'sailing_dates_for_cruise_teaser' => 'left',
      'sailing_date_view_block' => 'left',
      'sailing_date' => 'left',
      'ship_name' => 'left',
      'cruiseline_name_text' => 'left',
      'field_cruiseline' => 'left',
      'view_cruise_link' => 'left',
      'field_ships' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['cruises|cruises|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'destinations|destinations|teaser';
  $ds_layout->entity_type = 'destinations';
  $ds_layout->bundle = 'destinations';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'destination_info_formatted',
      ),
    ),
    'fields' => array(
      'destination_info_formatted' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['destinations|destinations|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ports|ports|default';
  $ds_layout->entity_type = 'ports';
  $ds_layout->bundle = 'ports';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_photo',
        1 => 'field_destination',
        2 => 'ports_info_fromatted',
      ),
    ),
    'fields' => array(
      'field_photo' => 'ds_content',
      'field_destination' => 'ds_content',
      'ports_info_fromatted' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['ports|ports|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'priceguide|priceguide|default';
  $ds_layout->entity_type = 'priceguide';
  $ds_layout->bundle = 'priceguide';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_4col';
  $ds_layout->settings = array(
    'regions' => array(
      'first' => array(
        0 => 'field_inside_cabin',
      ),
      'second' => array(
        1 => 'field_outside_cabin',
      ),
      'third' => array(
        2 => 'field_balcony',
      ),
      'fourth' => array(
        3 => 'field_suite',
      ),
    ),
    'fields' => array(
      'field_inside_cabin' => 'first',
      'field_outside_cabin' => 'second',
      'field_balcony' => 'third',
      'field_suite' => 'fourth',
    ),
    'classes' => array(),
    'wrappers' => array(
      'first' => 'div',
      'second' => 'div',
      'third' => 'div',
      'fourth' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['priceguide|priceguide|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ships|ships|default';
  $ds_layout->entity_type = 'ships';
  $ds_layout->bundle = 'ships';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
        1 => 'field_star_rating',
        2 => 'field_passenger_capacity',
        3 => 'field_passenger_space',
        4 => 'field_crew_size',
        5 => 'field_length',
        6 => 'field_beam',
        7 => 'field_draft',
        8 => 'field_speed',
        9 => 'field_tonnage',
        10 => 'field_cruiseline',
        11 => 'field_cruise_type',
        12 => 'field_maiden_voyage',
        13 => 'field_refurbished',
        14 => 'field_ship_rego',
        15 => 'field_nationality_of_crew',
        16 => 'field_nationality_of_officers',
        17 => 'field_nationality_of_dining_crew',
      ),
      'right' => array(
        18 => 'field_thumbnail',
        19 => 'field_ship_description',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'field_star_rating' => 'left',
      'field_passenger_capacity' => 'left',
      'field_passenger_space' => 'left',
      'field_crew_size' => 'left',
      'field_length' => 'left',
      'field_beam' => 'left',
      'field_draft' => 'left',
      'field_speed' => 'left',
      'field_tonnage' => 'left',
      'field_cruiseline' => 'left',
      'field_cruise_type' => 'left',
      'field_maiden_voyage' => 'left',
      'field_refurbished' => 'left',
      'field_ship_rego' => 'left',
      'field_nationality_of_crew' => 'left',
      'field_nationality_of_officers' => 'left',
      'field_nationality_of_dining_crew' => 'left',
      'field_thumbnail' => 'right',
      'field_ship_description' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['ships|ships|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ships|ships|larger_ship_image';
  $ds_layout->entity_type = 'ships';
  $ds_layout->bundle = 'ships';
  $ds_layout->view_mode = 'larger_ship_image';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_thumbnail',
      ),
    ),
    'fields' => array(
      'field_thumbnail' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['ships|ships|larger_ship_image'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ships|ships|teaser';
  $ds_layout->entity_type = 'ships';
  $ds_layout->bundle = 'ships';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_thumbnail',
      ),
    ),
    'fields' => array(
      'field_thumbnail' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['ships|ships|teaser'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function cruise_factory_feature_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'cruise_deal_cruise_footer';
  $ds_view_mode->label = 'Cruise Deal Cruise Footer';
  $ds_view_mode->entities = array(
    'cruises' => 'cruises',
  );
  $export['cruise_deal_cruise_footer'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'cruise_deal_cruise_intro';
  $ds_view_mode->label = 'Cruise Deal Cruise Intro';
  $ds_view_mode->entities = array(
    'cruises' => 'cruises',
  );
  $export['cruise_deal_cruise_intro'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'larger_ship_image';
  $ds_view_mode->label = 'Larger Ship Image';
  $ds_view_mode->entities = array(
    'ships' => 'ships',
  );
  $export['larger_ship_image'] = $ds_view_mode;

  return $export;
}
