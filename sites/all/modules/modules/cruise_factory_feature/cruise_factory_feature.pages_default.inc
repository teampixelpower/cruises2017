<?php
/**
 * @file
 * cruise_factory_feature.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function cruise_factory_feature_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__panel_context_2954c61d-f472-4d39-861e-9b3a390f3b45';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 2;
  $handler->conf = array(
    'title' => 'Destinations',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'destination' => 'destination',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'twocol_bricks';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left_above' => NULL,
      'right_above' => NULL,
      'middle' => NULL,
      'left_below' => NULL,
      'right_below' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'e9466a36-c156-4b6e-9733-175c19508c9f';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f7c89a56-c3b0-4daa-be45-eae20dabd66a';
    $pane->panel = 'left_above';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => 'More about %node:title',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f7c89a56-c3b0-4daa-be45-eae20dabd66a';
    $display->content['new-f7c89a56-c3b0-4daa-be45-eae20dabd66a'] = $pane;
    $display->panels['left_above'][0] = 'new-f7c89a56-c3b0-4daa-be45-eae20dabd66a';
    $pane = new stdClass();
    $pane->pid = 'new-ac61cf4d-9477-4ebb-9cee-98f1f3823fce';
    $pane->panel = 'left_above';
    $pane->type = 'views';
    $pane->subtype = 'destination_block';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '1',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'context' => array(
        0 => 'argument_entity_id:node_1.nid',
      ),
      'override_title' => 0,
      'override_title_text' => 'DESTINATION INFO FROM FEED',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ac61cf4d-9477-4ebb-9cee-98f1f3823fce';
    $display->content['new-ac61cf4d-9477-4ebb-9cee-98f1f3823fce'] = $pane;
    $display->panels['left_above'][1] = 'new-ac61cf4d-9477-4ebb-9cee-98f1f3823fce';
    $pane = new stdClass();
    $pane->pid = 'new-c5fa1329-8bae-43c6-b962-8b9ba6900587';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'ports_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'cruises_list',
      'context' => array(
        0 => 'argument_entity_id:node_1.nid',
      ),
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c5fa1329-8bae-43c6-b962-8b9ba6900587';
    $display->content['new-c5fa1329-8bae-43c6-b962-8b9ba6900587'] = $pane;
    $display->panels['middle'][0] = 'new-c5fa1329-8bae-43c6-b962-8b9ba6900587';
    $pane = new stdClass();
    $pane->pid = 'new-bd225c2e-48fa-4636-b454-2749d83542f1';
    $pane->panel = 'right_above';
    $pane->type = 'views';
    $pane->subtype = 'ports_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'ports_list',
      'context' => array(
        0 => 'argument_entity_id:node_1.nid',
      ),
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'bd225c2e-48fa-4636-b454-2749d83542f1';
    $display->content['new-bd225c2e-48fa-4636-b454-2749d83542f1'] = $pane;
    $display->panels['right_above'][0] = 'new-bd225c2e-48fa-4636-b454-2749d83542f1';
    $pane = new stdClass();
    $pane->pid = 'new-9031adbd-1493-44be-aba3-293b0d5957b4';
    $pane->panel = 'top';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_destination_node_image';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => 'slideshow',
        'image_link' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9031adbd-1493-44be-aba3-293b0d5957b4';
    $display->content['new-9031adbd-1493-44be-aba3-293b0d5957b4'] = $pane;
    $display->panels['top'][0] = 'new-9031adbd-1493-44be-aba3-293b0d5957b4';
    $pane = new stdClass();
    $pane->pid = 'new-422fc4db-a789-40a2-b652-11e6f5e23137';
    $pane->panel = 'top';
    $pane->type = 'views';
    $pane->subtype = 'banner';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'context' => array(
        0 => 'argument_entity_id:node_1.nid',
      ),
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '422fc4db-a789-40a2-b652-11e6f5e23137';
    $display->content['new-422fc4db-a789-40a2-b652-11e6f5e23137'] = $pane;
    $display->panels['top'][1] = 'new-422fc4db-a789-40a2-b652-11e6f5e23137';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view__panel_context_2954c61d-f472-4d39-861e-9b3a390f3b45'] = $handler;

  return $export;
}
