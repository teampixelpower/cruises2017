<?php
/**
 * @file
 * cruise_factory_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cruise_factory_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cruise_factory_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function cruise_factory_feature_eck_bundle_info() {
  $items = array(
    'cruiselines_cruiselines' => array(
      'machine_name' => 'cruiselines_cruiselines',
      'entity_type' => 'cruiselines',
      'name' => 'cruiselines',
      'label' => 'Cruise Lines',
    ),
    'cruises_cruises' => array(
      'machine_name' => 'cruises_cruises',
      'entity_type' => 'cruises',
      'name' => 'cruises',
      'label' => 'Cruises',
    ),
    'cruisetypes_cruisetypes' => array(
      'machine_name' => 'cruisetypes_cruisetypes',
      'entity_type' => 'cruisetypes',
      'name' => 'cruisetypes',
      'label' => 'Cruise Types',
    ),
    'currencies_currencies' => array(
      'machine_name' => 'currencies_currencies',
      'entity_type' => 'currencies',
      'name' => 'currencies',
      'label' => 'Currencies',
    ),
    'destinations_destinations' => array(
      'machine_name' => 'destinations_destinations',
      'entity_type' => 'destinations',
      'name' => 'destinations',
      'label' => 'Destinations',
    ),
    'itineraries_itineraries' => array(
      'machine_name' => 'itineraries_itineraries',
      'entity_type' => 'itineraries',
      'name' => 'itineraries',
      'label' => 'Itineraries',
    ),
    'ports_ports' => array(
      'machine_name' => 'ports_ports',
      'entity_type' => 'ports',
      'name' => 'ports',
      'label' => 'Ports',
    ),
    'priceguide_priceguide' => array(
      'machine_name' => 'priceguide_priceguide',
      'entity_type' => 'priceguide',
      'name' => 'priceguide',
      'label' => 'Price Guide',
    ),
    'sailingdates_sailingdates' => array(
      'machine_name' => 'sailingdates_sailingdates',
      'entity_type' => 'sailingdates',
      'name' => 'sailingdates',
      'label' => 'Sailing Dates',
    ),
    'shipphotos_shipphotos' => array(
      'machine_name' => 'shipphotos_shipphotos',
      'entity_type' => 'shipphotos',
      'name' => 'shipphotos',
      'label' => 'shipphotos',
    ),
    'ships_ships' => array(
      'machine_name' => 'ships_ships',
      'entity_type' => 'ships',
      'name' => 'ships',
      'label' => 'Ships',
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function cruise_factory_feature_eck_entity_type_info() {
  $items = array(
    'cruiselines' => array(
      'name' => 'cruiselines',
      'label' => 'Cruise Lines',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
    'cruises' => array(
      'name' => 'cruises',
      'label' => 'Cruises',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
    'cruisetypes' => array(
      'name' => 'cruisetypes',
      'label' => 'Cruise Types',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
    'currencies' => array(
      'name' => 'currencies',
      'label' => 'Currencies',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
    'destinations' => array(
      'name' => 'destinations',
      'label' => 'Destinations',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
    'itineraries' => array(
      'name' => 'itineraries',
      'label' => 'Itineraries',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
    'ports' => array(
      'name' => 'ports',
      'label' => 'Ports',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
    'priceguide' => array(
      'name' => 'priceguide',
      'label' => 'Price Guide',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
    'sailingdates' => array(
      'name' => 'sailingdates',
      'label' => 'Sailing Dates',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
    'shipphotos' => array(
      'name' => 'shipphotos',
      'label' => 'shipphotos',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
    'ships' => array(
      'name' => 'ships',
      'label' => 'Ships',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_image_default_styles().
 */
function cruise_factory_feature_image_default_styles() {
  $styles = array();

  // Exported image style: cruise_deal_image.
  $styles['cruise_deal_image'] = array(
    'label' => 'Cruise Deal Image',
    'effects' => array(
      8 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 445,
          'height' => 254,
        ),
        'weight' => 1,
      ),
      10 => array(
        'name' => 'adaptive_image',
        'data' => array(
          'resolutions' => '1200, 992, 768, 480',
          'mobile_first' => 0,
          'height' => '',
          'width' => 1382,
          'upscale' => '',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: cruise_line_logo.
  $styles['cruise_line_logo'] = array(
    'label' => 'Cruise Line Logo',
    'effects' => array(
      13 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 200,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: cruise_thumbnail.
  $styles['cruise_thumbnail'] = array(
    'label' => 'Cruise thumbnail',
    'effects' => array(
      7 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 200,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 2,
      ),
      9 => array(
        'name' => 'adaptive_image',
        'data' => array(
          'resolutions' => '1200, 992, 768, 480',
          'mobile_first' => 0,
          'height' => '',
          'width' => 1200,
          'upscale' => '',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: destinations_page_square_image.
  $styles['destinations_page_square_image'] = array(
    'label' => 'Destinations page square image',
    'effects' => array(
      11 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 220,
          'height' => 160,
        ),
        'weight' => 1,
      ),
      12 => array(
        'name' => 'adaptive_image',
        'data' => array(
          'resolutions' => '1200, 992, 768, 480',
          'mobile_first' => 1,
          'height' => '',
          'width' => 1382,
          'upscale' => '',
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function cruise_factory_feature_node_info() {
  $items = array(
    'cruise_line' => array(
      'name' => t('Cruise Line'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'cruise_type' => array(
      'name' => t('Cruise Type'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'destination' => array(
      'name' => t('Destination'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_default_search_api_index().
 */
function cruise_factory_feature_default_search_api_index() {
  $items = array();
  $items['cruises'] = entity_import('search_api_index', '{
    "name" : "Cruises",
    "machine_name" : "cruises",
    "description" : null,
    "server" : "mysql",
    "item_type" : "cruises",
    "options" : {
      "datasource" : { "bundles" : [] },
      "index_directly" : 0,
      "cron_limit" : "150",
      "fields" : {
        "field_cruise_brief_description" : { "type" : "text", "boost" : "2.0" },
        "field_cruise_description" : { "type" : "text" },
        "field_cruise_type" : { "type" : "integer", "entity_type" : "cruisetypes" },
        "field_cruiseline" : { "type" : "integer", "entity_type" : "cruiselines" },
        "field_currency_ref" : { "type" : "integer", "entity_type" : "currencies" },
        "field_destination" : { "type" : "integer", "entity_type" : "destinations" },
        "field_length" : { "type" : "integer" },
        "field_ships" : { "type" : "integer", "entity_type" : "ships" },
        "search_api_language" : { "type" : "string" },
        "search_api_viewed" : { "type" : "text", "boost" : "5.0" },
        "title" : { "type" : "text", "boost" : "3.0" }
      },
      "data_alter_callbacks" : {
        "search_api_ranges_alter" : { "status" : 1, "weight" : "-50", "settings" : [] },
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 1, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_aggregation" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : {
              "search_api_aggregation_1" : {
                "name" : "Title",
                "type" : "first_char",
                "fields" : [ "title" ],
                "description" : "A First letter aggregation of the following fields: Title."
              },
              "search_api_aggregation_2" : {
                "name" : "Cruise Type",
                "type" : "first_char",
                "fields" : [ "field_cruise_type" ],
                "description" : "A First letter aggregation of the following fields: Cruise Type."
              },
              "search_api_aggregation_3" : {
                "name" : "Cruiseline",
                "type" : "first_char",
                "fields" : [ "field_cruiseline" ],
                "description" : "A First letter aggregation of the following fields: Cruiseline."
              }
            }
          }
        },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_cruise_brief_description" : true,
              "field_cruise_description" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_cruise_brief_description" : true,
              "field_cruise_description" : true
            },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_cruise_brief_description" : true,
              "field_cruise_description" : true
            },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_cruise_brief_description" : true,
              "field_cruise_description" : true
            },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  $items['default_node_index'] = entity_import('search_api_index', '{
    "name" : "Default node index",
    "machine_name" : "default_node_index",
    "description" : "An automatically created search index for indexing node data. Might be configured to specific needs.",
    "server" : null,
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "data_alter_callbacks" : { "search_api_alter_node_access" : { "status" : 1, "weight" : "0", "settings" : [] } },
      "processors" : {
        "search_api_case_ignore" : { "status" : 1, "weight" : "0", "settings" : { "strings" : 0 } },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\nh2 = 3\\nh3 = 2\\nstrong = 2\\nb = 2\\nem = 1.5\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : { "spaces" : "[^\\\\p{L}\\\\p{N}]", "ignorable" : "[-]" }
        }
      },
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "body:value" : { "type" : "text" },
        "changed" : { "type" : "date" },
        "created" : { "type" : "date" },
        "promote" : { "type" : "boolean" },
        "search_api_language" : { "type" : "string" },
        "sticky" : { "type" : "boolean" },
        "title" : { "type" : "text", "boost" : "5.0" },
        "type" : { "type" : "string" }
      }
    },
    "enabled" : "0",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_page().
 */
function cruise_factory_feature_default_search_api_page() {
  $items = array();
  $items['search_cruises'] = entity_import('search_api_page', '{
    "index_id" : "cruises",
    "path" : "cruise\\/search",
    "name" : "Search Cruises",
    "machine_name" : "search_cruises",
    "description" : "",
    "options" : {
      "mode" : "terms",
      "fields" : {
        "field_cruise_brief_description" : "field_cruise_brief_description",
        "field_cruise_description" : "field_cruise_description",
        "search_api_viewed" : "search_api_viewed",
        "title" : "title"
      },
      "per_page" : "30",
      "result_page_search_form" : 1,
      "get_per_page" : 0,
      "view_mode" : "teaser",
      "empty_behavior" : "results"
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function cruise_factory_feature_default_search_api_server() {
  $items = array();
  $items['mysql'] = entity_import('search_api_server', '{
    "name" : "MySQL",
    "machine_name" : "mysql",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : "3",
      "partial_matches" : 1,
      "indexes" : { "cruises" : {
          "search_api_language" : {
            "table" : "search_api_db_cruises",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_cruise_type" : {
            "table" : "search_api_db_cruises",
            "column" : "field_cruise_type",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_cruiseline" : {
            "table" : "search_api_db_cruises",
            "column" : "field_cruiseline",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_currency_ref" : {
            "table" : "search_api_db_cruises",
            "column" : "field_currency_ref",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_destination" : {
            "table" : "search_api_db_cruises",
            "column" : "field_destination",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_length" : {
            "table" : "search_api_db_cruises",
            "column" : "field_length",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_ships" : {
            "table" : "search_api_db_cruises",
            "column" : "field_ships",
            "type" : "integer",
            "boost" : "1.0"
          },
          "title" : {
            "table" : "search_api_db_cruises_text",
            "type" : "text",
            "boost" : "3.0"
          },
          "field_cruise_brief_description" : {
            "table" : "search_api_db_cruises_text",
            "type" : "text",
            "boost" : "2.0"
          },
          "field_cruise_description" : {
            "table" : "search_api_db_cruises_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "search_api_viewed" : {
            "table" : "search_api_db_cruises_text",
            "type" : "text",
            "boost" : "5.0"
          }
        }
      }
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
