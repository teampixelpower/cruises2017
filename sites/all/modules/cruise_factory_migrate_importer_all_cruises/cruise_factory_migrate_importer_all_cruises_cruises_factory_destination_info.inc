<?php
  /**
   * @file
   *   Cruise Factory Migrate Importer.
   *   cruise_factory_migrate_importer_all_cruises.module
   */

	/*
	 *  cruiseline id = 14
	 */

// cruise_factory_migrate_importer_all_cruises_cruises_factory_destination_info.inc
class CruiseFactoryDestinationInfoMigration extends Migration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

		// $destination_title = $row->destination_group;
		$destination_title = $row->Title;
		// if ($destination_title == "Alaska") {
		// 	watchdog('cruises_factory_destination_info', 'destination_title: ' . $destination_title);
		// 	dpm($row);
		// }
		$final_id = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'base_destination_group' AND title = :title LIMIT 1", array(":title" => $destination_title))->fetchAll();
    $final_id = $final_id[0]->id;
		// if ($destination_title == "Alaska") {
		// 	watchdog('cruises_factory_destination_info', 'destination final_id: ' . $final_id);
		// }
    $row->id 					= $final_id;
    $row->uuid				= $final_id;
    $row->final_id 		= $final_id;
    $row->final_uuid	= $final_id;

    $row->migrate_map_sourceid1 = $final_id;
    $row->migrate_map_destid1 = $final_id;

	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryDestinationInfoMigration');



		$this->description = t('Import of Destination Extra Info.');

		$this->systemOfRecord = Migration::SOURCE;
		// $this->systemOfRecord = Migration::DESTINATION;

		$columns = array(
			0 => array('uuid',								'uuid'),
			1 => array('id',									'id'),
			2 => array('PageTitle',						'PageTitle'),
			3 => array('Title',								'Title'),
			4 => array('description',					'description'),
			5 => array('destination_photo',		'destination_photo'),
			6 => array('hide',								'hide'),
			7 => array('final_uuid',					'final_uuid'),
			8 => array('final_id',						'final_id'),

		);


		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
		$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
		$server_url = str_replace('https://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
		$server_url = str_replace('https://', '', $server_url);

		$local_path = 'http://cruises2017.local';
		$truserv_path = 'https://www.cruises.co.za';
		$truserv_staging_path = 'http://stage.cruises.co.za';
		$truserv_dev_path = 'http://dev.cruises.co.za';
		// $cruisescoza_path = 'https://www.cruises.co.za'; // 'http://197.189.205.34/
		$cruisescoza_path = 'https://www.cruises.co.za';
		$royal_path = 'https://www.royalcaribbean.co.za';
		$royal_stage_path = 'https://www.stage-royalcaribbean.co.za';
		$crystal_path =  'https://www.crystalyachtcruises.co.za';

		$file = '/sites/all/modules/cruise_factory_migrate_importer_all_cruises/import_files/destination-group-image-and-text.csv';

		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		} else if (substr($server_url, 0, 13) == 'cruises.co.za') {
			$items_url = $truserv_path . $file;
		} else if (substr($server_url, 0, 17) == 'dev.cruises.co.za') {
			$items_url = $truserv_dev_path . $file;
		} else if (substr($server_url, 0, 19) == 'stage.cruises.co.za') {
			$items_url = $truserv_staging_path . $file;
		} else if ($server_url == 'stage-royalcaribbean.co.za') {
			$items_url = $royal_stage_path . $file;
		} else if ($server_url == 'royalcaribean.co.za') {
			$items_url = $royal_path . $file;
		} else if ($server_url == 'crystalyachtcruises.co.za') {
			$items_url = $crystal_path . $file;
		} else {
			$items_url = 'http://' . $server_url . $file;
		}
		$csv_path = $items_url;
		
		$csv_path = str_replace('http://www.https://www.','https://www.',$csv_path);
		$csv_path = str_replace('http://https://www.','https://www.',$csv_path);
		$csv_path = str_replace('http://https://www.','https://www.',$csv_path);

		$csv_path = str_replace('http://http://dev.','http://dev.',$csv_path);
		$csv_path = str_replace('http://http://stage.','http://stage.',$csv_path);
		$csv_path = str_replace('http://https://dev.','http://dev.',$csv_path);
		$csv_path = str_replace('http://https://stage.','http://stage.',$csv_path);
		
		// watchdog('cruises_factory_destination_info', 'csv_path: ' . $csv_path);

		$this->source = new MigrateSourceCSV($csv_path, $columns, array('delimiter' => ',', 'header_rows' => 1, 'track_changes' => 1));

/*		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_destination_group');*/
/*		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_destination_group')
		);*/
			$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'base_destination_group');
			$this->map = new MigrateSQLMap($this->machineName,
						array(
							'final_uuid' => array(
								'type' => 'varchar',
								'length' => 255,
								'not null' => TRUE,
								'description' => 'final_uuid',
							),
						),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'base_destination_group')
			);


/*	$this->addFieldMapping("changed")														->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('type')															->defaultValue('cruise_factory_destination_group');
		$this->addFieldMapping('id',																'id')->sourceMigration('CruiseFactoryDealsStage6');
		$this->addFieldMapping('field_page_title',									'PageTitle');
		$this->addFieldMapping('field_description',									'description');
		$this->addFieldMapping('field_description:format')					->defaultValue('full_html');
		$this->addFieldMapping('field_hide_this_destination_from',	'hide');
		$this->addFieldMapping('field_destination_photo',						'destination_photo');

		$this->addFieldMapping('field_destination_photo:alt', 			'PageTitle');
		$this->addFieldMapping('field_destination_photo:title', 		'PageTitle');
		$this->addFieldMapping('field_destination_photo:file_replace')->defaultValue('FILE_EXISTS_REPLACE');*/

		///////// uuid,id,PageTitle,Title,description,destination_photo,hide

		$this->addFieldMapping('page_title',														'PageTitle');
		$this->addFieldMapping("changed")																->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("created")																->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('uuid',																	'final_uuid');
		$this->addFieldMapping('id',																		'final_id');
		$this->addFieldMapping('field_dest_group_hide',									'hide');
		// $this->addFieldMapping('title',															'title');
		$this->addFieldMapping('type')																	->defaultValue('base_destination_group');
		// $this->addFieldMapping('destination_group'										, 'destination_group');
		$this->addFieldMapping('dest_group_description'									, 'description');
		$this->addFieldMapping('dest_group_description:format')->defaultValue('full_html');
		$this->addFieldMapping('base_destination_photo'									, 'destination_photo');
		$this->addFieldMapping('base_destination_photo:alt'							, 'PageTitle');
		$this->addFieldMapping('base_destination_photo:title'						, 'PageTitle');
		$this->addFieldMapping('base_destination_photo:file_replace')		->defaultValue('FILE_EXISTS_REPLACE');
		$this->addFieldMapping('base_destination_photo:preserve_files')	->defaultValue(0);



	}
}
