#!/bin/sh
#
# This file sits on /var/www/html/cruise_factory_local_files/update.sh
# and is triggered by cron to get a local copy of the feeds so we can do fast imports
# New Idea: 31 Aug 2016: We import to temp files and then move them to the final names
# this means less time of empty files for an accident to happen where it tries to import nothing
#


now="$(date)";

echo 'stage: Importing new data from Cruise Factory: ' $now;
echo 'stage: Importing new data from Cruise Factory: ' $now >> /var/www/html/cruise_factory_local_files/logs/stage.cruise_factory_cron_import.log;

rm /var/www/html/cruise_factory_local_files/*.tmp;

# curl http://www.bookacruise.co.za/getXML.php?Table=amenities -o /var/www/html/cruise_factory_local_files/base_amenities.tmp;
# mv /var/www/html/cruise_factory_local_files/base_amenities.tmp /var/www/html/cruise_factory_local_files/base_amenities;
# curl http://www.bookacruise.co.za/getXML.php?Table=cabins -o /var/www/html/cruise_factory_local_files/base_cabins.tmp;
# mv /var/www/html/cruise_factory_local_files/base_cabins.tmp /var/www/html/cruise_factory_local_files/base_cabins;
# curl http://www.bookacruise.co.za/getXML.php?Table=cruiselines -o /var/www/html/cruise_factory_local_files/base_cruiselines.tmp;
# mv /var/www/html/cruise_factory_local_files/base_cruiselines.tmp /var/www/html/cruise_factory_local_files/base_cruiselines;
# curl http://www.bookacruise.co.za/getXML.php?Table=cruises -o /var/www/html/cruise_factory_local_files/base_cruises.tmp;
# mv /var/www/html/cruise_factory_local_files/base_cruises.tmp /var/www/html/cruise_factory_local_files/base_cruises;
# curl http://www.bookacruise.co.za/getXML.php?Table=cruisetypes -o /var/www/html/cruise_factory_local_files/base_cruisetypes.tmp;
# mv /var/www/html/cruise_factory_local_files/base_cruisetypes.tmp /var/www/html/cruise_factory_local_files/base_cruisetypes;
# curl http://www.bookacruise.co.za/getXML.php?Table=currencies -o /var/www/html/cruise_factory_local_files/base_currencies.tmp;
# mv /var/www/html/cruise_factory_local_files/base_currencies.tmp /var/www/html/cruise_factory_local_files/base_currencies;
# curl http://www.bookacruise.co.za/getXML.php?Table=deckplans -o /var/www/html/cruise_factory_local_files/base_deckplans.tmp;
# mv /var/www/html/cruise_factory_local_files/base_deckplans.tmp /var/www/html/cruise_factory_local_files/base_deckplans;
# curl http://www.bookacruise.co.za/getXML.php?Table=destinations -o /var/www/html/cruise_factory_local_files/base_destinations.tmp;
# mv /var/www/html/cruise_factory_local_files/base_destinations.tmp /var/www/html/cruise_factory_local_files/base_destinations;
# curl http://www.bookacruise.co.za/getXML.php?Table=dining -o /var/www/html/cruise_factory_local_files/base_dining.tmp;
# mv /var/www/html/cruise_factory_local_files/base_dining.tmp /var/www/html/cruise_factory_local_files/base_dining;
# curl http://www.bookacruise.co.za/getXML.php?Table=diningtimes -o /var/www/html/cruise_factory_local_files/base_diningtimes.tmp;
# mv /var/www/html/cruise_factory_local_files/base_diningtimes.tmp /var/www/html/cruise_factory_local_files/base_diningtimes;
# curl http://www.bookacruise.co.za/getXML.php?Table=facilities -o /var/www/html/cruise_factory_local_files/base_facilities.tmp;
# mv /var/www/html/cruise_factory_local_files/base_facilities.tmp /var/www/html/cruise_factory_local_files/base_facilities;
# curl http://www.bookacruise.co.za/getXML.php?Table=specialsailingdates -o /var/www/html/cruise_factory_local_files/base_specialsailingdates.tmp;
# mv /var/www/html/cruise_factory_local_files/base_specialsailingdates.tmp /var/www/html/cruise_factory_local_files/base_specialsailingdates;
# curl http://www.bookacruise.co.za/getXML.php?Table=specials -o /var/www/html/cruise_factory_local_files/base_specials.tmp;
# mv /var/www/html/cruise_factory_local_files/base_specials.tmp /var/www/html/cruise_factory_local_files/base_specials;
# curl http://www.bookacruise.co.za/getXML.php?Table=companionpricing -o /var/www/html/cruise_factory_local_files/base_companionpricing.tmp;
# mv /var/www/html/cruise_factory_local_files/base_companionpricing.tmp /var/www/html/cruise_factory_local_files/base_companionpricing;
# curl http://www.bookacruise.co.za/getXML.php?Table=latlong -o /var/www/html/cruise_factory_local_files/base_latlong.tmp;
# mv /var/www/html/cruise_factory_local_files/base_latlong.tmp /var/www/html/cruise_factory_local_files/base_latlong;
# curl http://www.bookacruise.co.za/getXML.php?Table=leadpricing -o /var/www/html/cruise_factory_local_files/base_leadpricing.tmp;
# mv /var/www/html/cruise_factory_local_files/base_leadpricing.tmp /var/www/html/cruise_factory_local_files/base_leadpricing;
# curl http://www.bookacruise.co.za/getXML.php?Table=specialsmultipricing -o /var/www/html/cruise_factory_local_files/base_specialsmultipricing.tmp;
# mv /var/www/html/cruise_factory_local_files/base_specialsmultipricing.tmp /var/www/html/cruise_factory_local_files/base_specialsmultipricing;
# curl http://www.bookacruise.co.za/getXML.php?Table=specialitineraries -o /var/www/html/cruise_factory_local_files/base_specialitineraries.tmp;
# mv /var/www/html/cruise_factory_local_files/base_specialitineraries.tmp /var/www/html/cruise_factory_local_files/base_specialitineraries;
# curl http://www.bookacruise.co.za/getXML.php?Table=specialspricing -o /var/www/html/cruise_factory_local_files/base_specialspricing.tmp;
# mv /var/www/html/cruise_factory_local_files/base_specialspricing.tmp /var/www/html/cruise_factory_local_files/base_specialspricing;
# curl http://www.bookacruise.co.za/getXML.php?Table=itineraries -o /var/www/html/cruise_factory_local_files/base_itineraries.tmp;
# mv /var/www/html/cruise_factory_local_files/base_itineraries.tmp /var/www/html/cruise_factory_local_files/base_itineraries;
# curl http://www.bookacruise.co.za/getXML.php?Table=kidsschedules -o /var/www/html/cruise_factory_local_files/base_kidsschedules.tmp;
# mv /var/www/html/cruise_factory_local_files/base_kidsschedules.tmp /var/www/html/cruise_factory_local_files/base_kidsschedules;
# curl http://www.bookacruise.co.za/getXML.php?Table=kidsprograms -o /var/www/html/cruise_factory_local_files/base_kidsprograms.tmp;
# mv /var/www/html/cruise_factory_local_files/base_kidsprograms.tmp /var/www/html/cruise_factory_local_files/base_kidsprograms;
# curl http://www.bookacruise.co.za/getXML.php?Table=menus -o /var/www/html/cruise_factory_local_files/base_menus.tmp;
# mv /var/www/html/cruise_factory_local_files/base_menus.tmp /var/www/html/cruise_factory_local_files/base_menus;
# curl http://www.bookacruise.co.za/getXML.php?Table=months -o /var/www/html/cruise_factory_local_files/base_months.tmp;
# mv /var/www/html/cruise_factory_local_files/base_months.tmp /var/www/html/cruise_factory_local_files/base_months;
# curl http://www.bookacruise.co.za/getXML.php?Table=ports -o /var/www/html/cruise_factory_local_files/base_ports.tmp;
# mv /var/www/html/cruise_factory_local_files/base_ports.tmp /var/www/html/cruise_factory_local_files/base_ports;
# curl http://www.bookacruise.co.za/getXML.php?Table=priceguide -o /var/www/html/cruise_factory_local_files/base_priceguide.tmp;
# mv /var/www/html/cruise_factory_local_files/base_priceguide.tmp /var/www/html/cruise_factory_local_files/base_priceguide;
# curl http://www.bookacruise.co.za/getXML.php?Table=sailingdates -o /var/www/html/cruise_factory_local_files/base_sailingdates.tmp;
# mv /var/www/html/cruise_factory_local_files/base_sailingdates.tmp /var/www/html/cruise_factory_local_files/base_sailingdates;
# curl http://www.bookacruise.co.za/getXML.php?Table=seasons -o /var/www/html/cruise_factory_local_files/base_seasons.tmp;
# mv /var/www/html/cruise_factory_local_files/base_seasons.tmp /var/www/html/cruise_factory_local_files/base_seasons;
# curl http://www.bookacruise.co.za/getXML.php?Table=shipphotos -o /var/www/html/cruise_factory_local_files/base_shipphotos.tmp;
# mv /var/www/html/cruise_factory_local_files/base_shipphotos.tmp /var/www/html/cruise_factory_local_files/base_shipphotos;
# curl http://www.bookacruise.co.za/getXML.php?Table=ships -o /var/www/html/cruise_factory_local_files/base_ships.tmp;
# mv /var/www/html/cruise_factory_local_files/base_ships.tmp /var/www/html/cruise_factory_local_files/base_ships;
# curl http://www.bookacruise.co.za/getXML.php?Table=starratings -o /var/www/html/cruise_factory_local_files/base_starratings.tmp;
# mv /var/www/html/cruise_factory_local_files/base_starratings.tmp /var/www/html/cruise_factory_local_files/base_starratings;
# curl http://www.bookacruise.co.za/getXML.php?Table=tipping -o /var/www/html/cruise_factory_local_files/base_tipping.tmp;
# mv /var/www/html/cruise_factory_local_files/base_tipping.tmp /var/www/html/cruise_factory_local_files/base_tipping;
# curl http://www.bookacruise.co.za/getXML.php?Table=winelists -o /var/www/html/cruise_factory_local_files/base_winelists.tmp;
# mv /var/www/html/cruise_factory_local_files/base_winelists.tmp /var/www/html/cruise_factory_local_files/base_winelists;

/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za dis -y auto_alt;

/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_amenitiesMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_cabinsMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_companionpricingMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --update     base_cruiselinesMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_cruisesMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_cruisetypesMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_currenciesMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_deckplansMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_diningMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_diningtimesMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_facilitiesMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_itinerariesMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_kidsprogramsMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_kidsschedulesMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_latlongMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_leadpricingMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_menusMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_monthsMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_portsMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_priceguideMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_sailingdatesMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_seasonsMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_shipphotosMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_shipsMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_specialitinerariesMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_specialsailingdatesMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_specialsMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_specialsmultipricingMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_specialspricingMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_starratingsMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_tippingMigration & 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_winelistsMigration & 

wait

/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_destinationGroupsMigration;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback   base_destinationsMigration;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --update   CruiseFactoryDestinationInfo;



# status update
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za ms --group=final_imports >> /var/www/html/cruise_factory_local_files/logs/cruise_factory_cron_import.log;

# import 4 segments of full import file
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za views-data-export all_new_cfe_cruise_combine views_data_export_1 /var/www/html/cruise_factory_local_files/stage.cruises-export-combine-cruises-750.tmp;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za views-data-export all_new_cfe_cruise_combine views_data_export_2 /var/www/html/cruise_factory_local_files/stage.cruises-export-combine-cruises-1500.tmp;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za views-data-export all_new_cfe_cruise_combine views_data_export_3 /var/www/html/cruise_factory_local_files/stage.cruises-export-combine-cruises-2250.tmp;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za views-data-export all_new_cfe_cruise_combine views_data_export_4 /var/www/html/cruise_factory_local_files/stage.cruises-export-combine-cruises-3000.tmp;

# combine individual imports into final single file
rm /var/www/html/cruise_factory_local_files/stage.cruises-export-combine-cruises-FINAL.tmp;
cat /var/www/html/cruise_factory_local_files/stage.cruises-export-combine-cruises-750.tmp /var/www/html/cruise_factory_local_files/stage.cruises-export-combine-cruises-1500.tmp /var/www/html/cruise_factory_local_files/stage.cruises-export-combine-cruises-2250.tmp /var/www/html/cruise_factory_local_files/stage.cruises-export-combine-cruises-3000.tmp > /var/www/html/cruise_factory_local_files/stage.cruises-export-combine-cruises-FINAL.tmp;
mv /var/www/html/cruise_factory_local_files/stage.cruises-export-combine-cruises-FINAL.tmp /var/www/html/cruise_factory_local_files/stage.cruises-export-deal-combine-cruises.csv;
chown -R www-data:www-data /var/www/html/cruise_factory_local_files;
cd /var/www/secure_html/stage.cruises.co.za;

/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback CruiseFactoryFinalCruisesMigration;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za sapi-i 20 0 200;
echo 'stage: Cruise Import done: ' $now >> /var/www/html/cruise_factory_local_files/logs/cruise_factory_cron_import.log;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za ms --group=final_imports  >> /var/www/html/cruise_factory_local_files/logs/cruise_factory_cron_import.log;


# import deals
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za views-data-export all_new_cfe_cruise_deal_combine views_data_export_1 /var/www/html/cruise_factory_local_files/stage.cruises-export-deal-combine.tmp;
mv /var/www/html/cruise_factory_local_files/stage.cruises-export-deal-combine.tmp /var/www/html/cruise_factory_local_files/stage.cruises-export-deal-combine.csv;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za mi --rollback CruiseFactoryFinalDealsMigration;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za ms CruiseFactoryFinalDealsMigration >> /var/www/html/cruise_factory_local_files/logs/stage.cruise_factory_cron_import.log;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za en -y auto_alt;

# Index and Clean Up
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za sapi-c 20;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za sapi-r 20;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za sapi-i 20 0 500;

/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za sapi-c 21;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za sapi-r 21;
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za sapi-i 21 0 900;

/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za sapi-i 0 0 900;

/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc drush          -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc theme-registry -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc menu           -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc css-js         -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc block          -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc module-list    -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc theme-list     -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc registry       -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc advagg         -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc libraries      -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc metatag        -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc token          -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc varnish        -y; 
/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za cc views					-y; 

/root/.composer/vendor/bin/drush -r /var/www/secure_html/stage.cruises.co.za image-flush --all -y;

chown -R www-data:www-data /var/www/secure_html;
