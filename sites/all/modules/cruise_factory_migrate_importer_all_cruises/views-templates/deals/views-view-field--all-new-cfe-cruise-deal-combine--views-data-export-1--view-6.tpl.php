<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php 
	$output = str_replace('<span class=""field-content"">','',$output);
	$output = str_replace('<span class="field-content">','',$output);
	$output = str_replace('<div class=""field-content"">','',$output);
  $output = str_replace('<div class="field-content">','',$output);
  $output = str_replace('</span>','',$output);
  $output = str_replace('<span>','',$output);
  $output = str_replace('</div>','',$output);
  $output = str_replace('<div>','',$output);
  $output = str_replace('<br />','',$output);
  $output = str_replace('<br>','',$output);
  $output = strip_tags($output);
	$output = str_replace("✎          ,                   ,             cruise_id          ,                   ,             base_id          ,                   ,             base_ship_id          ,                   ,             title          ,                   ,             cruise_factory_entity_type          ,                   ,             base_ship          ,                   ,             base_cruiseline          ,                   ,             base_destinations          ,                   ,             destination_group          ,                   ,             base_itineraries          ,                   ,             base_brief_description          ,                   ,             base_cruise_length          ,                   ,             base_description          ,                   ,             base_name          ,                   ,             base_photo          ,                   ,             base_start_price          ,                   ,             ship_star_rating          ,                   ,             ship_thumb          ,                   ,             embarkport          ,               ,     ,     ,           ,                   ,             ✎          ,", "", $output);
	
	$output = str_replace("	","",$output);
	$output = str_replace("%%%%","",$output);
	$output = str_replace("    ","",$output);
	print $output;
?>
