# Version 5 Notes  



## Master Importer

on cruise deal import:


`if (  (cruise_factory_entity_type == 'base_leadpricing') `
`	&&  (base_price_inside == '0.00') `
`	&&  (base_price_outside  == '0.00') `
`	&&  (base_price_balcony  == '0.00') `
`	&&  (base_price_suites  == '0.00')) { `
`		// skip this one `
`		return FALSE; `
`} `

  
  
Fields Reference:  
  
https://www.drupal.org/node/1879542  
  
`drush mr base_amenitiesMigration; drush mr base_cabinsMigration; drush mr base_companionpricingMigration; drush mr base_cruiselinesMigration; drush mr base_cruisesMigration; drush mr base_cruisetypesMigration; drush mr base_currenciesMigration; drush mr base_deckplansMigration; drush mr base_destinationsMigration; drush mr base_diningMigration; drush mr base_diningtimesMigration; drush mr base_facilitiesMigration; drush mr base_itinerariesMigration; drush mr base_kidsprogramsMigration; drush mr base_kidsschedulesMigration; drush mr base_latlongMigration; drush mr base_leadpricingMigration; drush mr base_menusMigration; drush mr base_monthsMigration; drush mr base_portsMigration; drush mr base_priceguideMigration; drush mr base_sailingdatesMigration; drush mr base_seasonsMigration; drush mr base_shipphotosMigration; drush mr base_shipsMigration; drush mr base_specialitinerariesMigration; drush mr base_specialsailingdatesMigration; drush mr base_specialsMigration; drush mr base_specialsmultipricingMigration; drush mr base_specialspricingMigration; drush mr base_starratingsMigration; drush mr base_tippingMigration; drush mr base_winelistsMigration;`  
  
  
## Create new entity:  
  
` $entity_type =  new EntityType();`  
` $entity_type->name = "eck_employee";`  
` $entity_type->label = "Employee";`  
` `  
` // Following line is taken from the eck_example.install file`  
` $entity_type->addProperty('name', 'Name', 'text');`  
` $entity_type->save();`  
  
## New Bundle:  
  
` $entity_type_bundle = new Bundle();`  
` $entity_type_bundle->name = 'eck_employee';`  
` $entity_type_bundle->entity_type = 'eck_employee';`  
` $entity_type_bundle->save();`  
  
  
  
  
## Creating Fields on Entities:  
  
http://drupal.stackexchange.com/questions/86857/add-a-field-programmatically-to-a-custom-entity  
  
  
  
  
## Image field example  
  
` function mymodule_enable() { `  
`   // Check if our field is not already created. `  
`   if (!field_info_field('my_image')) { `  
`     $field = array( `  
`       'field_name' => 'my_image', `  
`       'type' => 'image', `  
`       'locked' => TRUE, // Settings can not be changed `  
`       'settings' => array( `  
`         'no_ui' => TRUE, // Field is not visible in field UI and can only be instantiated programmatically. `  
`       ), `  
`     ); `  
`     field_create_field($field); `  
`  `  
`     // Create the instance on the bundle. `  
`     $instance = array( `  
`       'field_name' => 'my_image', `  
`       'entity_type' => 'my_entity', `  
`       'bundle' => 'my_entity_bundle', // If your entity does not have bundles, this is the same as the entity type. `  
`       'label' => 'Featured image', `  
`       'required' => TRUE, // Field must have a value. `  
`       'widget' => array( `  
`         'type' => 'image_image', `  
`       ), `  
`     ); `  
`     field_create_instance($instance); `  
`   } `  
` } `  
  
## Entity reference example  
  
` function mymodule_enable() { `  
`  `  
`   // Create an entityreference field. `  
`   $field = array( `  
`     'field_name' => 'my_entityreference', `  
`     'type' => 'entityreference', `  
`     'settings' => array( `  
`       'target_type' => 'my_other_entitytype', `  
`     ), `  
`     'cardinality' => 1, // Field may only have one value. `  
`     'translatable' => FALSE, `  
`   ); `  
`   field_create_field($field); `  
`  `  
`   $instance = array( `  
`     'field_name' => 'my_entityreference', `  
`     'entity_type' => 'my_entitytype', `  
`     'bundle' => 'my_entitytype_bundle', `  
`     'label' => 'Reference to my_other_entitytype', `  
`     'required' => TRUE, `  
`     'widget' => array( `  
`       'type' => 'options_select', `  
`     ), `  
`   ); `  
`   field_create_instance($instance); `  
` } `  
  

#### Notes on Views

##### itinerary_on_cruise_id

The order of these items is:

1. base_porderorder
2. base_day
3. base_arrive
4. base_depart
5. base_port_id




  
  
  
##### base_amenities  
  
field = base_id														===>>>								number_integer  
field = base_ship_id											===>>>								number_integer  
field = base_name													===>>>								text  
  
##### base_cabins  
  
field = base_id														===>>>								number_integer  
field = base_ship_id											===>>>								number_integer  
field = base_name													===>>>								text  
field = base_description									===>>>								text_long  
field = base_image												===>>>								text  
field = base_photo												===>>>								text  
field = base_cabin_order									===>>>								number_integer  
  
##### base_cruiselines  
  
field = base_id														===>>>								number_integer  
field = base_name													===>>>								text  
field = base_location											===>>>								text  
field = base_booking_email								===>>>								text  
field = base_brief_desc										===>>>								text  
field = base_company_bio									===>>>								text_long  
field = base_logosize											===>>>								text  
field = base_logotype											===>>>								text  
field = base_url													===>>>								text  
field = base_video_url										===>>>								text  
field = base_star_rating									===>>>								number_integer  
field = base_logodata											xxxxxx								base64 encoded binary  
  
  
##### base_cruises  
  
field = base_id														===>>>								number_integer  
field = base_cruiseline_id								===>>>								number_integer  
field = base_destination_id								===>>>								number_integer  
field = base_ship_id											===>>>								number_integer  
field = base_cruisetype_id								===>>>								number_integer  
field = base_length												===>>>								number_integer  
field = base_name													===>>>								text  
field = base_brief_description						===>>>								text  
field = base_description									===>>>								text_long  
field = base_photo												===>>>								text  
field = base_start_price									===>>>								number_decimal  
field = base_currency_id									===>>>								number_integer  
field = base_cruise_order									===>>>								number_integer  
  
##### base_cruisetypes  
  
field = base_id														===>>>								number_integer  
field = base_name													===>>>								text  
  
##### base_currencies  
  
field = base_id														===>>>								number_integer  
field = base_name													===>>>								text  
field = base_description									===>>>								text  
field = base_sign													===>>>								text  
  
##### base_deckplans  
  
field = base_id														===>>>								number_integer  
field = base_ship_id											===>>>								number_integer  
field = base_level												===>>>								text  
field = base_name													===>>>								text  
field = base_image												===>>>								text  
field = base_colorcode										===>>>								text  
  
##### base_destinations  
  
field = base_id														===>>>								number_integer  
field = base_name													===>>>								text  
field = base_description									===>>>								text_long  
field = base_image												===>>>								text  
field = base_banner												===>>>								text  
field = base_map_thumb										===>>>								text  
field = base_map_large										===>>>								text  
field = base_featured											===>>>								text  
field = base_featured_text								===>>>								text_long  
  
##### base_dining  
  
field = base_id														===>>>								number_integer  
field = base_cruiseline_id								===>>>								number_integer  
field = base_name													===>>>								text  
field = base_introduction									===>>>								text_long  
field = base_photo												===>>>								text  
  
##### base_diningtimes  
  
field = base_id														===>>>								number_integer  
field = base_cruiseline_id								===>>>								number_integer  
field = base_meal													===>>>								text  
field = base_normal_sitting								===>>>								text  
field = base_late_sitting									===>>>								text  
  
##### base_facilities  
  
field = base_id														===>>>								number_integer  
field = base_ship_id											===>>>								number_integer  
field = base_name													===>>>								text  
  
##### base_specialsailingdates  
  
field = base_id														===>>>								number_integer  
field = base_factory_id										===>>>								number_integer  
field = base_special_id										===>>>								number_integer  
field = base_sailingdate_id								===>>>								number_integer  
  
##### base_specials  
  
field = base_id														===>>>								number_integer  
field = base_cruise_id										===>>>								number_integer  
field = base_factory_id										===>>>								number_integer  
field = base_priority_id									===>>>								number_integer  
field = base_special_header								===>>>								text  
field = base_special_text									===>>>								text_long  
field = base_special_brief								===>>>								text_long  
field = base_instructions									===>>>								text_long  
field = base_booking_email								===>>>								text  
field = base_start_price									===>>>								number_decimal  
field = base_currency_id									===>>>								number_integer  
field = base_cruise_order									===>>>								number_integer  
field = base_main_special									===>>>								text  
field = base_dest_special									===>>>								text  
field = base_special_order								===>>>								number_integer  
field = base_validity_date_end						===>>> 								date_??_check_type_name  
field = base_validity_date_start					===>>> 								date_??_check_type_name  
field = base_checked											===>>>								text  
field = base_internal_notes								===>>>								text_long  
field = base_advert_code									===>>>								text  
field = base_exchange_rate								===>>>								number_decimal  
field = base_ex_rate_date									===>>> 								date_??_check_type_name  
field = base_currency_id_ref							===>>>								number_integer  
field = base_create_pdf										===>>>								text  
field = base_uploaded_pdf									===>>>								text  
field = base_withdrawn										===>>>								text  
field = base_quicksave										===>>>								text  
field = base_status												===>>>								text  
field = base_type													===>>>								text  
field = base_escorted											===>>>								text  
field = base_wedding											===>>>								text  
field = base_agentonly										===>>>								text  
field = base_special_conditions						===>>>								text_long  
field = base_specialpdf_filename					===>>>								text  
field = base_specialpdf_contents					===>>>								text_long  
field = base_seniors											===>>> 								list_boolean ("Yes, "No")  
field = base_singles											===>>> 								list_boolean ("Yes, "No")  
  
##### base_companionpricing  
  
field = base_id														===>>>								number_integer  
field = base_special_id										===>>>								number_integer  
field = base_price_inside									===>>>								number_decimal  
field = base_price_outside								===>>>								number_decimal  
field = base_price_balcony								===>>>								number_decimal  
field = base_price_suites									===>>>								number_decimal  
  
##### base_latlong  
  
field = base_id														===>>>								number_integer  
field = base_port_id											===>>>								number_integer  
field = base_lat													===>>>								number_float  
field = base_long													===>>>								number_float  
  
##### base_leadpricing  
  
field = base_id														===>>>								number_integer  
field = base_special_id										===>>>								number_integer  
field = base_price_inside									===>>>								number_decimal  
field = base_price_outside								===>>>								number_decimal  
field = base_price_balcony								===>>>								number_decimal  
field = base_price_suites									===>>>								number_decimal  
  
##### base_specialsmultipricing  
  
field = base_id														===>>>								number_integer  
field = base_special_id										===>>>								number_integer  
field = base_sailingdate									===>>> 								date_??_check_type_name  
field = base_inside												===>>>								text  
field = base_outside											===>>>								text  
field = base_balcony											===>>>								text  
field = base_suite												===>>>								text  
  
##### base_specialitineraries  
  
field = base_id														===>>>								number_integer  
field = base_special_id										===>>>								number_integer  
field = base_day													===>>>								number_integer  
field = base_activity											===>>>								text  
field = base_starttime										===>>>								text  
field = base_endtime											===>>>								text  
field = base_type													===>>>>								list_text ('pre','post')  
field = base_order												===>>>								number_integer  
  
##### base_specialspricing  
  
field = base_id														===>>>								number_integer  
field = base_factory_id										===>>>								number_integer  
field = base_special_id										===>>>								number_integer  
field = base_cruise_id										===>>>								number_integer  
field = base_cabin_id											===>>>								number_integer  
field = base_price												===>>>								number_decimal  
field = base_portcharges									===>>>								text  
field = base_currency_id									===>>>								number_integer  
  
##### base_itineraries  
  
field = base_id														===>>>								number_integer  
field = base_cruise_id										===>>>								number_integer  
field = base_day													===>>>								number_integer  
field = base_port_id											===>>>								number_integer  
field = base_arrive												===>>>								text  
field = base_depart												===>>>								text  
field = base_portorder										===>>>								number_integer  
  
##### base_kidsschedules  
  
field = base_id														===>>>								number_integer  
field = base_cruiseline_id								===>>>								number_integer  
field = base_name													===>>>								text  
field = base_description									===>>>								text_long  
  
##### base_kidsprograms  
  
field = base_id														===>>>								number_integer  
field = base_cruiseline_id								===>>>								number_integer  
field = base_name													===>>>								text  
field = base_description									===>>>								text_long  
field = base_photo												===>>>								text  
field = base_photo_order									===>>>								number_integer  
  
##### base_menus  
  
field = base_id														===>>>								number_integer  
field = base_cruiseline_id								===>>>								number_integer  
field = base_name													===>>>								text  
field = base_description									===>>>								text_long  
  
##### base_months  
  
field = base_id														===>>>								number_integer  
field = base_name													===>>>								text  
  
##### base_ports  
  
field = base_id														===>>>								number_integer  
field = base_destination_id								===>>>								number_integer  
field = base_name													===>>>								text  
field = base_description									===>>>								text_long  
field = base_photo												===>>>								text  
  
##### base_priceguide  
  
field = base_id														===>>>								number_integer  
field = base_sailing_id										===>>>								number_integer  
field = base_inside_cabin									===>>>								number_float  
field = base_outside_cabin								===>>>								number_float  
field = base_balcony											===>>>								number_float  
field = base_suite												===>>>								number_float  
field = base_exchange_rate								===>>>								number_float  
field = base_last_update									===>>> 								date_??_check_type_name  
field = base_update_name									===>>>								text  
field = base_currency											===>>>								text  
field = base_factory_id										===>>>								number_integer  
field = base_cruiseline_id								===>>>								number_integer  
  
##### base_sailingdates  
  
field = base_id														===>>>								number_integer  
field = base_cruise_id										===>>>								number_integer  
field = base_sailingdate									===>>> 								date_??_check_type_name  
field = base_embarkport_id								===>>>								number_integer  
  
##### base_seasons  
  
field = base_id														===>>>								number_integer  
field = base_destination_id								===>>>								number_integer  
field = base_start_month									===>>>								number_integer  
field = base_end_month										===>>>								number_integer  
field = base_name													===>>>								text  
  
##### base_shipphotos  
  
field = base_id														===>>>								number_integer  
field = base_ship_id											===>>>								number_integer  
field = base_name													===>>>								text  
field = base_description									===>>>								text_long  
field = base_photo												===>>>								text  
field = base_photo_order									===>>>								number_integer  
  
##### base_ships  
  
field = base_id														===>>>								number_integer  
field = base_cruiseline_id								===>>>								number_integer  
field = base_name													===>>>								text  
field = base_thumbnail										===>>>								text  
field = base_mainimage										===>>>								text  
field = base_maidenvoyage									===>>>								text  
field = base_refurbished									===>>>								text  
field = base_tonnage											===>>>								text  
field = base_length												===>>>								text  
field = base_beam													===>>>								text  
field = base_draft												===>>>								text  
field = base_speed												===>>>								text  
field = base_ship_rego										===>>>								text  
field = base_pass_capacity								===>>>								text  
field = base_pass_space										===>>>								text  
field = base_crew_size										===>>>								text  
field = base_nat_crew											===>>>								text  
field = base_nat_officers									===>>>								text  
field = base_nat_dining										===>>>								text  
field = base_description									===>>>								text_long  
field = base_star_rating									===>>>								number_integer  
field = base_cruisetype_id								===>>>								number_integer  
field = base_currency_id									===>>>								number_integer  
  
##### base_starratings  
  
field = base_id														===>>>								number_integer  
field = base_rating												===>>>								text  
field = base_order												===>>>								number_integer  
  
##### base_tipping  
  
field = base_id														===>>>								number_integer  
field = base_cruiseline_id								===>>>								number_integer  
field = base_name													===>>>								text  
field = base_description									===>>>								text_long  
  
##### base_winelists  
  
field = base_id														===>>>								number_integer  
field = base_cruiseline_id								===>>>								number_integer  
field = base_name													===>>>								text  
field = base_description									===>>>								text_long  
  
  
  
  
itinerary in deals:


		// $cruise_details
		// remove first ###
		// explode into array using %%
		// base_itineraries - explode using $$. #### means the start of a new row.

/*	1
		1
		0
		06:00 PM
		482
		####  2
		2
		08:00 AM
		05:00 PM
		1064
		####  3
		3
		08:00 AM
		06:00 PM
		1725
		####  4
		5
		09:00 AM
		overnight
		427
		####  5
		6
		0
		06:00 PM
		427
		####  6
		7
		11:00 AM
		overnight
		482
		####  7
		8
		0
		0
		482
		####*/  




  
