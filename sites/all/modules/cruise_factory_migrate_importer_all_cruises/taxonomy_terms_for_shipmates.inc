<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */


class base_destination_groups_taxonomy_migration extends Migration {
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) { return FALSE; }


		$row->point = 'POINT (' . $row->longitude . ' ' . $row->latitude. ')';

	}
	public function __construct($arguments) {
  	parent::__construct($arguments);
		$this->description = t('One of the Base Imports.');
		$this->systemOfRecord = Migration::SOURCE;

		$columns = array(
			0 => array('destination_group'		,		'destination_group'),
			1 => array('destination_title'		,		'destination_title'),
			2 => array('destination_id'				,		'destination_id'),
			3 => array('photo'								,		'photo'),
			4 => array('destinations_list'		,		'destinations_list'),
			5 => array('dest_id_list'					,		'dest_id_list'),
			6 => array('latitude'							,		'latitude'),
			7 => array('longitude'						,		'longitude'),
			8 => array('point'						,		'point'),
		);

		$destination_options = array(
      'allow_duplicate_terms' => FALSE,
    );

		// $csv_path = $base_url . '/' . drupal_get_path('module', 'cruise_factory_migrate_importer_all_cruises') . '/import_files/destination_groups_taxonomy.csv';
		$csv_path = drupal_get_path('module', 'cruise_factory_migrate_importer_all_cruises') . '/import_files/destination_groups_taxonomy.csv';

		$this->source = new MigrateSourceCSV($csv_path,
					$columns, array('delimiter' => ',', 'header_rows' => 1, 'track_changes' => 1));

		$this->destination = new MigrateDestinationTerm('base_destination_groups_taxonomy',
							 $destination_options);

		$this->map = new MigrateSQLMap($this->machineName,
					array(
						'destination_group' => array(
							'type' => 'varchar',
							'length' => 255,
							'not null' => TRUE,
							'description' => 'destination_group',
						),
					),
					MigrateDestinationTerm::getKeySchema()
		);

		// $this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);
		// $this->addFieldMapping("created")											->defaultValue(REQUEST_TIME);
		// $this->addFieldMapping('uuid',												'name');
		// $this->addFieldMapping('id',													'destination_group');
		$this->addFieldMapping('name',												'destination_group');
		// $this->addFieldMapping('description',									'point');
		// $this->addFieldMapping('field_gps',										'field_gps');
		// $this->addFieldMapping('field_gps:geo_type')					->defaultValue('point');
		// $this->addFieldMapping('field_gps:lat',								'latitude');
		// $this->addFieldMapping('field_gps:lon',								'longitude');

		$this->addFieldMapping('field_gps', 'point');
		$this->addFieldMapping('field_gps:geom', 'point');
		$this->addFieldMapping('field_gps:geo_type')->defaultValue('point');
	}

}


class base_cruiselines_taxonomy_migration extends XMLMigration {
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) { return FALSE; }
	}
	public function __construct($arguments) {
  	parent::__construct($arguments);
		$this->description = t('One of the Base Imports.');
		$this->systemOfRecord = Migration::SOURCE;
$fields = array(
'id'				                         => 				t('id'),
'name'				                       => 				t('name'),
);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
$server_url = str_replace('https://', '', $server_url);
$file = 'base_cruiselines';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}

		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);
    $destination_options = array(
      'allow_duplicate_terms' => FALSE,
    );
		$this->destination = new MigrateDestinationTerm('base_cruiselines_taxonomy',
					 $destination_options);
		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
		MigrateDestinationTerm::getKeySchema()
);
		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("created")											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('uuid',												'name')->xpath('name');
		$this->addFieldMapping('id',													'id')->xpath('id');
		$this->addFieldMapping('name',												'name')->xpath('name');
 
	}
}

class base_ships_taxonomy_migration extends XMLMigration {
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) { return FALSE; }
	}
	public function __construct($arguments) {
  	parent::__construct($arguments);
		$this->description = t('One of the Base Imports.');
		$this->systemOfRecord = Migration::SOURCE;

$fields = array(
'id'					                        => 				t('id'),
'name'					                      => 				t('name'),
);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
$server_url = str_replace('https://', '', $server_url);
$file = 'base_ships';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}

		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

$destination_options = array(
      'allow_duplicate_terms' => FALSE,
    );
		$this->destination = new MigrateDestinationTerm('base_ships_taxonomy',
					 $destination_options); // base_cruiselines_taxonomy
		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
		MigrateDestinationTerm::getKeySchema()
);
		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("created")											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('uuid',												'name')->xpath('name');
		$this->addFieldMapping('id',													'id')->xpath('id');
		$this->addFieldMapping('name',												'name')->xpath('name'); 

	}
}

class base_ports_taxonomy_migration extends XMLMigration {
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) { return FALSE; }
	}
	public function __construct($arguments) {
  	parent::__construct($arguments);
		$this->description = t('One of the Base Imports.');
		$this->systemOfRecord = Migration::SOURCE;

$fields = array(
'id'					                        => 				t('id'),
'name'					                      => 				t('name'),
);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
$server_url = str_replace('https://', '', $server_url);
$file = 'base_ports';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}

		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

$destination_options = array(
      'allow_duplicate_terms' => FALSE,
    );
		$this->destination = new MigrateDestinationTerm('base_ports_taxonomy',
					 $destination_options); // base_cruiselines_taxonomy
		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
		MigrateDestinationTerm::getKeySchema()
);
		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("created")											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('uuid',												'id')->xpath('id');
		$this->addFieldMapping('id',													'id')->xpath('id');
		$this->addFieldMapping('name',												'name')->xpath('name'); 

	}
}

