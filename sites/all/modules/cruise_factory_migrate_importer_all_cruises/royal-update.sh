#!/bin/sh
#
# This file sits on /var/www/html/cruise_factory_local_files/update.sh
# and is triggered by cron to get a local copy of the feeds so we can do fast imports
# New Idea: 31 Aug 2016: We import to temp files and then move them to the final names
# this means less time of empty files for an accident to happen where it tries to import nothing
#

now="$(date)";

echo 'Stage Royal Importing new data from Cruise Factory: ' $now;

echo 'Stage Royal Importing new data from Cruise Factory: ' $now >> /var/www/html/cruise_factory_local_files/logs/royal_factory_cron_import.log;

curl http://www.royalcaribbean.co.za/export-deal-combine -o /var/www/html/cruise_factory_local_files/royal-export-deal-combine.tmp;
mv /var/www/html/cruise_factory_local_files/royal-export-deal-combine.tmp /var/www/html/cruise_factory_local_files/royal-export-deal-combine.csv;


cd /var/www/html/royalcaribbean.co.za;

/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za dis -y auto_alt pathauto;

/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_amenitiesMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_cabinsMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_companionpricingMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_cruiselinesMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_cruisesMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_cruisetypesMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_currenciesMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_deckplansMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_destinationGroupsMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_destinationsMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_diningMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_diningtimesMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_facilitiesMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_itinerariesMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_kidsprogramsMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_kidsschedulesMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_latlongMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_leadpricingMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_menusMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_monthsMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_portsMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_priceguideMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_sailingdatesMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_seasonsMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_shipphotosMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_shipsMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_specialitinerariesMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_specialsailingdatesMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_specialsMigration;
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_specialsmultipricingMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_specialspricingMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_starratingsMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_tippingMigration; 
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback  base_winelistsMigration; 

/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za ms --group=final_imports >> /var/www/html/cruise_factory_local_files/logs/royal_factory_cron_import.log;

##############
## CRUISES
##############

curl https://www.royalcaribbean.co.za/export-cruise-combine-cruises-750 -o /var/www/html/cruise_factory_local_files/royal-export-deal-combine-cruises-750.tmp;
curl https://www.royalcaribbean.co.za/export-cruise-combine-cruises-1500 -o /var/www/html/cruise_factory_local_files/royal-export-deal-combine-cruises-1500.tmp;
curl https://www.royalcaribbean.co.za/export-cruise-combine-cruises-2250 -o /var/www/html/cruise_factory_local_files/royal-export-deal-combine-cruises-2250.tmp;
curl https://www.royalcaribbean.co.za/export-cruise-combine-cruises-3000 -o /var/www/html/cruise_factory_local_files/royal-export-deal-combine-cruises-3000.tmp;

rm /var/www/html/cruise_factory_local_files/royal-export-deal-combine-cruises-FINAL.tmp;
cat /var/www/html/cruise_factory_local_files/royal-export-deal-combine-cruises-750.tmp /var/www/html/cruise_factory_local_files/royal-export-deal-combine-cruises-1500.tmp /var/www/html/cruise_factory_local_files/royal-export-deal-combine-cruises-2250.tmp /var/www/html/cruise_factory_local_files/royal-export-deal-combine-cruises-3000.tmp >  /var/www/html/cruise_factory_local_files/royal-export-deal-combine-cruises-FINAL.tmp;
mv /var/www/html/cruise_factory_local_files/royal-export-deal-combine-cruises-FINAL.tmp /var/www/html/cruise_factory_local_files/royal-export-deal-combine-cruises.csv;


cd /var/www/html/royalcaribbean.co.za;

/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback CruiseFactoryFinalCruisesMigration;
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za sapi-r 35; /root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za sapi-i 35 0 200;

echo 'Stage Cruise Import done: ' $now >> /var/www/html/cruise_factory_local_files/logs/royal_factory_cron_import.log;
/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za ms --group=final_imports  >> /var/www/html/cruise_factory_local_files/logs/royal_factory_cron_import.log;
# cd  /var/www/html/cruise_factory_local_files/;

##############
## CRUISES END
##############


curl https://www.royalcaribbean.co.za/export-deal-combine -o /var/www/html/cruise_factory_local_files/royal-export-deal-combine.tmp;
mv /var/www/html/cruise_factory_local_files/royal-export-deal-combine.tmp /var/www/html/cruise_factory_local_files/royal-export-deal-combine.csv;

cd /var/www/html/royalcaribbean.co.za;

/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za dis -y auto_alt pathauto;

/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za mi --rollback CruiseFactoryFinalDealsMigration;

/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za ms CruiseFactoryFinalDealsMigration >> /var/www/html/cruise_factory_local_files/logs/royal_factory_cron_import.log;

/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za en -y auto_alt pathauto;

/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za sapi-r 36;

/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za sapi-i 36 0 50;

/root/.composer/vendor/bin/drush -r /var/www/html/royalcaribbean.co.za sapi-i 0 100;
