
		$destination = (String) $row->xml->id;

		if  ($destination == "7") 	{	$destgroupchosen = "Antarctica and Patagonia"; }
		if  ($destination == "52") 	{	$destgroupchosen = "Africa"; }
		if  ($destination == "2") 	{	$destgroupchosen = "Alaska"; }
		if  ($destination == "49") 	{	$destgroupchosen = "Arctic North Atlantic"; }
		if (($destination == "35")	|| ($destination == "54")) 	{	$destgroupchosen = "Asia"; }
		if  ($destination == "38") 	{	$destgroupchosen = "Atlantic Trans"; }
		if (($destination == "8")		|| ($destination == "43")) 	{	$destgroupchosen = "Australia and New Zealand"; }
		if  ($destination == "10") 	{	$destgroupchosen = "Bermuda"; }
		if  ($destination == "21") 	{	$destgroupchosen = "British Isles"; }
		if  ($destination == "11") 	{	$destgroupchosen = "Canada and New England"; }
		if  ($destination == "9") 	{	$destgroupchosen = "Caribbean"; }
		if  ($destination == "12") 	{	$destgroupchosen = "Caribbean"; }
		if  ($destination == "48") 	{	$destgroupchosen = "Central America"; }
		if (($destination == "50")	|| ($destination == "28")) 	{	$destgroupchosen = "Europe"; }
		if  ($destination == "29") 	{	$destgroupchosen = "Hawaii"; }
		if  ($destination == "22") 	{	$destgroupchosen = "Mediterranean"; }
		if  ($destination == "53") 	{	$destgroupchosen = "Mexico"; }
		if (($destination == "30")	|| ($destination == "45")) 	{	$destgroupchosen = "Middle East"; }
		if  ($destination == "32") 	{	$destgroupchosen = "North America Atlantic"; }
		if  ($destination == "34") 	{	$destgroupchosen = "North America Pacific"; }
		if (($destination == "25")	|| ($destination == "26")) 	{	$destgroupchosen = "Northern Europe"; }
		if  ($destination == "37") 	{	$destgroupchosen = "South America"; }
		if  ($destination == "40") 	{	$destgroupchosen = "World Cruises, Sectors and Transoceanic Voyages"; }

		$row->base_destination_group_ref = $destgroupchosen;
		$row->xml->base_destination_group_ref = $destgroupchosen;
