<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class ShipFactsMigration extends Migration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}
		$final_id = $row->Nameofship;
		$final_id = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'base_ships' AND uuid = :uuid", array(":uuid" => $final_id))->fetchAll();
    $final_id = $final_id[0]->id;
    $row->id 	= $final_id;
    $row->uuid 			= $row->Nameofship;

    $row->migrate_map_destid1 = $final_id;


		// multi-row processing:
		$Highlights					= strtolower($row->Highlights);
		$OtherAmenities			= strtolower($row->OtherAmenities);
		$Retail							= strtolower($row->Retail);
		$KidsTeens					= strtolower($row->KidsTeens);
		$Stateroom					= strtolower($row->Stateroom);
		$DynamicDining			= strtolower($row->DynamicDining);


		$Highlights					= ucwords($Highlights);
		$OtherAmenities			= ucwords($OtherAmenities);
		$Retail							= ucwords($Retail);
		$KidsTeens					= ucwords($KidsTeens);
		$Stateroom					= ucwords($Stateroom);
		$DynamicDining			= ucwords($DynamicDining);



		$Highlights = str_replace("\r", "|", $Highlights);
		$Highlights = str_replace("\n", "|", $Highlights);

		$OtherAmenities = str_replace("\r", "|", $OtherAmenities);
		$OtherAmenities = str_replace("\n", "|", $OtherAmenities);

		$Retail = str_replace("\r", "|", $Retail);
		$Retail = str_replace("\n", "|", $Retail);

		$KidsTeens = str_replace("\r", "|", $KidsTeens);
		$KidsTeens = str_replace("\n", "|", $KidsTeens);

		$Stateroom = str_replace("\r", "|", $Stateroom);
		$Stateroom = str_replace("\n", "|", $Stateroom);

		$DynamicDining = str_replace("\r", "|", $DynamicDining);
		$DynamicDining = str_replace("\n", "|", $DynamicDining);


		$row->Highlights 					= $Highlights;
		$row->OtherAmenities 			=	$OtherAmenities;
		$row->Retail 							= $Retail;
		$row->KidsTeens 					= $KidsTeens;
		$row->Stateroom 					= $Stateroom;
		$row->DynamicDining 			= $DynamicDining;

		// clean Numbers

		$TotalGuests							= str_replace(",", "", $row->TotalGuests);
		$TotalGuests							= str_replace(".", "", $row->TotalGuests);

		$TotalStateroom						= str_replace(",", "", $row->TotalStateroom);
		$TotalStateroom						= str_replace(".", "", $row->TotalStateroom);

		$FeetLong									= str_replace(",", "", $row->FeetLong);
		$FeetLong									= str_replace(".", "", $row->FeetLong);

		$FeetWide									= str_replace(",", "", $row->FeetWide);
		$FeetWide									= str_replace(".", "", $row->FeetWide);

		$Amountoftotaldecks				= str_replace(",", "", $row->Amountoftotaldecks);
		$Amountoftotaldecks				= str_replace(".", "", $row->Amountoftotaldecks);


		$InternationalCrew				= str_replace(",", "", $row->InternationalCrew);
		$InternationalCrew				= str_replace(".", "", $row->InternationalCrew);

		$Guestdoubleoccupancy			= str_replace(",", "", $row->Guestdoubleoccupancy);
		$Guestdoubleoccupancy			= str_replace(".", "", $row->Guestdoubleoccupancy);


		$row->TotalGuests 					= $TotalGuests;
		$row->TotalStateroom 				=	$TotalStateroom;
		$row->FeetLong 							= $FeetLong;
		$row->FeetWide 							= $FeetWide;
		$row->Amountoftotaldecks 		= $Amountoftotaldecks;
		$row->InternationalCrew 		= $InternationalCrew;
		$row->Guestdoubleoccupancy 	= $Guestdoubleoccupancy;



	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran Ship_Facts_Migration');


		$this->description = t('Import of Ship Facts.');
		// $this->softDependencies = array('Ships');
		$this->systemOfRecord = Migration::DESTINATION; // SOURCE; //

		$columns = array(
			0 => array('ship_id', 								'ship_id'),
			1 => array('Nameofship', 							'Nameofship'),
			2 => array('Guest-doubleoccupancy', 	'Guestdoubleoccupancy'),
			3 => array('TotalGuests', 						'TotalGuests'),
			4 => array('InternationalCrew', 			'InternationalCrew'),
			5 => array('Highlights', 							'Highlights'), // multi-line
			6 => array('OtherAmenities', 					'OtherAmenities'), // multi-line
			7 => array('Retail', 									'Retail'), // multi-line
			8 => array('KidsTeens', 							'KidsTeens'), // multi-line
			9 => array('TotalStateroom', 					'TotalStateroom'),
			10 => array('Stateroom', 							'Stateroom'), // multi-line
			11 => array('StateroomExtraInfo', 		'StateroomExtraInfo'),
			12 => array('FeetLong', 							'FeetLong'),
			13 => array('FeetWide', 							'FeetWide'),
			14 => array('Amountoftotaldecks', 		'Amountoftotaldecks'),
			15 => array('DynamicDining', 					'DynamicDining'), // multi-line
		);

		//The Source of the import
		global $base_url;
		$ship_facts_url = $base_url . '/' . drupal_get_path('module', 'cruise_factory_migrate_importer_all_cruises') . '/import_files/RCI-fast-facts.csv';
		// watchdog('_ship_facts', $ship_facts_url);
		// $this->source = new MigrateSourceCSV($ship_facts_url,
		$this->source = new MigrateSourceCSV($ship_facts_url,
			$columns, array('delimiter' => ',', 'header_rows' => 1, 'track_changes' => 1));

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'base_ships');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'ship_id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'ship_id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'base_ships')
		);
		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);

		$this->addFieldMapping('type')->defaultValue('base_ships');

		$this->addFieldMapping('base_id',														'ship_id');
		// $this->addFieldMapping('ship_id',											'ship_id')->sourceMigration('Ships');
		$this->addFieldMapping('uuid',													'Nameofship')->sourceMigration('base_shipsMigration');

		$this->addFieldMapping('title', 												'Nameofship');
		$this->addFieldMapping('ship_guest_doubleoccupancy', 	'Guest-doubleoccupancy');
		$this->addFieldMapping('ship_totalguests', 						'TotalGuests');
		$this->addFieldMapping('ship_internationalcrew', 			'InternationalCrew');
		$this->addFieldMapping('ship_totalstateroom', 					'TotalStateroom');
		$this->addFieldMapping('ship_stateroomextrainfo', 			'StateroomExtraInfo');
		$this->addFieldMapping('ship_feetlong', 								'FeetLong');
		$this->addFieldMapping('ship_feetwide', 								'FeetWide');
		$this->addFieldMapping('ship_amountoftotaldecks', 			'Amountoftotaldecks');

		$this->addFieldMapping('ship_highlights', 							'Highlights')->separator('|');
		$this->addFieldMapping('ship_otheramenities', 					'OtherAmenities')->separator('|');
		$this->addFieldMapping('ship_retail', 									'Retail')->separator('|');
		$this->addFieldMapping('ship_kids_teens', 							'KidsTeens')->separator('|');
		$this->addFieldMapping('ship_stateroom', 							'Stateroom')->separator('|');
		$this->addFieldMapping('ship_dynamicdining', 					'DynamicDining')->separator('|');



	}
}
