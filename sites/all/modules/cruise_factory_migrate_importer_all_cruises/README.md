# Cruise Factory Migrate Importer - All Cruiselines
Forked from cruise_factory_migrate_importer
Imports all Cruises from all cruiselines.

Created by Ash Glover for Benchmark Digital.

Importer that pulls info from Cruise Factory.


## Gotcha's on first import...

Before starting:
- set which taxonomy vocab advert_code should use on imports! 
~- in /admin/content/migrate/groups/default/CruiseFactorySpecialsLeadPricing:~
~	- field_sailing_entity_ref doesn't seem to be linking in - do this manually~

/*
Before Phase 2 import:
- when linking destination groups and destinations, these 2 don't work:
	- South Pacific & New Zealand
	- Africa - North & Middle East

Before CruiseFactorySailingDatesLink;
- make sure cruises-sailing-dates/export/cruises_sailing_dates.csv has imported on cron - it's a big file.
*/



## CHANGELOG

### FEB 2018
- January's fix doesn't seem to be working
- 15 Feb 2018 - also, jacob zuma resigned as president late last night. \m/


### JAN 2018
- Fixed a strange error finding prices for special cabin types

### MAR 2017
- ship and destination URLs were not retaining across updates. this seems to be resolved by deleting old aliases on import.

### FEB 2017
- We are at v5.1, reduced the number of views needed, as well as made the importer more consistent in creating entities with URLs that are consistent across multiple imports.
- 13 Feb: added full support for ROYAL "MINI" SITE 

### OCT 2016
- Today, the importer has uninstalled itself and is no longer working. F.M.L.


### SEP 2016
- Version 5.0. imports base tables, and then compiles them. 2 steps.
- allows imports of final deals, and final cruises
-

### AUG 2016
- Version up to 4.2 gives us this freaking multi-date, multi-sailing-date importer
- We are creating an admin page to allow selection of cruiselines. This is a Version Up level event; we are at version 4 now - 7.x-4.0-a if you wanna get picky.
- Version 4.2 - includes Royal and Crystal
- Added a fallback to handle new minisites...


### This is the order to import EVERYTHING first time round:

`drush mi --all`


// Phase 1

drush mi Cruiselines;  drush mi Ships;  drush mi CruiseFactoryDealsStage4;  drush mi CruiseFactoryDealsStage6;  drush mi Ports; drush mi CruiseFactoryDealsStage6NewLink;


// Phase 2.1
drush mi CruiseFactoryCruisesStage1;  drush mi CruiseFactoryCruisesStage2; drush mi CruiseFactorySailingDates;


wget http://dev.cruises.co.za:81/cruises-sailing-dates/export/cruises_sailing_dates.csv -O /var/www/html/cruise_factory_local_files/cruises_sailing_dates-staging.tmp
mv /var/www/html/cruise_factory_local_files/cruises_sailing_dates-staging.tmp /var/www/html/cruise_factory_local_files/cruises_sailing_dates-staging.csv


// Phase 2.2
drush mi CruiseFactorySailingDatesLink;

// Phase 2.3
drush mi CruiseFactoryCruisesStage3; drush mi CruiseFactoryCruisesStage4;


// Phase 3

drush mi CruiseFactoryDealIDs; drush mi CruiseFactoryCabins;  drush mi CruiseFactorySpecialsSailingDates;  drush mi CruiseFactorySpecialsSinglePricing;  drush mi CruiseFactoryDealsNewPrices;  drush mi CruiseFactoryDealsStage8; drush mi CruiseFactorySpecialsMultiPricing; drush mi CruiseFactorySpecialsLeadPricing;

// Phase 4

drush mi CruiseFactoryChildDeals;

// Phase 5 - not really needed mostly

drush mi CruiseFactoryChildDealsPrices;
drush mi Ships_Images;



// BIIIIIIG UPDATE LIVE CRUISES:

drush mi Cruiselines;  drush ms; drush mi Ships;  drush ms; drush mi CruiseFactoryDealsStage4;  drush ms; drush mi CruiseFactoryDealsStage6;   drush ms; drush mi CruiseFactoryDealsStage6NewLink; drush ms; drush mi CruiseFactoryCruisesStage1;  drush ms; drush mi CruiseFactoryCruisesStage2; drush ms; drush mi CruiseFactorySailingDates;   wget -q https://www.cruises.co.za:81/cruises-sailing-dates/export/cruises_sailing_dates.csv -O /var/www/html/cruise_factory_local_files/cruises_sailing_dates.tmp; mv /var/www/html/cruise_factory_local_files/cruises_sailing_dates.tmp /var/www/html/cruise_factory_local_files/cruises_sailing_dates.csv;    drush ms; drush mi CruiseFactorySailingDatesLink; drush ms; drush mi CruiseFactoryCruisesStage3; drush ms;


drush mi CruiseFactoryDealIDs; drush ms; drush mi CruiseFactoryCabins;  drush ms;    wget -q http://vitalitycruising.com/getXML.php?Table=specialsailingdates -O /var/www/html/cruise_factory_local_files/specialsailingdates.tmp; mv /var/www/html/cruise_factory_local_files/specialsailingdates.tmp /var/www/html/cruise_factory_local_files/specialsailingdates;    drush mi CruiseFactorySpecialsSailingDates;  drush ms; drush mi CruiseFactorySpecialsSinglePricing;  drush ms; drush mi CruiseFactoryDealsNewPrices;  drush ms; drush mi CruiseFactoryDealsStage8; drush ms; drush mi CruiseFactorySpecialsMultiPricing; drush ms; drush mi CruiseFactorySpecialsLeadPricing; drush ms;  wget -q https://www.cruises.co.za:81/cruise-deal-splitter/export/export.csv -O /var/www/html/cruise_factory_local_files/cruise-deal-splitter.tmp;  mv /var/www/html/cruise_factory_local_files/cruise-deal-splitter.tmp /var/www/html/cruise_factory_local_files/cruise-deal-splitter.csv;  drush mi CruiseFactoryChildDeals; drush ms;  sapi-r; drush sapi-i 12 0 5; drush sapi-i 14 0 10; drush sapi-i 8 0 10;

drush mi Ships_Images;



// BIIIIIIG UPDATE ROYAL STAGE:

drush mr --all; drush mi Cruiselines;  drush ms; drush mi Ships;  drush ms; drush mi CruiseFactoryDealsStage4;  drush ms; drush mi CruiseFactoryDealsStage6;   drush ms; drush mi CruiseFactoryDealsStage6NewLink; drush ms; drush mi Ports; drush ms; drush mi CruiseFactoryCruisesStage1;  drush ms; drush mi CruiseFactoryCruisesStage2; drush ms; drush mi CruiseFactorySailingDates;  wget -q http://www.stage-royalcaribbean.co.za/cruises-sailing-dates/export/cruises_sailing_dates.csv -O /var/www/html/cruise_factory_local_files/royalcaribbean-cruises_sailing_dates.tmp; mv /var/www/html/cruise_factory_local_files/royalcaribbean-cruises_sailing_dates.tmp /var/www/html/cruise_factory_local_files/royalcaribbean-cruises_sailing_dates.csv; drush ms; drush mi CruiseFactorySailingDatesLink; drush ms; drush mi CruiseFactoryCruisesStage3; drush ms; drush mi CruiseFactoryCruisesStage4; drush ms;  drush mi CruiseFactoryDealIDs; drush ms; drush mi CruiseFactoryCabins;  drush ms; drush mi CruiseFactorySpecialsSailingDates;  drush ms; drush mi CruiseFactorySpecialsSinglePricing;  drush ms; drush mi CruiseFactoryDealsNewPrices;  drush ms; drush mi CruiseFactoryDealsStage8; drush ms; drush mi CruiseFactorySpecialsMultiPricing; drush ms; drush mi CruiseFactorySpecialsLeadPricing; drush ms; drush mi CruiseFactoryChildDeals; drush ms; drush mi Ships_Images; drush sapi-r; drush sapi-i 30 0 5; drush sapi-i 29 0 10; drush sapi-i 0 0 100;







///////////




drush mi CruiseFactoryDealIDs; drush mi CruiseFactorySpecialsSailingDates; drush mi CruiseFactorySpecialsSinglePricing; drush mi CruiseFactorySpecialsMultiPricing; drush mi CruiseFactorySpecialsLeadPricing; drush mi CruiseFactoryDealsNewPrices; drush mi CruiseFactoryDealsStage8;
drush mi CruiseFactoryChildDeals;


drush mr CruiseFactoryDealIDs;  drush mr CruiseFactorySpecialsSailingDates;  drush mr CruiseFactorySpecialsSinglePricing;  drush mr CruiseFactorySpecialsMultiPricing;  drush mr CruiseFactorySpecialsLeadPricing;  drush mr CruiseFactoryDealsNewPrices;  drush mr CruiseFactoryDealsStage8;  drush mr CruiseFactoryChildDeals;  drush mr CruiseFactoryChildDealsPrices;



drush mr CruiseFactoryDealIDs;  drush mr CruiseFactorySpecialsSailingDates;  drush mr CruiseFactorySpecialsSinglePricing;  drush mr CruiseFactorySpecialsMultiPricing;  drush mr CruiseFactorySpecialsLeadPricing;  drush mr CruiseFactoryDealsNewPrices;  drush mr CruiseFactoryDealsStage8;  drush mr CruiseFactoryChildDeals;  drush mr CruiseFactoryChildDealsPrices;      drush mi CruiseFactoryDealIDs; drush mi CruiseFactorySpecialsSailingDates; drush mi CruiseFactorySpecialsSinglePricing; drush mi CruiseFactorySpecialsMultiPricing; drush mi CruiseFactorySpecialsLeadPricing; drush mi CruiseFactoryDealsNewPrices; drush mi CruiseFactoryDealsStage8; drush mi CruiseFactoryChildDeals; drush mi --all;




////////////////////////////
//      MISC ROUTINES     //
////////////////////////////




drush mi Cruiselines;drush mi Ships;

drush mi CruiseFactoryDealsStage4;drush mi CruiseFactoryDealsStage6;drush mi CruiseFactoryDealsStage6NewLink;
drush mi Ports;drush mi CruiseFactoryDestinationInfo;



drush mi CruiseFactoryCruisesStage1;drush mi CruiseFactoryCruisesStage2;drush mi CruiseFactorySailingDates;wget -q http://www.crystalyachtcruises.co.za:81/cruises-sailing-dates/export/cruises_sailing_dates.csv -O /var/www/html/cruise_factory_local_files/cruises_sailing_dates-crystal.tmp; mv /var/www/html/cruise_factory_local_files/cruises_sailing_dates-crystal.tmp /var/www/html/cruise_factory_local_files/cruises_sailing_dates-crystal.csv;drush mi CruiseFactorySailingDatesLink;


drush mi CruiseFactoryCruisesStage3;drush mi CruiseFactoryCruisesStage4;


drush mi CruiseFactoryDealIDs;drush mi CruiseFactoryCabins;drush mi CruiseFactorySpecialsSailingDates;

drush mi CruiseFactorySpecialsSinglePricing;drush mi CruiseFactorySpecialsMultiPricing;drush mi CruiseFactorySpecialsLeadPricing;drush mi CruiseFactoryDealsNewPrices;drush mi CruiseFactoryDealsStage8;


wget -q https://www.cruises.co.za:81/cruise-deal-splitter/export/export.csv -O /var/www/html/cruise_factory_local_files/cruise-deal-splitter.tmp; mv /var/www/html/cruise_factory_local_files/cruise-deal-splitter.tmp /var/www/html/cruise_factory_local_files/cruise-deal-splitter.csv; drush ms; drush mi CruiseFactoryChildDeals;


drush mi ShipFacts;drush mi Ships_Images;


drush mr CruiseFactoryChildDeals;drush mr CruiseFactorySpecialsSailingDates; drush mr CruiseFactorySpecialsSinglePricing; drush mr CruiseFactorySpecialsMultiPricing; drush mr CruiseFactorySpecialsLeadPricing; drush mr CruiseFactoryDealsNewPrices; drush mr CruiseFactoryDealsStage8; drush mr CruiseFactoryChildDeals;drush mi CruiseFactoryDealIDs;drush mi CruiseFactorySpecialsSailingDates; drush mi CruiseFactorySpecialsSinglePricing;drush mi CruiseFactorySpecialsMultiPricing;drush mi CruiseFactorySpecialsLeadPricing;drush mi CruiseFactoryDealsNewPrices;drush mi CruiseFactoryDealsStage8; drush ms; wget -q https://www.cruises.co.za:81/cruise-deal-splitter/export/export.csv -O /var/www/html/cruise_factory_local_files/cruise-deal-splitter.tmp; mv /var/www/html/cruise_factory_local_files/cruise-deal-splitter.tmp /var/www/html/cruise_factory_local_files/cruise-deal-splitter.csv; drush ms; drush mi CruiseFactoryChildDeals; drush ms;





drush mr CruiseFactoryCruisesStage1; drush mr CruiseFactoryCruisesStage2; drush mr CruiseFactorySailingDates; drush mr CruiseFactorySailingDatesLink; drush mr CruiseFactoryChildDeals; drush mr CruiseFactoryCruisesStage3; drush mr CruiseFactoryCruisesStage4; drush mr CruiseFactoryDealIDs; drush mr CruiseFactoryCabins; drush mr CruiseFactorySpecialsSailingDates; drush mr CruiseFactorySpecialsSinglePricing; drush mr CruiseFactorySpecialsMultiPricing; drush mr CruiseFactorySpecialsLeadPricing; drush mr CruiseFactoryDealsNewPrices; drush mr CruiseFactoryDealsStage8;     drush mi CruiseFactoryCruisesStage1;drush mi CruiseFactoryCruisesStage2;drush mi CruiseFactorySailingDates;wget -q http://www.crystalyachtcruises.co.za:81/cruises-sailing-dates/export/cruises_sailing_dates.csv -O /var/www/html/cruise_factory_local_files/cruises_sailing_dates-crystal.tmp; mv /var/www/html/cruise_factory_local_files/cruises_sailing_dates-crystal.tmp /var/www/html/cruise_factory_local_files/cruises_sailing_dates-crystal.csv;drush mi CruiseFactorySailingDatesLink;      drush ms;         drush mi CruiseFactoryCruisesStage3;drush mi CruiseFactoryCruisesStage4;      drush ms;         drush mi CruiseFactoryDealIDs;drush mi CruiseFactoryCabins;drush mi CruiseFactorySpecialsSailingDates;      drush ms;         drush mi CruiseFactorySpecialsSinglePricing;drush mi CruiseFactorySpecialsMultiPricing;drush mi CruiseFactorySpecialsLeadPricing;drush mi CruiseFactoryDealsNewPrices;drush mi CruiseFactoryDealsStage8;      drush ms;         wget -q https://www.cruises.co.za:81/cruise-deal-splitter/export/export.csv -O /var/www/html/cruise_factory_local_files/cruise-deal-splitter.tmp; mv /var/www/html/cruise_factory_local_files/cruise-deal-splitter.tmp /var/www/html/cruise_factory_local_files/cruise-deal-splitter.csv; drush ms; drush mi CruiseFactoryChildDeals;      drush ms;         drush mi ShipFacts;drush mi Ships_Images; drush sapi-r; drush sapi-i 30 0 20; drush sapi-i 29 0 100;




