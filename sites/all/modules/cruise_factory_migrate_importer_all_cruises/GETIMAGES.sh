#!/bin/sh
#

now="$(date)";
echo 'IMPORT IMAGES: ' $now >> /var/www/html/cruise_factory_local_files/logs/IMAGES.log;

Local_ships=$(wc -c < /var/www/html/cruise_factory_local_files/IMAGES/tar/ships.tar.gz)
Remote_ships=$(curl -sI http://images.cruisefactory.net/images/archive/ships.tar.gz | awk '/Content-Length/ {sub("\r",""); print $2}')
if [ $Local_ships != $Remote_ships ]; then
  rm -rf /var/www/html/cruise_factory_local_files/IMAGES/tar/ships.tar.gz;
  rm -rf /var/www/html/cruise_factory_local_files/IMAGES/ships;
  mkdir /var/www/html/cruise_factory_local_files/IMAGES/ships;
  curl http://images.cruisefactory.net/images/archive/ships.tar.gz -o /var/www/html/cruise_factory_local_files/IMAGES/tar/ships.tar.gz;
  tar xf /var/www/html/cruise_factory_local_files/IMAGES/tar/ships.tar.gz -C /var/www/html/cruise_factory_local_files/IMAGES
else
  echo "ships.tar.gz filesize unchanged." >> /var/www/html/cruise_factory_local_files/logs/IMAGES.log;
fi

Local_cruises=$(wc -c < /var/www/html/cruise_factory_local_files/IMAGES/tar/cruises.tar.gz)
Remote_cruises=$(curl -sI http://images.cruisefactory.net/images/archive/cruises.tar.gz | awk '/Content-Length/ {sub("\r",""); print $2}')
if [ $Local_cruises != $Remote_cruises ]; then
  rm -rf /var/www/html/cruise_factory_local_files/IMAGES/tar/cruises.tar.gz;
  rm -rf /var/www/html/cruise_factory_local_files/IMAGES/cruises;
  mkdir /var/www/html/cruise_factory_local_files/IMAGES/cruises;
  curl http://images.cruisefactory.net/images/archive/cruises.tar.gz -o /var/www/html/cruise_factory_local_files/IMAGES/tar/cruises.tar.gz;
  tar xf /var/www/html/cruise_factory_local_files/IMAGES/tar/cruises.tar.gz -C /var/www/html/cruise_factory_local_files/IMAGES
else
  echo "cruises.tar.gz filesize unchanged." >> /var/www/html/cruise_factory_local_files/logs/IMAGES.log;
fi


Local_destinations=$(wc -c < /var/www/html/cruise_factory_local_files/IMAGES/tar/destinations.tar.gz)
Remote_destinations=$(curl -sI http://images.cruisefactory.net/images/archive/destinations.tar.gz | awk '/Content-Length/ {sub("\r",""); print $2}')
if [ $Local_destinations != $Remote_destinations ]; then
  rm -rf /var/www/html/cruise_factory_local_files/IMAGES/tar/destinations.tar.gz;
  rm -rf /var/www/html/cruise_factory_local_files/IMAGES/destinations;
  mkdir /var/www/html/cruise_factory_local_files/IMAGES/destinations;
  curl http://images.cruisefactory.net/images/archive/destinations.tar.gz -o /var/www/html/cruise_factory_local_files/IMAGES/tar/destinations.tar.gz;
  tar xf /var/www/html/cruise_factory_local_files/IMAGES/tar/destinations.tar.gz -C /var/www/html/cruise_factory_local_files/IMAGES
else
  echo "destinations.tar.gz filesize unchanged." >> /var/www/html/cruise_factory_local_files/logs/IMAGES.log;
fi

Local_cruiselines=$(wc -c < /var/www/html/cruise_factory_local_files/IMAGES/tar/cruiselines.tar.gz)
Remote_cruiselines=$(curl -sI http://images.cruisefactory.net/images/archive/cruiselines.tar.gz | awk '/Content-Length/ {sub("\r",""); print $2}')
if [ $Local_cruiselines != $Remote_cruiselines ]; then
  rm -rf /var/www/html/cruise_factory_local_files/IMAGES/tar/cruiselines.tar.gz;
  rm -rf /var/www/html/cruise_factory_local_files/IMAGES/cruiselines;
  mkdir /var/www/html/cruise_factory_local_files/IMAGES/cruiselines;
  curl http://images.cruisefactory.net/images/archive/cruiselines.tar.gz -o /var/www/html/cruise_factory_local_files/IMAGES/tar/cruiselines.tar.gz;
  tar xf /var/www/html/cruise_factory_local_files/IMAGES/tar/cruiselines.tar.gz -C /var/www/html/cruise_factory_local_files/IMAGES
else
  echo "cruiselines.tar.gz filesize unchanged." >> /var/www/html/cruise_factory_local_files/logs/IMAGES.log;
fi

Local_ports=$(wc -c < /var/www/html/cruise_factory_local_files/IMAGES/tar/ports.tar.gz)
Remote_ports=$(curl -sI http://images.cruisefactory.net/images/archive/ports.tar.gz | awk '/Content-Length/ {sub("\r",""); print $2}')
if [ $Local_ports != $Remote_ports ]; then
  rm -rf /var/www/html/cruise_factory_local_files/IMAGES/tar/ports.tar.gz;
  rm -rf /var/www/html/cruise_factory_local_files/IMAGES/ports;
  mkdir /var/www/html/cruise_factory_local_files/IMAGES/ports;
  curl http://images.cruisefactory.net/images/archive/ports.tar.gz -o /var/www/html/cruise_factory_local_files/IMAGES/tar/ports.tar.gz;
  tar xf /var/www/html/cruise_factory_local_files/IMAGES/tar/ports.tar.gz -C /var/www/html/cruise_factory_local_files/IMAGES
else
  echo "ports.tar.gz filesize unchanged." >> /var/www/html/cruise_factory_local_files/logs/IMAGES.log;
fi

chown -R www-data:www-data /var/www/html/cruise_factory_local_files/IMAGES/

echo 'IMPORT tarball downloads complete: ' $now >> /var/www/html/cruise_factory_local_files/logs/IMAGES.log;




