<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class ShipImagesMigration extends Migration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

		// $finalurl_ImageFile = str_replace("https://www.cruises.co.za/sites/default/files/", "https://www.cruises.co.za/sites/default/files/styles/1500_wide/public/", $row->ImageFile);
		// $finalurl_ImageFile = $finalurl_ImageFile . "?itok=a-o12LwZ";
		// $row->ImageFile = $finalurl_ImageFile;

		$ImageFile = $row->ImageFile;
		if (preg_match('/.pdf/', $ImageFile)) {
			//watchdog('cruise_factory_migrate_importer_all_cruises_ship_images', 'image was a PDF! ' . $ImageFile);
			// return FALSE;
		}



				// Ship
		$ship_id = $row->Ship;
//		dpm($ship_id);
		$ship_id_final = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_ships' AND title = :title", array(":title" => $ship_id))->fetchAll();
    $ship_id 				= $ship_id_final[0]->id;
//		dpm($ship_id);
		$row->id 				=	$ship_id;
		$row->migrate_map_destid1 = $ship_id;


		$cruiseline_id = $row->Cruiseline_ID;
		$selected_cruiselines = variable_get('cruise_factory_migrate__selected_cruiseline_array');
	  if ($selected_cruiselines) {
		  // $selected_cruiselines_name = variable_get('cruise_factory_migrate__selected_cruiselines');
			if (!in_array($cruiseline_id, $selected_cruiselines)) {
				// dpm("we didn't chose: " . $cruiseline_id);
				// watchdog('cruise_factory__cruiselines', "we didn't chose: " . $cruiseline_id);
				return FALSE;
			}
	  }


	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran ShipImagesMigration');


		$this->description = t('Import of Ship Images.');
		$this->systemOfRecord = Migration::DESTINATION;

		// $this->softDependencies = array('Ships');
		// $this->softDependencies = array('CruiseFactoryChildDealsPrices');

		$columns = array(
			0 => array('Ship',									'Ship'),
			1 => array('CruiseLine',						'CruiseLine'),
			2 => array('id',										'id'),
			3 => array('ImageFile',							'ImageFile'),
			4 => array('CruiseLineID',					'CruiseLineID'),
			5 => array('Cruiseline_ID',					'Cruiseline_ID'),
		);

		$this->source = new MigrateSourceCSV('https://www.cruises.co.za/cruise-ship-hi-res-image-export-all/export.csv',
			$columns, array('delimiter' => ',', 'header_rows' => 1, 'track_changes' => 1));

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_ships');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'ImageFile' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'ImageFile',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_ships')
		);

		$this->addFieldMapping('type')->defaultValue('cruise_factory_ships');
		$this->addFieldMapping("changed")->			 	defaultValue(REQUEST_TIME);

		$this->addFieldMapping('id',												'id'); // ->sourceMigration('Ships');
		// $this->addFieldMapping('field_id',									'id')->sourceMigration('Ships');
		// $this->addFieldMapping('uuid',											'id')->sourceMigration('Ships');

		$this->addFieldMapping('title',											'Ship');
		$this->addFieldMapping('field_cruiseline_id_new',		'Cruiseline_ID')->sourceMigration('Cruiselines');
		// image fields extra info
		$this->addFieldMapping('field_main_image',					'ImageFile');
		$this->addFieldMapping('field_main_image:alt', 			'Ship');
		$this->addFieldMapping('field_main_image:title', 		'Ship');
		$this->addFieldMapping('field_main_image:file_replace')->defaultValue('FILE_EXISTS_REPLACE');


	}
}
