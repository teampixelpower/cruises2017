<?php
  /**
   * @file
   *   Cruise Factory Migrate Importer.
   *   cruise_factory_migrate_importer_all_cruises.module
   */

	/*
	 *  cruiseline id = 14
	 */


class CruiseFactoryDealsStage4Migration extends XMLMigration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

		// if ($row->name == "South Pacific & New Zealand") {
		// 	watchdog('cruise_factory__stage4-dests', 'We have South Pacific & New Zealand. row id is : ' . $row->id);
		// 	$row->uuid 	= '43';
		// 	$row->id 		= '43';
		// }

		// if ($row->name == "Africa - North & Middle East") {
		// 	watchdog('cruise_factory__stage4-dests', 'We have Africa - North & Middle East. row id is : ' . $row->id);
		// 	$row->uuid 	= '45';
		// 	$row->id 		= '45';
		// }


		// $finalurl_image = "http://images.cruisefactory.net/images/destinations/image/" . (String) $row->xml->image;
		$finalurl_image = "
http://images.cruisefactory.net/images/destinations/image/" . (String) $row->xml->image;
		// $finalurl_image = "/Users/ash/Desktop/image_archives/destinations/image/" . (String) $row->xml->image;
		$row->xml->image = $finalurl_image;

watchdog('cruise_factory__stage4-dests', (String)$row->xml->name . ' is name. ' . (String)$row->xml->id . ' is ID.');
			// $destination_title = (String) $row->xml->name;
			// $destination_id = (String) $row->xml->id;
			// $final_id = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE uuid = :uuid LIMIT 1", array(":uuid" => $destination_id))->fetchAll();
	  //   $final_id = $final_id[0]->id;


  //   $dest_group_id = $destination_id . '0' . $destination_id;
		// $final_id_dest_group = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE uuid = :uuid LIMIT 1", array(":uuid" => $dest_group_id))->fetchAll();
  //   $final_id_dest_group = $final_id_dest_group[0]->id;

		$final_id = (String)$row->xml->id;

    $row->id 		= $final_id;
    $row->uuid	= $final_id;



    // $row->destination_group	= $final_id_dest_group;
    // $row->xml->destination_group	= $final_id_dest_group;

    $row->xml->migrate_map_sourceid1 = $final_id;
    $row->migrate_map_sourceid1 = $final_id;
    // $row->migrate_map_destid1 = $final_id;

  }

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryDealsStage4Migration');

		$this->description = t('Import of Destinations.');

		$this->systemOfRecord = Migration::SOURCE;
		// $this->systemOfRecord = Migration::DESTINATION;

		$fields = array(
			'id'									=>			t('id'),
			'name'								=>			t('name'),
			'description'					=>			t('description'),
			'image'								=>			t('image'),
		);

		global $base_url;
$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/'; // 'http://197.189.205.34/cruise_factory_local_files/';

		$file = 'destinations';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}

		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity', 'cruise_factory_destination');

		$this->softDependencies = array('Ships');
		// $this->softDependencies = array('CruiseFactoryDealsStage3');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_destination')
		);

		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);

		$this->addFieldMapping('type')												->defaultValue('cruise_factory_destination');

		$this->addFieldMapping('id',														'id')->xpath('id');
		$this->addFieldMapping('uuid',													'id')->xpath('id');
		$this->addFieldMapping('title',													'name')->xpath('name');
		$this->addFieldMapping('field_page_title',							'name')->xpath('name');
		$this->addFieldMapping('field_destination_description',	'description')->xpath('description');

		$this->addFieldMapping('field_destination_photo',				'image')->xpath('image');
		$this->addFieldMapping('field_destination_photo:alt', 	'name')->xpath('name');
		$this->addFieldMapping('field_destination_photo:title', 'name')->xpath('name');
		$this->addFieldMapping('field_destination_photo:file_replace')->defaultValue('FILE_EXISTS_REPLACE');



	}

}
