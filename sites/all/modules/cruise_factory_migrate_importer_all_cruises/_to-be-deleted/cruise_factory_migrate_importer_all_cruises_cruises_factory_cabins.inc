<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryCabinsMigration extends XMLMigration {

	public function preImport() {
    parent::preImport();
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}
	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		$this->description = t('Import of Sailing Dates.');
		$this->systemOfRecord = Migration::SOURCE;

		$fields = array(
			'id'						=>	t('id'),
			'name'					=>	t('name'),
		);

		$this->softDependencies = array('CruiseFactoryDealIDs');

		global $base_url;
$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/';
		$file = 'cabins';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		} else if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}
		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_cabin');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_cabin')
		);

		$this->addFieldMapping("changed")									->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("created")									->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('type')										->defaultValue('cruise_factory_cabin');
		$this->addFieldMapping('uuid',									 	'id')					->xpath('id');
		$this->addFieldMapping('id',									 		'id')					->xpath('id');
		$this->addFieldMapping('field_id',							 	'id')					->xpath('id');

		$this->addFieldMapping('field_cabin_name',		 		'name')				->xpath('name');
		$this->addFieldMapping('title',		 								'name')				->xpath('name');
		$this->addFieldMapping('field_cabin_name‎',		 		'name')				->xpath('name');

	}
}
