<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryCruisesStage2Migration extends XMLMigration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

		$cruise_id = (String) $row->xml->cruise_id;
		$final_id = db_query("SELECT entity_id FROM {field_data_field_cruise_id_new} WHERE field_cruise_id_new_value = :field_cruise_id_new_value", array(":field_cruise_id_new_value" => $cruise_id))->fetchAll();
    $final_id = $final_id[0]->entity_id;
    $row->xml->migrate_map_destid1 = $final_id;
    $row->migrate_map_destid1 = $final_id;
    $row->xml->cruise_id = $cruise_id;
    $row->cruise_id = $cruise_id;

    if (!$final_id) {
    	watchdog('cruise_factory_migrate__stage2', 'final_id failed.');
//			return FALSE;
		}

		// first get cruise ID, then cruiseline ID. Cruise ID:
		// $cruise_id = entity ID
/*

		// cruiseline_id
    $cruiseline_uuid 				= (String) $row->xml->cruiseline_id; // = '14';
    $cruiseline_id 					= db_query("SELECT entity_id FROM {field_data_field_id} WHERE field_id_value = :field_id_value", array(":field_id_value" => $cruiseline_uuid))->fetchAll();
    $cruiseline_id 				= $cruiseline_id[0]->entity_id; // = e.g. 19270


		// cruise entity id based on cruiseline
    $cruise_id_uuid 				= (String) $row->xml->cruiseline_id; // = '14';
    $cruise_id_id 					= db_query("SELECT field_cruise_line_id_target_id FROM {field_data_field_cruise_line_id} WHERE field_cruise_line_id_target_id = :field_cruise_line_id_target_id", array(":field_cruise_line_id_target_id" => $cruise_id))->fetchAll();
    $cruiseline_id_on_cruise_id 					= $cruise_id_id[0]->field_cruise_line_id_target_id; // = e.g. 19270

    // $cruiseline_id_on_cruise_id = $cruiseline_id
    if ($cruiseline_id_on_cruise_id != $cruiseline_id) {
    	watchdog('cruise_factory_migrate__cruises_stage2', 'cruiseline_id_on_cruise_id: ' . $cruiseline_id_on_cruise_id . ' and cruiseline_id: ' . $cruiseline_id);
//			return FALSE;
		}


    $cruiseline_final_id = db_query("SELECT id, title, uuid FROM {eck_cruise_factory_entity} WHERE id = :cruiseline_id", array(":cruiseline_id" => $cruiseline_id))->fetchAll();
    $cruiseline_title 				= $cruiseline_final_id[0]->title;
    $cruiseline_final_id 			= $cruiseline_final_id[0]->id;
    $cruiseline_uuid 					= $cruiseline_final_id[0]->uuid;
    // $row->xml->cruiseline_id 	= $cruiseline_field_id;
    // $row->xml->cruiseline_uuid 	= $cruiseline_uuid;
    $row->xml->cruiseline_id 	= $cruiseline_title;
		$cruiseline_id = (String) $row->xml->cruiseline_id;

		$selected_cruiselines = variable_get('cruise_factory_migrate__selected_cruiseline_array');
	  if ($selected_cruiselines) {
		  // $selected_cruiselines_name = variable_get('cruise_factory_migrate__selected_cruiselines');
			if (!in_array($cruiseline_id, $selected_cruiselines)) {
				// dpm("we didn't chose: " . $cruiseline_id);
				watchdog('cruise_factory__cruiselines', "we didn't chose: " . $cruiseline_id);
				return FALSE;
			}
	  }
	*/

	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryCruisesStage2Migration');

		$this->description = t('Import of Cruises - sailing dates.');

		$this->systemOfRecord = Migration::DESTINATION;
		// $this->systemOfRecord = Migration::SOURCE;

		$fields = array(
			'id'										=>					t('id'),
			'cruise_id'							=>					t('cruise_id'),
			'sailingdate'						=>					t('sailingdate'),
			'embarkport_id'					=>					t('embarkport_id'),
		);


		$this->softDependencies = array('CruiseFactoryCruisesStage1');

		global $base_url;
$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/'; // 'http://197.189.205.34/cruise_factory_local_files/';

		$file = 'sailingdates';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}

		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_cruises');
//
		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'cruise_id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'cruise_id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_cruises')
		);


		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('type')												->defaultValue('cruise_factory_cruises');
		$this->addFieldMapping('field_sailing_id_new', 				'id')										->xpath('id');
		$this->addFieldMapping('field_sailing_date',					'sailingdate')					->xpath('sailingdate');
		$this->addFieldMapping('field_sailing_date:timezone')				->defaultvalue('Africa/Johannesburg');
		$this->addFieldMapping('field_sailing_date:to',					'sailingdate')					->xpath('sailingdate');

	}
}
