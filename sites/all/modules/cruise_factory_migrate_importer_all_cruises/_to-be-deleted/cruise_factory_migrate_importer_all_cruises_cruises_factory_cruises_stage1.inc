<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryCruisesStage1Migration extends XMLMigration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

		$entity_id = (String) $row->xml->id;

    $row->xml->migrate_map_sourceid1 		= $entity_id;
    $row->migrate_map_sourceid1 				= $entity_id;

    // $row->xml->migrate_map_destid1 	= $entity_id;
    // $row->migrate_map_destid1 				= $entity_id;


		$row->xml->id 											= $entity_id;

		$cruise_length_with_nights‎ 					= (String) $row->xml->length;
		if ($length_with_nights == '1') {
			$length_with_nights 							= '1 night';
		} else {
			$length_with_nights 							= (String) $row->xml->length . ' nights';
		}
		$row->xml->length_with_nights 			= $length_with_nights;


		// destination uuid
		$destination_id = (String) $row->xml->destination_id;
		$destination_final_id = db_query("SELECT id, title FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_destination' AND uuid = :uuid", array(":uuid" => $destination_id))->fetchAll();
    $destination_title 					= $destination_final_id[0]->title;
    $destination_id 						= $destination_final_id[0]->id;
		$row->xml->destination_id 	=	$destination_id;



		// cruiseline_id
/*    $cruiseline_uuid 				= (String) $row->xml->cruiseline_id; // = '14';
    $cruiseline_id 					= db_query("SELECT entity_id FROM {field_data_field_id} WHERE field_id_value = :field_id_value", array(":field_id_value" => $cruiseline_uuid))->fetchAll();
    $cruiseline_id 				= $cruiseline_id[0]->entity_id; // = e.g. 19270

    $cruiseline_final_id = db_query("SELECT id, title, uuid FROM {eck_cruise_factory_entity} WHERE id = :cruiseline_id", array(":cruiseline_id" => $cruiseline_id))->fetchAll();
    $cruiseline_title 				= $cruiseline_final_id[0]->title;
    $cruiseline_final_id 			= $cruiseline_final_id[0]->id;
    $cruiseline_uuid 					= $cruiseline_final_id[0]->uuid;
*/    // $row->xml->cruiseline_id 	= $cruiseline_field_id;
    // $row->xml->cruiseline_uuid 	= $cruiseline_uuid;
    // $row->xml->cruiseline_id 	= $cruiseline_title;

		$cruiseline_id = (String) $row->xml->cruiseline_id;
		// $cruiseline_id = (String) $row->xml->id;
		$selected_cruiselines = variable_get('cruise_factory_migrate__selected_cruiseline_array');
	  if ($selected_cruiselines) {
		  // $selected_cruiselines_name = variable_get('cruise_factory_migrate__selected_cruiselines');
			if (!in_array($cruiseline_id, $selected_cruiselines)) {
				// dpm("we didn't chose: " . $cruiseline_id);
				watchdog('cruise_factory__stage1', "we didn't chose: " . $cruiseline_id);
				return FALSE;
			}
	  }

	}

	public function __construct($arguments) {
  	parent::__construct($arguments);


		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryCruisesStage1Migration');

		$this->description = t('Import of Cruises.');

		// $this->systemOfRecord = Migration::DESTINATION;
		$this->systemOfRecord = Migration::SOURCE;

		$fields = array(
			'id'										=>					t('id'),
			'cruiseline_id'					=>					t('cruiseline_id'),
			'destination_id'				=>					t('destination_id'),
			'ship_id'								=>					t('ship_id'),
			'cruisetype_id'					=>					t('cruisetype_id'),
			'length'								=>					t('length'),
			'length_with_nights'		=>					t('length_with_nights'),
			'name'									=>					t('name'),
			'brief_description'			=>					t('brief_description'),
			'description'						=>					t('description'),
			'photo'									=>					t('photo'),
			'start_price'						=>					t('start_price'),
			'currency_id'						=>					t('currency_id'),
			'cruise_order'					=>					t('cruise_order'),
		);

		// $this->softDependencies = array('Ships');
		// $this->softDependencies = array('Destinations');
		// $this->softDependencies = array('CruiseFactoryDealsStage6');
		$this->softDependencies = array('Ports');

		global $base_url;
$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/'; // 'http://197.189.205.34/cruise_factory_local_files/';

		$file = 'cruises';
		$items_url = '';		// watchdog('cruise_factory__cruises', 'cruises url: ' . substr($server_url, 0, 5));
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}

		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_cruises');
//
		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_cruises')
		);

		$this->addFieldMapping('type')																												->defaultValue('cruise_factory_cruises');
		$this->addFieldMapping('uuid',		 														'id')										->xpath('id');
		$this->addFieldMapping('id', 																	'id')										->xpath('id');

		$this->addFieldMapping("created")															->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("changed")															->defaultValue(REQUEST_TIME);


		$this->addFieldMapping('field_cruise_id_new‎', 								'id')										->xpath('id');
		$this->addFieldMapping('field_cruise_id‎',			 								'id')										->xpath('id');

		$this->addFieldMapping('title',																'brief_description')		->xpath('brief_description');
		$this->addFieldMapping('field_cruise_description_new',				'description')					->xpath('description');
		$this->addFieldMapping('field_cruise_name',										'name')									->xpath('name');
		$this->addFieldMapping('field_cruise_line_id',								'cruiseline_id')				->xpath('cruiseline_id')	->sourceMigration('Cruiselines');
		$this->addFieldMapping('field_destination_specific',					'destination_id')				->xpath('destination_id');
		$this->addFieldMapping('field_destination_specific_code',			'destination_id')				->xpath('destination_id');
		// $this->addFieldMapping('field_destination_specific_code',	'destination_id')				->xpath('destination_id')	->sourceMigration('Destinations');
		$this->addFieldMapping('field_ship',													'ship_id')							->xpath('ship_id')				->sourceMigration('Ships');
		$this->addFieldMapping('field_cruise_days',										'length')								->xpath('length');
		$this->addFieldMapping('field_cruise_length_with_nights',			'length_with_nights')		->xpath('length_with_nights');
		$this->addFieldMapping('field_cruise_brief_description',			'brief_description')		->xpath('brief_description');



	}
}
