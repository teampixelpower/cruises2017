<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactorySailingDatesLinkMigration extends Migration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

    // $row->migrate_map_sourceid1 = $row->id;
    $row->migrate_map_destid1 = $row->id;
		$sailing_dates = $row->sailing_dates;
		$sailing_dates = preg_replace('/\s+/', '||', $sailing_dates);
		$sailing_dates = str_replace('||||', '||', $sailing_dates);

		watchdog('cruise_factory_migrate__sailing_dates_link', $sailing_dates);

		$row->sailing_dates = $sailing_dates;
	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryCruisesStage2Migration');

		$this->description = t('Linking of Sailing Dates to Cruises.');

		$this->systemOfRecord = Migration::DESTINATION;
		// $this->systemOfRecord = Migration::SOURCE;

		$columns = array(
			0 => array('id', 'id'),
			1 => array('sailing_dates', 'sailing_dates'),
		);

		$this->softDependencies = array('CruiseFactorySailingDates');

		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
$server_url = str_replace('https://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$truserv_staging_path = '/var/www/html/cruise_factory_local_files/cruises_sailing_dates-staging.csv';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/';

		$royal_path = '/var/www/html/cruise_factory_local_files/cruises_sailing_dates-royal.csv';
		$royal_stage_path = '/var/www/html/cruise_factory_local_files/cruises_sailing_dates-royal-staging.csv';
		$crystal_path =  '/var/www/html/cruise_factory_local_files/cruises_sailing_dates-crystal.csv';



		$file = 'cruises_sailing_dates.csv';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) {
			$items_url = $truserv_path . $file;
		} else if (substr($server_url, 0, 19) == 'stage.cruises.co.za') {
			$items_url = $truserv_staging_path;
		} else if ($server_url == 'stage-royalcaribbean.co.za') {
			$items_url = $royal_stage_path;
		} else if ($server_url == 'royalcaribbean.co.za') {
			$items_url = $royal_path;
		} else if ($server_url == 'crystalyachtcruises.co.za') {
			$items_url = $crystal_path;
		} else {
			$items_url = 'http://' . $server_url . '/cruises-sailing-dates/export/export.csv';
		}

		$csv_path = $items_url;

		$this->source = new MigrateSourceCSV($csv_path,
			$columns, array('delimiter' => ',', 'header_rows' => 1, 'track_changes' => 1));

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_cruises');
//
		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_cruises')
		);

		$this->addFieldMapping("changed")													->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('type')														->defaultValue('cruise_factory_cruises');
		$this->addFieldMapping('id',									 						'id')->sourceMigration('CruiseFactoryCruisesStage1');
		// $this->addFieldMapping('uuid',									 					'id')->sourceMigration('CruiseFactoryCruisesStage1');
		$this->addFieldMapping('field_sailing_date',							'sailing_dates')->separator('||');
		$this->addFieldMapping('field_sailing_date:to',						'sailing_dates')->separator('||');
		$this->addFieldMapping('field_sailing_date:timezone')			->defaultvalue('Africa/Johannesburg');

	}
}
