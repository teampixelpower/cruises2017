</<?php 


// base_cabins
$base_image = "
http://images.cruisefactory.net/images/ships/cabins/" . (String) $row->xml->image;
$row->xml->image = $base_image;
$base_photo = "
http://images.cruisefactory.net/images/ships/cabinphotos/" . (String) $row->xml->photo;
$row->xml->photo = $base_photo;


// base_cruises
$base_photo = "
http://images.cruisefactory.net/images/cruises/" . (String) $row->xml->photo;
$row->xml->photo = $base_photo;


// base_deckplans
$base_image = "
http://images.cruisefactory.net/images/ships/deckplans/" . (String) $row->xml->image;
$row->xml->image = $base_image;

$base_colorcode = "
http://images.cruisefactory.net/images/ships/deck_colorcode/" . (String) $row->xml->colorcode;
$row->xml->colorcode = $base_colorcode;


// base_destinations
$base_image = "
http://images.cruisefactory.net/images/destinations/image/" . (String) $row->xml->image;
$row->xml->image = $base_image;
$base_banner = "
http://images.cruisefactory.net/images/destinations/banners/" . (String) $row->xml->banner;
$row->xml->banner = $base_banner;
$base_map_thumb = "
http://images.cruisefactory.net/images/xxxxx/map_thumb/" . (String) $row->xml->map_thumb;
$row->xml->map_thumb = $base_map_thumb;
$base_map_large = "
http://images.cruisefactory.net/images/xxxxx/map_large/" . (String) $row->xml->map_large;
$row->xml->map_large = $base_map_large;


// base_dining
$base_photo = "
http://images.cruisefactory.net/images/cruiselines/dining/" . (String) $row->xml->photo;
$row->xml->photo = $base_photo;


// base_kidsprograms
$base_photo = "
http://images.cruisefactory.net/images/cruiselines/kidsprograms/" . (String) $row->xml->photo;
$row->xml->photo = $base_photo;


// base_ports
$base_photo = "
http://images.cruisefactory.net/images/ports/" . (String) $row->xml->photo;
$row->xml->photo = $base_photo;


// base_shipphotos
$base_photo = "
http://images.cruisefactory.net/images/ships/photos/" . (String) $row->xml->photo;
$row->xml->photo = $base_photo;


// base_ships
$base_thumbnail = "
http://images.cruisefactory.net/images/ships/thumbnails/" . (String) $row->xml->thumbnail;
$row->xml->thumbnail = $base_thumbnail;
$base_mainimage = "
http://images.cruisefactory.net/images/ships/largeimages/" . (String) $row->xml->mainimage;
$row->xml->mainimage = $base_mainimage;
