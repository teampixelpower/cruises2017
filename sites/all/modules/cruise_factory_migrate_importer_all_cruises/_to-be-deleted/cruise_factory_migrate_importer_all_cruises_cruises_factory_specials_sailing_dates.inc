<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactorySpecialsSailingDatesMigration extends XMLMigration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

/*	$cruise_id = (String) $row->xml->cruise_id;
		$final_id = db_query("SELECT * FROM {field_data_field_cruise_id_new} WHERE field_cruise_id_new_value = :field_cruise_id_new_value", array(":field_cruise_id_new_value" => $cruise_id))->fetchAll();
    $final_entity_id = $final_id[0]->entity_id;
*/

// migrate_map_cruisefactorycruisesstage1 - sourceid1 = 60485 - destid1 = 3106
		$special_sailing_id = (String) $row->xml->id;

    $row->xml->migrate_map_sourceid1 	= $special_sailing_id;
    $row->migrate_map_sourceid1 			= $special_sailing_id;

		$sailingdate_id = (String) $row->xml->sailingdate_id;
		$special_id = (String) $row->xml->special_id;


		$sailingdate_final_id = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_sailing_dates' AND uuid = :uuid", array(":uuid" => $sailingdate_id))->fetchAll();
    $sailingdate_final_id 						= $sailingdate_final_id[0]->id;


		$special_final_id = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_cruise_deal' AND uuid = :uuid", array(":uuid" => $special_id))->fetchAll();
    $special_final_id 						= $special_final_id[0]->id;

		$sailingdate = db_query("SELECT field_sailing_date_value FROM {field_data_field_sailing_date} WHERE entity_id = :entity_id", array(":entity_id" => $sailingdate_final_id))->fetchAll();
	  $sailingdate = $sailingdate[0]->field_sailing_date_value;
	  $sailingdate_clean = str_replace(" 00:00:00", "", $sailingdate);


	  $row->sailingdate_entity_ref = $sailingdate_final_id;
	  $row->special_id_entity_ref = $special_final_id;

	  $row->sailingdate = $sailingdate;
		$row->cruise_name =  $sailingdate_clean . ' for Special #' . $special_id;

	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryCruisesStage2Migration');

		$this->description = t('Import of Sailing Dates.');

		// $this->systemOfRecord = Migration::DESTINATION;
		$this->systemOfRecord = Migration::SOURCE;

		$fields = array(
			'id'										=>					t('id'),
			'factory_id'						=>					t('factory_id'),
			'special_id'						=>					t('special_id'),
			'sailingdate_id'				=>					t('sailingdate_id'),
			'sailingdate'						=>					t('sailingdate'), // this doesn't exist, link it
			'cruise_name'						=>					t('cruise_name'), // this doesn't exist, link it
			'sailingdate_entity_ref' =>					t('sailingdate_entity_ref'), // this doesn't exist, link it
			'special_id_entity_ref' =>					t('special_id_entity_ref'), // this doesn't exist, link it
		);

		// $this->softDependencies = array('CruiseFactoryDealIDs');
		$this->softDependencies = array('CruiseFactoryCabins');

		global $base_url;
$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/'; // 'http://197.189.205.34/cruise_factory_local_files/';

		$file = 'specialsailingdates';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}
		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_specialsail_dates');
//
		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_specialsail_dates')
		);

		$this->addFieldMapping("changed")													->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("created")													->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('type')														->defaultValue('cruise_factory_specialsail_dates');
		$this->addFieldMapping('uuid',									 					'id')										->xpath('id');
		$this->addFieldMapping('id',									 						'id')										->xpath('id');
		$this->addFieldMapping('field_id',							 					'id')										->xpath('id');
		$this->addFieldMapping('title',								 						'cruise_name');

		$this->addFieldMapping('field_special_id', 								'special_id')						->xpath('special_id');
		$this->addFieldMapping('field_special_entity_ref', 				'special_id_entity_ref');

		$this->addFieldMapping('field_sailing_entity_ref‎', 				'sailingdate_entity_ref');
		$this->addFieldMapping('field_sailing_id', 								'sailingdate_entity_ref');
		$this->addFieldMapping('field_sailing_id_new', 						'sailingdate_id')				->xpath('sailingdate_id');

		$this->addFieldMapping('field_sailing_date',							'sailingdate');
		$this->addFieldMapping('field_sailing_date:to',						'sailingdate');
		$this->addFieldMapping('field_sailing_date:timezone')			->defaultvalue('Africa/Johannesburg');

	}
}
