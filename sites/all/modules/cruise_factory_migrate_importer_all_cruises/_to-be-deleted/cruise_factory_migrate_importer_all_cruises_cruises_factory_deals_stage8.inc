<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryDealsStage8Migration extends Migration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));

	// The following importer relies on a CSV file that somehow is clinging to cache like the aformentioned tiny, evil, meercat
	// Define static var.
	// define('DRUPAL_ROOT', getcwd());
	// include_once('./includes/bootstrap.inc');
	// drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	// drupal_flush_all_caches();

  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

    $row->migrate_map_destid1 		= 		$row->id;

		/*
		$id 													= 		$row->id;
    $row->migrate_map_destid1 		= 		$id;
		$id														=			$row->id;
		$title												=			$row->title;
		$cruise_id										=			$row->cruise_id;
		$cruise_deal_price						=			$row->cruise_deal_price;
		$destination_group						=			$row->destination_group;
		$destination_specific_code		=			$row->destination_specific_code;
		$destination_specific					=			$row->destination_specific;
		$cruise_reference							=			$row->cruise_reference;
		$ship													=			$row->ship;
		$cruiseline_id								=			$row->cruiseline_id;*/

		// watchdog('cruise_factory_migrate_importer___stage8', 'cruise_reference: ' . $row->cruise_reference);
		// watchdog('cruise_factory_migrate_importer___stage8', 'ship_id_number: ' . $row->ship_id_number);

		// dpm("row->ship_id_number: ");
		// dpm($row->ship_id_number);
		// dpm($row);

		// Ship
		// $ship_id = $row->ship;
		//  watchdog('cruise_factory_migrate___stage8', 'starting with ship: ', $ship);
		// $ship_id_final = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_ships' AND uuid = :uuid", array(":uuid" => $ship_id))->fetchAll();
	  //   $ship_id 				= $ship_id_final[0]->id;
		//  watchdog('cruise_factory_migrate___stage8', 'ending with ship_id: ', $ship_id);
    // $row->ship 			= $ship_id;


	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		$this->description = t('Import of Cruises Site Deals, pulling already imported cruises info. Inception.');
		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryDealsStage8Migration');

		$this->systemOfRecord = Migration::DESTINATION;
		// $this->systemOfRecord = Migration::SOURCE;
		$columns = array(
			0 => array('✎',													'✎'),
			1 => array('id',												'id'),
			2 => array('title',											'title'),
			3 => array('cruise_id',									'cruise_id'),
			4 => array('cruise_deal_price',					'cruise_deal_price'),
			5 => array('destination_group',					'destination_group'),
			6 => array('destination_specific_code',	'destination_specific_code'),
			7 => array('destination_specific',			'destination_specific'),
			8 => array('cruise_reference',					'cruise_reference'),
			9 => array('ship_id_number',						'ship_id_number'),
			10 => array('destination_group_id',			'destination_group_id'),
			11 => array('sailing_date',							'sailing_date'),
			12 => array('cruise_length',						'cruise_length'),
			13 => array('cruise_length_with_nights','cruise_length_with_nights'),
			14 => array('cruiseline_id',						'cruiseline_id'),
		);

		// $this->softDependencies = array('CruiseFactorySpecialsSailingDates');
		$this->softDependencies = array('CruiseFactoryDealsNewPrices');

		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
$server_url = str_replace('https://', '', $server_url);

		$local_path = 'http://local.cruises/admin/cruise-deals-that-need-cruise-info/export/export.csv';
		$truserv_path = 'https://www.cruises.co.za/admin/cruise-deals-that-need-cruise-info/export/export.csv';
		$truserv_staging_path = 'http://dev.cruises.co.za/admin/cruise-deals-that-need-cruise-info/export/export.csv';
		// $cruisescoza_path = 'https://www.cruises.co.za/admin/cruise-deals-that-need-cruise-info/export/export.csv'; // 'http://197.189.205.34/
		$cruisescoza_path = 'https://www.cruises.co.za/cruise-deals-that-need-cruise-info/export/export.csv';
		$royal_path = 'http://www.royalcaribbean.co.za/cruise-deals-that-need-cruise-info/export/export.csv';
		$royal_stage_path = 'http://www.stage-royalcaribbean.co.za/cruise-deals-that-need-cruise-info/export/export.csv';
		$crystal_path =  'http://www.crystalyachtcruises.co.za/cruise-deals-that-need-cruise-info/export/export.csv';

		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path; // . $file;
		} else if ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) {
			$items_url = $truserv_path; // . $file;
		} else if (substr($server_url, 0, 19) == 'stage.cruises.co.za') {
			$items_url = $truserv_staging_path;
		} else if ($server_url == 'stage-royalcaribbean.co.za') {
			$items_url = $royal_stage_path;
		} else if ($server_url == 'royalcaribbean.co.za') {
			$items_url = $royal_path;
		} else if ($server_url == 'crystalyachtcruises.co.za') {
			$items_url = $crystal_path;
		} else {
			$items_url = 'http://' . $server_url . '/cruise-deals-that-need-cruise-info/export/export.csv';
		}
		$csv_path = $items_url;
		$this->source = new MigrateSourceCSV($csv_path, $columns, array('delimiter' => ',', 'header_rows' => 1, 'track_changes' => 1));

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_cruise_deal');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_cruise_deal')
		);

		$this->addFieldMapping("changed")														->defaultValue(REQUEST_TIME);

		$this->addFieldMapping('type')															->defaultValue('cruise_factory_cruise_deal');

		$this->addFieldMapping('id',																'id')->sourceMigration('CruiseFactoryDealIDs');
		$this->addFieldMapping('title',															'title');
		//	$this->addFieldMapping('',															'cruise_id
		// 	cruise_deal_price
		$this->addFieldMapping('field_destination_group',						'destination_group_id');
		$this->addFieldMapping('field_destination_specific',				'destination_specific_code');
		$this->addFieldMapping('field_ship',												'ship_id_number');
		$this->addFieldMapping('field_sailing_date',								'sailing_date')->separator('||');
		$this->addFieldMapping('field_sailing_date:timezone')				->defaultvalue('Africa/Johannesburg');
		$this->addFieldMapping('field_sailing_date:to',								'sailing_date')->separator('||');
		$this->addFieldMapping('field_cruise_reference',						'cruise_reference');
		$this->addFieldMapping('field_cruise_days',									'cruise_length');
		$this->addFieldMapping('field_cruise_length_with_nights', 	'cruise_length_with_nights');
		$this->addFieldMapping('field_cruise_line_id', 							'cruiseline_id');

	}
}
