<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryDealsStage6NewLinkMigration extends Migration {

  /**
   * Prepare a proper unique key.
   */
/*  public function prepareKey($source_key, $row) {
    $key = array();
		$destination_id = $row->xml->id;
    $uuid_final = $destination_id . '0' . $destination_id;
    $row->group_id = $uuid_final;
    $key['group_id'] = $row->uuid;
    return $key;
  }
*/

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    // module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

    $row->uuid	= $row->id;

		// watchdog('cruise_factory__stage4-dests', 'dest: ' . $row->destination_group);
/*
		if ($row->destination_group == "South Pacific & New Zealand") {
			// watchdog('cruise_factory__stage4-dests', 'We have South Pacific & New Zealand. row id is : ' . $row->id);
			$row->uuid 	= '43';
			$row->id 		= '43';
		}

		if ($row->destination_group == "Africa - North & Middle East") {
			// watchdog('cruise_factory__stage4-dests', 'We have Africa - North & Middle East. row id is : ' . $row->id);
			$row->uuid 	= '45';
			$row->id 		= '45';
		}
*/


    $destination_group_id = $row->destination_group_id;
    watchdog('cruise_factory__stage6-newlink', 'grp id: ' . $row->destination_group . ' = SELECT * FROM eck_cruise_factory_entity WHERE uuid = ' . $destination_group_id);
    $dest_group_id 	= db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_destination_group' AND uuid = :uuid", array(":uuid" => $destination_group_id))->fetchAll();
    $dest_group_id 	= $dest_group_id[0]->id;

    $destination_id = $row->id;
    // watchdog('cruise_factory__stage6-newlink', 'dest id: ' . $row->destination_specific . ' = SELECT * FROM eck_cruise_factory_entity WHERE uuid = ' . $destination_id);
    $destination_id = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_destination' AND uuid = :uuid", array(":uuid" => $destination_id))->fetchAll();
    $destination_id = $destination_id[0]->id;

    $row->id 											= $destination_id;
    $row->migrate_map_destid1 		= $destination_id;
    $row->destination_group_id 		= $dest_group_id;

  }

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryDealsStage4Migration');

		$this->description = t('Link Destinations to Groups.');

		// $this->systemOfRecord = Migration::SOURCE;
		$this->systemOfRecord = Migration::DESTINATION;

		$columns = array(
			0 => array('id', 										'id'),
			1 => array('destination_specific', 	'destination_specific'),
			2 => array('destination_group', 		'destination_group'),
			3 => array('destination_group_id', 	'destination_group_id'),
			4 => array('uuid', 'uuid'),
		);

/*		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
$server_url = str_replace('https://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$truserv_staging_path = '/var/www/html/cruise_factory_local_files/destination-export/export-export_destinations-staging.csv';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/';
		$royal_path = 'http://www.royalcaribbean.co.za/destination-export/export/export.csv';
		$royal_stage_path = 'http://www.stage-royalcaribbean.co.za/destination-export/export/export.csv';
		$crystal_path =  'http://www.crystalyachtcruises.co.za/destination-export/export/export.csv';

		$file = 'destination-export-export-export_destinations.csv';

	This is not site specific, this uses the old destination entities to link them the way we want.
*/
		$items_url = 'https://www.cruises.co.za/destination-export/export/export.csv';
/*		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		} else if ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) {
			$items_url = $truserv_path . $file;
		} else if (substr($server_url, 0, 19) == 'stage.cruises.co.za') {
			$items_url = $truserv_staging_path;
		} else if ($server_url == 'stage-royalcaribbean.co.za') {
			$items_url = $royal_stage_path;
		} else if ($server_url == 'royalcaribbean.co.za') {
			$items_url = $royal_path;
		} else if ($server_url == 'crystalyachtcruises.co.za') {
			$items_url = $crystal_path;
		} else {
			$items_url = $truserv_path . $file;
		}
*/		$csv_path = $items_url;

		$this->source = new MigrateSourceCSV($csv_path,
			$columns, array('delimiter' => ',', 'header_rows' => 1, 'track_changes' => 1));

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity', 'cruise_factory_destination');

		$this->softDependencies = array('CruiseFactoryDealsStage6');
		// $this->softDependencies = array('CruiseFactoryDealsStage3');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_destination')
		);

		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);

		$this->addFieldMapping('type')												->defaultValue('cruise_factory_destination');

		$this->addFieldMapping('id',														'id')->sourceMigration('CruiseFactoryDealsStage4');
		// $this->addFieldMapping('uuid',													'uuid');
		// $this->addFieldMapping('title',													'destination_specific');
		// $this->addFieldMapping('field_page_title',							'destination_specific');

		// $this->addFieldMapping('field_destination_group',				'destination_group');
		$this->addFieldMapping('field_destination_group',				'destination_group_id');

		// $this->addFieldMapping('field_destination_specific',		'destination_specific')->separator(";");
		// $this->addFieldMapping('field_destination_specific_code','destination_specific');
		// $this->addFieldMapping('field_destination_photo',				'image');

		// $this->addFieldMapping('field_destination_photo:alt', 	'group');
		// $this->addFieldMapping('field_destination_photo:title', 'group');
		// $this->addFieldMapping('field_destination_photo:file_replace')->defaultValue('FILE_EXISTS_REPLACE');



	}

}
