<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class ShipsMigration extends XMLMigration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {

		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

		// $finalurl_mainimage = "http://images.cruisefactory.net/images/ships/thumbnails/" . (String) $row->xml->thumbnail;
		$finalurl_mainimage = "
http://images.cruisefactory.net/images/ships/thumbnails/" . (String) $row->xml->thumbnail;
		$row->xml->thumbnail 	= $finalurl_mainimage;


		$row->xml->speed_measurement_unit = preg_replace('/[0-9]+/', '', (String) $row->xml->speed);
		$row->xml->speed_measurement_unit = trim( (String) $row->xml->speed_measurement_unit);
		$row->speed_measurement_unit = trim( (String) $row->xml->speed_measurement_unit);


		$maidenvoyage = (String) $row->xml->maidenvoyage;
		if ($maidenvoyage) {
			$maidenvoyage = str_replace(",", " ", $maidenvoyage);
			$maidenvoyage = strtotime($maidenvoyage);
			$maidenvoyage = date("M Y", $maidenvoyage);
			$row->xml->maidenvoyage = $maidenvoyage;
			$row->maidenvoyage = $maidenvoyage;
		}

		$refurbished = (String) $row->xml->refurbished;
		if ($refurbished) {
			$refurbished = str_replace(",", " ", $refurbished);
			$refurbished = strtotime($refurbished);
			$refurbished = date("M Y", $refurbished);
			$row->xml->refurbished = $refurbished;
			$row->refurbished = $refurbished;
		}

		// cruiseline_id
    $cruiseline_uuid 				= (String) $row->xml->cruiseline_id;
    $cruiseline_id 					= db_query("SELECT entity_id FROM {field_data_field_id} WHERE field_id_value = :field_id_value", array(":field_id_value" => $cruiseline_uuid))->fetchAll();
    $cruiseline_id 					= $cruiseline_id[0]->entity_id;
    $cruiseline_final_id 		= db_query("SELECT id, title FROM {eck_cruise_factory_entity} WHERE id = :cruiseline_id", array(":cruiseline_id" => $cruiseline_id))->fetchAll();
    $cruiseline_title 			= $cruiseline_final_id[0]->title;
    $cruiseline_final_id 		= $cruiseline_final_id[0]->id;

    $row->xml->cruiseline_id 	= $cruiseline_final_id;
		if (!$cruiseline_final_id) {
			return FALSE;
		}
	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran ShipsMigration');

		/*
			'destinations',
			'ports',
			'ships',
			'cruises',
			'itineraries',
			'sailingdates'
			// 'priceguide',
			// 'shipphotos',
			// 'cruisetypes',
			// 'currencies'
		*/

		// The Description of the import. This Description is shown on the Migrate GUI
		$this->description = t('Import of Ships.');

		$this->systemOfRecord = Migration::SOURCE;

		$fields = array(
			'id'									=>			t('id'),
			'cruiseline_id'				=>			t('cruiseline_id'),
			'name'								=>			t('name'),
			'thumbnail'						=>			t('thumbnail'),
			'mainimage'						=>			t('mainimage'),
			'maidenvoyage'				=>			t('maidenvoyage'),
			'refurbished'					=>			t('refurbished'),
			'tonnage'							=>			t('tonnage'),
			'length'							=>			t('length'),
			'beam'								=>			t('beam'),
			'draft'								=>			t('draft'),
			'speed'								=>			t('speed'),
			'speed_measurement_unit'=>		t('speed_measurement_unit'),
			'ship_rego'						=>			t('ship_rego'),
			'pass_capacity'				=>			t('pass_capacity'),
			'pass_space'					=>			t('pass_space'),
			'crew_size'						=>			t('crew_size'),
			'nat_crew'						=>			t('nat_crew'),
			'nat_officers'				=>			t('nat_officers'),
			'nat_dining'					=>			t('nat_dining'),
			'description'					=>			t('description'),
			'star_rating'					=>			t('star_rating'),
			// 'cruisetype_id'		=>			t('cruisetype_id'),
			// 'currency_id'			=>			t('currency_id'),
		);


		global $base_url;
$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/'; // 'http://197.189.205.34/cruise_factory_local_files/';

		$file = 'ships';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}


		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_ships');

		$this->softDependencies = array('Cruiselines');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_ships')
		);

		$this->addFieldMapping('type')->defaultValue('cruise_factory_ships');
		$this->addFieldMapping("created")->				defaultValue(REQUEST_TIME);
		$this->addFieldMapping("changed")->			 	defaultValue(REQUEST_TIME);

		$this->addFieldMapping('id',										'id')									->xpath('id');
		$this->addFieldMapping('field_id',							'id')									->xpath('id');
		$this->addFieldMapping('uuid',									'id')									->xpath('id');
		$this->addFieldMapping('field_cruiseline_id_new', 'cruiseline_id')	->xpath('cruiseline_id');
		$this->addFieldMapping('title',									'name')								->xpath('name');

		$this->addFieldMapping('field_maiden_voyage',		'maidenvoyage')				->xpath('maidenvoyage');
		$this->addFieldMapping('field_refurbished',			'refurbished')				->xpath('refurbished');
		$this->addFieldMapping('field_tonnage',					'tonnage')						->xpath('tonnage');
		$this->addFieldMapping('field_length',					'length')							->xpath('length');
		$this->addFieldMapping('field_beam',						'beam')								->xpath('beam');
		$this->addFieldMapping('field_draft',						'draft')							->xpath('draft');
		$this->addFieldMapping('field_speed',						'speed')							->xpath('speed');
		$this->addFieldMapping('field_speed_measurement_unit', 'speed_measurement_unit')				->xpath('speed_measurement_unit');
		$this->addFieldMapping('field_ship_rego',				'ship_rego')					->xpath('ship_rego');
		$this->addFieldMapping('field_passenger_capacity',		'pass_capacity')			->xpath('pass_capacity');
		$this->addFieldMapping('field_passenger_space',			'pass_space')					->xpath('pass_space');
		$this->addFieldMapping('field_crew_size',				'crew_size')					->xpath('crew_size');
		$this->addFieldMapping('field_nationality_of_crew',				'nat_crew')						->xpath('nat_crew');
		$this->addFieldMapping('field_nationality_of_officers',		'nat_officers')				->xpath('nat_officers');
		$this->addFieldMapping('field_nationality_of_dining_crew',			'nat_dining')					->xpath('nat_dining');
		$this->addFieldMapping('field_description',			'description')				->xpath('description');
		$this->addFieldMapping('field_star_rating_new',			'star_rating')				->xpath('star_rating');
		$this->addFieldMapping('field_description:format')->defaultValue('full_html');


		$this->addFieldMapping('field_thumbnail',				'thumbnail')					->xpath('thumbnail');
		// $this->addFieldMapping('field_main_image',		'mainimage')					->xpath('mainimage');
		$this->addFieldMapping('field_thumbnail:alt', 			'name')->xpath('name');
		$this->addFieldMapping('field_thumbnail:title', 		'name')->xpath('name');
		$this->addFieldMapping('field_thumbnail:file_replace')->defaultValue('FILE_EXISTS_REPLACE');


	}
}
