<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryCruisesStage3Migration extends XMLMigration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

		// destination uuid
		$destination_id = (String) $row->xml->destination_id;
		$destination_final_id = db_query("SELECT id, title FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_destination' AND uuid = :uuid", array(":uuid" => $destination_id))->fetchAll();
    $destination_title 						= $destination_final_id[0]->title;
    $destination_id 							= $destination_final_id[0]->id;
		$row->xml->destination_id 		=	$destination_id;

/*		// destination group id
		$destination_group_id = db_query("SELECT field_destination_group_target_id FROM {field_data_field_destination_group} WHERE entity_id = :destination_id", array(":destination_id" => $destination_id))->fetchAll();
    $destination_group_id = $destination_group_id[0]->field_destination_group_target_id;
    $row->xml->destination_group = $destination_group_id;

*/    // dpm("-> 'SELECT field_destination_group_target_id FROM field_data_field_destination_group WHERE entity_id = " . $destination_id . "'' <- will equal: " . $destination_group_id);


		// cruise uuid
		$cruise_uuid 	= (String) $row->xml->id;
		$cruise_id 		= db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_cruises' AND uuid = :uuid", array(":uuid" => $cruise_uuid))->fetchAll();
    $cruise_id 		= $cruise_id[0]->id;

    $row->xml->migrate_map_destid1 = $cruise_id;
    $row->migrate_map_destid1 = $cruise_id;

		$cruiseline_id = (String) $row->xml->cruiseline_id;
		// $cruiseline_id = (String) $row->xml->id;
		$selected_cruiselines = variable_get('cruise_factory_migrate__selected_cruiseline_array');
	  if ($selected_cruiselines) {
		  // $selected_cruiselines_name = variable_get('cruise_factory_migrate__selected_cruiselines');
			if (!in_array($cruiseline_id, $selected_cruiselines)) {
				// dpm("we didn't chose: " . $cruiseline_id);
				watchdog('cruise_factory__cruiselines', "we didn't chose: " . $cruiseline_id);
				return FALSE;
			}
	  }


	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryCruisesStage3Migration');

		$this->description = t('Import of Cruises - destination groups.');

		$this->systemOfRecord = Migration::DESTINATION;
		// $this->systemOfRecord = Migration::SOURCE;

		$fields = array(
			'id'										=>					t('id'),
			'cruiseline_id'					=>					t('cruiseline_id'),
			'destination_id'				=>					t('destination_id'),
			'destination_group'			=>					t('destination_group'),
			'ship_id'								=>					t('ship_id'),
			'cruisetype_id'					=>					t('cruisetype_id'),
			'length'								=>					t('length'),
			'length_with_nights'		=>					t('length_with_nights'),
			'name'									=>					t('name'),
			'brief_description'			=>					t('brief_description'),
			'description'						=>					t('description'),
			'photo'									=>					t('photo'),
			'start_price'						=>					t('start_price'),
			'currency_id'						=>					t('currency_id'),
			'cruise_order'					=>					t('cruise_order'),
		);


		// $this->softDependencies = array('CruiseFactoryCruisesStage2');
		$this->softDependencies = array('CruiseFactorySailingDatesLink');

		global $base_url;
$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/'; // 'http://197.189.205.34/cruise_factory_local_files/';

		$file = 'cruises';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}

		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_cruises');
//
		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_cruises')
		);

		$this->addFieldMapping('type')																													->defaultValue('cruise_factory_cruises');

		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);


		$this->addFieldMapping('id', 														'id')											->xpath('id')->sourceMigration('CruiseFactoryCruisesStage1');

		$this->addFieldMapping('field_destination_specific',		'destination_id')				->xpath('destination_id');
		// $this->addFieldMapping('field_destination_group',				'destination_group')			->xpath('destination_group');



	}
}
