<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactorySailingDatesMigration extends XMLMigration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

/*	$cruise_id = (String) $row->xml->cruise_id;
		$final_id = db_query("SELECT * FROM {field_data_field_cruise_id_new} WHERE field_cruise_id_new_value = :field_cruise_id_new_value", array(":field_cruise_id_new_value" => $cruise_id))->fetchAll();
    $final_entity_id = $final_id[0]->entity_id;
*/

// migrate_map_cruisefactorycruisesstage1 - sourceid1 = 60485 - destid1 = 3106
		$cruise_id = (String) $row->xml->cruise_id;
		$sailing_id = (String) $row->xml->id;

		$cruise_entity_id = db_query("SELECT * FROM {migrate_map_cruisefactorycruisesstage1} WHERE sourceid1 = :sourceid1", array(":sourceid1" => $cruise_id))->fetchAll();
    $cruise_entity_id = $cruise_entity_id[0]->destid1;
    $cruise_id = $cruise_entity_id[0]->sourceid;


    $row->xml->migrate_map_sourceid1 	= $sailing_id;
    $row->migrate_map_sourceid1 			= $sailing_id;

    $row->xml->cruise_id 	= $cruise_entity_id;
    $row->cruise_id 			= $cruise_entity_id;

// field_data_field_cruise_name -  entity_id - field_cruise_name_value

		$sailingdate = (String) $row->xml->sailingdate;

		$cruise_title = db_query("SELECT field_cruise_name_value FROM {field_data_field_cruise_name} WHERE entity_id = :entity_id", array(":entity_id" => $cruise_entity_id))->fetchAll();
		// dpm($cruise_title[0]);
    $cruise_title = $cruise_title[0]->field_cruise_name_value;

	  if ($cruise_title) {
			$row->cruise_name =  $cruise_title . ', ' . $sailingdate;
		} else {
			// return FALSE;
			$row->cruise_name =  '*null - ' . $sailingdate;
		}
	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryCruisesStage2Migration');

		$this->description = t('Import of Sailing Dates.');

		// $this->systemOfRecord = Migration::DESTINATION;
		$this->systemOfRecord = Migration::SOURCE;

		$fields = array(
			'id'										=>					t('id'),
			'cruise_id'							=>					t('cruise_id'),
			'sailingdate'						=>					t('sailingdate'),
			'embarkport_id'					=>					t('embarkport_id'),
			'cruise_name'						=>					t('cruise_name'),
		);

		$this->softDependencies = array('CruiseFactoryCruisesStage2');

		global $base_url;
$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/'; // 'http://197.189.205.34/cruise_factory_local_files/';

		$file = 'sailingdates';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}
		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_sailing_dates');
//
		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_sailing_dates')
		);

		$this->addFieldMapping("changed")													->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("created")													->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('type')														->defaultValue('cruise_factory_sailing_dates');
		$this->addFieldMapping('uuid',									 					'id')										->xpath('id');
		$this->addFieldMapping('id',									 						'id')										->xpath('id');
		$this->addFieldMapping('title',								 						'cruise_name');
		$this->addFieldMapping('field_sailing_id_new', 						'id')										->xpath('id');
		$this->addFieldMapping('field_cruise_id_new', 						'cruise_id')						->xpath('cruise_id');
		$this->addFieldMapping('field_cruise_reference',					'cruise_id')						->xpath('cruise_id');
		$this->addFieldMapping('field_sailing_date',							'sailingdate')					->xpath('sailingdate');
		$this->addFieldMapping('field_sailing_date:to',						'sailingdate')					->xpath('sailingdate');
		// $this->addFieldMapping('field_sailing_date:from',					'sailingdate')					->xpath('sailingdate');
		$this->addFieldMapping('field_sailing_date:timezone')			->defaultvalue('Africa/Johannesburg');

	}
}
