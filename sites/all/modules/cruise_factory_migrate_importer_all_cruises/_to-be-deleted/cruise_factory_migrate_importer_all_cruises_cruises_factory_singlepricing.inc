<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactorySpecialsSinglePricingMigration extends XMLMigration {

	public function preImport() {
    parent::preImport();
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

		$special_sailing_id = (String) $row->xml->id;
    $cabin_id = (String) $row->xml->cabin_id;
    $cruise_id = (String) $row->xml->cruise_id;

    $row->xml->migrate_map_sourceid1 	= $special_sailing_id;
    $row->migrate_map_sourceid1 			= $special_sailing_id;

		$sailingdate_id = (String) $row->xml->sailingdate_id;
		$special_id = (String) $row->xml->special_id;

		// $cruise_title = db_query("SELECT field_cruise_name_value FROM {field_data_field_cruise_name} WHERE entity_id = :entity_id", array(":entity_id" => $cruise_id))->fetchAll();
		//  dpm($cruise_title[0]);
		//   $cruise_title = $cruise_title[0]->field_cruise_name_value;


		$cabin_name = db_query("SELECT title FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_cabin' AND uuid = :uuid", array(":uuid" => $cabin_id))->fetchAll();
    $cabin_name				= $cabin_name[0]->title;

		$sailingdate_final_id = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_sailing_dates' AND uuid = :uuid", array(":uuid" => $sailingdate_id))->fetchAll();
    $sailingdate_final_id 						 = $sailingdate_final_id[0]->id;

		$special_final_id = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_cruise_deal' AND uuid = :uuid", array(":uuid" => $special_id))->fetchAll();
    $special_final_id 						 = $special_final_id[0]->id;


    if (!$special_final_id) {
    	watchdog('cruise_factory__singlepricing', 'No Special ID found for: ' . $row->id);
    	return FALSE;
    }

		// $sailingdate = db_query("SELECT field_sailing_date_value FROM {field_data_field_sailing_date} WHERE entity_id = :entity_id", array(":entity_id" => $sailingdate_final_id))->fetchAll();
	  // $sailingdate = $sailingdate[0]->field_sailing_date_value;
	  $sailingdate = $row->xml->sailingdate;
	  $sailingdate_clean = str_replace(" 00:00:00", "", $sailingdate);


	  $row->sailingdate_entity_ref = $sailingdate_final_id;
	  $row->special_id_entity_ref = $special_final_id;

	  $row->xml->cabin_name = $cabin_name;
	  $row->cabin_name = $cabin_name;

	  $row->sailingdate = $sailingdate;
		$row->cruise_name =  'Single Price for #' . $special_id . ' in ' . $cabin_name;

		if ($row->xml->price == '0.00') {
			$row->xml->price = ''; // $suite}
		}


	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		$this->description = t('Import of Sailing Dates.');

		$this->systemOfRecord = Migration::SOURCE;

		$fields = array(
			'id'						=>		t('id'),
			'factory_id'		=>		t('factory_id'),
			'special_id'		=>		t('special_id'),
			'cruise_id'			=>		t('cruise_id'),
			'cabin_id'			=>		t('cabin_id'),
			'price'					=>		t('price'),
			'portcharges'		=>		t('portcharges'),

			// these don't exist, link it
			'cabin_name'							=>	t('cabin_name'),
			'cruise_name'							=>	t('cruise_name'),
			'sailingdate_entity_ref' 	=>	t('sailingdate_entity_ref'),
			'special_id_entity_ref' 	=>	t('special_id_entity_ref'),

		);

		$this->softDependencies = array('CruiseFactorySpecialsSailingDates');

		global $base_url;
$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/';
		$file = 'specialspricing';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}
		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_special_sgl_price');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_special_sgl_price')
		);

		$this->addFieldMapping("changed")									->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("created")									->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('type')										->defaultValue('cruise_factory_special_sgl_price');
		$this->addFieldMapping('uuid',									 	'id')				->xpath('id');
		$this->addFieldMapping('id',									 		'id')				->xpath('id');
		$this->addFieldMapping('field_id',							 	'id')				->xpath('id');

		$this->addFieldMapping('field_cruise_id_new',			'cruise_id')->xpath('cruise_id');
		$this->addFieldMapping('field_cruise_reference',	'cruise_id')->xpath('cruise_id');

		$this->addFieldMapping('title',								 		'cruise_name');
		$this->addFieldMapping('field_cabin_name',		 		'cabin_name');
		$this->addFieldMapping('field_cruise_deal_price',	'price')->xpath('price');
		$this->addFieldMapping('field_port_charges',			'portcharges')->xpath('portcharges');

		$this->addFieldMapping('field_cabin_id',		 			'cabin_id')->xpath('cabin_id');
		$this->addFieldMapping('field_special_id', 				'special_id')->xpath('special_id');

		$this->addFieldMapping('field_special_entity_ref','special_id_entity_ref');
		$this->addFieldMapping('field_sailing_entity_ref‎','sailingdate_entity_ref');

		// $this->addFieldMapping('field_sailing_date',			'sailingdate')->xpath('sailingdate');
		// $this->addFieldMapping('field_sailing_date:to',		'sailingdate')->xpath('sailingdate');
		// $this->addFieldMapping('field_sailing_date:timezone')->defaultvalue('Africa/Johannesburg');

	}
}
