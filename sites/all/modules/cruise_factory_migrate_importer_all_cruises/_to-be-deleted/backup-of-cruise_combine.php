<?php 
$view = new view();
$view->name = 'all_new_cfe_cruise_combine';
$view->description = 'View to generate Cruises from various _base entities';
$view->tag = 'All-New CFE Cruise Combine';
$view->base_table = 'eck_cruise_factory_entity';
$view->human_name = 'All-New CFE Cruise Combine';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['disable_sql_rewrite'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'edit_link' => 'edit_link',
  'base_special_id' => 'base_special_id',
  'view_3' => 'view_3',
  'type' => 'type',
  'base_inside' => 'base_inside',
  'base_price_inside' => 'base_price_inside',
  'base_inside_cabin' => 'base_inside_cabin',
  'base_outside' => 'base_outside',
  'base_price_outside' => 'base_price_outside',
  'base_outside_cabin' => 'base_outside_cabin',
  'base_balcony' => 'base_balcony',
  'base_price_balcony' => 'base_price_balcony',
  'base_suite' => 'base_suite',
  'base_price_suites' => 'base_price_suites',
  'base_cabin_id' => 'base_cabin_id',
  'base_price' => 'base_price',
  'base_portcharges' => 'base_portcharges',
  'view_1' => 'view_1',
  'view_2' => 'view_2',
  'view' => 'view',
  'base_sailingdate' => 'base_sailingdate',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'edit_link' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'base_special_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'view_3' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'type' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'base_inside' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'base_price_inside' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'base_inside_cabin' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'base_outside' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'base_price_outside' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'base_outside_cabin' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'base_balcony' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'base_price_balcony' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'base_suite' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'base_price_suites' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'base_cabin_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'base_price' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'base_portcharges' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'view_1' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'view_2' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'view' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'base_sailingdate' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
);
$handler->display->display_options['style_options']['override'] = FALSE;
/* Field: Cruise_factory_entity: Edit link */
$handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
$handler->display->display_options['fields']['edit_link']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
$handler->display->display_options['fields']['edit_link']['label'] = '✎';
$handler->display->display_options['fields']['edit_link']['text'] = '✎';
/* Field: base_cruise_id */
$handler->display->display_options['fields']['base_cruise_id']['id'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['table'] = 'field_data_base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['field'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['ui_name'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: Cruise_factory_entity: Id */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['id']['field'] = 'id';
$handler->display->display_options['fields']['id']['label'] = 'cruise_id';
$handler->display->display_options['fields']['id']['separator'] = '';
/* Field: cruise_factory_entity: base_cruiseline_id */
$handler->display->display_options['fields']['base_cruiseline_id']['id'] = 'base_cruiseline_id';
$handler->display->display_options['fields']['base_cruiseline_id']['table'] = 'field_data_base_cruiseline_id';
$handler->display->display_options['fields']['base_cruiseline_id']['field'] = 'base_cruiseline_id';
$handler->display->display_options['fields']['base_cruiseline_id']['label'] = '';
$handler->display->display_options['fields']['base_cruiseline_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['base_cruiseline_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_cruiseline_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_destination_id */
$handler->display->display_options['fields']['base_destination_id']['id'] = 'base_destination_id';
$handler->display->display_options['fields']['base_destination_id']['table'] = 'field_data_base_destination_id';
$handler->display->display_options['fields']['base_destination_id']['field'] = 'base_destination_id';
$handler->display->display_options['fields']['base_destination_id']['label'] = '';
$handler->display->display_options['fields']['base_destination_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['base_destination_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_destination_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_id */
$handler->display->display_options['fields']['base_id']['id'] = 'base_id';
$handler->display->display_options['fields']['base_id']['table'] = 'field_data_base_id';
$handler->display->display_options['fields']['base_id']['field'] = 'base_id';
$handler->display->display_options['fields']['base_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_ship_id */
$handler->display->display_options['fields']['base_ship_id']['id'] = 'base_ship_id';
$handler->display->display_options['fields']['base_ship_id']['table'] = 'field_data_base_ship_id';
$handler->display->display_options['fields']['base_ship_id']['field'] = 'base_ship_id';
$handler->display->display_options['fields']['base_ship_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_ship_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: Cruise_factory_entity: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'title';
/* Field: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['type']['field'] = 'type';
$handler->display->display_options['fields']['type']['label'] = 'cruise_factory_entity_type';
$handler->display->display_options['fields']['type']['machine_name'] = TRUE;
/* Field: base_ship */
$handler->display->display_options['fields']['view']['id'] = 'view';
$handler->display->display_options['fields']['view']['table'] = 'views';
$handler->display->display_options['fields']['view']['field'] = 'view';
$handler->display->display_options['fields']['view']['ui_name'] = 'base_ship';
$handler->display->display_options['fields']['view']['label'] = 'base_ship';
$handler->display->display_options['fields']['view']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view']['display'] = 'base_entities';
$handler->display->display_options['fields']['view']['arguments'] = '[%base_ship_id]/base_ships';
/* Field: base_cruiseline */
$handler->display->display_options['fields']['view_1']['id'] = 'view_1';
$handler->display->display_options['fields']['view_1']['table'] = 'views';
$handler->display->display_options['fields']['view_1']['field'] = 'view';
$handler->display->display_options['fields']['view_1']['ui_name'] = 'base_cruiseline';
$handler->display->display_options['fields']['view_1']['label'] = 'base_cruiseline';
$handler->display->display_options['fields']['view_1']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_1']['display'] = 'block_1';
$handler->display->display_options['fields']['view_1']['arguments'] = '[%base_cruiseline_id]/base_cruiselines';
/* Field: base_destinations */
$handler->display->display_options['fields']['view_2']['id'] = 'view_2';
$handler->display->display_options['fields']['view_2']['table'] = 'views';
$handler->display->display_options['fields']['view_2']['field'] = 'view';
$handler->display->display_options['fields']['view_2']['ui_name'] = 'base_destinations';
$handler->display->display_options['fields']['view_2']['label'] = 'base_destinations';
$handler->display->display_options['fields']['view_2']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_2']['display'] = 'base_entities';
$handler->display->display_options['fields']['view_2']['arguments'] = '[%base_destination_id]/base_destinations';
/* Field: destination_group */
$handler->display->display_options['fields']['view_3']['id'] = 'view_3';
$handler->display->display_options['fields']['view_3']['table'] = 'views';
$handler->display->display_options['fields']['view_3']['field'] = 'view';
$handler->display->display_options['fields']['view_3']['ui_name'] = 'destination_group';
$handler->display->display_options['fields']['view_3']['label'] = 'destination_group';
$handler->display->display_options['fields']['view_3']['view'] = 'export_destinations';
$handler->display->display_options['fields']['view_3']['display'] = 'dest_groups_cfe_import';
$handler->display->display_options['fields']['view_3']['arguments'] = '[%base_destination_id]';
/* Field: base_sailingid */
$handler->display->display_options['fields']['view_4']['id'] = 'view_4';
$handler->display->display_options['fields']['view_4']['table'] = 'views';
$handler->display->display_options['fields']['view_4']['field'] = 'view';
$handler->display->display_options['fields']['view_4']['ui_name'] = 'base_sailingid';
$handler->display->display_options['fields']['view_4']['label'] = 'sailing_id';
$handler->display->display_options['fields']['view_4']['exclude'] = TRUE;
$handler->display->display_options['fields']['view_4']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_4']['display'] = 'sailing_on_cruise_id';
$handler->display->display_options['fields']['view_4']['arguments'] = '[%base_id]';
/* Field: sailing_date */
$handler->display->display_options['fields']['view_5']['id'] = 'view_5';
$handler->display->display_options['fields']['view_5']['table'] = 'views';
$handler->display->display_options['fields']['view_5']['field'] = 'view';
$handler->display->display_options['fields']['view_5']['ui_name'] = 'sailing_date';
$handler->display->display_options['fields']['view_5']['label'] = 'sailing_date';
$handler->display->display_options['fields']['view_5']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['view_5']['hide_alter_empty'] = FALSE;
$handler->display->display_options['fields']['view_5']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_5']['display'] = 'sailingdate_on_cruise_id';
$handler->display->display_options['fields']['view_5']['arguments'] = '[%base_id]';
/* Field: base_itineraries */
$handler->display->display_options['fields']['view_6']['id'] = 'view_6';
$handler->display->display_options['fields']['view_6']['table'] = 'views';
$handler->display->display_options['fields']['view_6']['field'] = 'view';
$handler->display->display_options['fields']['view_6']['ui_name'] = 'base_itineraries';
$handler->display->display_options['fields']['view_6']['label'] = 'base_itineraries';
$handler->display->display_options['fields']['view_6']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['view_6']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_6']['display'] = 'itinerary_on_cruise_id';
$handler->display->display_options['fields']['view_6']['arguments'] = '[%base_id]';
/* Field: cruise_factory_entity: base_brief_description */
$handler->display->display_options['fields']['base_brief_description']['id'] = 'base_brief_description';
$handler->display->display_options['fields']['base_brief_description']['table'] = 'field_data_base_brief_description';
$handler->display->display_options['fields']['base_brief_description']['field'] = 'base_brief_description';
/* Field: cruise_factory_entity: base_cruise_length */
$handler->display->display_options['fields']['base_cruise_length']['id'] = 'base_cruise_length';
$handler->display->display_options['fields']['base_cruise_length']['table'] = 'field_data_base_cruise_length';
$handler->display->display_options['fields']['base_cruise_length']['field'] = 'base_cruise_length';
$handler->display->display_options['fields']['base_cruise_length']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_description */
$handler->display->display_options['fields']['base_description']['id'] = 'base_description';
$handler->display->display_options['fields']['base_description']['table'] = 'field_data_base_description';
$handler->display->display_options['fields']['base_description']['field'] = 'base_description';
/* Field: cruise_factory_entity: base_name */
$handler->display->display_options['fields']['base_name']['id'] = 'base_name';
$handler->display->display_options['fields']['base_name']['table'] = 'field_data_base_name';
$handler->display->display_options['fields']['base_name']['field'] = 'base_name';
/* Field: cruise_factory_entity: base_photo */
$handler->display->display_options['fields']['base_photo']['id'] = 'base_photo';
$handler->display->display_options['fields']['base_photo']['table'] = 'field_data_base_photo';
$handler->display->display_options['fields']['base_photo']['field'] = 'base_photo';
$handler->display->display_options['fields']['base_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['base_photo']['settings'] = array(
  'image_style' => '60x60',
  'image_link' => '',
);
/* Field: cruise_factory_entity: base_start_price */
$handler->display->display_options['fields']['base_start_price']['id'] = 'base_start_price';
$handler->display->display_options['fields']['base_start_price']['table'] = 'field_data_base_start_price';
$handler->display->display_options['fields']['base_start_price']['field'] = 'base_start_price';
$handler->display->display_options['fields']['base_start_price']['type'] = 'number_unformatted';
$handler->display->display_options['fields']['base_start_price']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: ship_star_rating */
$handler->display->display_options['fields']['view_7']['id'] = 'view_7';
$handler->display->display_options['fields']['view_7']['table'] = 'views';
$handler->display->display_options['fields']['view_7']['field'] = 'view';
$handler->display->display_options['fields']['view_7']['ui_name'] = 'ship_star_rating';
$handler->display->display_options['fields']['view_7']['label'] = 'ship_star_rating';
$handler->display->display_options['fields']['view_7']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_7']['display'] = 'ship_specs_star_rating';
$handler->display->display_options['fields']['view_7']['arguments'] = '[%base_ship_id]/base_ships';
/* Field: ship_thumb */
$handler->display->display_options['fields']['view_8']['id'] = 'view_8';
$handler->display->display_options['fields']['view_8']['table'] = 'views';
$handler->display->display_options['fields']['view_8']['field'] = 'view';
$handler->display->display_options['fields']['view_8']['ui_name'] = 'ship_thumb';
$handler->display->display_options['fields']['view_8']['label'] = 'ship_thumb';
$handler->display->display_options['fields']['view_8']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_8']['display'] = 'ship_specs_thumbnail';
$handler->display->display_options['fields']['view_8']['arguments'] = '[%base_ship_id]/base_ships';
/* Field: embarkport */
$handler->display->display_options['fields']['view_9']['id'] = 'view_9';
$handler->display->display_options['fields']['view_9']['table'] = 'views';
$handler->display->display_options['fields']['view_9']['field'] = 'view';
$handler->display->display_options['fields']['view_9']['ui_name'] = 'embarkport';
$handler->display->display_options['fields']['view_9']['label'] = 'embarkport';
$handler->display->display_options['fields']['view_9']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_9']['display'] = 'embarkport_on_cruise_id';
$handler->display->display_options['fields']['view_9']['arguments'] = '[%base_id';
/* Sort criterion: cruise_factory_entity: base_id (base_id) */
$handler->display->display_options['sorts']['base_id_value']['id'] = 'base_id_value';
$handler->display->display_options['sorts']['base_id_value']['table'] = 'field_data_base_id';
$handler->display->display_options['sorts']['base_id_value']['field'] = 'base_id_value';
/* Contextual filter: cruise_factory_entity: base_cruise_id (base_cruise_id) */
$handler->display->display_options['arguments']['base_cruise_id_value']['id'] = 'base_cruise_id_value';
$handler->display->display_options['arguments']['base_cruise_id_value']['table'] = 'field_data_base_cruise_id';
$handler->display->display_options['arguments']['base_cruise_id_value']['field'] = 'base_cruise_id_value';
$handler->display->display_options['arguments']['base_cruise_id_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['base_cruise_id_value']['summary']['format'] = 'default_summary';
/* Filter criterion: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'base_specialsailingdates' => 'base_specialsailingdates',
  'base_specials' => 'base_specials',
  'base_specialsmultipricing' => 'base_specialsmultipricing',
  'base_specialspricing' => 'base_specialspricing',
  'base_priceguide' => 'base_priceguide',
);

/* Display: cruise-combine */
$handler = $view->new_display('page', 'cruise-combine', 'cruise_combine');
$handler->display->display_options['defaults']['group_by'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Cruise_factory_entity: Edit link */
$handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
$handler->display->display_options['fields']['edit_link']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
$handler->display->display_options['fields']['edit_link']['label'] = '✎';
$handler->display->display_options['fields']['edit_link']['text'] = '✎';
/* Field: Cruise_factory_entity: Id */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['id']['field'] = 'id';
$handler->display->display_options['fields']['id']['label'] = 'cruise_id';
$handler->display->display_options['fields']['id']['separator'] = '';
/* Field: cruise_factory_entity: base_cruiseline_id */
$handler->display->display_options['fields']['base_cruiseline_id']['id'] = 'base_cruiseline_id';
$handler->display->display_options['fields']['base_cruiseline_id']['table'] = 'field_data_base_cruiseline_id';
$handler->display->display_options['fields']['base_cruiseline_id']['field'] = 'base_cruiseline_id';
$handler->display->display_options['fields']['base_cruiseline_id']['label'] = '';
$handler->display->display_options['fields']['base_cruiseline_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['base_cruiseline_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_cruiseline_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_destination_id */
$handler->display->display_options['fields']['base_destination_id']['id'] = 'base_destination_id';
$handler->display->display_options['fields']['base_destination_id']['table'] = 'field_data_base_destination_id';
$handler->display->display_options['fields']['base_destination_id']['field'] = 'base_destination_id';
$handler->display->display_options['fields']['base_destination_id']['label'] = '';
$handler->display->display_options['fields']['base_destination_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['base_destination_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_destination_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_id */
$handler->display->display_options['fields']['base_id']['id'] = 'base_id';
$handler->display->display_options['fields']['base_id']['table'] = 'field_data_base_id';
$handler->display->display_options['fields']['base_id']['field'] = 'base_id';
$handler->display->display_options['fields']['base_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_ship_id */
$handler->display->display_options['fields']['base_ship_id']['id'] = 'base_ship_id';
$handler->display->display_options['fields']['base_ship_id']['table'] = 'field_data_base_ship_id';
$handler->display->display_options['fields']['base_ship_id']['field'] = 'base_ship_id';
$handler->display->display_options['fields']['base_ship_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_ship_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: Cruise_factory_entity: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'title';
/* Field: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['type']['field'] = 'type';
$handler->display->display_options['fields']['type']['label'] = 'cruise_factory_entity_type';
$handler->display->display_options['fields']['type']['machine_name'] = TRUE;
/* Field: base_ship */
$handler->display->display_options['fields']['view']['id'] = 'view';
$handler->display->display_options['fields']['view']['table'] = 'views';
$handler->display->display_options['fields']['view']['field'] = 'view';
$handler->display->display_options['fields']['view']['ui_name'] = 'base_ship';
$handler->display->display_options['fields']['view']['label'] = 'base_ship';
$handler->display->display_options['fields']['view']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view']['display'] = 'base_entities';
$handler->display->display_options['fields']['view']['arguments'] = '[%base_ship_id]/base_ships';
/* Field: base_cruiseline */
$handler->display->display_options['fields']['view_1']['id'] = 'view_1';
$handler->display->display_options['fields']['view_1']['table'] = 'views';
$handler->display->display_options['fields']['view_1']['field'] = 'view';
$handler->display->display_options['fields']['view_1']['ui_name'] = 'base_cruiseline';
$handler->display->display_options['fields']['view_1']['label'] = 'base_cruiseline';
$handler->display->display_options['fields']['view_1']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_1']['display'] = 'block_1';
$handler->display->display_options['fields']['view_1']['arguments'] = '[%base_cruiseline_id]/base_cruiselines';
/* Field: base_destinations */
$handler->display->display_options['fields']['view_2']['id'] = 'view_2';
$handler->display->display_options['fields']['view_2']['table'] = 'views';
$handler->display->display_options['fields']['view_2']['field'] = 'view';
$handler->display->display_options['fields']['view_2']['ui_name'] = 'base_destinations';
$handler->display->display_options['fields']['view_2']['label'] = 'base_destinations';
$handler->display->display_options['fields']['view_2']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_2']['display'] = 'base_entities';
$handler->display->display_options['fields']['view_2']['arguments'] = '[%base_destination_id]/base_destinations';
/* Field: destination_group */
$handler->display->display_options['fields']['view_3']['id'] = 'view_3';
$handler->display->display_options['fields']['view_3']['table'] = 'views';
$handler->display->display_options['fields']['view_3']['field'] = 'view';
$handler->display->display_options['fields']['view_3']['ui_name'] = 'destination_group';
$handler->display->display_options['fields']['view_3']['label'] = 'destination_group';
$handler->display->display_options['fields']['view_3']['view'] = 'export_destinations';
$handler->display->display_options['fields']['view_3']['display'] = 'dest_groups_cfe_import';
$handler->display->display_options['fields']['view_3']['arguments'] = '[%base_destination_id]';
/* Field: base_itineraries */
$handler->display->display_options['fields']['view_6']['id'] = 'view_6';
$handler->display->display_options['fields']['view_6']['table'] = 'views';
$handler->display->display_options['fields']['view_6']['field'] = 'view';
$handler->display->display_options['fields']['view_6']['ui_name'] = 'base_itineraries';
$handler->display->display_options['fields']['view_6']['label'] = 'base_itineraries';
$handler->display->display_options['fields']['view_6']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['view_6']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_6']['display'] = 'itinerary_on_cruise_id';
$handler->display->display_options['fields']['view_6']['arguments'] = '[%base_id]';
/* Field: cruise_factory_entity: base_brief_description */
$handler->display->display_options['fields']['base_brief_description']['id'] = 'base_brief_description';
$handler->display->display_options['fields']['base_brief_description']['table'] = 'field_data_base_brief_description';
$handler->display->display_options['fields']['base_brief_description']['field'] = 'base_brief_description';
/* Field: cruise_factory_entity: base_cruise_length */
$handler->display->display_options['fields']['base_cruise_length']['id'] = 'base_cruise_length';
$handler->display->display_options['fields']['base_cruise_length']['table'] = 'field_data_base_cruise_length';
$handler->display->display_options['fields']['base_cruise_length']['field'] = 'base_cruise_length';
$handler->display->display_options['fields']['base_cruise_length']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_description */
$handler->display->display_options['fields']['base_description']['id'] = 'base_description';
$handler->display->display_options['fields']['base_description']['table'] = 'field_data_base_description';
$handler->display->display_options['fields']['base_description']['field'] = 'base_description';
$handler->display->display_options['fields']['base_description']['type'] = 'text_trimmed';
$handler->display->display_options['fields']['base_description']['settings'] = array(
  'trim_length' => '50',
);
/* Field: cruise_factory_entity: base_name */
$handler->display->display_options['fields']['base_name']['id'] = 'base_name';
$handler->display->display_options['fields']['base_name']['table'] = 'field_data_base_name';
$handler->display->display_options['fields']['base_name']['field'] = 'base_name';
/* Field: cruise_factory_entity: base_photo */
$handler->display->display_options['fields']['base_photo']['id'] = 'base_photo';
$handler->display->display_options['fields']['base_photo']['table'] = 'field_data_base_photo';
$handler->display->display_options['fields']['base_photo']['field'] = 'base_photo';
$handler->display->display_options['fields']['base_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['base_photo']['settings'] = array(
  'image_style' => '60x60',
  'image_link' => '',
);
$handler->display->display_options['fields']['base_photo']['group_column'] = 'fid';
/* Field: cruise_factory_entity: base_start_price */
$handler->display->display_options['fields']['base_start_price']['id'] = 'base_start_price';
$handler->display->display_options['fields']['base_start_price']['table'] = 'field_data_base_start_price';
$handler->display->display_options['fields']['base_start_price']['field'] = 'base_start_price';
$handler->display->display_options['fields']['base_start_price']['type'] = 'number_unformatted';
$handler->display->display_options['fields']['base_start_price']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: ship_star_rating */
$handler->display->display_options['fields']['view_7']['id'] = 'view_7';
$handler->display->display_options['fields']['view_7']['table'] = 'views';
$handler->display->display_options['fields']['view_7']['field'] = 'view';
$handler->display->display_options['fields']['view_7']['ui_name'] = 'ship_star_rating';
$handler->display->display_options['fields']['view_7']['label'] = 'ship_star_rating';
$handler->display->display_options['fields']['view_7']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_7']['display'] = 'ship_specs_star_rating';
$handler->display->display_options['fields']['view_7']['arguments'] = '[%base_ship_id]/base_ships';
/* Field: ship_thumb */
$handler->display->display_options['fields']['view_8']['id'] = 'view_8';
$handler->display->display_options['fields']['view_8']['table'] = 'views';
$handler->display->display_options['fields']['view_8']['field'] = 'view';
$handler->display->display_options['fields']['view_8']['ui_name'] = 'ship_thumb';
$handler->display->display_options['fields']['view_8']['label'] = 'ship_thumb';
$handler->display->display_options['fields']['view_8']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_8']['display'] = 'ship_specs_thumbnail';
$handler->display->display_options['fields']['view_8']['arguments'] = '[%base_ship_id]/base_ships';
/* Field: embarkport */
$handler->display->display_options['fields']['view_9']['id'] = 'view_9';
$handler->display->display_options['fields']['view_9']['table'] = 'views';
$handler->display->display_options['fields']['view_9']['field'] = 'view';
$handler->display->display_options['fields']['view_9']['ui_name'] = 'embarkport';
$handler->display->display_options['fields']['view_9']['label'] = 'embarkport';
$handler->display->display_options['fields']['view_9']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_9']['display'] = 'embarkport_on_cruise_id';
$handler->display->display_options['fields']['view_9']['arguments'] = '[%base_id';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: cruise_factory_entity: base_id (base_id) */
$handler->display->display_options['arguments']['base_id_value']['id'] = 'base_id_value';
$handler->display->display_options['arguments']['base_id_value']['table'] = 'field_data_base_id';
$handler->display->display_options['arguments']['base_id_value']['field'] = 'base_id_value';
$handler->display->display_options['arguments']['base_id_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['base_id_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['base_id_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['base_id_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'base_cruises' => 'base_cruises',
);
$handler->display->display_options['path'] = 'admin/all-new-cfe-cruise-combine';

/* Display: sailing-dates */
$handler = $view->new_display('page', 'sailing-dates', 'sailing_dates');
$handler->display->display_options['enabled'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Cruise_factory_entity: Edit link */
$handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
$handler->display->display_options['fields']['edit_link']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
$handler->display->display_options['fields']['edit_link']['label'] = '✎';
$handler->display->display_options['fields']['edit_link']['text'] = '✎';
/* Field: base_cruise_id */
$handler->display->display_options['fields']['base_cruise_id']['id'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['table'] = 'field_data_base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['field'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['ui_name'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: Cruise_factory_entity: Id */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['id']['field'] = 'id';
$handler->display->display_options['fields']['id']['label'] = 'cruise_id';
$handler->display->display_options['fields']['id']['separator'] = '';
/* Field: cruise_factory_entity: base_id */
$handler->display->display_options['fields']['base_id']['id'] = 'base_id';
$handler->display->display_options['fields']['base_id']['table'] = 'field_data_base_id';
$handler->display->display_options['fields']['base_id']['field'] = 'base_id';
$handler->display->display_options['fields']['base_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: base_cruiseline_id */
$handler->display->display_options['fields']['view_11']['id'] = 'view_11';
$handler->display->display_options['fields']['view_11']['table'] = 'views';
$handler->display->display_options['fields']['view_11']['field'] = 'view';
$handler->display->display_options['fields']['view_11']['ui_name'] = 'base_cruiseline_id';
$handler->display->display_options['fields']['view_11']['label'] = 'base_cruiseline_id';
$handler->display->display_options['fields']['view_11']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_11']['display'] = 'cruiseline_id_from_cruise_id';
$handler->display->display_options['fields']['view_11']['arguments'] = '[!base_cruise_id]';
/* Field: base_cruiseline */
$handler->display->display_options['fields']['view_1']['id'] = 'view_1';
$handler->display->display_options['fields']['view_1']['table'] = 'views';
$handler->display->display_options['fields']['view_1']['field'] = 'view';
$handler->display->display_options['fields']['view_1']['ui_name'] = 'base_cruiseline';
$handler->display->display_options['fields']['view_1']['label'] = 'base_cruiseline';
$handler->display->display_options['fields']['view_1']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_1']['display'] = 'base_entities';
$handler->display->display_options['fields']['view_1']['arguments'] = '[%view_11]/base_cruiselines';
/* Field: destination_id */
$handler->display->display_options['fields']['view_10']['id'] = 'view_10';
$handler->display->display_options['fields']['view_10']['table'] = 'views';
$handler->display->display_options['fields']['view_10']['field'] = 'view';
$handler->display->display_options['fields']['view_10']['ui_name'] = 'destination_id';
$handler->display->display_options['fields']['view_10']['label'] = 'destination_id';
$handler->display->display_options['fields']['view_10']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_10']['display'] = 'dest_id_from_cruise_id';
$handler->display->display_options['fields']['view_10']['arguments'] = '[%base_cruise_id]';
/* Field: cruise_factory_entity: base_ship_id */
$handler->display->display_options['fields']['base_ship_id']['id'] = 'base_ship_id';
$handler->display->display_options['fields']['base_ship_id']['table'] = 'field_data_base_ship_id';
$handler->display->display_options['fields']['base_ship_id']['field'] = 'base_ship_id';
$handler->display->display_options['fields']['base_ship_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_ship_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: Cruise_factory_entity: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'title';
/* Field: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['type']['field'] = 'type';
$handler->display->display_options['fields']['type']['label'] = 'cruise_factory_entity_type';
$handler->display->display_options['fields']['type']['machine_name'] = TRUE;
/* Field: base_ship */
$handler->display->display_options['fields']['view']['id'] = 'view';
$handler->display->display_options['fields']['view']['table'] = 'views';
$handler->display->display_options['fields']['view']['field'] = 'view';
$handler->display->display_options['fields']['view']['ui_name'] = 'base_ship';
$handler->display->display_options['fields']['view']['label'] = 'base_ship';
$handler->display->display_options['fields']['view']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view']['display'] = 'base_entities';
$handler->display->display_options['fields']['view']['arguments'] = '[%base_id_from_cruise_id]/base_ships';
/* Field: base_destinations */
$handler->display->display_options['fields']['view_2']['id'] = 'view_2';
$handler->display->display_options['fields']['view_2']['table'] = 'views';
$handler->display->display_options['fields']['view_2']['field'] = 'view';
$handler->display->display_options['fields']['view_2']['ui_name'] = 'base_destinations';
$handler->display->display_options['fields']['view_2']['label'] = 'base_destinations';
$handler->display->display_options['fields']['view_2']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_2']['display'] = 'base_entities';
$handler->display->display_options['fields']['view_2']['arguments'] = '[%base_destination_id]/base_destinations';
/* Field: destination_group */
$handler->display->display_options['fields']['view_3']['id'] = 'view_3';
$handler->display->display_options['fields']['view_3']['table'] = 'views';
$handler->display->display_options['fields']['view_3']['field'] = 'view';
$handler->display->display_options['fields']['view_3']['ui_name'] = 'destination_group';
$handler->display->display_options['fields']['view_3']['label'] = 'destination_group';
$handler->display->display_options['fields']['view_3']['view'] = 'export_destinations';
$handler->display->display_options['fields']['view_3']['display'] = 'dest_groups_cfe_import';
$handler->display->display_options['fields']['view_3']['arguments'] = '[%base_destination_id]';
/* Field: base_sailingid */
$handler->display->display_options['fields']['view_4']['id'] = 'view_4';
$handler->display->display_options['fields']['view_4']['table'] = 'views';
$handler->display->display_options['fields']['view_4']['field'] = 'view';
$handler->display->display_options['fields']['view_4']['ui_name'] = 'base_sailingid';
$handler->display->display_options['fields']['view_4']['label'] = 'sailing_id';
$handler->display->display_options['fields']['view_4']['exclude'] = TRUE;
$handler->display->display_options['fields']['view_4']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_4']['display'] = 'sailing_on_cruise_id';
$handler->display->display_options['fields']['view_4']['arguments'] = '[%base_id]';
/* Field: sailing_date */
$handler->display->display_options['fields']['view_5']['id'] = 'view_5';
$handler->display->display_options['fields']['view_5']['table'] = 'views';
$handler->display->display_options['fields']['view_5']['field'] = 'view';
$handler->display->display_options['fields']['view_5']['ui_name'] = 'sailing_date';
$handler->display->display_options['fields']['view_5']['label'] = 'sailing_date';
$handler->display->display_options['fields']['view_5']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['view_5']['hide_alter_empty'] = FALSE;
$handler->display->display_options['fields']['view_5']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_5']['display'] = 'sailingdate_on_cruise_id';
$handler->display->display_options['fields']['view_5']['arguments'] = '[%base_id]';
/* Field: base_itineraries */
$handler->display->display_options['fields']['view_6']['id'] = 'view_6';
$handler->display->display_options['fields']['view_6']['table'] = 'views';
$handler->display->display_options['fields']['view_6']['field'] = 'view';
$handler->display->display_options['fields']['view_6']['ui_name'] = 'base_itineraries';
$handler->display->display_options['fields']['view_6']['label'] = 'base_itineraries';
$handler->display->display_options['fields']['view_6']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['view_6']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_6']['display'] = 'itinerary_on_cruise_id';
$handler->display->display_options['fields']['view_6']['arguments'] = '[%base_id]';
/* Field: cruise_factory_entity: base_brief_description */
$handler->display->display_options['fields']['base_brief_description']['id'] = 'base_brief_description';
$handler->display->display_options['fields']['base_brief_description']['table'] = 'field_data_base_brief_description';
$handler->display->display_options['fields']['base_brief_description']['field'] = 'base_brief_description';
/* Field: cruise_factory_entity: base_cruise_length */
$handler->display->display_options['fields']['base_cruise_length']['id'] = 'base_cruise_length';
$handler->display->display_options['fields']['base_cruise_length']['table'] = 'field_data_base_cruise_length';
$handler->display->display_options['fields']['base_cruise_length']['field'] = 'base_cruise_length';
$handler->display->display_options['fields']['base_cruise_length']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_description */
$handler->display->display_options['fields']['base_description']['id'] = 'base_description';
$handler->display->display_options['fields']['base_description']['table'] = 'field_data_base_description';
$handler->display->display_options['fields']['base_description']['field'] = 'base_description';
/* Field: cruise_factory_entity: base_name */
$handler->display->display_options['fields']['base_name']['id'] = 'base_name';
$handler->display->display_options['fields']['base_name']['table'] = 'field_data_base_name';
$handler->display->display_options['fields']['base_name']['field'] = 'base_name';
/* Field: cruise_factory_entity: base_photo */
$handler->display->display_options['fields']['base_photo']['id'] = 'base_photo';
$handler->display->display_options['fields']['base_photo']['table'] = 'field_data_base_photo';
$handler->display->display_options['fields']['base_photo']['field'] = 'base_photo';
$handler->display->display_options['fields']['base_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['base_photo']['settings'] = array(
  'image_style' => '60x60',
  'image_link' => '',
);
/* Field: cruise_factory_entity: base_start_price */
$handler->display->display_options['fields']['base_start_price']['id'] = 'base_start_price';
$handler->display->display_options['fields']['base_start_price']['table'] = 'field_data_base_start_price';
$handler->display->display_options['fields']['base_start_price']['field'] = 'base_start_price';
$handler->display->display_options['fields']['base_start_price']['type'] = 'number_unformatted';
$handler->display->display_options['fields']['base_start_price']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: ship_star_rating */
$handler->display->display_options['fields']['view_7']['id'] = 'view_7';
$handler->display->display_options['fields']['view_7']['table'] = 'views';
$handler->display->display_options['fields']['view_7']['field'] = 'view';
$handler->display->display_options['fields']['view_7']['ui_name'] = 'ship_star_rating';
$handler->display->display_options['fields']['view_7']['label'] = 'ship_star_rating';
$handler->display->display_options['fields']['view_7']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_7']['display'] = 'ship_specs_star_rating';
$handler->display->display_options['fields']['view_7']['arguments'] = '[%base_ship_id]/base_ships';
/* Field: ship_thumb */
$handler->display->display_options['fields']['view_8']['id'] = 'view_8';
$handler->display->display_options['fields']['view_8']['table'] = 'views';
$handler->display->display_options['fields']['view_8']['field'] = 'view';
$handler->display->display_options['fields']['view_8']['ui_name'] = 'ship_thumb';
$handler->display->display_options['fields']['view_8']['label'] = 'ship_thumb';
$handler->display->display_options['fields']['view_8']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_8']['display'] = 'ship_specs_thumbnail';
$handler->display->display_options['fields']['view_8']['arguments'] = '[%base_ship_id]/base_ships';
/* Field: embarkport */
$handler->display->display_options['fields']['view_9']['id'] = 'view_9';
$handler->display->display_options['fields']['view_9']['table'] = 'views';
$handler->display->display_options['fields']['view_9']['field'] = 'view';
$handler->display->display_options['fields']['view_9']['ui_name'] = 'embarkport';
$handler->display->display_options['fields']['view_9']['label'] = 'embarkport';
$handler->display->display_options['fields']['view_9']['view'] = 'elements_on_all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_9']['display'] = 'embarkport_on_cruise_id';
$handler->display->display_options['fields']['view_9']['arguments'] = '[%base_id';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '[view_11]';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'cruise_factory_cruises' => 'cruise_factory_cruises',
);
$handler->display->display_options['path'] = 'admin/all-new-cfe-cruise-combine/sailing-dates';

/* Display: NEW-sailing-dates */
$handler = $view->new_display('page', 'NEW-sailing-dates', 'new_sailing_dates');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: base_cruise_id */
$handler->display->display_options['fields']['base_cruise_id']['id'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['table'] = 'field_data_base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['field'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['ui_name'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_sailingdate */
$handler->display->display_options['fields']['base_sailingdate']['id'] = 'base_sailingdate';
$handler->display->display_options['fields']['base_sailingdate']['table'] = 'field_data_base_sailingdate';
$handler->display->display_options['fields']['base_sailingdate']['field'] = 'base_sailingdate';
$handler->display->display_options['fields']['base_sailingdate']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Global: View */
$handler->display->display_options['fields']['view']['id'] = 'view';
$handler->display->display_options['fields']['view']['table'] = 'views';
$handler->display->display_options['fields']['view']['field'] = 'view';
$handler->display->display_options['fields']['view']['label'] = 'cruise_info';
$handler->display->display_options['fields']['view']['view'] = 'all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view']['display'] = 'cruise_combine';
$handler->display->display_options['fields']['view']['arguments'] = '[%base_cruise_id]';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: cruise_factory_entity: base_cruise_id (base_cruise_id) */
$handler->display->display_options['sorts']['base_cruise_id_value']['id'] = 'base_cruise_id_value';
$handler->display->display_options['sorts']['base_cruise_id_value']['table'] = 'field_data_base_cruise_id';
$handler->display->display_options['sorts']['base_cruise_id_value']['field'] = 'base_cruise_id_value';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: cruise_factory_entity: base_cruise_id (base_cruise_id) */
$handler->display->display_options['arguments']['base_cruise_id_value']['id'] = 'base_cruise_id_value';
$handler->display->display_options['arguments']['base_cruise_id_value']['table'] = 'field_data_base_cruise_id';
$handler->display->display_options['arguments']['base_cruise_id_value']['field'] = 'base_cruise_id_value';
$handler->display->display_options['arguments']['base_cruise_id_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['base_cruise_id_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['base_cruise_id_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['base_cruise_id_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'base_sailingdates' => 'base_sailingdates',
);
$handler->display->display_options['path'] = 'admin/all-new-cfe-cruise-combine/NEW-sailing-dates';

/* Display: combine/DEALS */
$handler = $view->new_display('page', 'combine/DEALS', 'combine_deals');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: base_cruise_id */
$handler->display->display_options['fields']['base_cruise_id']['id'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['table'] = 'field_data_base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['field'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['ui_name'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_special_id */
$handler->display->display_options['fields']['base_special_id']['id'] = 'base_special_id';
$handler->display->display_options['fields']['base_special_id']['table'] = 'field_data_base_special_id';
$handler->display->display_options['fields']['base_special_id']['field'] = 'base_special_id';
$handler->display->display_options['fields']['base_special_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_id */
$handler->display->display_options['fields']['base_id']['id'] = 'base_id';
$handler->display->display_options['fields']['base_id']['table'] = 'field_data_base_id';
$handler->display->display_options['fields']['base_id']['field'] = 'base_id';
$handler->display->display_options['fields']['base_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_advert_code */
$handler->display->display_options['fields']['base_advert_code']['id'] = 'base_advert_code';
$handler->display->display_options['fields']['base_advert_code']['table'] = 'field_data_base_advert_code';
$handler->display->display_options['fields']['base_advert_code']['field'] = 'base_advert_code';
/* Field: cruise_factory_entity: base_special_header */
$handler->display->display_options['fields']['base_special_header']['id'] = 'base_special_header';
$handler->display->display_options['fields']['base_special_header']['table'] = 'field_data_base_special_header';
$handler->display->display_options['fields']['base_special_header']['field'] = 'base_special_header';
/* Field: Global: View */
$handler->display->display_options['fields']['view']['id'] = 'view';
$handler->display->display_options['fields']['view']['table'] = 'views';
$handler->display->display_options['fields']['view']['field'] = 'view';
$handler->display->display_options['fields']['view']['label'] = 'pricing_info';
$handler->display->display_options['fields']['view']['view'] = 'all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view']['display'] = 'pricing_info';
$handler->display->display_options['fields']['view']['arguments'] = '[%base_id]';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: cruise_factory_entity: base_cruise_id (base_cruise_id) */
$handler->display->display_options['sorts']['base_cruise_id_value']['id'] = 'base_cruise_id_value';
$handler->display->display_options['sorts']['base_cruise_id_value']['table'] = 'field_data_base_cruise_id';
$handler->display->display_options['sorts']['base_cruise_id_value']['field'] = 'base_cruise_id_value';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: cruise_factory_entity: base_cruise_id (base_cruise_id) */
$handler->display->display_options['arguments']['base_cruise_id_value']['id'] = 'base_cruise_id_value';
$handler->display->display_options['arguments']['base_cruise_id_value']['table'] = 'field_data_base_cruise_id';
$handler->display->display_options['arguments']['base_cruise_id_value']['field'] = 'base_cruise_id_value';
$handler->display->display_options['arguments']['base_cruise_id_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['base_cruise_id_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['base_cruise_id_value']['default_argument_options']['argument'] = '58830';
$handler->display->display_options['arguments']['base_cruise_id_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['base_cruise_id_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['base_cruise_id_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'base_specials' => 'base_specials',
);
$handler->display->display_options['path'] = 'admin/all-new-cfe-cruise-combine/DEALS';

/* Display: Pricing Info */
$handler = $view->new_display('block', 'Pricing Info', 'pricing_info');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: cruise_factory_entity: base_id */
$handler->display->display_options['fields']['base_id']['id'] = 'base_id';
$handler->display->display_options['fields']['base_id']['table'] = 'field_data_base_id';
$handler->display->display_options['fields']['base_id']['field'] = 'base_id';
$handler->display->display_options['fields']['base_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_special_id */
$handler->display->display_options['fields']['base_special_id']['id'] = 'base_special_id';
$handler->display->display_options['fields']['base_special_id']['table'] = 'field_data_base_special_id';
$handler->display->display_options['fields']['base_special_id']['field'] = 'base_special_id';
/* Field: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['type']['field'] = 'type';
$handler->display->display_options['fields']['type']['label'] = 'cruise_factory_entity_type';
$handler->display->display_options['fields']['type']['machine_name'] = TRUE;
/* Field: cruise_factory_entity: base_price_inside */
$handler->display->display_options['fields']['base_price_inside']['id'] = 'base_price_inside';
$handler->display->display_options['fields']['base_price_inside']['table'] = 'field_data_base_price_inside';
$handler->display->display_options['fields']['base_price_inside']['field'] = 'base_price_inside';
$handler->display->display_options['fields']['base_price_inside']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_inside */
$handler->display->display_options['fields']['base_inside']['id'] = 'base_inside';
$handler->display->display_options['fields']['base_inside']['table'] = 'field_data_base_inside';
$handler->display->display_options['fields']['base_inside']['field'] = 'base_inside';
/* Field: cruise_factory_entity: base_price_outside */
$handler->display->display_options['fields']['base_price_outside']['id'] = 'base_price_outside';
$handler->display->display_options['fields']['base_price_outside']['table'] = 'field_data_base_price_outside';
$handler->display->display_options['fields']['base_price_outside']['field'] = 'base_price_outside';
$handler->display->display_options['fields']['base_price_outside']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_outside */
$handler->display->display_options['fields']['base_outside']['id'] = 'base_outside';
$handler->display->display_options['fields']['base_outside']['table'] = 'field_data_base_outside';
$handler->display->display_options['fields']['base_outside']['field'] = 'base_outside';
/* Field: cruise_factory_entity: base_price_balcony */
$handler->display->display_options['fields']['base_price_balcony']['id'] = 'base_price_balcony';
$handler->display->display_options['fields']['base_price_balcony']['table'] = 'field_data_base_price_balcony';
$handler->display->display_options['fields']['base_price_balcony']['field'] = 'base_price_balcony';
$handler->display->display_options['fields']['base_price_balcony']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_balcony */
$handler->display->display_options['fields']['base_balcony']['id'] = 'base_balcony';
$handler->display->display_options['fields']['base_balcony']['table'] = 'field_data_base_balcony';
$handler->display->display_options['fields']['base_balcony']['field'] = 'base_balcony';
/* Field: cruise_factory_entity: base_price_suites */
$handler->display->display_options['fields']['base_price_suites']['id'] = 'base_price_suites';
$handler->display->display_options['fields']['base_price_suites']['table'] = 'field_data_base_price_suites';
$handler->display->display_options['fields']['base_price_suites']['field'] = 'base_price_suites';
$handler->display->display_options['fields']['base_price_suites']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_suite */
$handler->display->display_options['fields']['base_suite']['id'] = 'base_suite';
$handler->display->display_options['fields']['base_suite']['table'] = 'field_data_base_suite';
$handler->display->display_options['fields']['base_suite']['field'] = 'base_suite';
/* Field: cruise_factory_entity: base_sailingdate */
$handler->display->display_options['fields']['base_sailingdate']['id'] = 'base_sailingdate';
$handler->display->display_options['fields']['base_sailingdate']['table'] = 'field_data_base_sailingdate';
$handler->display->display_options['fields']['base_sailingdate']['field'] = 'base_sailingdate';
$handler->display->display_options['fields']['base_sailingdate']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: cruise_factory_entity: base_cabin_id */
$handler->display->display_options['fields']['base_cabin_id']['id'] = 'base_cabin_id';
$handler->display->display_options['fields']['base_cabin_id']['table'] = 'field_data_base_cabin_id';
$handler->display->display_options['fields']['base_cabin_id']['field'] = 'base_cabin_id';
$handler->display->display_options['fields']['base_cabin_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_portcharges */
$handler->display->display_options['fields']['base_portcharges']['id'] = 'base_portcharges';
$handler->display->display_options['fields']['base_portcharges']['table'] = 'field_data_base_portcharges';
$handler->display->display_options['fields']['base_portcharges']['field'] = 'base_portcharges';
/* Field: cruise_factory_entity: base_price */
$handler->display->display_options['fields']['base_price']['id'] = 'base_price';
$handler->display->display_options['fields']['base_price']['table'] = 'field_data_base_price';
$handler->display->display_options['fields']['base_price']['field'] = 'base_price';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: cruise_factory_entity: base_special_id (base_special_id) */
$handler->display->display_options['arguments']['base_special_id_value']['id'] = 'base_special_id_value';
$handler->display->display_options['arguments']['base_special_id_value']['table'] = 'field_data_base_special_id';
$handler->display->display_options['arguments']['base_special_id_value']['field'] = 'base_special_id_value';
$handler->display->display_options['arguments']['base_special_id_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['base_special_id_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['base_special_id_value']['default_argument_options']['argument'] = '66527';
$handler->display->display_options['arguments']['base_special_id_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['base_special_id_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['base_special_id_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'base_specials' => 'base_specials',
  'base_specialsmultipricing' => 'base_specialsmultipricing',
  'base_specialspricing' => 'base_specialspricing',
  'base_priceguide' => 'base_priceguide',
);

/* Display: DEALS-PRICING */
$handler = $view->new_display('page', 'DEALS-PRICING', 'deals_pricing');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Cruise_factory_entity: Edit link */
$handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
$handler->display->display_options['fields']['edit_link']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
$handler->display->display_options['fields']['edit_link']['label'] = '✎';
$handler->display->display_options['fields']['edit_link']['text'] = '✎';
/* Field: cruise_factory_entity: base_special_id */
$handler->display->display_options['fields']['base_special_id']['id'] = 'base_special_id';
$handler->display->display_options['fields']['base_special_id']['table'] = 'field_data_base_special_id';
$handler->display->display_options['fields']['base_special_id']['field'] = 'base_special_id';
$handler->display->display_options['fields']['base_special_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_id_on_special_id */
$handler->display->display_options['fields']['view_3']['id'] = 'view_3';
$handler->display->display_options['fields']['view_3']['table'] = 'views';
$handler->display->display_options['fields']['view_3']['field'] = 'view';
$handler->display->display_options['fields']['view_3']['ui_name'] = 'cruise_id_on_special_id';
$handler->display->display_options['fields']['view_3']['label'] = 'cruise_id';
$handler->display->display_options['fields']['view_3']['view'] = 'all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_3']['display'] = 'cruise_id_on_special_id';
$handler->display->display_options['fields']['view_3']['arguments'] = '[%base_special_id]';
/* Field: cruise_factory_entity: base_sailingdate */
$handler->display->display_options['fields']['base_sailingdate']['id'] = 'base_sailingdate';
$handler->display->display_options['fields']['base_sailingdate']['table'] = 'field_data_base_sailingdate';
$handler->display->display_options['fields']['base_sailingdate']['field'] = 'base_sailingdate';
$handler->display->display_options['fields']['base_sailingdate']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['fields']['type']['field'] = 'type';
/* Field: cruise_factory_entity: base_inside */
$handler->display->display_options['fields']['base_inside']['id'] = 'base_inside';
$handler->display->display_options['fields']['base_inside']['table'] = 'field_data_base_inside';
$handler->display->display_options['fields']['base_inside']['field'] = 'base_inside';
/* Field: cruise_factory_entity: base_price_inside */
$handler->display->display_options['fields']['base_price_inside']['id'] = 'base_price_inside';
$handler->display->display_options['fields']['base_price_inside']['table'] = 'field_data_base_price_inside';
$handler->display->display_options['fields']['base_price_inside']['field'] = 'base_price_inside';
$handler->display->display_options['fields']['base_price_inside']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_inside_cabin */
$handler->display->display_options['fields']['base_inside_cabin']['id'] = 'base_inside_cabin';
$handler->display->display_options['fields']['base_inside_cabin']['table'] = 'field_data_base_inside_cabin';
$handler->display->display_options['fields']['base_inside_cabin']['field'] = 'base_inside_cabin';
$handler->display->display_options['fields']['base_inside_cabin']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_outside */
$handler->display->display_options['fields']['base_outside']['id'] = 'base_outside';
$handler->display->display_options['fields']['base_outside']['table'] = 'field_data_base_outside';
$handler->display->display_options['fields']['base_outside']['field'] = 'base_outside';
/* Field: cruise_factory_entity: base_price_outside */
$handler->display->display_options['fields']['base_price_outside']['id'] = 'base_price_outside';
$handler->display->display_options['fields']['base_price_outside']['table'] = 'field_data_base_price_outside';
$handler->display->display_options['fields']['base_price_outside']['field'] = 'base_price_outside';
$handler->display->display_options['fields']['base_price_outside']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_outside_cabin */
$handler->display->display_options['fields']['base_outside_cabin']['id'] = 'base_outside_cabin';
$handler->display->display_options['fields']['base_outside_cabin']['table'] = 'field_data_base_outside_cabin';
$handler->display->display_options['fields']['base_outside_cabin']['field'] = 'base_outside_cabin';
$handler->display->display_options['fields']['base_outside_cabin']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_balcony */
$handler->display->display_options['fields']['base_balcony']['id'] = 'base_balcony';
$handler->display->display_options['fields']['base_balcony']['table'] = 'field_data_base_balcony';
$handler->display->display_options['fields']['base_balcony']['field'] = 'base_balcony';
/* Field: cruise_factory_entity: base_price_balcony */
$handler->display->display_options['fields']['base_price_balcony']['id'] = 'base_price_balcony';
$handler->display->display_options['fields']['base_price_balcony']['table'] = 'field_data_base_price_balcony';
$handler->display->display_options['fields']['base_price_balcony']['field'] = 'base_price_balcony';
$handler->display->display_options['fields']['base_price_balcony']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_suite */
$handler->display->display_options['fields']['base_suite']['id'] = 'base_suite';
$handler->display->display_options['fields']['base_suite']['table'] = 'field_data_base_suite';
$handler->display->display_options['fields']['base_suite']['field'] = 'base_suite';
/* Field: cruise_factory_entity: base_price_suites */
$handler->display->display_options['fields']['base_price_suites']['id'] = 'base_price_suites';
$handler->display->display_options['fields']['base_price_suites']['table'] = 'field_data_base_price_suites';
$handler->display->display_options['fields']['base_price_suites']['field'] = 'base_price_suites';
$handler->display->display_options['fields']['base_price_suites']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_cabin_id */
$handler->display->display_options['fields']['base_cabin_id']['id'] = 'base_cabin_id';
$handler->display->display_options['fields']['base_cabin_id']['table'] = 'field_data_base_cabin_id';
$handler->display->display_options['fields']['base_cabin_id']['field'] = 'base_cabin_id';
$handler->display->display_options['fields']['base_cabin_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_price */
$handler->display->display_options['fields']['base_price']['id'] = 'base_price';
$handler->display->display_options['fields']['base_price']['table'] = 'field_data_base_price';
$handler->display->display_options['fields']['base_price']['field'] = 'base_price';
$handler->display->display_options['fields']['base_price']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 0,
);
/* Field: cruise_factory_entity: base_portcharges */
$handler->display->display_options['fields']['base_portcharges']['id'] = 'base_portcharges';
$handler->display->display_options['fields']['base_portcharges']['table'] = 'field_data_base_portcharges';
$handler->display->display_options['fields']['base_portcharges']['field'] = 'base_portcharges';
/* Field: sailingdate_id */
$handler->display->display_options['fields']['view_1']['id'] = 'view_1';
$handler->display->display_options['fields']['view_1']['table'] = 'views';
$handler->display->display_options['fields']['view_1']['field'] = 'view';
$handler->display->display_options['fields']['view_1']['ui_name'] = 'sailingdate_id';
$handler->display->display_options['fields']['view_1']['label'] = 'sailingdate_id';
$handler->display->display_options['fields']['view_1']['view'] = 'all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_1']['display'] = 'sailingdate_id_on_special_id';
$handler->display->display_options['fields']['view_1']['arguments'] = '[%base_special_id]';
/* Field: sailingdate */
$handler->display->display_options['fields']['view_2']['id'] = 'view_2';
$handler->display->display_options['fields']['view_2']['table'] = 'views';
$handler->display->display_options['fields']['view_2']['field'] = 'view';
$handler->display->display_options['fields']['view_2']['ui_name'] = 'sailingdate';
$handler->display->display_options['fields']['view_2']['label'] = 'sailingdate';
$handler->display->display_options['fields']['view_2']['view'] = 'all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view_2']['display'] = 'sailingdate_on_sailingdate_id';
$handler->display->display_options['fields']['view_2']['arguments'] = '[!view_1]';
/* Field: cruise_details */
$handler->display->display_options['fields']['view']['id'] = 'view';
$handler->display->display_options['fields']['view']['table'] = 'views';
$handler->display->display_options['fields']['view']['field'] = 'view';
$handler->display->display_options['fields']['view']['ui_name'] = 'cruise_details';
$handler->display->display_options['fields']['view']['label'] = 'cruise_details';
$handler->display->display_options['fields']['view']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['view']['view'] = 'all_new_cfe_cruise_combine';
$handler->display->display_options['fields']['view']['display'] = 'cruise_combine';
$handler->display->display_options['fields']['view']['arguments'] = '[%view_3]';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: cruise_factory_entity: base_special_id (base_special_id) */
$handler->display->display_options['arguments']['base_special_id_value']['id'] = 'base_special_id_value';
$handler->display->display_options['arguments']['base_special_id_value']['table'] = 'field_data_base_special_id';
$handler->display->display_options['arguments']['base_special_id_value']['field'] = 'base_special_id_value';
$handler->display->display_options['arguments']['base_special_id_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['base_special_id_value']['default_argument_options']['argument'] = '66532';
$handler->display->display_options['arguments']['base_special_id_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['base_special_id_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['base_special_id_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'base_specials' => 'base_specials',
  'base_leadpricing' => 'base_leadpricing',
  'base_specialsmultipricing' => 'base_specialsmultipricing',
  'base_specialspricing' => 'base_specialspricing',
);
$handler->display->display_options['filters']['type']['group'] = 1;
$handler->display->display_options['path'] = 'admin/all-new-cfe-cruise-combine/DEALS-PRICING';

/* Display: sailingdate_id_on_special_id */
$handler = $view->new_display('block', 'sailingdate_id_on_special_id', 'sailingdate_id_on_special_id');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: cruise_factory_entity: base_sailingdate_id */
$handler->display->display_options['fields']['base_sailingdate_id']['id'] = 'base_sailingdate_id';
$handler->display->display_options['fields']['base_sailingdate_id']['table'] = 'field_data_base_sailingdate_id';
$handler->display->display_options['fields']['base_sailingdate_id']['field'] = 'base_sailingdate_id';
$handler->display->display_options['fields']['base_sailingdate_id']['label'] = '';
$handler->display->display_options['fields']['base_sailingdate_id']['alter']['trim_whitespace'] = TRUE;
$handler->display->display_options['fields']['base_sailingdate_id']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['base_sailingdate_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_sailingdate_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: cruise_factory_entity: base_special_id (base_special_id) */
$handler->display->display_options['arguments']['base_special_id_value']['id'] = 'base_special_id_value';
$handler->display->display_options['arguments']['base_special_id_value']['table'] = 'field_data_base_special_id';
$handler->display->display_options['arguments']['base_special_id_value']['field'] = 'base_special_id_value';
$handler->display->display_options['arguments']['base_special_id_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['base_special_id_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['base_special_id_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['base_special_id_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'base_specialsailingdates' => 'base_specialsailingdates',
);

/* Display: sailingdate_on_sailingdate_id */
$handler = $view->new_display('block', 'sailingdate_on_sailingdate_id', 'sailingdate_on_sailingdate_id');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['row_class_special'] = FALSE;
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: cruise_factory_entity: base_sailingdate */
$handler->display->display_options['fields']['base_sailingdate']['id'] = 'base_sailingdate';
$handler->display->display_options['fields']['base_sailingdate']['table'] = 'field_data_base_sailingdate';
$handler->display->display_options['fields']['base_sailingdate']['field'] = 'base_sailingdate';
$handler->display->display_options['fields']['base_sailingdate']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: cruise_factory_entity: base_id */
$handler->display->display_options['fields']['base_id']['id'] = 'base_id';
$handler->display->display_options['fields']['base_id']['table'] = 'field_data_base_id';
$handler->display->display_options['fields']['base_id']['field'] = 'base_id';
$handler->display->display_options['fields']['base_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: cruise_factory_entity: base_sailingdate_id (base_sailingdate_id) */
$handler->display->display_options['arguments']['base_sailingdate_id_value']['id'] = 'base_sailingdate_id_value';
$handler->display->display_options['arguments']['base_sailingdate_id_value']['table'] = 'field_data_base_sailingdate_id';
$handler->display->display_options['arguments']['base_sailingdate_id_value']['field'] = 'base_sailingdate_id_value';
$handler->display->display_options['arguments']['base_sailingdate_id_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['base_sailingdate_id_value']['default_argument_options']['argument'] = '384462';
$handler->display->display_options['arguments']['base_sailingdate_id_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['base_sailingdate_id_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['base_sailingdate_id_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'base_sailingdates' => 'base_sailingdates',
);

/* Display: cruise_id_on_special_id */
$handler = $view->new_display('block', 'cruise_id_on_special_id', 'cruise_id_on_special_id');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['row_class_special'] = FALSE;
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['default_field_elements'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: cruise_factory_entity: base_cruise_id */
$handler->display->display_options['fields']['base_cruise_id']['id'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['table'] = 'field_data_base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['field'] = 'base_cruise_id';
$handler->display->display_options['fields']['base_cruise_id']['label'] = '';
$handler->display->display_options['fields']['base_cruise_id']['alter']['trim_whitespace'] = TRUE;
$handler->display->display_options['fields']['base_cruise_id']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['base_cruise_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['base_cruise_id']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['base_cruise_id']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 0,
);
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: cruise_factory_entity: base_id (base_id) */
$handler->display->display_options['arguments']['base_id_value']['id'] = 'base_id_value';
$handler->display->display_options['arguments']['base_id_value']['table'] = 'field_data_base_id';
$handler->display->display_options['arguments']['base_id_value']['field'] = 'base_id_value';
$handler->display->display_options['arguments']['base_id_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['base_id_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['base_id_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['base_id_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Cruise_factory_entity: cruise_factory_entity type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'eck_cruise_factory_entity';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'base_specials' => 'base_specials',
);
