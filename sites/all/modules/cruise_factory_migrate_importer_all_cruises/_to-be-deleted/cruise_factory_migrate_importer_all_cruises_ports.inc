<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class PortsMigration extends XMLMigration {
	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}
		// $porturl = 'http://images.cruisefactory.net/images/ports/' . $row->photo;
		$porturl = '
http://images.cruisefactory.net/images/ports/' . $row->photo;
		$row->photo = $porturl;



		// find the Taxonomy Term ID based on the UUID
		$destination_id = (String) $row->xml->destination_id;
		$destination_id = db_query("SELECT id, title FROM {eck_cruise_factory_entity} WHERE uuid = :uuid", array(":uuid" => $destination_id))->fetchAll();
    $destination_id = $destination_id[0]->id;
    $destination_title = $destination_id[0]->title;
    $row->destination_id = $destination_id;
    $row->xml->destination_id = $destination_id;


	}
	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran PortsMigration');

		// The Description of the import. This Description is shown on the Migrate GUI
		$this->description = t('Import of Ports.');

		$this->systemOfRecord = Migration::SOURCE;

		$fields = array(
			'id'									=>			t('id'),
			'destination_id'			=>			t('destination_id'),
			'name'								=>			t('name'),
			'description'					=>			t('description'),
			'photo'								=>			t('photo'),
		);

		global $base_url;
$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/'; // 'http://197.189.205.34/cruise_factory_local_files/';

		$file = 'ports';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}

		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';
		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);
    $destination_options = array(
      'allow_duplicate_terms' => FALSE,
    );
		$this->destination = new MigrateDestinationTerm('ports',
			$destination_options);

		// $this->softDependencies = array('CruiseFactoryDestinationInfo');
		// $this->softDependencies = array('CruiseFactoryDealsStage6');
		$this->softDependencies = array('CruiseFactoryDealsStage6NewLink');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationTerm::getKeySchema()
		);
		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("created")											->defaultValue(REQUEST_TIME);

		// $this->addFieldMapping('id', 													'id')									->xpath('id');
		$this->addFieldMapping('uuid', 												'id')									->xpath('id');
		$this->addFieldMapping('field_destination_specific',	'destination_id')			->xpath('destination_id');
		$this->addFieldMapping('name',												'name')								->xpath('name');
		// $this->addFieldMapping('field_description',						'description')				->xpath('description');
		$this->addFieldMapping('description',									'description')				->xpath('description');
		// $this->addFieldMapping('photo',												'photo')							->xpath('photo');
		// $this->addFieldMapping('field_photo‎',									'photo')							->xpath('photo');

		// $this->addFieldMapping('field_photo‎:alt', 						'name')								->xpath('name');
		// $this->addFieldMapping('field_photo‎:title', 					'name')								->xpath('name');
		// $this->addFieldMapping('field_description:format')		->defaultValue('full_html');
		$this->addFieldMapping('format')											->defaultValue('full_html');
		// $this->addFieldMapping('field_photo‎:file_replace')		->defaultValue('FILE_EXISTS_REPLACE');


	}
}
