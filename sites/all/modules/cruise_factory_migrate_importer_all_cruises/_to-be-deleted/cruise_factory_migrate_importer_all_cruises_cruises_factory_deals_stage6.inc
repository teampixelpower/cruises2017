<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryDealsStage6Migration extends Migration {

  /**
   * Prepare a proper unique key.
   */
/*  public function prepareKey($source_key, $row) {
    $key = array();
		$destination_id = $row->xml->id;
    $uuid_final = $destination_id . '0' . $destination_id;
    $row->destination_group_id = $uuid_final;
    $key['destination_group_id'] = $row->uuid;
    return $key;
  }
*/

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

    $final_id = $row->destination_group_id; //  = $final_id;

    // $row->id 		= $final_id;
    // $row->uuid	= $final_id;

    // $row->destination_group	= $final_id_dest_group;

    $row->migrate_map_sourceid1 = $final_id;
    // $row->migrate_map_destid1 = $final_id;

  }

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryDealsStage4Migration');

		$this->description = t('Import of Destinations.');

		$this->systemOfRecord = Migration::SOURCE;
		// $this->systemOfRecord = Migration::DESTINATION;

		$columns = array(
			0 => array('id', 'id'),
			1 => array('destination_specific', 'destination_specific'),
			2 => array('destination_group', 'destination_group'),
			3 => array('destination_group_id', 'destination_group_id'),
		);

/*		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
$server_url = str_replace('https://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$truserv_staging_path = '/var/www/html/cruise_factory_local_files/destination-export/export-export_destinations-staging.csv';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/';
		$royal_path = 'http://www.royalcaribbean.co.za/destination-export/export/export.csv';
		$royal_stage_path = 'http://www.stage-royalcaribbean.co.za/destination-export/export/export.csv';
		$crystal_path =  'http://www.crystalyachtcruises.co.za/destination-export/export/export.csv';

		$file = 'destination-export-export-export_destinations.csv';

	This is not site specific, this uses the old destination entities to link them the way we want.
*/
		$items_url = 'https://www.cruises.co.za/destination-export/export/export.csv';
/*		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		} else if ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) {
			$items_url = $truserv_path . $file;
		} else if (substr($server_url, 0, 19) == 'stage.cruises.co.za') {
			$items_url = $truserv_staging_path;
		} else if ($server_url == 'stage-royalcaribbean.co.za') {
			$items_url = $royal_stage_path;
		} else if ($server_url == 'royalcaribbean.co.za') {
			$items_url = $royal_path;
		} else if ($server_url == 'crystalyachtcruises.co.za') {
			$items_url = $crystal_path;
		} else {
			$items_url = $truserv_path . $file;
		}
*/
		$csv_path = $items_url;
		$this->source = new MigrateSourceCSV($csv_path,
			$columns, array('delimiter' => ',', 'header_rows' => 1, 'track_changes' => 1));

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity', 'cruise_factory_destination_group');

		$this->softDependencies = array('CruiseFactoryDealsStage4');
		// $this->softDependencies = array('CruiseFactoryDealsStage3');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'destination_group_id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'destination_group_id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_destination_group')
		);

		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);

		$this->addFieldMapping('type')												->defaultValue('cruise_factory_destination_group');

		$this->addFieldMapping('id',														'destination_group_id');
		$this->addFieldMapping('uuid',													'destination_group_id');
		$this->addFieldMapping('title',													'destination_group');
		$this->addFieldMapping('field_page_title',							'destination_group');
		// $this->addFieldMapping('field_destination_group',				'destination_group');
		// $this->addFieldMapping('field_destination_specific',		'destination_specific');
		// $this->addFieldMapping('field_destination_specific_code','id');

		// $this->addFieldMapping('field_destination_photo',				'image');
		// $this->addFieldMapping('field_destination_photo:alt', 	'destination_group');
		// $this->addFieldMapping('field_destination_photo:title', 'destination_group');
		// $this->addFieldMapping('field_destination_photo:file_replace')->defaultValue('FILE_EXISTS_REPLACE');



	}

}
