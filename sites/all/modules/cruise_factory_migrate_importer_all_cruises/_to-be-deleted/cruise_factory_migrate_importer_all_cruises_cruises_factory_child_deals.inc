<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryChildDealsMigration extends Migration {

	public function preImport() {
		parent::preImport();
		module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
	}
	public function postImport() {
		parent::postImport();
		module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
	}
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}
		$id							= $row->id;
		$cruise_id			= $row->cruise_id;

		$row->migrate_map_sourceid1 	= $id;
		// $row->migrate_map_destid1 	= $id;
		// $row->xml->migrate_map_destid1 	= $id;

		$row->import_block = '';

		// watchdog('cruise_factory__child_deals', 'advert: ' . $row->advert_code);
		// watchdog('cruise_factory__child_deals', 'promo: ' . $row->promotion_name);

		if (($row->advert_code == '') || ($row->advert_code == '0')) {
			$row->advert_code = $row->promotion_name;
		}
		if (($row->promotion_name == '') || ($row->promotion_name == '0')) {
			$row->promotion_name = $row->advert_code;
		}

		$embark_port = strip_tags($row->embark_port);
		$row->embark_port = $embark_port;

		$title_edit 				=		$row->title . " - " . $row->sailing_date;
		$row->field_parent_or_child_deal = 'child';
		$cruise_id 					= 	$row->cruise_id;
		$row->title 				= 	$title_edit;

    $cruise_entity_id 					= db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_cruise_deal' AND uuid = :uuid", array(":uuid" => $cruise_id))->fetchAll();
    $cruise_entity_id 				= $cruise_entity_id[0]->id; // = e.g. 408

    $row->uuid 					= 	$cruise_entity_id;

    // $cruiseline_entity_id 					= db_query("SELECT field_cruise_line_id_target_id FROM {field_data_field_cruise_line_id} WHERE entity_id = :entity_id", array(":entity_id" => $cruise_entity_id))->fetchAll();
    // $cruiseline_entity_id 				= $cruiseline_entity_id[0]->field_cruise_line_id_target_id; // = e.g. 19270

	  //  $import_block 				= db_query("SELECT field_import_block_value FROM {field_data_field_import_block} WHERE entity_id = :entity_id", array(":entity_id" => $cruise_entity_id))->fetchAll();
	  //  $import_block 				= $import_block[0]->field_import_block_value; // = e.g.
	  //  if ($import_block == '1') {
				// watchdog('cruise_factory__child_deals', 'Import Block in Effect on ID: ' . $cruise_entity_id);
				// return false;
	  //  }


		// watchdog('cruise_factory__child_deals', 'sailing date: ' . $row->sailing_date);
		// watchdog('cruise_factory__child_deals', 'inside: ' . $row->inside_cabin);
		// watchdog('cruise_factory__child_deals', 'outside: ' . $row->outside_cabin);
		// watchdog('cruise_factory__child_deals', 'balcony: ' . $row->balcony);
		// watchdog('cruise_factory__child_deals', 'suite: ' . $row->suite);
		// watchdog('cruise_factory__child_deals', 'cabin_name: ' . $row->cabin_name);
		// watchdog('cruise_factory__child_deals', 'cruise_deal_price: ' . $row->cruise_deal_price);

		$sailing_date 			= (String) $row->sailing_date;
		$inside_cabin 			= (String) $row->inside_cabin;
		$outside_cabin 			= (String) $row->outside_cabin;
		$balcony 						= (String) $row->balcony;
		$suite 							= (String) $row->suite;
		$cabin_name 				= (String) $row->cabin_name;
		$cruise_deal_price 	= (String) $row->cruise_deal_price;


		$all_prices = array($inside_cabin, $outside_cabin, $balcony, $suite, $cruise_deal_price);
		if (isset($all_prices)) {
			if (array_filter($all_prices)) {
				$lowest_price = min(array_filter($all_prices));
			} else {
				$lowest_price = "";
			}
		} else {
			$lowest_price = "";
		}
		$row->start_price = $lowest_price;

		// watchdog('cruise_factory__child_deals', 'sailing date: ' . $sailing_date .  ' - inside: ' . $inside_cabin .  ' - outside: ' . $outside_cabin .  ' - balcony: ' . $balcony .  ' - suite: ' . $suite .  ' - cabin_name: ' . $cabin_name .  ' - cruise_deal_price: ' . $cruise_deal_price);


		if (
 		   (!$inside_cabin) &&
		   (!$outside_cabin) &&
		   (!$balcony) &&
		   (!$suite) &&
		   (!$row->cruise_deal_price)
		   ) {
				watchdog('cruise_factory__child_deals', 'NO PRICES for ID # ' . $row->id);
	 			return FALSE;
			}

		if (!$sailing_date) {
			watchdog('cruise_factory__child_deals', 'NO SAILING DATE for ID # ' . $row->id);
			return FALSE;
		}
		// watchdog('cruise_factory__child_deals', 'SAILING DATE: ' . $sailing_date);

		// watchdog('cruise_factory__child_deals', 'PRICES...' . " Inside_cabin: " . $inside_cabin . " - outside_cabin: " . $outside_cabin . " - balcony: " . $balcony . " - suite: " . $suite . " - cruise_deal_price: " . $cruise_deal_price);


		// Booleans
		$main_special 	= $row->main_special;
		$dest_special 	= $row->destination_special;
		$checked 				= $row->checked;
		$create_pdf 		= $row->create_pdf;
		$withdrawn 			= $row->withdrawn;
		$status 				= $row->status;
		$escorted 			= $row->escorted;
		$wedding 				= $row->wedding;
		$agent_only 		= $row->agent_only;
		$seniors 				= $row->seniors;
		$singles 				= $row->singles;

		if (!$main_special) {		$main_special = 0;		} else {		if ( ($main_special == 'Y') || ($main_special == 'y') || ($main_special == '1') ) {		$main_special = 1;		} else {	$main_special = 0;		}		}
		if (!$dest_special) {		$dest_special = '0';		} else {		if ( ($dest_special == 'Y') || ($dest_special == 'y') || ($dest_special == '1') ) {			$dest_special = '1';		} else { $dest_special = '0'; }		}
		if (!$checked) {		$checked = '0';		} else {		if ( ($checked == 'Y') || ($checked == 'y') || ($checked == '1') ) {			$checked = '1';		} else { $checked = '0'; }		}
		if (!$create_pdf) {		$create_pdf = '0';		} else {		if ( ($create_pdf == 'Y') || ($create_pdf == 'y') || ($create_pdf == '1') ) {			$create_pdf = '1';		} else { $create_pdf = '0'; }		}
		if (!$withdrawn) {		$withdrawn = '0';		} else {		if ( ($withdrawn == 'Y') || ($withdrawn == 'y') || ($withdrawn == '1') ) {			$withdrawn = '1';		} else { $withdrawn = '0'; }		}
		if (!$status) {		$status = '0';		} else {		if ( ($status == 'Y') || ($status == 'y') || ($status == '1') ) {			$status = '1';		} else { $status = '0'; }		}
		if (!$escorted) {		$escorted = '0';		} else {		if ( ($escorted == 'Y') || ($escorted == 'y') || ($escorted == '1') ) {			$escorted = '1';		} else { $escorted = '0'; }		}
		if (!$wedding) {		$wedding = '0';		} else {		if ( ($wedding == 'Y') || ($wedding == 'y') || ($wedding == '1') ) {			$wedding = '1';		} else { $wedding = '0'; }		}
		if (!$agent_only) {		$agent_only = '0';		} else {		if ( ($agent_only == 'Y') || ($agent_only == 'y') || ($agent_only == '1') ) {			$agent_only = '1';		} else { $agent_only = '0'; }		}
		if (!$seniors) {		$seniors = '0';		} else {		if ( ($seniors == 'Y') || ($seniors == 'y') || ($seniors == '1') ) {			$seniors = '1';		} else { $seniors = '0'; }		}
		if (!$singles) {		$singles = '0';		} else {		if ( ($singles == 'Y') || ($singles == 'y') || ($singles == '1') ) {			$singles = '1';		} else { $singles = '0'; }		}

		$row->main_special 		= $main_special;
		$row->destination_special 		= $dest_special;
		$row->checked 				= $checked;
		$row->create_pdf 			= $create_pdf;
		$row->withdrawn 			= $withdrawn;
		$row->status 					= $status;
		$row->escorted 				= $escorted;
		$row->wedding 				= $wedding;
		$row->agent_only 			= $agent_only;
		$row->seniors 				= $seniors;
		$row->singles 				= $singles;
		$row->cruise_id 			= $cruise_id;

	  // $i = 0;
	  // foreach ($row as $key => $value) {
	  // 	watchdog('cruise_factory__child_deals', "#" . $i . " " . $key . ' -> ' . $value);
	  // 	$i++;
	  // }

		// if (!$cruiseline_entity_id) {
		// 	watchdog('cruise_factory__child_deals', 'We have a cruiseline missing: ' . $cruiseline_entity_id);
		// 	return false;
		// } else {
		// 	watchdog('cruise_factory__child_deals', 'We have a cruiseline: ' . $cruiseline_entity_id);
		// }
	}

	public function __construct($arguments) {
		parent::__construct($arguments);

		$this->description = t("Import of Cruises Site Deal's Child Deals.");
		// $this->softDependencies = array('CruiseFactorySpecialsLeadPricing');
		$this->softDependencies = array('CruiseFactoryDealsStage8');

		$this->systemOfRecord = Migration::SOURCE;
		// $this->systemOfRecord = Migration::DESTINATION;

		$columns = array(
			0		=> array("uuid",										"uuid"),
			1		=> array("id",											"id"),
			2		=> array("title",										"title"),
			3		=> array("cruise_id",								"cruise_id"),
			4		=> array("destination_group",				"destination_group"),
			5		=> array("destination_specific",		"destination_specific"),
			6		=> array("special_id",							"special_id"),
			7		=> array("sailing_date",						"sailing_date"),
			8		=> array("inside_cabin",						"inside_cabin"),
			9		=> array("outside_cabin",						"outside_cabin"),
			10	=> array("balcony",									"balcony"),
			11	=> array("suite",										"suite"),
			12	=> array("cabin_name",							"cabin_name"),
			13	=> array("cabin_id",								"cabin_id"),
			14	=> array("cruise_deal_price",				"cruise_deal_price"),
			15	=> array("advert_code",							"advert_code"),
			16	=> array("agent_only",							"agent_only"),
			17	=> array("checked",									"checked"),
			18	=> array("create_pdf",							"create_pdf"),
			19	=> array("cruise_deal_image",				"cruise_deal_image"),
			20	=> array("cruise_description",			"cruise_description"),
			21	=> array("cruise_length",						"cruise_length"),
			22	=> array("cruise_length_with_nights","cruise_length_with_nights"),
			23	=> array("cruise_line_id",					"cruise_line_id"),
			24	=> array("cruise_name",							"cruise_name"),
			25	=> array("cruise_reference",				"cruise_reference"),
			26	=> array("destination_special",			"destination_special"),
			27	=> array("embark_port",							"embark_port"),
			28	=> array("escorted",								"escorted"),
			29	=> array("featured",								"featured"),
			30	=> array("import_block",						"import_block"),
			31	=> array("instructions",						"instructions"),
			32	=> array("main_special",						"main_special"),
			33	=> array("offer_detail",						"offer_detail"),
			34	=> array("price_type",							"price_type"),
			35	=> array("priority_id",							"priority_id"),
			36	=> array("seniors",									"seniors"),
			37	=> array("ship",										"ship"),
			38	=> array("singles",									"singles"),
			39	=> array("special_brief",						"special_brief"),
			40	=> array("special_header",					"special_header"),
			41	=> array("special_order",						"special_order"),
			42	=> array("special_text",						"special_text"),
			43	=> array("status",									"status"),
			44	=> array("type",										"type"),
			45	=> array("uploaded_pdf",						"uploaded_pdf"),
			46	=> array("Validity_date_end",				"Validity_date_end"),
			47	=> array("validity_date_start",			"validity_date_start"),
			48	=> array("wedding",									"wedding"),
			49	=> array("withdrawn",								"withdrawn"),
			50	=> array("booking_email",						"booking_email"),
			51	=> array("brief_description",				"brief_description"),
			52	=> array("deal_specifics",					"deal_specifics"),
			53	=> array("factory_id",							"factory_id"),
			54	=> array("price_suffix",						"price_suffix"),
			55	=> array("promotion_name",					"promotion_name"),
			56	=> array("terms_conditions",				"terms_conditions"),
			57	=> array("unpublish_date",					"unpublish_date"),
			58	=> array("special_entity_ref",			"special_entity_ref"),
			59	=> array("start_price",							"start_price"),

		);

		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
$server_url = str_replace('https://', '', $server_url);


		// $local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$local_path = 'http://www.local.cruises/cruise-deal-splitter/export/export.csv';
		// $truserv_path = '/var/www/html/cruise_factory_local_files/';
		$truserv_path = 'https://www.cruises.co.za/cruise-deal-splitter/export/export.csv';
		$truserv_staging_path = 'http://www.cruiseslive.co.za/cruise-deal-splitter/export/export.csv';
		$royal_path = 'http://www.royalcaribbean.co.za/cruise-deal-splitter/export/export.csv';
		$royal_stage_path = 'http://www.stage-royalcaribbean.co.za/cruise-deal-splitter/export/export.csv';
		$crystal_path =  'http://www.crystalyachtcruises.co.za/cruise-deal-splitter/export/export.csv';

		// $file = 'cruise-deal-splitter.csv';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path; // . $file;
		} else if ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) {
			$items_url = $truserv_path; // . $file;
		} else if (substr($server_url, 0, 19) == 'stage.cruises.co.za') {
			$items_url = $truserv_staging_path;
		} else if ($server_url == 'stage-royalcaribbean.co.za') {
			$items_url = $royal_stage_path;
		} else if ($server_url == 'royalcaribbean.co.za') {
			$items_url = $royal_path;
		} else if ($server_url == 'crystalyachtcruises.co.za') {
			$items_url = $crystal_path;
		} else {
			$items_url = 'https://' . $server_url . '/' . $file;
		}
		$csv_path = $items_url;

		$this->source = new MigrateSourceCSV($csv_path,
			$columns, array('delimiter' => ',', 'header_rows' => 1, 'track_changes' => 1));
		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_cruise_deal');
		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_cruise_deal')
		);

		$this->addFieldMapping('field_parent_or_child_deal')					->defaultValue('child');
		$this->addFieldMapping('type')																->defaultValue('cruise_factory_cruise_deal');
		$this->addFieldMapping('title',																'title');
		$this->addFieldMapping('id',																	'id')->sourceMigration('CruiseFactoryDealIDs');
		$this->addFieldMapping('uuid',																'uuid');
		$this->addFieldMapping('field_special_id',										'special_id');
		$this->addFieldMapping('field_cruise_id‎',											'cruise_id');
		$this->addFieldMapping('field_cruise_id_new‎',									'cruise_id');
		$this->addFieldMapping('field_cruise_line_id',								'cruise_line_id');
		$this->addFieldMapping('created')															->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('changed')															->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('field_factory_id',										'factory_id');
		$this->addFieldMapping('field_special_header',								'special_header');
		$this->addFieldMapping('field_special_text',									'special_text');
		$this->addFieldMapping('field_special_brief',									'special_brief');

		$this->addFieldMapping('field_destination_specific',					'destination_specific');
		$this->addFieldMapping('field_destination_group',							'destination_group');
		$this->addFieldMapping('field_cruise_description_new',				'cruise_description');
		$this->addFieldMapping('field_cruise_days',										'cruise_length');
		$this->addFieldMapping('field_cruise_length_with_nights',			'cruise_length_with_nights');
		$this->addFieldMapping('field_cruise_reference',							'cruise_reference');
		$this->addFieldMapping('field_ship',													'ship');
		$this->addFieldMapping('field_price_suffix',									'price_suffix');
		$this->addFieldMapping('field_deal_specifics',								'deal_specifics');
		$this->addFieldMapping('field_cruise_description',						'brief_description');
		$this->addFieldMapping('field_offer_detail_new',							'offer_detail');
		$this->addFieldMapping('field_price_type',										'price_type');
		$this->addFieldMapping('field_special_order',									'special_order');
		$this->addFieldMapping('field_featured',											'featured');
		$this->addFieldMapping('field_embark_port',										'embark_port');
		$this->addFieldMapping('field_special_entity_ref',						'special_entity_ref');
		$this->addFieldMapping('field_cruise_name',										'cruise_name');

		$this->addFieldMapping('field_instructions',									'instructions');
		$this->addFieldMapping('field_booking_email',									'booking_email');

		$this->addFieldMapping('field_sailing_date',					'sailing_date');
		$this->addFieldMapping('field_sailing_date:to',				'sailing_date');
		$this->addFieldMapping('field_sailing_date:timezone')	->defaultvalue('Africa/Johannesburg');

		$this->addFieldMapping('field_validity_date_end',							'Validity_date_end');
		$this->addFieldMapping('field_validity_date_end:to',					'Validity_date_end');
		$this->addFieldMapping('field_validity_date_end:timezone')		->defaultvalue('Africa/Johannesburg');

		$this->addFieldMapping('field_validity_date_start',						'validity_date_start');
		$this->addFieldMapping('field_validity_date_start:to',				'validity_date_start');
		$this->addFieldMapping('field_validity_date_start:timezone')	->defaultvalue('Africa/Johannesburg');

		$this->addFieldMapping('field_publish_date',									'validity_date_start');
		$this->addFieldMapping('field_publish_date:to',								'validity_date_start');
		$this->addFieldMapping('field_publish_date:timezone')					->defaultvalue('Africa/Johannesburg');
		$this->addFieldMapping('field_unpublish_date',								'Validity_date_end');
		$this->addFieldMapping('field_unpublish_date:to',							'Validity_date_end');
		$this->addFieldMapping('field_unpublish_date:timezone')				->defaultvalue('Africa/Johannesburg');

		$this->addFieldMapping('field_advert_code',										'advert_code');
		$this->addFieldMapping('field_promotion_name',								'advert_code')->separator(',');
		$this->addFieldMapping('field_promotion_name:source_type')		->defaultValue('name');
		$this->addFieldMapping('field_promotion_name:ignore_case')		->defaultValue(TRUE);
		$this->addFieldMapping('field_promotion_name:create_term')		->defaultValue(TRUE);


		$this->addFieldMapping('field_cruise_deal_image',							'cruise_deal_image');
		$this->addFieldMapping('field_cruise_deal_image:title',	'title');
		$this->addFieldMapping('field_cruise_deal_image:alt',	'title');
		$this->addFieldMapping('field_cruise_deal_image:file_replace')->defaultValue('FILE_EXISTS_REPLACE');
		$this->addFieldMapping('field_cruise_deal_image:preserve_files')->defaultValue(0);

		// Booleans
		$this->addFieldMapping('field_main_special',									'main_special');
		$this->addFieldMapping('field_dest_special',									'destination_special');
		$this->addFieldMapping('field_checked',												'checked');
		$this->addFieldMapping('field_create_pdf',										'create_pdf');
		$this->addFieldMapping('field_withdrawn',											'withdrawn');
		$this->addFieldMapping('field_status',												'status');
		$this->addFieldMapping('field_type',													'type');
		$this->addFieldMapping('field_escorted',											'escorted');
		$this->addFieldMapping('field_wedding',												'wedding');
		$this->addFieldMapping('field_agentonly',											'agent_only');
		$this->addFieldMapping('field_terms_conditions',							'terms_conditions');
		$this->addFieldMapping('field_seniors',												'seniors');
		$this->addFieldMapping('field_singles',												'singles');


		$this->addFieldMapping('field_inside_cabin',									'inside_cabin');
		$this->addFieldMapping('field_outside_cabin',									'outside_cabin');
		$this->addFieldMapping('field_balcony',												'balcony');
		$this->addFieldMapping('field_suite',													'suite');
		$this->addFieldMapping('field_cabin_name',										'cabin_name');
		$this->addFieldMapping('field_cruise_deal_price',							'cruise_deal_price');
		$this->addFieldMapping('field_start_price',										'start_price');



	}

}
