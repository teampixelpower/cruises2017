<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryCruisesStage4Migration extends XMLMigration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

		// find the Taxonomy Term ID based on the UUID
		$taxonomy_id = (String) $row->xml->port_id;
		$taxonomy_id = db_query("SELECT * FROM {taxonomy_term_data} WHERE uuid = :uuid", array(":uuid" => $taxonomy_id))->fetchAll();
    $taxonomy_id = $taxonomy_id[0]->name;
    // $taxonomy_id = '"' . $taxonomy_id . '"';


    // watchdog('cruise_factory_migrate__cruises_stage4', 'port_id: ' . (String) $row->xml->port_id);
    // watchdog('cruise_factory_migrate__cruises_stage4', 'taxonomy_id: ' . $taxonomy_id);

    $row->port_id = $taxonomy_id;
    $row->xml->port_id = $taxonomy_id;
    $row->uuid = $taxonomy_id;
    $row->xml->uuid = $taxonomy_id;


		// find the Entity ID based on the UUID
		$final_id = (String) $row->xml->cruise_id;
		$final_id = db_query("SELECT entity_id FROM {field_data_field_cruise_id_new} WHERE field_cruise_id_new_value = :field_cruise_id_new_value", array(":field_cruise_id_new_value" => $final_id))->fetchAll();
    $final_id = $final_id[0]->entity_id;

  	$row->cruise_id 						= $final_id;
  	$row->xml->cruise_id 				= $final_id;
   	$row->host_entity_id 				= $final_id;
   	$row->xml->host_entity_id 	= $final_id;

    if ($final_id) {
    	$row->cruise_id 					= $final_id;
    	$row->xml->cruise_id 			= $final_id;
    	$row->host_entity_id 			= $final_id;
    	$row->xml->host_entity_id = $final_id;
    	// watchdog('cruise_factory_migrate__cruises_stage4', 'final_id: ' . $final_id);
    } else {
			// return FALSE;
		}


	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryCruisesStage4Migration');

		$this->description = t('Import of Cruises - Itineraries field collection.');

		// $this->systemOfRecord = Migration::DESTINATION;
		$this->systemOfRecord = Migration::SOURCE;

		$fields = array(
			'id'										=>					t('id'),
			'cruise_id'							=>					t('cruise_id'),
			'day'										=>					t('day'),
			'port_id'								=>					t('port_id'),
			'arrive'								=>					t('arrive'),
			'depart'								=>					t('depart'),
			'portorder'							=>					t('portorder'),
			// 'host_entity_id'				=>					t('host_entity_id'),
		);


		$this->softDependencies = array('CruiseFactoryCruisesStage3');


		global $base_url;
$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/'; // 'http://197.189.205.34/cruise_factory_local_files/';

		$file = 'itineraries';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}

		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

		$this->destination = new MigrateDestinationFieldCollection(
      'field_itinerary_fieldcolle',
      array('host_entity_type' => 'cruise_factory_entity')
    );

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationFieldCollection::getKeySchema()
		);
		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);

		$this->addFieldMapping("host_entity_id",							'cruise_id')	->xpath('cruise_id');
		$this->addFieldMapping('field_cruise_id_new', 				'cruise_id')	->xpath('cruise_id');
		$this->addFieldMapping('item_id', 										'id')					->xpath('id');
		$this->addFieldMapping('field_id', 										'id')					->xpath('id');

		$this->addFieldMapping('field_itineraries_day', 			'day')				->xpath('day');
		$this->addFieldMapping('field_itineraries_arrive',		'arrive')			->xpath('arrive');
		$this->addFieldMapping('field_itinerary_depart', 			'depart')			->xpath('depart');
		$this->addFieldMapping('field_itineraries_port_order','portorder')	->xpath('portorder');

		$this->addFieldMapping('field_itineraries_port', 			'port_id')		->xpath('port_id')->separator(";");
		$this->addFieldMapping('uuid', 												'port_id')		->xpath('port_id');
		$this->addFieldMapping("field_itineraries_port:source_type")				->defaultValue('name');
		$this->addFieldMapping("field_itineraries_port:create_term")				->defaultValue('1');
		$this->addFieldMapping("field_itineraries_port:ignore_case")				->defaultValue(TRUE);


	}
}
