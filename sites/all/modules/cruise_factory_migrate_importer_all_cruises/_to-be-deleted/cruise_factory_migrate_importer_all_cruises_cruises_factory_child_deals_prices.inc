<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryChildDealsPricesMigration extends Migration {

	public function preImport() {
		parent::preImport();
		module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
	}
	public function postImport() {
		parent::postImport();
		module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
	}
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

		// $row->migrate_map_sourceid1 	= $id;
		// find the Entity ID based on the UUID
		$final_id = $row->id;
		$final_id = db_query("SELECT entity_id FROM {field_data_field_cruise_id_new} WHERE uuid = :uuid", array(":uuid" => $final_id))->fetchAll();
    $final_id = $final_id[0]->entity_id;

    watchdog('cruise_factory__child_deals_prices', 'final_id: ' . $final_id);
  	$row->final_id 						= $final_id;
  	$row->host_entity_id			= $final_id;

    $import_block 				= db_query("SELECT field_import_block_value FROM {field_data_field_import_block} WHERE entity_id = :entity_id", array(":entity_id" => $cruise_entity_id))->fetchAll();
    $import_block 				= $import_block[0]->field_import_block_value; // = e.g.

    if ($import_block == '1') {
			watchdog('cruise_factory__child_deals_prices', 'Import Block in Effect on ID: ' . $cruise_entity_id);
			return false;
    }

	}

	public function __construct($arguments) {
		parent::__construct($arguments);

		$this->description = t("Import of Cruises Site Deal's Child Deals' Prices.");
		$this->softDependencies = array('CruiseFactoryChildDeals');
		$this->systemOfRecord = Migration::SOURCE;
		// $this->systemOfRecord = Migration::DESTINATION;

		$columns = array(
			1		=> array("id",											"id"),
			7		=> array("sailing_date",						"sailing_date"),
			8		=> array("inside_cabin",						"inside_cabin"),
			9		=> array("outside_cabin",						"outside_cabin"),
			10	=> array("balcony",									"balcony"),
			11	=> array("suite",										"suite"),
			12	=> array("cabin_name",							"cabin_name"),
			14	=> array("cruise_deal_price",				"cruise_deal_price"),
			30	=> array("import_block",						"import_block"),
		);

		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
$server_url = str_replace('https://', '', $server_url);


		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$truserv_staging_path = '/var/www/html/cruise_factory_local_files/cruise-deal-splitter-staging.csv';
		$royal_path = 'http://www.royalcaribbean.co.za/cruise-deal-splitter/export/export.csv';
		$royal_stage_path = 'http://www.stage-royalcaribbean.co.za/cruise-deal-splitter/export/export.csv';
		$crystal_path = 'http://www.crystalyachtcruises.co.za/cruise-deal-splitter/export/export.csv';

		$file = 'cruise-deal-splitter.csv';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		} else if ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) {
			$items_url = $truserv_path . $file;
		} else if (substr($server_url, 0, 19) == 'stage.cruises.co.za') {
			$items_url = $truserv_staging_path;
		} else if ($server_url == 'stage-royalcaribbean.co.za') {
			$items_url = $royal_stage_path;
		} else if ($server_url == 'royalcaribbean.co.za') {
			$items_url = $royal_path;
		} else if ($server_url == 'crystalyachtcruises.co.za') {
			$items_url = $crystal_path;
		} else {
			$items_url = 'http://' . $server_url . '/' . $file;
		}
		$csv_path = $items_url;

		$this->source = new MigrateSourceCSV($csv_path,
			$columns, array('delimiter' => ',', 'header_rows' => 1, 'track_changes' => 1));

		$this->destination = new MigrateDestinationFieldCollection(
      'field_pricing_table',
      array('host_entity_type' => 'cruise_factory_entity')
    );
    $this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationFieldCollection::getKeySchema()
		);

		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("host_entity_id",							'id');

		$this->addFieldMapping('field_inside_cabin',					'inside_cabin');
		$this->addFieldMapping('field_outside_cabin',					'outside_cabin');
		$this->addFieldMapping('field_balcony',								'balcony');
		$this->addFieldMapping('field_suite',									'suite');
		$this->addFieldMapping('field_cabin_name',						'cabin_name');
		$this->addFieldMapping('field_cruise_deal_price',			'cruise_deal_price');

		$this->addFieldMapping('field_sailing_date',					'sailing_date');
		$this->addFieldMapping('field_sailing_date:to',				'sailing_date');
		$this->addFieldMapping('field_sailing_date:timezone')	->defaultvalue('Africa/Johannesburg');

	}

}
