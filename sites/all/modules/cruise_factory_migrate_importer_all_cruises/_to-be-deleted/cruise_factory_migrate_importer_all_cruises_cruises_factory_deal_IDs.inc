<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryDealIDsMigration extends XMLMigration {

	public function preImport() {
		parent::preImport();
		module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
	}
	public function postImport() {
		parent::postImport();
		module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
	}
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}


		$advert_code 		= (String)$row->xml->advert_code;

		if ($advert_code == '0') {
			$advert_code = '';
		}

		$row->xml->advert_code = $advert_code;
		$row->advert_code = $advert_code;
		if ($advert_code) {
			watchdog('cruise_factory__deal_IDs', 'promo code tracking: ' . $advert_code . ' for ID #' . (String)$row->xml->id);
		}

		$id							= (String) $row->xml->id;
		$cruise_id			= (String) $row->xml->cruise_id;

		$row->xml->migrate_map_sourceid1 		= $id;
		$row->migrate_map_sourceid1 				= $id;

		// Booleans
		$main_special 	= (String) $row->xml->main_special;
		$dest_special 	= (String) $row->xml->dest_special;
		$checked 				= (String) $row->xml->checked;
		$create_pdf 		= (String) $row->xml->create_pdf;
		$withdrawn 			= (String) $row->xml->withdrawn;
		$status 				= (String) $row->xml->status;
		$escorted 			= (String) $row->xml->escorted;
		$wedding 				= (String) $row->xml->wedding;
		$agentonly 			= (String) $row->xml->agentonly;
		$seniors 				= (String) $row->xml->seniors;
		$singles 				= (String) $row->xml->singles;

		if (!$main_special) {		$main_special = 0;		} else {		if ( ($main_special == 'Y') || ($main_special == 'y') || ($main_special == '1') ) {		$main_special = 1;		} else {	$main_special = 0;		}		}
		if (!$dest_special) {		$dest_special = '0';		} else {		if ( ($dest_special == 'Y') || ($dest_special == 'y') || ($dest_special == '1') ) {			$dest_special = '1';		} else { $dest_special = '0'; }		}
		if (!$checked) {		$checked = '0';		} else {		if ( ($checked == 'Y') || ($checked == 'y') || ($checked == '1') ) {			$checked = '1';		} else { $checked = '0'; }		}
		if (!$create_pdf) {		$create_pdf = '0';		} else {		if ( ($create_pdf == 'Y') || ($create_pdf == 'y') || ($create_pdf == '1') ) {			$create_pdf = '1';		} else { $create_pdf = '0'; }		}
		if (!$withdrawn) {		$withdrawn = '0';		} else {		if ( ($withdrawn == 'Y') || ($withdrawn == 'y') || ($withdrawn == '1') ) {			$withdrawn = '1';		} else { $withdrawn = '0'; }		}
		if (!$status) {		$status = '0';		} else {		if ( ($status == 'Y') || ($status == 'y') || ($status == '1') ) {			$status = '1';		} else { $status = '0'; }		}
		if (!$escorted) {		$escorted = '0';		} else {		if ( ($escorted == 'Y') || ($escorted == 'y') || ($escorted == '1') ) {			$escorted = '1';		} else { $escorted = '0'; }		}
		if (!$wedding) {		$wedding = '0';		} else {		if ( ($wedding == 'Y') || ($wedding == 'y') || ($wedding == '1') ) {			$wedding = '1';		} else { $wedding = '0'; }		}
		if (!$agentonly) {		$agentonly = '0';		} else {		if ( ($agentonly == 'Y') || ($agentonly == 'y') || ($agentonly == '1') ) {			$agentonly = '1';		} else { $agentonly = '0'; }		}
		if (!$seniors) {		$seniors = '0';		} else {		if ( ($seniors == 'Y') || ($seniors == 'y') || ($seniors == '1') ) {			$seniors = '1';		} else { $seniors = '0'; }		}
		if (!$singles) {		$singles = '0';		} else {		if ( ($singles == 'Y') || ($singles == 'y') || ($singles == '1') ) {			$singles = '1';		} else { $singles = '0'; }		}

		$row->xml->main_special 		= $main_special;
		$row->xml->dest_special 		= $dest_special;
		$row->xml->checked 					= $checked;
		$row->xml->create_pdf 			= $create_pdf;
		$row->xml->withdrawn 				= $withdrawn;
		$row->xml->status 					= $status;
		$row->xml->escorted 				= $escorted;
		$row->xml->wedding 					= $wedding;
		$row->xml->agentonly 				= $agentonly;
		$row->xml->seniors 					= $seniors;
		$row->xml->singles 					= $singles;
		$row->xml->cruise_id 				= $cruise_id;

		$cruise_id 									= (String) $row->xml->cruise_id;

    $cruise_entity_id 					= db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE uuid = :uuid", array(":uuid" => $cruise_id))->fetchAll();
    $cruise_entity_id 				= $cruise_entity_id[0]->id; // = e.g. 408

    $cruiseline_entity_id 					= db_query("SELECT field_cruise_line_id_target_id FROM {field_data_field_cruise_line_id} WHERE entity_id = :entity_id", array(":entity_id" => $cruise_entity_id))->fetchAll();
    $cruiseline_entity_id 				= $cruiseline_entity_id[0]->field_cruise_line_id_target_id; // = e.g. 19270

    $import_block 				= db_query("SELECT field_import_block_value FROM {field_data_field_import_block} WHERE entity_id = :entity_id", array(":entity_id" => $cruise_entity_id))->fetchAll();
    $import_block 				= $import_block[0]->field_import_block_value; // = e.g.

    if ($import_block == '1') {
			watchdog('cruise_factory_migrate__deal_IDs', 'Import Block in Effect on ID: ' . $cruise_entity_id);
			return false;
    }

    $row->title = $row->special_header;

		if (!$cruiseline_entity_id) {
			return false;
		}/* else {
			watchdog('cruise_factory___CruiseFactoryDealIDs', 'We have a cruiseline: ' . $cruiseline_entity_id);
		}*/
	}

	public function __construct($arguments) {
		parent::__construct($arguments);

		$this->description = t('Import of Cruises Site Deals IDs.');
		$this->softDependencies = array('CruiseFactoryCruisesStage4');
		$this->systemOfRecord = Migration::SOURCE;
		// $this->systemOfRecord = Migration::DESTINATION;

		$fields = array(
			'id'										=>			t('id'),
			'cruise_id'							=>			t('cruise_id'),
			'factory_id'						=>			t('factory_id'),
			'priority_id'						=>			t('priority_id'),
			'special_header'				=>			t('special_header'),
			'special_text'					=>			t('special_text'),
			'special_brief'					=>			t('special_brief'),
			'instructions'					=>			t('instructions'),
			'booking_email'					=>			t('booking_email'),
			'start_price'						=>			t('start_price'),
			'currency_id'						=>			t('currency_id'),
			'cruise_order'					=>			t('cruise_order'),
			'main_special'					=>			t('main_special'),
			'dest_special'					=>			t('dest_special'),
			'special_order'					=>			t('special_order'),
			'validity_date_end'			=>			t('validity_date_end'),
			'validity_date_start'		=>			t('validity_date_start'),
			'checked'								=>			t('checked'),
			'internal_notes'				=>			t('internal_notes'),
			'advert_code'						=>			t('advert_code'),
			'exchange_rate'					=>			t('exchange_rate'),
			'ex_rate_date'					=>			t('ex_rate_date'),
			'currency_id_ref'				=>			t('currency_id_ref'),
			'create_pdf'						=>			t('create_pdf'),
			'uploaded_pdf'					=>			t('uploaded_pdf'),
			'withdrawn'							=>			t('withdrawn'),
			'quicksave'							=>			t('quicksave'),
			'status'								=>			t('status'),
			'type'									=>			t('type'),
			'escorted'							=>			t('escorted'),
			'wedding'								=>			t('wedding'),
			'agentonly'							=>			t('agentonly'),
			'special_conditions'		=>			t('special_conditions'),
			'specialpdf_filename'		=>			t('specialpdf_filename'),
			'specialpdf_contents'		=>			t('specialpdf_contents'),
			'seniors'								=>			t('seniors'),
			'singles'								=>			t('singles'),
			'ship_id'								=>			t('ship_id'),
			'cruiseline_id'					=>			t('cruiseline_id'),
		);

		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
$server_url = str_replace('https://', '', $server_url);


		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/'; // 'http://197.189.205.34/cruise_factory_local_files/';

		$file = 'specials';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}

		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';
		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
		$this->source 	= new MigrateSourceMultiItems($items_class, $fields);
		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_cruise_deal');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_cruise_deal')
		);

		$this->addFieldMapping('type')																												->defaultValue('cruise_factory_cruise_deal');
		$this->addFieldMapping('id',																	'id')										->xpath('id');
		$this->addFieldMapping('uuid',																'id')										->xpath('id');
		$this->addFieldMapping('field_special_id',										'id')										->xpath('id');
		$this->addFieldMapping('field_cruise_id_new‎',									'cruise_id')						->xpath('cruise_id');
		$this->addFieldMapping('field_cruise_line_id',								'cruiseline_id')				->xpath('cruiseline_id');
		$this->addFieldMapping('field_cruise_reference',							'cruise_id')						->xpath('cruise_id')->sourceMigration('CruiseFactoryCruisesStage1');
		$this->addFieldMapping('created')																											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('changed')																											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('field_factory_id',										'factory_id')						->xpath('factory_id');
		$this->addFieldMapping('field_priority_id',										'priority_id')					->xpath('priority_id');
		$this->addFieldMapping('title',																'special_header')				->xpath('special_header');
		$this->addFieldMapping('field_special_header',								'special_header')				->xpath('special_header');
		$this->addFieldMapping('field_special_text',									'special_text')					->xpath('special_text');
		$this->addFieldMapping('field_special_brief',									'special_brief')				->xpath('special_brief');
		$this->addFieldMapping('field_instructions',									'instructions')					->xpath('instructions');
		$this->addFieldMapping('field_booking_email',									'booking_email')				->xpath('booking_email');
		$this->addFieldMapping('field_start_price‎',										'start_price')					->xpath('start_price');
		$this->addFieldMapping('field_special_order',									'special_order')				->xpath('special_order');
		$this->addFieldMapping('field_validity_date_end',							'validity_date_end')		->xpath('validity_date_end');
		$this->addFieldMapping('field_validity_date_end:to',					'validity_date_end')		->xpath('validity_date_end');
		$this->addFieldMapping('field_validity_date_end:timezone')														->defaultvalue('Africa/Johannesburg');

		$this->addFieldMapping('field_validity_date_start',						'validity_date_start')	->xpath('validity_date_start');
		$this->addFieldMapping('field_validity_date_start:to',				'validity_date_start')	->xpath('validity_date_start');
		$this->addFieldMapping('field_validity_date_start:timezone')													->defaultvalue('Africa/Johannesburg');

		$this->addFieldMapping('field_publish_date',									'validity_date_start')	->xpath('validity_date_start');
		$this->addFieldMapping('field_publish_date:to',								'validity_date_start')	->xpath('validity_date_start');
		$this->addFieldMapping('field_publish_date:timezone')																	->defaultvalue('Africa/Johannesburg');
		$this->addFieldMapping('field_unpublish_date',								'validity_date_end')		->xpath('validity_date_end');
		$this->addFieldMapping('field_unpublish_date:to',							'validity_date_end')		->xpath('validity_date_end');
		$this->addFieldMapping('field_unpublish_date:timezone')																->defaultvalue('Africa/Johannesburg');

		$this->addFieldMapping('field_advert_code',										'advert_code')					->xpath('advert_code')->separator(',');
		$this->addFieldMapping('field_promotion_name',								'advert_code')					->xpath('advert_code')->separator(',');
		$this->addFieldMapping('field_promotion_name:source_type')		->defaultValue('name');
		$this->addFieldMapping('field_promotion_name:ignore_case')		->defaultValue(TRUE);
		$this->addFieldMapping('field_promotion_name:create_term')		->defaultValue(TRUE);

		$this->addFieldMapping('field_uploaded_pdf',									'specialpdf_filename')	->xpath('specialpdf_filename');
		$this->addFieldMapping('field_uploaded_pdf:description',			'special_header')				->xpath('special_header');
		$this->addFieldMapping('field_uploaded_pdf:file_replace')	 														->defaultValue('FILE_EXISTS_REPLACE');
		$this->addFieldMapping('field_uploaded_pdf:preserve_files')														->defaultValue(0);

		// Booleans
		$this->addFieldMapping('field_main_special',									'main_special')					->xpath('main_special');
		$this->addFieldMapping('field_dest_special',									'dest_special')					->xpath('dest_special');
		$this->addFieldMapping('field_checked',												'checked')							->xpath('checked');
		$this->addFieldMapping('field_create_pdf',										'create_pdf')						->xpath('create_pdf');
		$this->addFieldMapping('field_withdrawn',											'withdrawn')						->xpath('withdrawn');
		$this->addFieldMapping('field_status',												'status')								->xpath('status');
		$this->addFieldMapping('field_type',													'type')									->xpath('type');
		$this->addFieldMapping('field_escorted',											'escorted')							->xpath('escorted');
		$this->addFieldMapping('field_wedding',												'wedding')							->xpath('wedding');
		$this->addFieldMapping('field_agentonly',											'agentonly')						->xpath('agentonly');
		$this->addFieldMapping('field_terms_conditions',							'special_conditions')		->xpath('special_conditions');
		$this->addFieldMapping('field_seniors',												'seniors')							->xpath('seniors');
		$this->addFieldMapping('field_singles',												'singles')							->xpath('singles');

	}

}
