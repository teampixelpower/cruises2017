<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryDealsNewPricesMigration extends Migration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));

	// The following importer relies on a CSV file that somehow is clinging to cache like the aformentioned tiny, evil, meercat
	// Define static var.
	// define('DRUPAL_ROOT', getcwd());
	// include_once('./includes/bootstrap.inc');
	// drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	// drupal_flush_all_caches();


  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

    $row->migrate_map_sourceid1 		= 		$row->id;

    $sailing_date = $row->sailing_date;
    $balcony = $row->balcony;
    $inside_cabin = $row->inside_cabin;
    $outside_cabin = $row->outside_cabin;
    $suite = $row->suite;
    $cabin_name = $row->cabin_name;

    $title = $row->title;


		if ($inside_cabin == '0.00') {
			$row->xml->inside_cabin = ''; // $inside;
		}
		if ($outside_cabin == '0.00') {
			$row->xml->outside_cabin = ''; // $outside;
		}
		if ($balcony == '0.00') {
			$row->xml->balcony = ''; // $balcony;
		}
		if ($suite == '0.00') {
			$row->xml->suite = ''; // $suite}
		}


    if (!$sailing_date) {
    	watchdog('cruise_factory__new_prices', 'empty sailing date: ' . $title);
    	return FALSE;
    }
    /*
    if (($balcony == '') && ($inside_cabin == '') &&  ($outside_cabin == '') &&  ($suite == '') && ($cabin_name == '')) {
    	watchdog('cruise_factory__new_prices', 'no prices AND no cabin name: ' . $title);
    	return FALSE;

    }*/


	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		$this->description = t('Import of Cruises Site Deals, pulling already imported price info. Inception.');
		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiseFactoryDealsStage8Migration');

		// $this->systemOfRecord = Migration::DESTINATION;
		$this->systemOfRecord = Migration::SOURCE;

		$columns = array(
			0 		=> 	array("UUID",										"UUID"),
			1 		=> 	array("id",											"id"),
			2 		=> 	array("title",									"title"),
			3 		=> 	array("sailing_date",						"sailing_date"),
			4 		=> 	array("balcony",								"balcony"),
			5 		=> 	array("inside_cabin",						"inside_cabin"),
			6 		=> 	array("outside_cabin",					"outside_cabin"),
			7 		=> 	array("suite",									"suite"),
			8 		=> 	array("cruise_deal_price_start","cruise_deal_price_start"),
			9 		=> 	array("destination_specific",		"destination_specific"),
			10 		=> 	array("cabin_name",							"cabin_name"),
			11 		=> 	array("cruise_deal_price",			"cruise_deal_price"),
		);

		// $this->softDependencies = array('CruiseFactorySpecialsSinglePricing');
		$this->softDependencies = array('CruiseFactorySpecialsLeadPricing');
		// $this->softDependencies = array('CruisesSiteDeals');

		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
$server_url = str_replace('https://', '', $server_url);

		$local_path = 'http://local.cruises/admin/deals-with-special-sailing-dates/export/export.csv';
		$truserv_path = 'https://www.cruises.co.za/admin/deals-with-special-sailing-dates/export/export.csv';
		$truserv_staging_path = 'http://dev.cruises.co.za/admin/deals-with-special-sailing-dates/export/export.csv';
		$cruisescoza_path = 'https://www.cruises.co.za/admin/deals-with-special-sailing-dates/export/export.csv';
		$royal_path = 'http://www.royalcaribbean.co.za/admin/deals-with-special-sailing-dates/export/export.csv';
		$royal_stage_path = 'http://www.stage-royalcaribbean.co.za/admin/deals-with-special-sailing-dates/export/export.csv';
		$crystal_path =  'http://www.crystalyachtcruises.co.za/admin/deals-with-special-sailing-dates/export/export.csv';

		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path; // . $file;
		} else if ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) {
			$items_url = $truserv_path; // . $file;
		} else if (substr($server_url, 0, 19) == 'stage.cruises.co.za') {
			$items_url = $truserv_staging_path;
		} else if ($server_url == 'stage-royalcaribbean.co.za') {
			$items_url = $royal_stage_path;
		} else if ($server_url == 'royalcaribbean.co.za') {
			$items_url = $royal_path;
		} else if ($server_url == 'crystalyachtcruises.co.za') {
			$items_url = $crystal_path;
		} else {
			$items_url = 'http://' . $server_url . '/admin/deals-with-special-sailing-dates/export/export.csv';
		}
		$csv_path = $items_url;
		$this->source = new MigrateSourceCSV($csv_path, $columns, array('delimiter' => ',', 'header_rows' => 1, 'track_changes' => 1));

		$this->destination = new MigrateDestinationFieldCollection('field_pricing_table',
      array('host_entity_type' => 'cruise_factory_entity')
    );

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationFieldCollection::getKeySchema()
		);
		$this->addFieldMapping('host_entity_id',						'id');
		$this->addFieldMapping('field_inside_cabin',				'inside_cabin');
		$this->addFieldMapping('field_outside_cabin',				'outside_cabin');
		$this->addFieldMapping('field_balcony',							'balcony');
		$this->addFieldMapping('field_suite',								'suite');
		$this->addFieldMapping('field_cruise_deal_price',		'cruise_deal_price');
		$this->addFieldMapping('field_cabin_name',					'cabin_name');
		$this->addFieldMapping('field_sailing_date',				'sailing_date');
		$this->addFieldMapping('field_sailing_date:to',			'sailing_date');
		$this->addFieldMapping('field_sailing_date:timezone')->defaultvalue('Africa/Johannesburg');


	}
}
