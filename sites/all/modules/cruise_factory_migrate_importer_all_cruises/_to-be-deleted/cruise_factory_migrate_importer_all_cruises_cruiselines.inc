<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiselinesMigration extends XMLMigration {

	public function preImport() {
    parent::preImport();
    // Code to execute before first article row is imported
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    // Code to execute before first article row is imported
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}
		$cruiseline_id = (String) $row->xml->id;
		$selected_cruiselines = variable_get('cruise_factory_migrate__selected_cruiseline_array');
		watchdog('cruise_factory__cruiselines', 'Cruiselines Array: ');
		watchdog('cruise_factory__cruiselines', $selected_cruiselines);
	  if ($selected_cruiselines) {
		  // $selected_cruiselines_name = variable_get('cruise_factory_migrate__selected_cruiselines');
			if (!in_array($cruiseline_id, $selected_cruiselines)) {
				// dpm("we didn't chose: " . $cruiseline_id);
				watchdog('cruise_factory__cruiselines', "we didn't chose: " . $cruiseline_id);
				return FALSE;
			}
	  }
	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		// watchdog('cruise_factory_migrate_importer_all_cruises', 'ran CruiselinesMigration');

		$this->description = t('Import of Cruise Lines.');
		$this->systemOfRecord = Migration::SOURCE;

		$fields = array(
			'id'										=>					t('id'),
			'name'									=>					t('name'),
			'location'							=>					t('location'),
			'booking_email'					=>					t('booking_email'),
			'brief_desc'						=>					t('brief_desc'),
			'company_bio'						=>					t('company_bio'),
			'url'										=>					t('url'),
			'video_url'							=>					t('video_url'),
			'star_rating'						=>					t('star_rating'),
		);

		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/'; // http://197.189.205.34/cruise_factory_local_files/';

global $base_url;
$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
// watchdog('cruise_factory_migrate_importer_all_cruises_cruiselines', 'server_url: ' . $server_url);

		$file = 'cruiselines';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		} else if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}
		// $items_url = "http://vitalitycruising.com/getXML.php?Table=cruiselines";

		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_cruiselines');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_cruiselines')
		);

		$this->addFieldMapping('type')												->defaultValue('cruise_factory_cruiselines');
		$this->addFieldMapping("changed")											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("created")											->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('uuid',												'name')->xpath('name');
		$this->addFieldMapping('id',													'id')->xpath('id');
		$this->addFieldMapping('field_id',										'id')->xpath('id');
		$this->addFieldMapping('field_cruiseline_id_code',		'id')->xpath('id');
		$this->addFieldMapping('title',												'name')->xpath('name');

	}

}
