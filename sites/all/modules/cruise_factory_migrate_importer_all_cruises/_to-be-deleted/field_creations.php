<?php

function create_field($entity_field_name, $field_type = 'text', $widget = 'text_textfield', $entity_bundle_name, $entity_type_name = 'cruise_factory_entity', $allowed_values = null) {
	// $entity_field_name
	// $field_type = 'text'
	// $widget = 'text_textfield'
	// $entity_bundle_name
	// $entity_type_name = 'cruise_factory_entity'
	if (!field_info_field($entity_field_name)) {
		watchdog('cruise_factory_create_new_entity', 'base field DOES NOT EXIST: ' . $entity_field_name);
		drupal_set_message('base field DOES NOT EXIST: ' . $entity_field_name);
		// Create field.
	  $field = array(
	    'field_name' => $entity_field_name,
	    'type' => $field_type,
	    'cardinality' => 1,
	    'translatable' => FALSE,
	  );
	  field_create_field($field);
	}
	watchdog('cruise_factory_create_new_entity', 'Creating instance of ' . $entity_field_name . ' on entity bundle ' . $entity_bundle_name);
	drupal_set_message('Creating instance of ' . $entity_field_name . ' on entity bundle ' . $entity_bundle_name);
	if (!$allowed_values) {
	  $instance = array(
	    'field_name' => $entity_field_name,
	    'entity_type' => $entity_type_name,
	    'bundle' => $entity_bundle_name,
	    'label' => 'field for ' . $entity_field_name,
	    'required' => FALSE,
	    'widget' => array('type' => $widget),
	  );
	} else {
		$instance = array(
	    'field_name' => $entity_field_name,
	    'entity_type' => $entity_type_name,
	    'bundle' => $entity_bundle_name,
	    'label' => 'field for ' . $entity_field_name,
	    'required' => FALSE,
	    'widget' => array('type' => $widget),
			'settings' => array('allowed_values' => array($allowed_values)),
	  );
	}
  field_create_instance($instance);
}
function create_all_fields() {

	// ##### base_amenities   //
	watchdog('field_creations', 'base_amenities field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_amenities');  
	create_field($entity_field_name = 'base_ship_id', $field_type = 'number_integer', $entity_bundle_name = 'base_amenities');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_amenities');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_cabins   //
	watchdog('field_creations', 'base_cabins field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_cabins');  
	create_field($entity_field_name = 'base_ship_id', $field_type = 'number_integer', $entity_bundle_name = 'base_cabins');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_cabins');  
	create_field($entity_field_name = 'base_description', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_cabins');  
	create_field($entity_field_name = 'base_image', $field_type = 'text', $entity_bundle_name = 'base_cabins');  
	create_field($entity_field_name = 'base_photo', $field_type = 'text', $entity_bundle_name = 'base_cabins');  
	create_field($entity_field_name = 'base_cabin_order', $field_type = 'number_integer', $entity_bundle_name = 'base_cabins');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_cruiselines   //
	watchdog('field_creations', 'base_cruiselines field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_cruiselines');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_cruiselines');  
	create_field($entity_field_name = 'base_location', $field_type = 'text', $entity_bundle_name = 'base_cruiselines');  
	create_field($entity_field_name = 'base_booking_email', $field_type = 'text', $entity_bundle_name = 'base_cruiselines');  
	create_field($entity_field_name = 'base_brief_desc', $field_type = 'text', $entity_bundle_name = 'base_cruiselines');  
	create_field($entity_field_name = 'base_company_bio', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_cruiselines');  
	create_field($entity_field_name = 'base_logosize', $field_type = 'text', $entity_bundle_name = 'base_cruiselines');  
	create_field($entity_field_name = 'base_logotype', $field_type = 'text', $entity_bundle_name = 'base_cruiselines');  
	create_field($entity_field_name = 'base_url', $field_type = 'text', $entity_bundle_name = 'base_cruiselines');  
	create_field($entity_field_name = 'base_video_url', $field_type = 'text', $entity_bundle_name = 'base_cruiselines');  
	create_field($entity_field_name = 'base_star_rating', $field_type = 'number_integer', $entity_bundle_name = 'base_cruiselines');  
	  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_cruises   //
	watchdog('field_creations', 'base_cruises field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_cruises');  
	create_field($entity_field_name = 'base_cruiseline_id', $field_type = 'number_integer', $entity_bundle_name = 'base_cruises');  
	create_field($entity_field_name = 'base_destination_id', $field_type = 'number_integer', $entity_bundle_name = 'base_cruises');  
	create_field($entity_field_name = 'base_ship_id', $field_type = 'number_integer', $entity_bundle_name = 'base_cruises');  
	create_field($entity_field_name = 'base_cruisetype_id', $field_type = 'number_integer', $entity_bundle_name = 'base_cruises');  
	create_field($entity_field_name = 'base_length', $field_type = 'number_integer', $entity_bundle_name = 'base_cruises');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_cruises');  
	create_field($entity_field_name = 'base_brief_description', $field_type = 'text', $entity_bundle_name = 'base_cruises');  
	create_field($entity_field_name = 'base_description', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_cruises');
	create_field($entity_field_name = 'base_photo', $field_type = 'text', $entity_bundle_name = 'base_cruises');
	create_field($entity_field_name = 'base_start_price', $field_type = 'number_decimal', $entity_bundle_name = 'base_cruises');
	create_field($entity_field_name = 'base_currency_id', $field_type = 'number_integer', $entity_bundle_name = 'base_cruises');
	create_field($entity_field_name = 'base_cruise_order', $field_type = 'number_integer', $entity_bundle_name = 'base_cruises');
	watchdog('field_creations', 'field creation complete');  
	// ##### base_cruisetypes   //
	watchdog('field_creations', 'base_cruisetypes field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_cruisetypes');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_cruisetypes');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_currencies   //
	watchdog('field_creations', 'base_currencies field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_currencies');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_currencies');  
	create_field($entity_field_name = 'base_description', $field_type = 'text', $entity_bundle_name = 'base_currencies');  
	create_field($entity_field_name = 'base_sign', $field_type = 'text', $entity_bundle_name = 'base_currencies');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_deckplans   //
	watchdog('field_creations', 'base_deckplans field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_deckplans');  
	create_field($entity_field_name = 'base_ship_id', $field_type = 'number_integer', $entity_bundle_name = 'base_deckplans');  
	create_field($entity_field_name = 'base_level', $field_type = 'text', $entity_bundle_name = 'base_deckplans');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_deckplans');  
	create_field($entity_field_name = 'base_image', $field_type = 'text', $entity_bundle_name = 'base_deckplans');  
	create_field($entity_field_name = 'base_colorcode', $field_type = 'text', $entity_bundle_name = 'base_deckplans');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_destinations   //
	watchdog('field_creations', 'base_destinations field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_destinations');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_destinations');  
	create_field($entity_field_name = 'base_description', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_destinations');  
	create_field($entity_field_name = 'base_image', $field_type = 'text', $entity_bundle_name = 'base_destinations');  
	create_field($entity_field_name = 'base_banner', $field_type = 'text', $entity_bundle_name = 'base_destinations');  
	create_field($entity_field_name = 'base_map_thumb', $field_type = 'text', $entity_bundle_name = 'base_destinations');  
	create_field($entity_field_name = 'base_map_large', $field_type = 'text', $entity_bundle_name = 'base_destinations');  
	create_field($entity_field_name = 'base_featured', $field_type = 'text', $entity_bundle_name = 'base_destinations');  
	create_field($entity_field_name = 'base_featured_text', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_destinations');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_dining   //
	watchdog('field_creations', 'base_dining field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_dining');  
	create_field($entity_field_name = 'base_cruiseline_id', $field_type = 'number_integer', $entity_bundle_name = 'base_dining');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_dining');  
	create_field($entity_field_name = 'base_introduction', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_dining');  
	create_field($entity_field_name = 'base_photo', $field_type = 'text', $entity_bundle_name = 'base_dining');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_diningtimes   //
	watchdog('field_creations', 'base_diningtimes field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_diningtimes');  
	create_field($entity_field_name = 'base_cruiseline_id', $field_type = 'number_integer', $entity_bundle_name = 'base_diningtimes');  
	create_field($entity_field_name = 'base_meal', $field_type = 'text', $entity_bundle_name = 'base_diningtimes');  
	create_field($entity_field_name = 'base_normal_sitting', $field_type = 'text', $entity_bundle_name = 'base_diningtimes');  
	create_field($entity_field_name = 'base_late_sitting', $field_type = 'text', $entity_bundle_name = 'base_diningtimes');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_facilities   //
	watchdog('field_creations', 'base_facilities field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_facilities');  
	create_field($entity_field_name = 'base_ship_id', $field_type = 'number_integer', $entity_bundle_name = 'base_facilities');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_facilities');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_specialsailingdates   //
	watchdog('field_creations', 'base_specialsailingdates field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialsailingdates');  
	create_field($entity_field_name = 'base_factory_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialsailingdates');  
	create_field($entity_field_name = 'base_special_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialsailingdates');  
	create_field($entity_field_name = 'base_sailingdate_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialsailingdates');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_specials   //
	watchdog('field_creations', 'base_specials field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_cruise_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_factory_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_priority_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_special_header', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_special_text', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_special_brief', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_instructions', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_booking_email', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_start_price', $field_type = 'number_decimal', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_currency_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_cruise_order', $field_type = 'number_integer', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_main_special', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_dest_special', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_special_order', $field_type = 'number_integer', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_validity_date_end', $field_type = 'datetime', $widget = 'list', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_validity_date_start', $field_type = 'datetime', $widget = 'list', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_checked', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_internal_notes', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_advert_code', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_exchange_rate', $field_type = 'number_decimal', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_ex_rate_date', $field_type = 'datetime', $widget = 'list', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_currency_id_ref', $field_type = 'number_integer', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_create_pdf', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_uploaded_pdf', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_withdrawn', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_quicksave', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_status', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_type', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_escorted', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_wedding', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_agentonly', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_special_conditions', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_specialpdf_filename', $field_type = 'text', $entity_bundle_name = 'base_specials');  
	create_field($entity_field_name = 'base_specialpdf_contents', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_specials');
	create_field($entity_field_name = 'base_seniors', $field_type = 'list_boolean', $widget = 'list', $entity_bundle_name = 'base_specials', $allowed_values = '"Yes, "No"');   
	create_field($entity_field_name = 'base_singles', $field_type = 'list_boolean', $widget = 'list', $entity_bundle_name = 'base_specials', $allowed_values = '"Yes, "No"');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_companionpricing   //
	watchdog('field_creations', 'base_companionpricing field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_companionpricing');  
	create_field($entity_field_name = 'base_special_id', $field_type = 'number_integer', $entity_bundle_name = 'base_companionpricing');  
	create_field($entity_field_name = 'base_price_inside', $field_type = 'number_decimal', $entity_bundle_name = 'base_companionpricing');  
	create_field($entity_field_name = 'base_price_outside', $field_type = 'number_decimal', $entity_bundle_name = 'base_companionpricing');  
	create_field($entity_field_name = 'base_price_balcony', $field_type = 'number_decimal', $entity_bundle_name = 'base_companionpricing');  
	create_field($entity_field_name = 'base_price_suites', $field_type = 'number_decimal', $entity_bundle_name = 'base_companionpricing');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_latlong   //
	watchdog('field_creations', 'base_latlong field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_latlong');  
	create_field($entity_field_name = 'base_port_id', $field_type = 'number_integer', $entity_bundle_name = 'base_latlong');  
	create_field($entity_field_name = 'base_lat', $field_type = 'number_float', $entity_bundle_name = 'base_latlong');  
	create_field($entity_field_name = 'base_long', $field_type = 'number_float', $entity_bundle_name = 'base_latlong');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_leadpricing   //
	watchdog('field_creations', 'base_leadpricing field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_leadpricing');  
	create_field($entity_field_name = 'base_special_id', $field_type = 'number_integer', $entity_bundle_name = 'base_leadpricing');  
	create_field($entity_field_name = 'base_price_inside', $field_type = 'number_decimal', $entity_bundle_name = 'base_leadpricing');  
	create_field($entity_field_name = 'base_price_outside', $field_type = 'number_decimal', $entity_bundle_name = 'base_leadpricing');  
	create_field($entity_field_name = 'base_price_balcony', $field_type = 'number_decimal', $entity_bundle_name = 'base_leadpricing');  
	create_field($entity_field_name = 'base_price_suites', $field_type = 'number_decimal', $entity_bundle_name = 'base_leadpricing');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_specialsmultipricing   //
	watchdog('field_creations', 'base_specialsmultipricing field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialsmultipricing');  
	create_field($entity_field_name = 'base_special_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialsmultipricing');  
	create_field($entity_field_name = 'base_sailingdate', $field_type = 'datetime', $widget = 'list', $entity_bundle_name = 'base_specialsmultipricing');  
	create_field($entity_field_name = 'base_inside', $field_type = 'text', $entity_bundle_name = 'base_specialsmultipricing');  
	create_field($entity_field_name = 'base_outside', $field_type = 'text', $entity_bundle_name = 'base_specialsmultipricing');  
	create_field($entity_field_name = 'base_balcony', $field_type = 'text', $entity_bundle_name = 'base_specialsmultipricing');  
	create_field($entity_field_name = 'base_suite', $field_type = 'text', $entity_bundle_name = 'base_specialsmultipricing');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_specialitineraries   //
	watchdog('field_creations', 'base_specialitineraries field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialitineraries');  
	create_field($entity_field_name = 'base_special_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialitineraries');  
	create_field($entity_field_name = 'base_day', $field_type = 'number_integer', $entity_bundle_name = 'base_specialitineraries');  
	create_field($entity_field_name = 'base_activity', $field_type = 'text', $entity_bundle_name = 'base_specialitineraries');  
	create_field($entity_field_name = 'base_starttime', $field_type = 'text', $entity_bundle_name = 'base_specialitineraries');  
	create_field($entity_field_name = 'base_endtime', $field_type = 'text', $entity_bundle_name = 'base_specialitineraries');  
	create_field($entity_field_name = 'base_type', $field_type = 'list_text', $entity_bundle_name = 'base_specialitineraries'); // ???  ('pre','post')  
	create_field($entity_field_name = 'base_order', $field_type = 'number_integer', $entity_bundle_name = 'base_specialitineraries');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_specialspricing   //
	watchdog('field_creations', 'base_specialspricing field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialspricing');  
	create_field($entity_field_name = 'base_factory_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialspricing');  
	create_field($entity_field_name = 'base_special_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialspricing');  
	create_field($entity_field_name = 'base_cruise_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialspricing');  
	create_field($entity_field_name = 'base_cabin_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialspricing');  
	create_field($entity_field_name = 'base_price', $field_type = 'number_decimal', $entity_bundle_name = 'base_specialspricing');  
	create_field($entity_field_name = 'base_portcharges', $field_type = 'text', $entity_bundle_name = 'base_specialspricing');  
	create_field($entity_field_name = 'base_currency_id', $field_type = 'number_integer', $entity_bundle_name = 'base_specialspricing');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_itineraries   //
	watchdog('field_creations', 'base_itineraries field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_itineraries');  
	create_field($entity_field_name = 'base_cruise_id', $field_type = 'number_integer', $entity_bundle_name = 'base_itineraries');  
	create_field($entity_field_name = 'base_day', $field_type = 'number_integer', $entity_bundle_name = 'base_itineraries');  
	create_field($entity_field_name = 'base_port_id', $field_type = 'number_integer', $entity_bundle_name = 'base_itineraries');  
	create_field($entity_field_name = 'base_arrive', $field_type = 'text', $entity_bundle_name = 'base_itineraries');  
	create_field($entity_field_name = 'base_depart', $field_type = 'text', $entity_bundle_name = 'base_itineraries');  
	create_field($entity_field_name = 'base_portorder', $field_type = 'number_integer', $entity_bundle_name = 'base_itineraries');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_kidsschedules   //
	watchdog('field_creations', 'base_kidsschedules field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_kidsschedules');  
	create_field($entity_field_name = 'base_cruiseline_id', $field_type = 'number_integer', $entity_bundle_name = 'base_kidsschedules');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_kidsschedules');  
	create_field($entity_field_name = 'base_description', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_kidsschedules');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_kidsprograms   //
	watchdog('field_creations', 'base_kidsprograms field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_kidsprograms');  
	create_field($entity_field_name = 'base_cruiseline_id', $field_type = 'number_integer', $entity_bundle_name = 'base_kidsprograms');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_kidsprograms');  
	create_field($entity_field_name = 'base_description', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_kidsprograms');  
	create_field($entity_field_name = 'base_photo', $field_type = 'text', $entity_bundle_name = 'base_kidsprograms');  
	create_field($entity_field_name = 'base_photo_order', $field_type = 'number_integer', $entity_bundle_name = 'base_kidsprograms');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_menus   //
	watchdog('field_creations', 'base_menus field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_menus');  
	create_field($entity_field_name = 'base_cruiseline_id', $field_type = 'number_integer', $entity_bundle_name = 'base_menus');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_menus');  
	create_field($entity_field_name = 'base_description', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_menus');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_months   //
	watchdog('field_creations', 'base_months field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_months');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_months');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_ports   //
	watchdog('field_creations', 'base_ports field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_ports');  
	create_field($entity_field_name = 'base_destination_id', $field_type = 'number_integer', $entity_bundle_name = 'base_ports');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_ports');  
	create_field($entity_field_name = 'base_description', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_ports');  
	create_field($entity_field_name = 'base_photo', $field_type = 'text', $entity_bundle_name = 'base_ports');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_priceguide   //
	watchdog('field_creations', 'base_priceguide field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_priceguide');  
	create_field($entity_field_name = 'base_sailing_id', $field_type = 'number_integer', $entity_bundle_name = 'base_priceguide');  
	create_field($entity_field_name = 'base_inside_cabin', $field_type = 'number_float', $entity_bundle_name = 'base_priceguide');  
	create_field($entity_field_name = 'base_outside_cabin', $field_type = 'number_float', $entity_bundle_name = 'base_priceguide');  
	create_field($entity_field_name = 'base_balcony', $field_type = 'number_float', $entity_bundle_name = 'base_priceguide');  
	create_field($entity_field_name = 'base_suite', $field_type = 'number_float', $entity_bundle_name = 'base_priceguide');  
	create_field($entity_field_name = 'base_exchange_rate', $field_type = 'number_float', $entity_bundle_name = 'base_priceguide');  
	create_field($entity_field_name = 'base_last_update', $field_type = 'datetime', $widget = 'list', $entity_bundle_name = 'base_priceguide');  
	create_field($entity_field_name = 'base_update_name', $field_type = 'text', $entity_bundle_name = 'base_priceguide');  
	create_field($entity_field_name = 'base_currency', $field_type = 'text', $entity_bundle_name = 'base_priceguide');  
	create_field($entity_field_name = 'base_factory_id', $field_type = 'number_integer', $entity_bundle_name = 'base_priceguide');  
	create_field($entity_field_name = 'base_cruiseline_id', $field_type = 'number_integer', $entity_bundle_name = 'base_priceguide');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_sailingdates   //
	watchdog('field_creations', 'base_sailingdates field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_sailingdates');  
	create_field($entity_field_name = 'base_cruise_id', $field_type = 'number_integer', $entity_bundle_name = 'base_sailingdates');  
	create_field($entity_field_name = 'base_sailingdate', $field_type = 'datetime', $widget = 'list', $entity_bundle_name = 'base_sailingdates');  
	create_field($entity_field_name = 'base_embarkport_id', $field_type = 'number_integer', $entity_bundle_name = 'base_sailingdates');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_seasons   //
	watchdog('field_creations', 'base_seasons field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_seasons');  
	create_field($entity_field_name = 'base_destination_id', $field_type = 'number_integer', $entity_bundle_name = 'base_seasons');  
	create_field($entity_field_name = 'base_start_month', $field_type = 'number_integer', $entity_bundle_name = 'base_seasons');  
	create_field($entity_field_name = 'base_end_month', $field_type = 'number_integer', $entity_bundle_name = 'base_seasons');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_seasons');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_shipphotos   //
	watchdog('field_creations', 'base_shipphotos field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_shipphotos');  
	create_field($entity_field_name = 'base_ship_id', $field_type = 'number_integer', $entity_bundle_name = 'base_shipphotos');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_shipphotos');  
	create_field($entity_field_name = 'base_description', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_shipphotos');  
	create_field($entity_field_name = 'base_photo', $field_type = 'text', $entity_bundle_name = 'base_shipphotos');  
	create_field($entity_field_name = 'base_photo_order', $field_type = 'number_integer', $entity_bundle_name = 'base_shipphotos');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_ships   //
	watchdog('field_creations', 'base_ships field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_cruiseline_id', $field_type = 'number_integer', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_thumbnail', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_mainimage', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_maidenvoyage', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_refurbished', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_tonnage', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_length', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_beam', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_draft', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_speed', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_ship_rego', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_pass_capacity', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_pass_space', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_crew_size', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_nat_crew', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_nat_officers', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_nat_dining', $field_type = 'text', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_description', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_star_rating', $field_type = 'number_integer', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_cruisetype_id', $field_type = 'number_integer', $entity_bundle_name = 'base_ships');  
	create_field($entity_field_name = 'base_currency_id', $field_type = 'number_integer', $entity_bundle_name = 'base_ships');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_starratings   //
	watchdog('field_creations', 'base_starratings field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_starratings');  
	create_field($entity_field_name = 'base_rating', $field_type = 'text', $entity_bundle_name = 'base_starratings');  
	create_field($entity_field_name = 'base_order', $field_type = 'number_integer', $entity_bundle_name = 'base_starratings');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_tipping   //
	watchdog('field_creations', 'base_tipping field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_tipping');  
	create_field($entity_field_name = 'base_cruiseline_id', $field_type = 'number_integer', $entity_bundle_name = 'base_tipping');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_tipping');  
	create_field($entity_field_name = 'base_description', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_tipping');  
	watchdog('field_creations', 'field creation complete');  
	// ##### base_winelists   //
	watchdog('field_creations', 'base_winelists field creations begins');  
	create_field($entity_field_name = 'base_id', $field_type = 'number_integer', $entity_bundle_name = 'base_winelists');  
	create_field($entity_field_name = 'base_cruiseline_id', $field_type = 'number_integer', $entity_bundle_name = 'base_winelists');  
	create_field($entity_field_name = 'base_name', $field_type = 'text', $entity_bundle_name = 'base_winelists');  
	create_field($entity_field_name = 'base_description', $field_type = 'text_long', $widget = 'textarea', $entity_bundle_name = 'base_winelists');  
	watchdog('field_creations', 'field creation complete');
}
