<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactorySpecialsLeadPricingMigration extends XMLMigration {

	public function preImport() {
    parent::preImport();
    module_disable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function postImport() {
    parent::postImport();
    module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
  }
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

		$special_sailing_id = (String) $row->xml->id;

		$price_inside 				= (String) $row->xml->price_inside;
		$price_outside 				= (String) $row->xml->price_outside;
		$price_balcony 				= (String) $row->xml->price_balcony;
		$price_suite 					= (String) $row->xml->price_suite;


		if ($price_inside == '0.00') {
			$row->xml->price_inside = ''; // $price_inside;
		}
		if ($price_outside == '0.00') {
			$row->xml->price_outside = ''; // $price_outside;
		}
		if ($price_balcony == '0.00') {
			$row->xml->price_balcony = ''; // $price_balcony;
		}
		if ($price_suite == '0.00') {
			$row->xml->price_suite = ''; // $price_suite}
		}


    $row->xml->migrate_map_sourceid1 	= $special_sailing_id;
    $row->migrate_map_sourceid1 			= $special_sailing_id;

		$special_id = (String) $row->xml->special_id;

		$special_final_id = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_cruise_deal' AND uuid = :uuid", array(":uuid" => $special_id))->fetchAll();
    $special_final_id 						= $special_final_id[0]->id;

    if (!$special_final_id) {
    	watchdog('cruise_factory_migrate__leadpricing', 'No Special ID found for: ' . $row->id);
    	return FALSE;
    }

    // special_id = 66706
    // special_final_id = 49248
    // sailing_date_entity = 51099

		$sailing_date_entity = db_query("SELECT entity_id FROM {field_data_field_special_entity_ref} WHERE  field_special_entity_ref_target_id = :field_special_entity_ref_target_id", array(":field_special_entity_ref_target_id" => $special_final_id))->fetchAll();
		$sailing_date_entity 						= $sailing_date_entity[0]->entity_id;



$sailingdate = db_query("SELECT field_sailing_date_value FROM {field_data_field_sailing_date} WHERE bundle = 'cruise_factory_specialsail_dates' AND entity_id = :entity_id", array(":entity_id" => $sailing_date_entity))->fetchAll();
$sailingdate 						= $sailingdate[0]->field_sailing_date_value;


// watchdog('cruise_factory_migrate__leadpricing', ' special_id: ' . $special_id . ' special_final_id: ' . $special_final_id . ' sailing_date_entity: ' . $sailing_date_entity);
// 	watchdog('cruise_factory_migrate__leadpricing', "sailing_date_entity = SELECT entity_id FROM field_data_field_special_entity_ref WHERE  field_special_entity_ref_target_id = '" . $special_final_id . "'");
// 	watchdog('cruise_factory_migrate__leadpricing', "sailingdate = SELECT field_sailing_date_value FROM field_data_field_sailing_date WHERE bundle = 'cruise_factory_specialsail_dates' AND entity_id = '" . $special_final_id . "'");

	  $row->xml->sailingdate = $sailingdate;
	  $sailingdate_clean = str_replace(" 00:00:00", "", $sailingdate);

	  $row->sailingdate_entity_ref = $sailingdate_final_id;
	  $row->special_id_entity_ref = $special_final_id;
	  $row->xml->sailingdate_entity_ref = $sailingdate_final_id;
	  $row->xml->special_id_entity_ref = $special_final_id;

	  $row->sailingdate = $sailingdate;
		$title = 'Lead-Price for #' . $special_id . ' on ' . $sailingdate_clean;
		$row->cruise_name =  $title;

		if ( ($price_balcony == '') && ($price_inside_cabin == '') &&  ($price_outside_cabin == '') &&  ($price_suite == '') ) {
    	watchdog('cruise_factory_migrate__leadpricing', 'no prices AND no cabin name: ' . $title);
    	return FALSE;
    }


	}

	public function __construct($arguments) {
  	parent::__construct($arguments);

		$this->description = t('Import of Sailing Dates.');

		$this->systemOfRecord = Migration::SOURCE;

		$fields = array(
			'id'											=>		t('id'),
			'special_id'							=>		t('special_id'),
			'price_inside'						=>		t('price_inside'),
			'price_outside'						=>		t('price_outside'),
			'price_balcony'						=>		t('price_balcony'),
			'price_suite'							=>		t('price_suite'),
			// these don't exist, link it
			'cruise_name'							=>		t('cruise_name'),
			'sailingdate' 						=>		t('sailingdate'),
			'sailingdate_entity_ref' 	=>		t('sailingdate_entity_ref'),
			'special_id_entity_ref' 	=>		t('special_id_entity_ref'),

		);

		$this->softDependencies = array('CruiseFactorySpecialsMultiPricing');

		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
$server_url = str_replace('https://', '', $server_url);
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$cruisescoza_path = '/usr/home/cruismskez/public_html/cruise_factory_local_files/';
		$file = 'leadpricing';
		$items_url = '';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$items_url = $local_path . $file;
		}
		if ( ((substr($server_url, 0, 13) == 'cruises.co.za') || (substr($server_url, 0, 17) == 'dev.cruises.co.za')) || (substr($server_url, 0, 19) == 'stage.cruises.co.za') ) {
			$items_url = $truserv_path . $file;
		} else {
			$items_url = $truserv_path . $file;
		}
		$item_xpath = '/database/table/row';
		$item_ID_xpath = 'id';

		$items_class 		= new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source 	= new MigrateSourceMultiItems($items_class, $fields);

		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_specialsmultipric');

		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_specialsmultipric')
		);

		$this->addFieldMapping("changed")									->defaultValue(REQUEST_TIME);
		$this->addFieldMapping("created")									->defaultValue(REQUEST_TIME);
		$this->addFieldMapping('type')										->defaultValue('cruise_factory_specialsmultipric');
		$this->addFieldMapping('uuid',									 	'id')								->xpath('id');
		$this->addFieldMapping('id',									 		'id')								->xpath('id');
		$this->addFieldMapping('field_id',							 	'id')								->xpath('id');
		$this->addFieldMapping('field_inside_cabin',			'price_inside')			->xpath('price_inside');
		$this->addFieldMapping('field_outside_cabin',			'price_outside')		->xpath('price_outside');
		$this->addFieldMapping('field_balcony',						'price_balcony')		->xpath('price_balcony');
		$this->addFieldMapping('field_suite',							'price_suite')			->xpath('price_suite');

		$this->addFieldMapping('title',								 		'cruise_name');
		$this->addFieldMapping('field_special_id', 				'special_id')								->xpath('special_id');

		// $this->addFieldMapping('field_special_entity_ref', 'special_id_entity_ref');
		// $this->addFieldMapping('field_sailing_entity_ref‎', 'sailingdate_entity_ref');

		$this->addFieldMapping('field_special_entity_ref', 'special_id_entity_ref');
		$this->addFieldMapping('field_sailing_entity_ref‎', 'sailingdate_entity_ref');

		$this->addFieldMapping('field_sailing_date',					'sailingdate')	->xpath('sailingdate');
		$this->addFieldMapping('field_sailing_date:to',				'sailingdate')	->xpath('sailingdate');
		$this->addFieldMapping('field_sailing_date:timezone')	->defaultvalue('Africa/Johannesburg');


	}
}
