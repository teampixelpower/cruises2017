<?php
/**
 * @file
 * cruise_factory_migrate_importer_all_cruises.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function cruise_factory_migrate_importer_all_cruises_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_panels_entity_panels_enabled';
  $strongarm->value = array(
    'node' => array(
      'page' => array(),
      'webform' => array(),
      'marketing_collateral_youtube_vid' => array(
        'default' => TRUE,
      ),
    ),
    'cruise_factory_entity' => array(
      'cruise_factory_cruiselines' => array(
        'teaser' => TRUE,
        'default' => TRUE,
      ),
      'cruise_factory_destination' => array(
        'teaser' => TRUE,
        'default' => TRUE,
      ),
      'cruise_factory_destination_group' => array(
        'teaser' => TRUE,
        'default' => TRUE,
        'full' => TRUE,
      ),
      'cruise_factory_ships' => array(
        'teaser' => TRUE,
      ),
      'cruise_factory_complete_deal' => array(
        'teaser' => TRUE,
        'wider_teaser' => TRUE,
        'full' => TRUE,
      ),
      'cruise_factory_complete_cruise' => array(
        'teaser' => TRUE,
        'full' => TRUE,
      ),
      'base_destinations' => array(
        'teaser' => TRUE,
      ),
      'base_destination_group' => array(
        'teaser' => TRUE,
        'default' => TRUE,
      ),
    ),
    'cruiselines' => array(
      'cruiselines' => array(
        'default' => TRUE,
        'tiny_logo' => TRUE,
        'teaser' => TRUE,
        'full' => TRUE,
      ),
    ),
    'destinations' => array(
      'destinations' => array(
        'teaser' => TRUE,
        'destination_images' => TRUE,
      ),
    ),
    'destination_group' => array(
      'destination_group' => array(
        'teaser' => TRUE,
        'default' => TRUE,
      ),
    ),
    'ports' => array(
      'ports' => array(
        'teaser' => TRUE,
      ),
    ),
    'ships' => array(
      'ships' => array(
        'default' => TRUE,
        'teaser' => TRUE,
        'larger_ship_image' => TRUE,
        'small_image' => TRUE,
      ),
    ),
  );
  $export['entity_panels_entity_panels_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_panels_entity_types';
  $strongarm->value = array(
    'cruiselines' => 'cruiselines',
    'cruises' => 'cruises',
    'cruisetypes' => 'cruisetypes',
    'cruise_factory_entity' => 'cruise_factory_entity',
    'currencies' => 'currencies',
    'destinations' => 'destinations',
    'destination_group' => 'destination_group',
    'itineraries' => 'itineraries',
    'ports' => 'ports',
    'priceguide' => 'priceguide',
    'sailingdates' => 'sailingdates',
    'shipphotos' => 'shipphotos',
    'ships' => 'ships',
    'node' => 'node',
    'entityform' => 0,
    'entityqueue_subqueue' => 0,
    'field_collection_item' => 0,
    'profile2' => 0,
    'quiz_result' => 0,
    'quiz_result_answer' => 0,
    'taxonomy_term' => 0,
    'user' => 0,
    'rules_config' => 0,
  );
  $export['entity_panels_entity_types'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_cruise_factory_entity__cruise_factory_complete_cruise';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => TRUE,
      ),
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'wider_teaser' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(
        'title' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'full' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'wider_teaser' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
        'created' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'full' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'wider_teaser' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
        'changed' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'full' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'wider_teaser' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
        'uuid' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'full' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'wider_teaser' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_cruise_factory_entity__cruise_factory_complete_cruise'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_cruise_factory_entity__cruise_factory_complete_deal';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => TRUE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'wider_teaser' => array(
        'custom_settings' => TRUE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(
        'title' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'wider_teaser' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'full' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
        'created' => array(
          'default' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
          'wider_teaser' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
          'full' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
        ),
        'changed' => array(
          'default' => array(
            'weight' => '3',
            'visible' => TRUE,
          ),
          'wider_teaser' => array(
            'weight' => '3',
            'visible' => TRUE,
          ),
          'full' => array(
            'weight' => '3',
            'visible' => TRUE,
          ),
        ),
        'uuid' => array(
          'default' => array(
            'weight' => '4',
            'visible' => TRUE,
          ),
          'wider_teaser' => array(
            'weight' => '4',
            'visible' => TRUE,
          ),
          'full' => array(
            'weight' => '4',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_cruise_factory_entity__cruise_factory_complete_deal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_cruise_factory_entity_cruise_factory_complete_cruise_pattern';
  $strongarm->value = 'cruises/[cruise_factory_entity:title]';
  $export['pathauto_cruise_factory_entity_cruise_factory_complete_cruise_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_cruise_factory_entity_cruise_factory_complete_deal_pattern';
  $strongarm->value = 'cruise-deals/[cruise_factory_entity:title]';
  $export['pathauto_cruise_factory_entity_cruise_factory_complete_deal_pattern'] = $strongarm;

  return $export;
}
