<?php
/**
 * @file
 * cruise_factory_migrate_importer_all_cruises.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cruise_factory_migrate_importer_all_cruises_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
