<?php
/**
 * @file
 *   Cruise Factory Migrate Importer.
 *   cruise_factory_migrate_importer_all_cruises.module
 */

class CruiseFactoryFinalCruisesMigration extends Migration {

	public function preImport() {
		parent::preImport();
		module_enable(array('pathauto', 'pathauto_entity'));
		module_disable(array('auto_alt'));
	}
	public function postImport() {
		parent::postImport();
		module_enable(array('pathauto', 'auto_alt', 'pathauto_entity'));
	}
	public function prepareRow($row) {
		if (parent::prepareRow($row) === FALSE) {
			return FALSE;
		}

		if ($row->cruise_id == 'cruise_id') {
			return FALSE;
		}
		if ($row->cruise_id == '67513') {
			foreach ($row as $row_key => $each_row) {
   			watchdog('final_cruise_importer', $row_key . ' = ' . $each_row);
   			dpm($row_key . ' = ' . $each_row);
   		}
		}




			$cruise_id 											=		$row->cruise_id;
			$base_id 												=		$row->base_id;
			$base_ship_id 									=		$row->base_ship_id;
			$title 													=		$row->title;
			$cruise_factory_entity_type 		=		$row->cruise_factory_entity_type;
			$base_ship 											=		$row->base_ship;
			$base_cruiseline 								=		$row->base_cruiseline;
			$base_destinations 							=		$row->base_destinations;
			$destination_group 							=		$row->destination_group;
			$base_itineraries 							=		$row->base_itineraries;
			$base_cruise_length 						=		$row->base_cruise_length;
			$base_name	 										=		$row->base_name;
			$base_photo 										=		$row->base_photo;
			$base_start_price 							=		$row->base_start_price;
			$ship_star_rating 							=		$row->ship_star_rating;
			$ship_thumb 										=		$row->ship_thumb;
			$embarkport 										=		$row->embarkport;

$base_photo = preg_replace("/<img/", "", $base_photo);
$base_photo = preg_replace("/typeof=\"foaf:Image\"/", "", $base_photo);
$base_photo = preg_replace("/class=\"img-responsive\"/", "", $base_photo);
$base_photo = preg_replace("/src=\"/", "", $base_photo);
$base_photo = preg_replace("/height=\"60\"/", "", $base_photo);
$base_photo = preg_replace("/width=\"60\"/", "", $base_photo);
$base_photo = preg_replace("/\/>/", "", $base_photo);
$base_photo = preg_replace("/\"/", "", $base_photo);
$base_photo = str_replace(" ", "", $base_photo);
$base_photo = str_replace("sites/default/files/styles/60x60/public", "sites/default/files", $base_photo);
$base_photo = substr($base_photo, 0, strpos($base_photo, "?itok="));


		$row->final_ship_id_reference						=		$base_ship;
		$row->cruise_id_on_base_id 							=		$cruise_id;
		$row->final_ship_reference				    	=		$base_ship;
		$row->final_cruiseline				        	=		$base_cruiseline;
		$row->final_destinations_reference			=		$base_destinations;
		// $row->final_brief_description				 		=		$base_brief_description;
		$row->final_cruise_length				     		=		$base_cruise_length;
		// $row->final_description				       		=		$base_description;
		$row->final_name				              	=		$title;
		$row->title				                 			=		$title;
		$row->final_photo				             		=		$base_photo;
		$row->final_start_price				       		=		$base_start_price;
		$row->final_ship_star_rating				  	=		$ship_star_rating;
		$row->final_ship_thumb				        	=		$ship_thumb;
		$row->final_embarkport_reference				=		$embarkport;
		// $row->final_cruise_description					=		$base_description;
		$row->final_destination_group						=		$destination_group;

		$row->base_ship_id_reference						=		$base_ship;
		$row->base_ship_reference				    		=		$base_ship;
		$row->base_cruiseline				        		=		$base_cruiseline;
		$row->base_destinations_reference				=		$base_destinations;
		// $row->base_brief_description				 		=		$base_brief_description;
		$row->base_cruise_length				     		=		$base_cruise_length;
		// $row->base_description				       		=		$base_description;
		$row->base_name				              		=		$base_brief_description;
		$row->base_photo				             		=		$base_photo;
		$row->base_start_price				       		=		$base_start_price;
		$row->base_ship_star_rating				  		=		$ship_star_rating;
		$row->base_ship_thumb				        		=		$ship_thumb;
		$row->base_embarkport_reference					=		$embarkport;


		$cruise_length_with_nights‎ 					= (String) $base_cruise_length;
		if ($length_with_nights == '1') {
			$length_with_nights 							= '1 night';
		} else {
			$length_with_nights 							= (String) $row->length . ' nights';
		}
		$row->length_with_nights 						= $length_with_nights;

		if (count(explode(' ', $row->sailingdate_id)) > 1) {
		  // some white spaces are there.
		  $row->sailingdate_id = '';
		  // watchdog('final_deal_importer', 'sailingdate_id has multiple sailing dates, stripping out.');
		}

		$id							= $row->id;
		$cruise_id			= $row->cruise_id;

		$row->migrate_map_sourceid1 	= $id;
		$row->migrate_map_destid1 		= $id;
		$row->import_block = '';

		$cruiseline_id = $row->final_cruiseline;
		$selected_cruiselines = variable_get('cruise_factory_migrate__selected_cruiseline_array');

	 	$cruiseline_base_id 	= db_query("SELECT base_id_value FROM field_data_base_id WHERE bundle = 'base_cruiselines' AND entity_id = :cruiseline_id", array(":cruiseline_id" => $cruiseline_id))->fetchAll();
    $cruiseline_base_id 	= $cruiseline_base_id[0]->base_id_value;

	  if ($selected_cruiselines) {
			if (!in_array($cruiseline_base_id, $selected_cruiselines)) {
				// watchdog('cruiseline filter', "we didn't choose cruiseline: " . $cruiseline_base_id . ' on: final_cruise_import');
				return FALSE;
			}
	  }


		$dest_group_id 	= db_query("SELECT base_id_value FROM {field_data_base_id} WHERE bundle = 'base_destinations' AND entity_id = :dest_id", array(":dest_id" => $row->base_destinations))->fetchAll();
		$dest_group_id 	= $dest_group_id[0]->base_id_value;
		$destination = $dest_group_id;

		if  ($destination == "7") 	{	$destgroupchosen = "Antarctica and Patagonia"; }
		if  ($destination == "52") 	{	$destgroupchosen = "Africa"; }
		if  ($destination == "2") 	{	$destgroupchosen = "Alaska"; }
		if  ($destination == "49") 	{	$destgroupchosen = "Arctic North Atlantic"; }
		if (($destination == "35")	|| ($destination == "54")) 	{	$destgroupchosen = "Asia"; }
		if  ($destination == "38") 	{	$destgroupchosen = "Atlantic Trans"; }
		if (($destination == "8")		|| ($destination == "43")) 	{	$destgroupchosen = "Australia and New Zealand"; }
		if  ($destination == "10") 	{	$destgroupchosen = "Bermuda"; }
		if  ($destination == "21") 	{	$destgroupchosen = "British Isles"; }
		if  ($destination == "11") 	{	$destgroupchosen = "Canada and New England"; }
		if  ($destination == "9") 	{	$destgroupchosen = "Caribbean"; }
		if  ($destination == "12") 	{	$destgroupchosen = "Caribbean"; }
		if  ($destination == "48") 	{	$destgroupchosen = "Central America"; }
		if (($destination == "50")	|| ($destination == "28")) 	{	$destgroupchosen = "Europe"; }
		if  ($destination == "29") 	{	$destgroupchosen = "Hawaii"; }
		if  ($destination == "22") 	{	$destgroupchosen = "Mediterranean"; }
		if  ($destination == "53") 	{	$destgroupchosen = "Mexico"; }
		if (($destination == "30")	|| ($destination == "45")) 	{	$destgroupchosen = "Middle East"; }
		if  ($destination == "32") 	{	$destgroupchosen = "North America Atlantic"; }
		if  ($destination == "34") 	{	$destgroupchosen = "North America Pacific"; }
		if (($destination == "25")	|| ($destination == "26")) 	{	$destgroupchosen = "Northern Europe"; }
		if  ($destination == "37") 	{	$destgroupchosen = "South America"; }
		if  ($destination == "40") 	{	$destgroupchosen = "World Cruises, Sectors and Transoceanic Voyages"; }

		$destgroupchosen_id 	= db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'base_destination_group' AND title = :title", array(":title" => $destgroupchosen))->fetchAll();
    $destgroupchosen_id 	= $destgroupchosen_id[0]->id;

		$row->base_destination_group = $destgroupchosen_id;
		$row->final_destination_group = $destgroupchosen_id;

		// foreach ($row as $row_key => $each_row) {
		// 	watchdog('final_cruise_importer', $row_key . ' = ' . $each_row);
		// 	dpm($row_key . ' = ' . $each_row);
		// }

		$title_for_path 	= str_replace(' ', '-', strtolower($row->title));
		$title_for_path		=	str_replace(',', '-', $title_for_path);
		$title_for_path		=	str_replace('.', '-', $title_for_path);
		$title_for_path		=	str_replace('/', '-', $title_for_path);
		$title_for_path		=	str_replace('\\', '-', $title_for_path);
		$title_for_path		=	str_replace('@', '-', $title_for_path);
		$title_for_path		=	str_replace('%', '-', $title_for_path);
		$title_for_path		=	str_replace('&', 'and', $title_for_path);
		$title_for_path		=	str_replace('--', '-', $title_for_path);

		// watchdog('final_cruise_importer', 'attempted path: ' . $title_for_path);
		// dpm('attempted path: ' . $title_for_path);

		$title_for_path 		= "cruises/" . $title_for_path . '-' . $row->base_id;
		$row->path 					= $title_for_path;

		// watchdog('final_cruise_importer', 'ID: ' . $row->base_id);
		// dpm('ID: ' . $row->base_id);

	}

	public function __construct($arguments) {
		parent::__construct($arguments);

		$this->description = t("Combining of all _base elements into Final Deal Form.");
		$this->softDependencies = array('base_winelistsMigration');

		$this->systemOfRecord = Migration::SOURCE;

		$columns = array(
			0 => array("cruise_id" 												, "cruise_id"),
			1 => array("base_id" 													, "base_id"),
			2 => array("base_ship_id"											, "base_ship_id"),
			3 => array("title"														, "title"),
			4 => array("cruise_factory_entity_type"				, "cruise_factory_entity_type"),
			5 => array("base_ship"												, "base_ship"),
			6 => array("base_cruiseline"									, "base_cruiseline"),
			7 => array("base_destinations"								, "base_destinations"),
			8 => array("destination_group"								, "destination_group"),
			9 => array("base_itineraries"									, "base_itineraries"),
			10 => array("base_cruise_length"							, "base_cruise_length"),
			11 => array("base_name"												, "base_name"),
			12 => array("base_photo"											, "base_photo"),
			13 => array("base_start_price"								, "base_start_price"),
			14 => array("ship_star_rating"								, "ship_star_rating"),
			15 => array("ship_thumb"											, "ship_thumb"),
			16 => array("embarkport"											, "embarkport"),
			// Fakies
			17 => array("cruise_id_on_base_id"										,				"cruise_id_on_base_id"),
			18 => array("final_cruise_length"											,				"final_cruise_length"),
			19 => array("final_cruiseline"												,				"final_cruiseline"),
			// 20 => array("final_destination_group"									,				"final_destination_group"),
			20 => array("final_destination_group"									,				"base_destination_group"),
			21 => array("final_destinations_reference"						,				"final_destinations_reference"),
			22 => array("final_embarkport_reference"							,				"final_embarkport_reference"),
			23 => array("final_name"															,				"final_name"),
			24 => array("final_photo"															,				"final_photo"),
			25 => array("final_brief_description"									,				"final_brief_description"),
			26 => array("base_sailingdate"												,				"base_sailingdate"),
			27 => array("final_ship_id_reference"									,				"final_ship_id_reference"),
			28 => array("final_ship_star_rating"									,				"final_ship_star_rating"),
			29 => array("final_ship_thumb"												,				"final_ship_thumb"),
			30 => array("final_brief_description"									,				"final_brief_description"),
			31 => array("path"																		,				"path"),
			32 => array("cruise_id_on_base_id"										,				"cruise_id_on_base_id"),
			33 => array("base_destination_group"									,				"base_destination_group"),
			// 33 => array("final_destination_group"									,				"final_destination_group"),
		);

		global $base_url;
		$server_url = str_replace('http://www.', '', $base_url);
		$server_url = str_replace('http://', '', $server_url);
		$server_url = str_replace('https://www.', '', $server_url);
		$server_url = str_replace('https://', '', $server_url);
		// $file = 'export-deal-combine';
		// $csv_path = 'http://www.' . $server_url . '/' . $file;
		$local_path = '/Users/ash/Sites/ash.localdev/cruises2017/cruise_factory_local_files/';
		$truserv_path = '/var/www/html/cruise_factory_local_files/';
		$file = 'export-deal-combine-cruises.csv';

		// "stage-royal-" + "export-deal-combine.csv"
		$http = 'https://';
		if (substr($server_url, 0, 11) == 'cruises2017') {
			$csv_path = $local_path . 'cruises-' . $file;
			$http = 'http://';
		} else if ($server_url == 'cruises.co.za') {
			$http =  'https://';
			$csv_path = $truserv_path . 'cruises-' . $file;			
		} else if (($server_url == 'dev.cruises.co.za') ||
							($server_url == 'stage.cruises.co.za')) {
			$http =  'http://';
			$csv_path = $truserv_path . 'dev.cruises-' . $file;
		} else if ($server_url == 'stage-royalcaribbean.co.za') {
			$csv_path = $truserv_path . 'stage-royal-' . $file;
			$http = 'http://';
		} else if ($server_url == 'royalcaribbean.co.za') {
			$csv_path = $truserv_path . 'royal-' . $file;
			$http = 'http://';
		} else {
			// $csv_path = $http . 'www.' . $server_url . ':81/export-deal-combine';
			$csv_path = $http . 'www.' . $server_url . '/export-deal-combine';
		}
		
		$csv_path = str_replace('http://www.https://www.','https://www.',$csv_path);
		$csv_path = str_replace('http://https://www.','https://www.',$csv_path);
		$csv_path = str_replace('http://https://www.','https://www.',$csv_path);

		$csv_path = str_replace('http://http://dev.','http://dev.',$csv_path);
		$csv_path = str_replace('http://http://stage.','http://stage.',$csv_path);
		$csv_path = str_replace('http://https://dev.','http://dev.',$csv_path);
		$csv_path = str_replace('http://https://stage.','http://stage.',$csv_path);


		$this->source = new MigrateSourceCSV($csv_path,
			$columns, array('delimiter' => ',', 'header_rows' => 1, 'track_changes' => 1));
		$this->destination = new MigrateDestinationEntityAPI('cruise_factory_entity',
			'cruise_factory_complete_cruise');
		$this->map = new MigrateSQLMap($this->machineName,
			array(
				'base_id' => array(
					'type' => 'varchar',
					'length' => 255,
					'not null' => TRUE,
					'description' => 'base_id',
				),
			),
			MigrateDestinationEntityAPI::getKeySchema('cruise_factory_entity', 'cruise_factory_complete_cruise')
		);

			$this->addFieldMapping('type')																->defaultValue('cruise_factory_complete_cruise');
			
			$this->addFieldMapping('path'							                  	,				'path');
			$this->addFieldMapping('pathauto')							              ->defaultValue(0);

			$this->addFieldMapping('title'							                  ,				'title');
			$this->addFieldMapping('id',																	'base_id');
			$this->addFieldMapping('uuid',																'cruise_id_on_base_id');
			$this->addFieldMapping('created')															->defaultValue(REQUEST_TIME);
			$this->addFieldMapping('changed')															->defaultValue(REQUEST_TIME);

			$this->addFieldMapping('final_id'															,				'base_id');
			
			$this->addFieldMapping('final_name'														,				'final_name');
			$this->addFieldMapping('final_cruise_id_on_base_id'				    ,				'cruise_id_on_base_id');
			$this->addFieldMapping('final_cruise_id_reference'				    ,				'cruise_id_on_base_id');
			// $this->addFieldMapping('final_sailingdate_id_reference'				,				'base_sailingdate');
			$this->addFieldMapping('final_sailingdate'				            ,				'base_sailingdate');
			$this->addFieldMapping('final_sailingdate:to'									,				'base_sailingdate');
			$this->addFieldMapping('final_sailingdate:timezone')					->defaultvalue('Africa/Johannesburg');

			// $this->addFieldMapping('final_cruise_details'				          ,				'cruise_details');
			$this->addFieldMapping('final_cruise_details'				          ,				'base_itineraries');
			$this->addFieldMapping('final_ship_id_reference'				      ,				'final_ship_id_reference');
			$this->addFieldMapping('final_ship_reference'				          ,				'final_ship_id_reference');
			$this->addFieldMapping('final_cruiseline'				              ,				'final_cruiseline');
			$this->addFieldMapping('final_destinations_reference'				  ,				'final_destinations_reference');
			$this->addFieldMapping('final_destination_group'				      ,				'final_destination_group');
			
			// $this->addFieldMapping('final_brief_description'				      ,				'final_brief_description');
			$this->addFieldMapping('final_cruise_length'				          ,				'final_cruise_length');
			// $this->addFieldMapping('final_cruise_length_with_nights'			,				'length_with_nights');
			// $this->addFieldMapping('final_description'				            ,				'final_description');
			
			$this->addFieldMapping('final_photo'				                  ,				'final_photo');
			// $this->addFieldMapping('final_photo:description'							,				'final_brief_description');
			$this->addFieldMapping('final_photo:alt'											,				'final_brief_description');
			$this->addFieldMapping('final_photo:title'										,				'final_brief_description');
			$this->addFieldMapping('final_photo:file_replace')	 					->defaultValue('FILE_EXISTS_REPLACE');
			$this->addFieldMapping('final_photo:preserve_files')					->defaultValue(0);

			$this->addFieldMapping('final_ship_star_rating'				        ,				'final_ship_star_rating');
			$this->addFieldMapping('final_ship_thumb'				              ,				'final_ship_thumb');
			// $this->addFieldMapping('final_ship_thumb:description'					,				'final_brief_description');
			$this->addFieldMapping('final_ship_thumb:alt'									,				'final_brief_description');
			$this->addFieldMapping('final_ship_thumb:title'								,				'final_brief_description');
			$this->addFieldMapping('final_ship_thumb:file_replace')	 			->defaultValue('FILE_EXISTS_REPLACE');
			$this->addFieldMapping('final_ship_thumb:preserve_files')			->defaultValue(0);

			$this->addFieldMapping('final_embarkport_reference'				    ,				'final_embarkport_reference');

			// $this->addFieldMapping('final_cruise_description'						,				'final_cruise_description');
			$this->addFieldMapping('final_cruise_description:format')		->defaultValue('full_html');

	}
}
