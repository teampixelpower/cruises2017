<?php
/**
 * @file
 * cruises_entity_repair.features.inc
 */

/**
 * Implements hook_eck_entity_type_info().
 */
function cruises_entity_repair_eck_entity_type_info() {
  $items = array(
    'destination_group' => array(
      'name' => 'destination_group',
      'label' => 'Destination Group',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'uuid' => array(
          'label' => 'UUID',
          'type' => 'text',
          'behavior' => 'uuid',
        ),
      ),
    ),
  );
  return $items;
}
