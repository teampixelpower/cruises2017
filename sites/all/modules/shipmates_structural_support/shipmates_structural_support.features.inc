<?php
/**
 * @file
 * shipmates_structural_support.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function shipmates_structural_support_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function shipmates_structural_support_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function shipmates_structural_support_node_info() {
  $items = array(
    'marketing_collateral_brochure' => array(
      'name' => t('Marketing Collateral: Brochures'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'marketing_collateral_image' => array(
      'name' => t('Marketing Collateral: Image'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'marketing_collateral_logo' => array(
      'name' => t('Marketing Collateral: Logo'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'marketing_collateral_video' => array(
      'name' => t('Marketing Collateral: Video'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'shipmates_training_of_the_week' => array(
      'name' => t('Shipmates Training of the Week'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'training_workshops' => array(
      'name' => t('Shipmates Training Workshops'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
