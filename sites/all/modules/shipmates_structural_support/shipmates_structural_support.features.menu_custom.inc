<?php
/**
 * @file
 * shipmates_structural_support.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function shipmates_structural_support_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: features.
  $menus['features'] = array(
    'menu_name' => 'features',
    'title' => 'Features',
    'description' => 'Menu items for any enabled features.',
  );
  // Exported menu: menu-shipmates-menu.
  $menus['menu-shipmates-menu'] = array(
    'menu_name' => 'menu-shipmates-menu',
    'title' => 'Shipmates Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Features');
  t('Menu items for any enabled features.');
  t('Shipmates Menu');


  return $menus;
}
