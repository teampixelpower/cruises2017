<?php
/**
 * @file
 * shipmates_structural_support.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function shipmates_structural_support_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'marketing_collateral';
  $page->task = 'page';
  $page->admin_title = 'Marketing Collateral';
  $page->admin_description = '';
  $page->path = 'shipmates/marketing';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Marketing Collateral',
    'name' => 'menu-shipmates-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_marketing_collateral__panel_context_81ac665b-c944-4046-8317-cc571700696f';
  $handler->task = 'page';
  $handler->subtask = 'marketing_collateral';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Marketing Collateral';
  $display->uuid = '6982042b-86a6-4023-aac5-6c3237749634';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-fd9dc06c-c5f2-4dee-966b-c7421b35b9b3';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'cruiseline_collateral_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fd9dc06c-c5f2-4dee-966b-c7421b35b9b3';
    $display->content['new-fd9dc06c-c5f2-4dee-966b-c7421b35b9b3'] = $pane;
    $display->panels['middle'][0] = 'new-fd9dc06c-c5f2-4dee-966b-c7421b35b9b3';
    $pane = new stdClass();
    $pane->pid = 'new-60824a83-ee83-4b60-b17c-3717829e1e5a';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'marketing_collateral_brochures';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '20',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 1,
      'args' => '',
      'url' => '',
      'display' => 'page',
      'override_title' => 1,
      'override_title_text' => 'Brochrues',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '60824a83-ee83-4b60-b17c-3717829e1e5a';
    $display->content['new-60824a83-ee83-4b60-b17c-3717829e1e5a'] = $pane;
    $display->panels['middle'][1] = 'new-60824a83-ee83-4b60-b17c-3717829e1e5a';
    $pane = new stdClass();
    $pane->pid = 'new-13702d13-a91b-4b30-8fda-ff0299dc5746';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'marketing_collateral_logos';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '20',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 1,
      'args' => '',
      'url' => '',
      'display' => 'page',
      'override_title' => 1,
      'override_title_text' => 'Logos',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '13702d13-a91b-4b30-8fda-ff0299dc5746';
    $display->content['new-13702d13-a91b-4b30-8fda-ff0299dc5746'] = $pane;
    $display->panels['middle'][2] = 'new-13702d13-a91b-4b30-8fda-ff0299dc5746';
    $pane = new stdClass();
    $pane->pid = 'new-ddbb873d-4b98-4265-9aac-d41f02bcae64';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'marketing_collateral_images';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '20',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'page',
      'override_title' => 1,
      'override_title_text' => 'Images',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'ddbb873d-4b98-4265-9aac-d41f02bcae64';
    $display->content['new-ddbb873d-4b98-4265-9aac-d41f02bcae64'] = $pane;
    $display->panels['middle'][3] = 'new-ddbb873d-4b98-4265-9aac-d41f02bcae64';
    $pane = new stdClass();
    $pane->pid = 'new-e1ee3c11-25b0-48df-9f1d-40b2fdb13de7';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'marketing_collateral_video';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '20',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'page',
      'override_title' => 1,
      'override_title_text' => 'Video',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'e1ee3c11-25b0-48df-9f1d-40b2fdb13de7';
    $display->content['new-e1ee3c11-25b0-48df-9f1d-40b2fdb13de7'] = $pane;
    $display->panels['middle'][4] = 'new-e1ee3c11-25b0-48df-9f1d-40b2fdb13de7';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-60824a83-ee83-4b60-b17c-3717829e1e5a';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['marketing_collateral'] = $page;

  return $pages;

}
