<?php
/**
 * @file
 * shipmates_structural_support.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function shipmates_structural_support_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-shipmates-menu_captainup-leaderboard:node/102
  $menu_links['menu-shipmates-menu_captainup-leaderboard:node/102'] = array(
    'menu_name' => 'menu-shipmates-menu',
    'link_path' => 'node/102',
    'router_path' => 'node/%',
    'link_title' => 'CaptainUp Leaderboard',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-shipmates-menu_captainup-leaderboard:node/102',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-shipmates-menu_captainup-profile:<nolink>
  $menu_links['menu-shipmates-menu_captainup-profile:<nolink>'] = array(
    'menu_name' => 'menu-shipmates-menu',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'CaptainUp Profile',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'cpt-open-player-profile',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-shipmates-menu_captainup-profile:<nolink>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-shipmates-menu_marketing-collateral:shipmates/marketing
  $menu_links['menu-shipmates-menu_marketing-collateral:shipmates/marketing'] = array(
    'menu_name' => 'menu-shipmates-menu',
    'link_path' => 'shipmates/marketing',
    'router_path' => 'shipmates/marketing',
    'link_title' => 'Marketing Collateral',
    'options' => array(
      'identifier' => 'menu-shipmates-menu_marketing-collateral:shipmates/marketing',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
  );
  // Exported menu link: menu-shipmates-menu_my-quiz-results:shipmates/my-quiz-results
  $menu_links['menu-shipmates-menu_my-quiz-results:shipmates/my-quiz-results'] = array(
    'menu_name' => 'menu-shipmates-menu',
    'link_path' => 'shipmates/my-quiz-results',
    'router_path' => 'shipmates/my-quiz-results',
    'link_title' => 'My Quiz Results',
    'options' => array(
      'identifier' => 'menu-shipmates-menu_my-quiz-results:shipmates/my-quiz-results',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-shipmates-menu_sales-leaderboard:shipmates/sales-leaderboard
  $menu_links['menu-shipmates-menu_sales-leaderboard:shipmates/sales-leaderboard'] = array(
    'menu_name' => 'menu-shipmates-menu',
    'link_path' => 'shipmates/sales-leaderboard',
    'router_path' => 'shipmates/sales-leaderboard',
    'link_title' => 'Sales Leaderboard',
    'options' => array(
      'identifier' => 'menu-shipmates-menu_sales-leaderboard:shipmates/sales-leaderboard',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-shipmates-menu_training-of-the-week:shipmates/training-of-the-week
  $menu_links['menu-shipmates-menu_training-of-the-week:shipmates/training-of-the-week'] = array(
    'menu_name' => 'menu-shipmates-menu',
    'link_path' => 'shipmates/training-of-the-week',
    'router_path' => 'shipmates/training-of-the-week',
    'link_title' => 'Training of the Week',
    'options' => array(
      'identifier' => 'menu-shipmates-menu_training-of-the-week:shipmates/training-of-the-week',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-shipmates-menu_training-workshops:shipmates/training-workshops
  $menu_links['menu-shipmates-menu_training-workshops:shipmates/training-workshops'] = array(
    'menu_name' => 'menu-shipmates-menu',
    'link_path' => 'shipmates/training-workshops',
    'router_path' => 'shipmates/training-workshops',
    'link_title' => 'Training Workshops',
    'options' => array(
      'identifier' => 'menu-shipmates-menu_training-workshops:shipmates/training-workshops',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: menu-shipmates-menu_weekly-wednesday-quiz:shipmates/weekly-wednesday-quiz
  $menu_links['menu-shipmates-menu_weekly-wednesday-quiz:shipmates/weekly-wednesday-quiz'] = array(
    'menu_name' => 'menu-shipmates-menu',
    'link_path' => 'shipmates/weekly-wednesday-quiz',
    'router_path' => 'shipmates/weekly-wednesday-quiz',
    'link_title' => 'Weekly Wednesday Quiz',
    'options' => array(
      'identifier' => 'menu-shipmates-menu_weekly-wednesday-quiz:shipmates/weekly-wednesday-quiz',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('CaptainUp Leaderboard');
  t('CaptainUp Profile');
  t('Marketing Collateral');
  t('My Quiz Results');
  t('Sales Leaderboard');
  t('Training Workshops');
  t('Training of the Week');
  t('Weekly Wednesday Quiz');

  return $menu_links;
}
