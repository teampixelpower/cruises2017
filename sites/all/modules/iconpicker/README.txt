This is an add-on module for the Icon API module.

https://www.drupal.org/project/icon

It allows a more user friendly way of selecting icons.
Instead of the Icon API's default dropdown selecte element, it shows the
available icons in a (searchable) grid and let's you select one by clicking it.

INSTALLATION
================================================================================

Just enable the module, and it should replace the default Icon API select box.

On the settings page of each icon bundle, you can select in which size the icons
of that bundle are rendered in the icon picker.