// $Id$

(function ($) {
  Drupal.behaviors.iconPicker = {
    attach: function (context) {
      // Add an area to show the selected icon
      $(".field-type-icon-field .form-item", context).after("<div class='selection-preview'></div>");

//      $(".field-type-icon-field select", context).hide();

      // Update the preview
      function previewSelection(item){
        var selection = $("select", item).val();
        if (selection) {
          var icon = $("[data-select-name='" + selection + "'", item).parent(".icon-wrapper").clone();
          var selectionPreview = $(".selection-preview" , item).empty();
          icon.appendTo(selectionPreview);
          $(" <span class='select-icon'><a>" + Drupal.t("Change selection") + "</a></span>").appendTo(selectionPreview);
          $(" <span class='remove-icon'><a>" + Drupal.t("Remove selection") + "</a></span>").appendTo(selectionPreview);
          // Show the available icons again if we want to change the selection
          $(".select-icon", selectionPreview).click(function(){
            $(".icon-picker-icons-list", item).fadeIn();
          });
          // Remove the selected icon
          $(".remove-icon", selectionPreview).click(function(){
            $("select", item).val(null);
            $(".icon-picker-icons-list", item).fadeIn();
            $(".selected", item).removeClass("selected");
            selectionPreview.empty();
          });
          // Hide the availabel icons if a selection was made.
          $(".icon-picker-icons-list", item).fadeOut();
        }
      }
      $(".field-type-icon-field").each(function(){
        previewSelection(this);
      })

      // Clicking on an icon should select is as the value for our form.
      $(".field-type-icon-field .icon-wrapper", context).click(function (context) {
        // Which icon was clicked?
        var selectedIcon = $(this).find(".icon").attr("data-select-name");
        var parent = $(this).closest(".field-type-icon-field");

        $(this).addClass("selected");
        $(".selected").not(this).removeClass("selected");

        var select = $("select", parent);
        // Update the select element with the chosen icon.
        select.val(selectedIcon);
        previewSelection(parent);
      });
    }
  };
})(jQuery);
