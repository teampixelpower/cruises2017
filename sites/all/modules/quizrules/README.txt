DESCRIPTION

This modules integrates Quiz with Rules.
It makes Quiz events such as start and finish available for Rules, as well as 
provides Quiz questions and answers as Rules Conditions.
