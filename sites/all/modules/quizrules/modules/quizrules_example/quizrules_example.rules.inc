<?php

/**
 * @file
 * QuizRules Example conditions and actions for Rules.
 */

/**
 * Implements hook_rules_action_info().
 */
function quizrules_example_rules_action_info() {
  $items = array();

  $items['quizrules_example_action'] = array(
    'label' => t('Your module'),
    'module' => 'quizrules_example',
    'group' => 'Quiz Rules Example',
    'parameter' => array(
      'dataId' => array(
        'type' => 'text',
        'label' => t('Data'),
        'options list' => 'quizrules_example_get_data_list',
      ),
    ),
  );

  return $items;
}
