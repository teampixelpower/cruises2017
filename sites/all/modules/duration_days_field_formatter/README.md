# Duration Days Field Formatter Module 

Ash Glover
ash@pixelpower.co.za

This module provides a field formatter that displays the selected field with simply 'days' added


## CHANGELOG

### Sept 2016
- added new field types. Now supports: 'text', 'number_integer', 'number_decimal', 'list', 'list_text'
