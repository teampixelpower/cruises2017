<?php
/**
 * @file
 * temp_additions_to_importer_v5.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function temp_additions_to_importer_v5_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function temp_additions_to_importer_v5_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
