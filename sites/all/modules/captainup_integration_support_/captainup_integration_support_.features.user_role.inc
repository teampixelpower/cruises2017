<?php
/**
 * @file
 * captainup_integration_support_.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function captainup_integration_support__user_default_roles() {
  $roles = array();

  // Exported role: Shipmate.
  $roles['Shipmate'] = array(
    'name' => 'Shipmate',
    'weight' => 3,
  );

  return $roles;
}
