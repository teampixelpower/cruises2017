# Currency Convert Field Formatter Module 

Ash Glover
ash@benchark.digital


This module provides a central page for admins to update the exchange rate, and a field formatter that displays the selected field multiplied through the exchange rate.

This allows admins to update the prices of a full website based on movements of the currency. Building in support for automatic updates from a web source will also be simple, but is not wanted for this first branch.

## CHANGELOG

- March 2017: Module now first attempts to use the exchange rate specified on a deal, before falling back to a default exchange rate set on the module admin page
- Sept 2016: Added support for more field types
- Apr 2016: version 0.2, added number field support
- Nov 2015: added permissions



## TODO:

~~A: add module file, add info file~~

1. admin page that allows us to set exchange rate
2. module sets a global variable that can be used for theming
3. field formatter that takes the field and multiplies it by the global variable of the exchange rate
4. field formatter should also strip out any spaces, commas, currency symbols, etc, before calculation.
5. field formatter will add currency symbol and decimal places.


## Wishlist

1. Admin page has option for manual exchange rate or automatic from web service like XE or Google
2. Select Currency (default module will be Rand / Dollar)

