<?php
/**
 * @file
 * cruise_factory_entities_search_api.features.inc
 */

/**
 * Implements hook_default_search_api_index().
 */
function cruise_factory_entities_search_api_default_search_api_index() {
  $items = array();
  $items['cruise_factory_complete_cruises'] = entity_import('search_api_index', '{
    "name" : "Cruise Factory Complete Cruises",
    "machine_name" : "cruise_factory_complete_cruises",
    "description" : null,
    "server" : "apache_solr",
    "item_type" : "cruise_factory_entity",
    "options" : {
      "datasource" : { "bundles" : [ "cruise_factory_complete_cruise" ] },
      "index_directly" : 0,
      "cron_limit" : "10",
      "sorts_widget" : {
        "enabled" : false,
        "title" : "",
        "autosubmit" : false,
        "autosubmit_hide" : false
      },
      "fields" : {
        "base_name" : { "type" : "text" },
        "final_brief_description" : { "type" : "text" },
        "final_cruise_id_reference" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_cruise_length" : { "type" : "text" },
        "final_cruiseline" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_destination_group" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_destinations_reference" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_embarkport_reference" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_sailingdate" : { "type" : "date" },
        "final_ship_reference" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "search_api_language" : { "type" : "string" },
        "title" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "search_api_ranges_alter" : { "status" : 0, "weight" : "-50", "settings" : { "fields" : [] } },
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_multi_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_metatag_alter_callback" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "final_brief_description" : true,
              "final_cruise_length" : true,
              "final_cruise_details" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : {
              "title" : true,
              "final_brief_description" : true,
              "final_cruise_length" : true,
              "final_cruise_details" : true
            },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 0,
          "weight" : "15",
          "settings" : { "fields" : {
              "title" : true,
              "final_brief_description" : true,
              "final_cruise_length" : true,
              "final_cruise_details" : true
            }
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : {
              "title" : true,
              "final_brief_description" : true,
              "final_cruise_length" : true,
              "final_cruise_details" : true
            },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : {
              "title" : true,
              "final_brief_description" : true,
              "final_cruise_length" : true,
              "final_cruise_details" : true
            },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  $items['cruise_factory_complete_deals'] = entity_import('search_api_index', '{
    "name" : "Cruise Factory Complete Deals",
    "machine_name" : "cruise_factory_complete_deals",
    "description" : null,
    "server" : "apache_solr",
    "item_type" : "cruise_factory_entity",
    "options" : {
      "datasource" : { "bundles" : [ "cruise_factory_complete_deal" ] },
      "index_directly" : 1,
      "cron_limit" : "20",
      "sorts_widget" : {
        "enabled" : false,
        "title" : "",
        "autosubmit" : false,
        "autosubmit_hide" : false
      },
      "fields" : {
        "final_advert_code" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "final_brief_description" : { "type" : "text" },
        "final_cabin_id_reference" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_cruise_id_reference" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_cruise_length" : { "type" : "integer" },
        "final_cruiseline" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_destination_group" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_embarkport_reference" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_sailingdate" : { "type" : "date" },
        "final_sailingdate_id_reference" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_ship_reference" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_special_id_reference" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "cruise_factory_entity"
        },
        "final_start_price" : { "type" : "decimal" },
        "final_validity_date_end" : { "type" : "date" },
        "final_validity_date_start" : { "type" : "date" },
        "id" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "search_api_viewed" : { "type" : "text" },
        "title" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "search_api_ranges_alter" : { "status" : 0, "weight" : "-50", "settings" : { "fields" : [] } },
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 1, "weight" : "0", "settings" : { "mode" : "teaser" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_multi_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_metatag_alter_callback" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true, "final_brief_description" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true, "final_brief_description" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 0,
          "weight" : "15",
          "settings" : { "fields" : { "title" : true, "final_brief_description" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true, "final_brief_description" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "final_brief_description" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_page().
 */
function cruise_factory_entities_search_api_default_search_api_page() {
  $items = array();
  $items['cruise_search_complete'] = entity_import('search_api_page', '{
    "index_id" : "cruise_factory_complete_cruises",
    "path" : "cruise\\/search-complete",
    "name" : "Find a Cruise",
    "machine_name" : "cruise_search_complete",
    "description" : "",
    "options" : {
      "mode" : "terms",
      "fields" : { "title" : "title" },
      "per_page" : "10",
      "result_page_search_form" : 1,
      "get_per_page" : 0,
      "view_mode" : "teaser",
      "empty_behavior" : "results"
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  $items['search_test'] = entity_import('search_api_page', '{
    "index_id" : "cruise_factory_complete_deals",
    "path" : "specials",
    "name" : "Search Specials",
    "machine_name" : "search_test",
    "description" : "",
    "options" : {
      "mode" : "terms",
      "fields" : {
        "final_brief_description" : "final_brief_description",
        "search_api_viewed" : "search_api_viewed",
        "title" : "title"
      },
      "per_page" : "20",
      "result_page_search_form" : 1,
      "get_per_page" : 0,
      "view_mode" : "teaser",
      "empty_behavior" : "results"
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
