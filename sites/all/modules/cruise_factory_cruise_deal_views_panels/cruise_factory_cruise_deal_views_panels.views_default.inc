<?php
/**
 * @file
 * cruise_factory_cruise_deal_views_panels.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cruise_factory_cruise_deal_views_panels_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'cruise_deals_api';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_cruise_deals';
  $view->human_name = 'Cruise Deals API';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '16';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'four columns';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser';
  /* Field: Indexed cruise_factory_entity: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'search_api_index_cruise_deals';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
    3 => 'OR',
  );
  /* Filter criterion: Indexed cruise_factory_entity: Ship */
  $handler->display->display_options['filters']['field_ship']['id'] = 'field_ship';
  $handler->display->display_options['filters']['field_ship']['table'] = 'search_api_index_cruise_deals';
  $handler->display->display_options['filters']['field_ship']['field'] = 'field_ship';
  $handler->display->display_options['filters']['field_ship']['value'] = '4480';
  $handler->display->display_options['filters']['field_ship']['group'] = 1;
  /* Filter criterion: Indexed cruise_factory_entity: Publish Date */
  $handler->display->display_options['filters']['field_publish_date']['id'] = 'field_publish_date';
  $handler->display->display_options['filters']['field_publish_date']['table'] = 'search_api_index_cruise_deals';
  $handler->display->display_options['filters']['field_publish_date']['field'] = 'field_publish_date';
  $handler->display->display_options['filters']['field_publish_date']['operator'] = '<=';
  $handler->display->display_options['filters']['field_publish_date']['value'] = 'now';
  $handler->display->display_options['filters']['field_publish_date']['group'] = 2;
  /* Filter criterion: Indexed cruise_factory_entity: Validity Date Start */
  $handler->display->display_options['filters']['field_validity_date_start']['id'] = 'field_validity_date_start';
  $handler->display->display_options['filters']['field_validity_date_start']['table'] = 'search_api_index_cruise_deals';
  $handler->display->display_options['filters']['field_validity_date_start']['field'] = 'field_validity_date_start';
  $handler->display->display_options['filters']['field_validity_date_start']['operator'] = '<=';
  $handler->display->display_options['filters']['field_validity_date_start']['value'] = 'now';
  $handler->display->display_options['filters']['field_validity_date_start']['group'] = 2;
  /* Filter criterion: Indexed cruise_factory_entity: Unpublish Date */
  $handler->display->display_options['filters']['field_unpublish_date']['id'] = 'field_unpublish_date';
  $handler->display->display_options['filters']['field_unpublish_date']['table'] = 'search_api_index_cruise_deals';
  $handler->display->display_options['filters']['field_unpublish_date']['field'] = 'field_unpublish_date';
  $handler->display->display_options['filters']['field_unpublish_date']['operator'] = '>=';
  $handler->display->display_options['filters']['field_unpublish_date']['value'] = 'now';
  $handler->display->display_options['filters']['field_unpublish_date']['group'] = 3;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'cruise-deals/three';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'six columns';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'wider_teaser';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['path'] = 'cruise-deals';
  $export['cruise_deals_api'] = $view;

  $view = new view();
  $view->name = 'itinerary_field_collection';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'field_collection_item';
  $view->human_name = 'Itinerary Field Collection';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Itinerary';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Relationship: Field collection item: Entity with the Itinerary (field_itinerary_fieldcolle) */
  $handler->display->display_options['relationships']['field_itinerary_fieldcolle_cruise_factory_entity']['id'] = 'field_itinerary_fieldcolle_cruise_factory_entity';
  $handler->display->display_options['relationships']['field_itinerary_fieldcolle_cruise_factory_entity']['table'] = 'field_collection_item';
  $handler->display->display_options['relationships']['field_itinerary_fieldcolle_cruise_factory_entity']['field'] = 'field_itinerary_fieldcolle_cruise_factory_entity';
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_field_cruise_reference_cruise_factory_entity']['id'] = 'reverse_field_cruise_reference_cruise_factory_entity';
  $handler->display->display_options['relationships']['reverse_field_cruise_reference_cruise_factory_entity']['table'] = 'eck_cruise_factory_entity';
  $handler->display->display_options['relationships']['reverse_field_cruise_reference_cruise_factory_entity']['field'] = 'reverse_field_cruise_reference_cruise_factory_entity';
  $handler->display->display_options['relationships']['reverse_field_cruise_reference_cruise_factory_entity']['relationship'] = 'field_itinerary_fieldcolle_cruise_factory_entity';
  /* Field: Field collection item: Itineraries Day */
  $handler->display->display_options['fields']['field_itineraries_day']['id'] = 'field_itineraries_day';
  $handler->display->display_options['fields']['field_itineraries_day']['table'] = 'field_data_field_itineraries_day';
  $handler->display->display_options['fields']['field_itineraries_day']['field'] = 'field_itineraries_day';
  $handler->display->display_options['fields']['field_itineraries_day']['label'] = 'Day';
  /* Field: Field collection item: Itineraries Port */
  $handler->display->display_options['fields']['field_itineraries_port']['id'] = 'field_itineraries_port';
  $handler->display->display_options['fields']['field_itineraries_port']['table'] = 'field_data_field_itineraries_port';
  $handler->display->display_options['fields']['field_itineraries_port']['field'] = 'field_itineraries_port';
  $handler->display->display_options['fields']['field_itineraries_port']['label'] = 'Port';
  $handler->display->display_options['fields']['field_itineraries_port']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_itineraries_port']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_itineraries_port']['delta_offset'] = '0';
  /* Field: Field collection item: Itineraries Arrive */
  $handler->display->display_options['fields']['field_itineraries_arrive']['id'] = 'field_itineraries_arrive';
  $handler->display->display_options['fields']['field_itineraries_arrive']['table'] = 'field_data_field_itineraries_arrive';
  $handler->display->display_options['fields']['field_itineraries_arrive']['field'] = 'field_itineraries_arrive';
  $handler->display->display_options['fields']['field_itineraries_arrive']['label'] = 'Arrive';
  $handler->display->display_options['fields']['field_itineraries_arrive']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_itineraries_arrive']['empty_zero'] = TRUE;
  /* Field: Field collection item: Itinerary Depart */
  $handler->display->display_options['fields']['field_itinerary_depart']['id'] = 'field_itinerary_depart';
  $handler->display->display_options['fields']['field_itinerary_depart']['table'] = 'field_data_field_itinerary_depart';
  $handler->display->display_options['fields']['field_itinerary_depart']['field'] = 'field_itinerary_depart';
  $handler->display->display_options['fields']['field_itinerary_depart']['label'] = 'Depart';
  $handler->display->display_options['fields']['field_itinerary_depart']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_itinerary_depart']['empty_zero'] = TRUE;
  /* Field: Field collection item: Itineraries Port Order */
  $handler->display->display_options['fields']['field_itineraries_port_order']['id'] = 'field_itineraries_port_order';
  $handler->display->display_options['fields']['field_itineraries_port_order']['table'] = 'field_data_field_itineraries_port_order';
  $handler->display->display_options['fields']['field_itineraries_port_order']['field'] = 'field_itineraries_port_order';
  $handler->display->display_options['fields']['field_itineraries_port_order']['label'] = '';
  $handler->display->display_options['fields']['field_itineraries_port_order']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_itineraries_port_order']['element_label_colon'] = FALSE;
  /* Field: Cruise_factory_entity: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_cruise_factory_entity';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['relationship'] = 'reverse_field_cruise_reference_cruise_factory_entity';
  $handler->display->display_options['fields']['id']['label'] = '';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['id']['separator'] = '';
  /* Sort criterion: Field collection item: Itineraries Day (field_itineraries_day) */
  $handler->display->display_options['sorts']['field_itineraries_day_value']['id'] = 'field_itineraries_day_value';
  $handler->display->display_options['sorts']['field_itineraries_day_value']['table'] = 'field_data_field_itineraries_day';
  $handler->display->display_options['sorts']['field_itineraries_day_value']['field'] = 'field_itineraries_day_value';
  /* Contextual filter: Cruise_factory_entity: Id */
  $handler->display->display_options['arguments']['id']['id'] = 'id';
  $handler->display->display_options['arguments']['id']['table'] = 'eck_cruise_factory_entity';
  $handler->display->display_options['arguments']['id']['field'] = 'id';
  $handler->display->display_options['arguments']['id']['relationship'] = 'reverse_field_cruise_reference_cruise_factory_entity';
  $handler->display->display_options['arguments']['id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['id']['summary_options']['items_per_page'] = '25';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');

  /* Display: Directly on Cruise */
  $handler = $view->new_display('block', 'Directly on Cruise', 'directly_cruise');
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Cruise_factory_entity: Id */
  $handler->display->display_options['arguments']['id']['id'] = 'id';
  $handler->display->display_options['arguments']['id']['table'] = 'eck_cruise_factory_entity';
  $handler->display->display_options['arguments']['id']['field'] = 'id';
  $handler->display->display_options['arguments']['id']['relationship'] = 'field_itinerary_fieldcolle_cruise_factory_entity';
  $handler->display->display_options['arguments']['id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['id']['summary_options']['items_per_page'] = '25';
  $export['itinerary_field_collection'] = $view;

  $view = new view();
  $view->name = 'teaser_entities';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_cruise_factory_entity';
  $view->human_name = 'Teaser Entities';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Teaser Entities';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser';
  /* Field: Cruise_factory_entity: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_cruise_factory_entity';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  /* Filter criterion: Cruise_factory_entity: cruise_factory_entity type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_cruise_factory_entity';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'cruise_factory_cruise_deal' => 'cruise_factory_cruise_deal',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'teaser-entities';
  $export['teaser_entities'] = $view;

  return $export;
}
