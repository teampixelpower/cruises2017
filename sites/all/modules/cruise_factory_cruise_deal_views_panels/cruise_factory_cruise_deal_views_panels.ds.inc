<?php
/**
 * @file
 * cruise_factory_cruise_deal_views_panels.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function cruise_factory_cruise_deal_views_panels_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'wider_teaser';
  $ds_view_mode->label = 'Wider Teaser';
  $ds_view_mode->entities = array(
    'cruise_factory_entity' => 'cruise_factory_entity',
  );
  $export['wider_teaser'] = $ds_view_mode;

  return $export;
}
