jQuery(document).ready(function($) {

  $("#node-2701").addClass("hide-it");

  var isMobile = "false";
  // Browser Body Classes
  if (navigator != undefined && navigator.userAgent != undefined) {
    user_agent = navigator.userAgent.toLowerCase();
    if (user_agent.indexOf('android') > -1) {
      $(document.body).addClass('android');
      isMobile = "true";
    }
    else if (user_agent.indexOf('iphone') > -1) {
      $(document.body).addClass('iphone');
      isMobile = "true";
    }
    else if (user_agent.indexOf('ipad') > -1) {
      $(document.body).addClass('ipad');
      isMobile = "true";
    }else{
        $(".header").sticky({
            className: "sticky-header",
            wrapperClassName: "sticky-is-on"
        });
    }
    if (user_agent.indexOf('firefox') > -1) {
      $(document.body).addClass('firefox');
    }
    if (user_agent.indexOf('chrome') > -1) {
      $(document.body).addClass('chrome');
    }
    if (user_agent.indexOf('safari') > -1) {
      $(document.body).addClass('safari');
    }
    $(document.body).addClass(user_agent);
  }

  $(".region-nav-right h2").on('click', function() {
    $(".region-nav-right .block .content").toggle("slide");
    $(".region-nav-right h2").toggleClass('close-search');
  });


  $('#logo').attr('alt', 'Cruises International');
  $('#logozone a').attr('alt', 'Cruises International');


  //browser window scroll (in pixels) after which the "back to top" link is shown
  var offset = 200,
    //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
    offset_opacity = 1200,
    //duration of the top scrolling animation (in ms)
    scroll_top_duration = 700;
  //grab the "back to top" link
  $back_to_top = $('.go-top');
  //hide or show the "back to top" link
  $(window).scroll(function() {
    ($(this).scrollTop() > offset) ? $back_to_top.addClass('is-visible'): $back_to_top.removeClass('is-visible fade-out');
    if ($(this).scrollTop() > offset_opacity) {
      $back_to_top.addClass('fade-out');
    }
  });

  //smooth scroll to top
  $back_to_top.on('click', function(e) {
    e.preventDefault();
    $('body,html').animate({
      scrollTop: 0,
    }, scroll_top_duration);
  });


  $("#show-full-itinerary").on('click', function(e) {
    e.preventDefault();
    $(".view-display-id-itinerary_more_than_ten .view-content").slideToggle("slide");
    if ($(this).text() == "Show full itinerary") {
      $(this).text('Collapse itinerary');
    } else if ($(this).text() == "Collapse itinerary") {
      $(this).text('Show full itinerary');
    }
  });

  $('a[href*="#"]').on('click', function(event) {

      fullurl = location.protocol +"//"+ location.host + Drupal.settings.basePath;
      var href = $(this).attr("href").replace(/<front>/, fullurl);
      $(this).attr("href", href);

      $('html,body').animate({
        scrollTop: $(this.hash).offset().top - 250
      }, 500);
  });

  var homelink = "front";
  $("nav").each(function () {
    if ($(this).find("a[href='" + homelink + "']").length) {

      fullurl = location.protocol +"//"+ location.host + Drupal.settings.basePath;
      var href = $(this).attr("href").replace(/<front>/, fullurl);
      $(this).attr("href", href);
    }
  });


  function submit_pre_check() {
    final_destination_group   =   $('select[name=final_destination_group]').val();
    if (!final_destination_group) {
      final_destination_group =   $('select[name=cruise_factory_entity%3Afinal_destination_group]').val();
    }
    final_cruiseline          =   $('select[name=final_cruiseline]').val();
    if (!final_cruiseline) {
      final_cruiseline        =   $('select[name=cruise_factory_entity%3Afinal_cruiseline]').val();
    }
    final_sailingdate         =   $('select[name=final_sailingdate]').val();
    if (!final_sailingdate) {
      final_sailingdate       =   $('select[name=cruise_factory_entity%3Afinal_sailingdate]').val();
    }

    if (final_destination_group == '_none') {
      final_destination_group_blank = true;
    } else {
      final_destination_group_blank = false;
    }
    if (final_cruiseline == '_none') {
      final_cruiseline_blank = true;
    } else {
      final_cruiseline_blank = false;
    }
    if (final_sailingdate == '_none') {
      final_sailingdate_blank = true;
    } else {
      final_sailingdate_blank = false;
    }

    pre_check_results = {
      "final_destination_group_blank":final_destination_group_blank,
      "final_cruiseline_blank":final_cruiseline_blank,
      "final_sailingdate_blank":final_sailingdate_blank
    }
    return pre_check_results;
  }

  function ready_up_or_validate() {

    pre_check_results = submit_pre_check();

    final_destination_group_blank = pre_check_results["final_destination_group_blank"];
    final_cruiseline_blank = pre_check_results["final_cruiseline_blank"];
    final_sailingdate_blank = pre_check_results["final_sailingdate_blank"];

    if (final_destination_group_blank) {
      $(".form-item-final-destination-group .select2-container").addClass('error-highlight');
      $("#facetapi-facet-search-apicruise-factory-complete-deals-block-final-destination-group-wrapper").append("<div class='warnings-box-destination'>Please make a selection for Destination</div>");
    } else {
      $(".form-item-final-destination-group .select2-container").removeClass('error-highlight');
      $('.warnings-box-destination').remove();
      $('.warnings-box-destination').hide();
    }
    if (final_sailingdate_blank) {
      $(".form-item-final-sailingdate .select2-container").addClass('error-highlight');
      $("#facetapi-facet-search-apicruise-factory-complete-deals-block-final-sailingdate-wrapper").append("<div class='warnings-box-sailingdate'>Please make a selection for Sailing Date</div>");
      $('.warnings-box-sailingdate').show();
    } else {
      $(".form-item-final-sailingdate .select2-container").removeClass('error-highlight');
      $('.warnings-box-sailingdate').remove();
      $('.warnings-box-sailingdate').hide();
    }

    if ((!final_destination_group_blank) &&
        // (!final_cruiseline_blank) &&
        (!final_sailingdate_blank)) {
      $('#frontpage_submit').addClass('animated bounceIn');
    }


  if ((!final_destination_group_blank) &&
        (!final_sailingdate_blank)) {
      search_destination = "/cruises";
      final_search_url = search_destination
                       + "?f[]=final_destination_group:" + final_destination_group 
                       + "&f[]=final_cruiseline:" + final_cruiseline
                       + "&f[]=final_sailingdate:" + final_sailingdate;
      return final_search_url;
    } else {
      return false;
    }
  }

  // var this_page = window.location.pathname;
  // var cabin_generic_prices_button = $("a.enquire_now_button").attr("href");
  // console.log("enquire_now_button: " + cabin_generic_prices_button);
  // cabin_generic_prices_button_with_url = cabin_generic_prices_button + "?cruise_deal=" + this_page;
  // cabin_generic_prices_button_with_url_w_cabin_name = cabin_generic_prices_button + "&cruise_deal=" + this_page;
  // if (cabin_generic_prices_button.indexOf("cabin_name") >= 0) {
  //   $("a.enquire_now_button").attr("href", cabin_generic_prices_button_with_url_w_cabin_name);
  // } else {
  //   $("a.enquire_now_button").attr("href", cabin_generic_prices_button_with_url);
  // }

  function frontpage_submit() {
    final_search_url = ready_up_or_validate();
    if (final_search_url) {
      window.location.href = final_search_url; // alert(final_search_url);
    }
  }
  var throbber = $("<div id='throbber' class='throbber opacityoff'>&nbsp;</div>"); 
  $(".facetapi-facetapi-select-dropdowns select").after(throbber);

  $(".facetapi-facetapi-select-dropdowns select").on('change', function(e) {  
    $(this).next(".throbber").toggleClass('opacityoff');
  });

  $('#facet_filters').on('change', function(e) {
    pre_check_results = submit_pre_check();
    if (
       (!pre_check_results["final_destination_group_blank"]) &&
       (!pre_check_results["final_cruiseline_blank"]) &&
       (!pre_check_results["final_sailingdate_blank"])) {
          $('#frontpage_submit').addClass('animated bounceIn');
    } else if (
       (!pre_check_results["final_destination_group_blank"]) &&
       (!pre_check_results["final_sailingdate_blank"])) {
          $('#frontpage_submit').addClass('animated bounceIn');
    }
  });

  $("#frontpage_submit").on('click', function(e) {
    e.preventDefault();
    frontpage_submit();
  });

  $("#precheck").on('click', function(e) {
    e.preventDefault();
    submit_pre_check();
  });

  // $("#homepage-newsletter h2.pane-title").on('click', function (c){
  //   alert("iiiyyy");
  //   // if($(document).width() < 480){
  //   //     $('#node-2701').toggleClass('see-form');
  //   // }
  // });


  $("#homepage-newsletter h2.pane-title").click(function(){
    console.log("its working");
    $("#node-2701").toggleClass("hide-it");
  });

});


