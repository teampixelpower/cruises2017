# Set this to the root of your project when deployed: 
# http_path = "/"

# Set the images directory relative to your http_path or change
# the location of the images themselves using http_images_path: 
# http_images_dir = "assets/images"

# Production Assets URL

Encoding.default_external = "utf-8"

css_dir = "css"
sass_dir = "sass"

# images_dir = "images"
# generated_images_dir = "images"
# http_images_dir = "images"

# javascripts_dir = "js"

output_style = :expanded #compact
environment = :development #production

# relative_assets = true

line_comments = false
