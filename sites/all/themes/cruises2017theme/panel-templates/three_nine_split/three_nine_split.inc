<?php

/**
 * @file
 * Implementation for the three_nine_split panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Three Nine Split'),
  'category' => t('Benchmark Digital'),
  'icon' => 'three_nine_split.png',
  'theme' => 'panels_three_nine_split',
  'css' => 'three_nine_split.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
