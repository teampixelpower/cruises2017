<?php

/**
 * @file
 * Implementation for the five_seven_split panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Four Five Three Split'),
  'category' => t('Benchmark Digital'),
  'icon' => 'four_five_three_split.png',
  'theme' => 'panels_four_five_three_split',
  'css' => 'four_five_three_split.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'middle' => t('Middle'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
