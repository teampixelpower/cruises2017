<?php
/**
 * @file
 * Template for the seventy_fifty_split panel layout.
 */
?>
<script src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 -->
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
jQuery(document).ready(function($) {
	$( "#cruises2017_cruiseline_tabs" ).tabs();
});
</script>
 -->
<div id="cruises2017_cruiseline_tabs">
	<?php if ($content['intro-top-block']): ?>
		<div class="panel-col-top panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
			<div class="inside">
				<?php print $content['intro-top-block']; ?>
			</div>
		</div>
	<?php endif; ?>
	<ul class="top-tab-links flexbox-wrapper">
		<li class="flexbox-child"><a href="#cruises-tab" class="tab-icon deals">Cruise Deals</a></li>
		<li class="flexbox-child"><a href="#ships-tab" class="tab-icon ships">Ships</a></li>
		<li class="flexbox-child"><a href="#info-tab" class="tab-icon info">More Info</a></li>
		<li class="flexbox-child"><a href="#gallery-tab" class="tab-icon gallery">Gallery</a></li>
	</ul>

	<div class="tab-panel cruises-tab" id="cruises-tab">
		<div class="panel-display panel-cruises2017_cruiseline_tabs clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
			<?php if ($content['cruises-tab_top']): ?>
				<div class="panel-col-top panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['cruises-tab_top']; ?></div>
				</div>
			<?php endif; ?>
			<div class="center-wrapper flexbox-wrapper">
				<?php if ($content['cruises-tab_left']): ?>
					<div class="panel-col-first panel-panel col-md-3 flexbox-child">
						<div class="inside"><?php print $content['cruises-tab_left']; ?></div>
					</div>
				<?php endif; ?>
				<?php if ($content['cruises-tab_right']): ?>
					<div class="panel-col-last panel-panel col-md-9 flexbox-child cruiseline-full-width-image">
						<div class="inside"><?php print $content['cruises-tab_right']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($content['cruises-tab_bottom']): ?>
				<div class="panel-col-bottom panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['cruises-tab_bottom']; ?></div>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="tab-panel ships-tab" id="ships-tab">
		<div class="panel-display panel-cruises2017_cruiseline_tabs clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
			<?php if ($content['ships-tab_top']): ?>
				<div class="panel-col-top panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['ships-tab_top']; ?></div>
				</div>
			<?php endif; ?>
			<div class="center-wrapper">
				<?php if ($content['ships-tab_left']): ?>
					<div class="panel-col-first panel-panel col-md-6">
						<div class="inside"><?php print $content['ships-tab_left']; ?></div>
					</div>
				<?php endif; ?>

				<?php if ($content['ships-tab_right']): ?>
					<div class="panel-col-last panel-panel col-md-6">
						<div class="inside"><?php print $content['ships-tab_right']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($content['ships-tab_bottom']): ?>
				<div class="panel-col-bottom panel-panel col-md-12">
					<div class="inside"><?php print $content['ships-tab_bottom']; ?></div>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="tab-panel gallery-tab" id="gallery-tab">
		<div class="panel-display panel-cruises2017_cruiseline_tabs clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
			<?php if ($content['gallery-tab_top']): ?>
				<div class="panel-col-top panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['gallery-tab_top']; ?></div>
				</div>
			<?php endif; ?>
			<div class="center-wrapper">
				<?php if ($content['gallery-tab_left']): ?>
					<div class="panel-col-first panel-panel col-md-6">
						<div class="inside"><?php print $content['gallery-tab_left']; ?></div>
					</div>
				<?php endif; ?>

				<?php if ($content['gallery-tab_right']): ?>
					<div class="panel-col-last panel-panel col-md-6">
						<div class="inside"><?php print $content['gallery-tab_right']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($content['gallery-tab_bottom']): ?>
				<div class="panel-col-bottom panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['gallery-tab_bottom']; ?></div>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="tab-panel info-tab" id="info-tab">
		<div class="panel-display panel-cruises2017_cruiseline_tabs clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
			<?php if ($content['info-tab_top']): ?>
				<div class="panel-col-top panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['info-tab_top']; ?></div>
				</div>
			<?php endif; ?>
			<div class="center-wrapper">
				<?php if ($content['info-tab_left']): ?>
					<div class="panel-col-first panel-panel col-md-6">
						<div class="inside"><?php print $content['info-tab_left']; ?></div>
					</div>
				<?php endif; ?>

				<?php if ($content['info-tab_right']): ?>
					<div class="panel-col-last panel-panel col-md-6">
						<div class="inside"><?php print $content['info-tab_right']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($content['info-tab_bottom']): ?>
				<div class="panel-col-bottom panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['info-tab_bottom']; ?></div>
				</div>
			<?php endif; ?>
		</div>
	</div>

</div>
	<?php if ($content['below-tabs-block']): ?>
		<div class="panel-col-top panel-panel col-md-12 col-sm-no-padding col-xs-no-padding below-tabs-block">
			<div class="inside">
				<?php print $content['below-tabs-block']; ?>
			</div>
		</div>
	<?php endif; ?>

