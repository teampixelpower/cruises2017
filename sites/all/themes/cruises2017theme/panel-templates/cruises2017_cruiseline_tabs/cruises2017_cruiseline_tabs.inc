<?php

/**
 * @file
 * Implementation for the cruises2017_cruiseline_tabs panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Cruises 2017 Tabbed Cruiseline Page'),
  'category' => t('Benchmark Digital'),
  'icon' => 'cruises2017_cruiseline_tabs.png',
  'theme' => 'panels_cruises2017_cruiseline_tabs',
  'css' => 'cruises2017_cruiseline_tabs.css',
  'regions' => array(

    'intro-top-block' => t('Intro Top Block'),

    'cruises-tab_top' => t('Cruises Tab: Top'),
    'cruises-tab_left' => t('Cruises Tab: Left side'),
    'cruises-tab_right' => t('Cruises Tab: Right side'),
    'cruises-tab_bottom' => t('Cruises Tab: Bottom'),

    'gallery-tab_top' => t('Gallery Tab: Top'),
    'gallery-tab_left' => t('Gallery Tab: Left side'),
    'gallery-tab_right' => t('Gallery Tab: Right side'),
    'gallery-tab_bottom' => t('Gallery Tab: Bottom'),
   
    'info-tab_top' => t('Info Tab: Top'),
    'info-tab_left' => t('Info Tab: Left side'),
    'info-tab_right' => t('Info Tab: Right side'),
    'info-tab_bottom' => t('Info Tab: Bottom'),

    'ships-tab_top' => t('Ships Tab: Top'),
    'ships-tab_left' => t('Ships Tab: Left side'),
    'ships-tab_right' => t('Ships Tab: Right side'),
    'ships-tab_bottom' => t('Ships Tab: Bottom'),


    'below-tabs-block' => t('Below Tabs Block Content')

  ),
);
