<?php

/**
 * @file
 * Implementation for the nine_three_split panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Nine Three Split'),
  'category' => t('Benchmark Digital'),
  'icon' => 'nine_three_split.png',
  'theme' => 'panels_nine_three_split',
  'css' => 'nine_three_split.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
