<?php
/**
 * @file
 * Template for the thirty-seventy-split panel layout.
 */
?>
<div class="panel-display panel-thirty-seventy-split clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <?php if ($content['top']): ?>
    <div class="panel-col-top panel-panel col-lg-12 col-md-12 col-sm-6 col-xs-12">
      <div class="inside"><?php print $content['top']; ?></div>
    </div>
  <?php endif; ?>

  <div class="center-wrapper">
    <?php if ($content['left']): ?>
      <div class="panel-col-first panel-panel col-lg-12 col-md-3 col-sm-6 col-xs-12">
        <div class="inside"><?php print $content['left']; ?></div>
      </div>
    <?php endif; ?>
    <?php if ($content['right']): ?>
      <div class="panel-col-last panel-panel col-lg-9 col-md-9 col-sm-6 col-xs-12">
        <div class="inside"><?php print $content['right']; ?></div>
      </div>
    <?php endif; ?>
  </div>

  <?php if ($content['bottom']): ?>
    <div class="panel-col-bottom panel-panel col-lg-12 col-md-12 col-sm-6 col-xs-12">
      <div class="inside"><?php print $content['bottom']; ?></div>
    </div>
  <?php endif; ?>
</div>
