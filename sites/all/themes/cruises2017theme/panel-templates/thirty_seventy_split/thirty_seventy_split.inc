<?php

/**
 * @file
 * Implementation for the thirty-seventy-split panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Thirty Seventy Split'),
  'category' => t('Benchmark Digital'),
  'icon' => 'thirty_seventy_split.png',
  'theme' => 'panels_thirty_seventy_split',
  'css' => 'thirty_seventy_split.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
