<?php

/**
 * @file
 * Implementation for the seventy_fifty_split panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Four Eight Split'),
  'category' => t('Benchmark Digital'),
  'icon' => 'four_eight_columns_split.png',
  'theme' => 'panels_four_eight_columns_split',
  'css' => 'four_eight_columns_split.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
