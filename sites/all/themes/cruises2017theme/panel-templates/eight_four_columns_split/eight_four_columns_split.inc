<?php

/**
 * @file
 * Implementation for the seventy_fifty_split panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Eight col-md-4 Split'),
  'category' => t('Benchmark Digital'),
  'icon' => 'eight_four_columns_split.png',
  'theme' => 'panels_eight_four_columns_split',
  'css' => 'eight_four_columns_split.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
