<?php
/**
 * @file
 * Template for the seventy_fifty_split panel layout.
 */
?>

<script src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 -->
<script>
jQuery(document).ready(function($) {
	$(".accordion").accordion({active: false, autoHeight: true, collapsible: true, heightStyle: "content"});

$('.ui-accordion-header').on('click', function(event) {
    // event.preventDefault();
    this_top = $(this).offset().top; //  - 250;
    console.log("this_top: " + this_top);
    scroll_top_duration = 700;
    $('body,html').animate({
      scrollTop: this_top,
    }, scroll_top_duration);
  });


});
</script>
<div id="cruises2017_deal_tabs">
<?php if ($content['intro-top-block']): ?>
	<div class="panel-col-top panel-panel col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sm-no-padding col-xs-no-padding">
		<div class="inside">
			<?php print $content['intro-top-block']; ?>
		</div>
	</div>
<?php endif; ?>
	<ul class="top-tab-links flexbox-wrapper">
		<li class="flexbox-child"><a href="#overview-tab" class="tab-icon info">Cruise Overview</a></li>
		<li class="flexbox-child"><a href="#staterooms-rates-tab" class="tab-icon deals">Staterooms & Rates</a></li>
		<li class="flexbox-child"><a href="#ship-tab" class="tab-icon ships">Ship</a></li>
		<li class="flexbox-child"><a href="#highlights-tab" class="tab-icon highlights">Highlights</a></li>
		<li class="flexbox-child"><a href="#gallery-tab" class="tab-icon gallery">Gallery</a></li>
	</ul>

	<div class="tab-panel overview-tab" id="overview-tab">
		<div class="panel-display panel-cruises2017_deal_tabs clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
			<?php if ($content['overview-tab_top']): ?>
				<div class="panel-col-top panel-panel col-lg-12 col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['overview-tab_top']; ?></div>
				</div>
			<?php endif; ?>
			<div class="center-wrapper flexbox-wrapper">
				<?php if ($content['overview-tab_left']): ?>
					<div class="panel-col-first panel-panel col-md-3 col-sm-12 col-xs-12 flexbox-child  col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['overview-tab_left']; ?></div>
					</div>
				<?php endif; ?>

				<?php if ($content['overview-tab_right']): ?>
					<div class="panel-col-last panel-panel col-md-9 hidden-sm hidden-xs flexbox-child cruise-deal-full-width-image  col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['overview-tab_right']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($content['overview-tab_bottom']): ?>
				<div class="panel-col-bottom panel-panel col-lg-12 col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['overview-tab_bottom']; ?></div>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="tab-panel staterooms-rates-tab" id="staterooms-rates-tab">
		<div class="panel-display panel-cruises2017_deal_tabs clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
			<?php if ($content['staterooms-rates-tab_top']): ?>
				<div class="panel-col-top panel-panel col-lg-12 col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['staterooms-rates-tab_top']; ?></div>
				</div>
			<?php endif; ?>
			<div class="center-wrapper">
				<?php if ($content['staterooms-rates-tab_left']): ?>
					<div class="panel-col-first panel-panel col-md-6 col-sm-12 col-xs-12 col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['staterooms-rates-tab_left']; ?></div>
					</div>
				<?php endif; ?>

				<?php if ($content['staterooms-rates-tab_right']): ?>
					<div class="panel-col-last panel-panel col-md-6 col-sm-12 col-xs-12 col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['staterooms-rates-tab_right']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($content['staterooms-rates-tab_bottom']): ?>
				<div class="panel-col-bottom panel-panel col-lg-12 col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['staterooms-rates-tab_bottom']; ?></div>
				</div>
			<?php endif; ?>
		</div>
</div>

	<div class="tab-panel ship-tab" id="ship-tab">
		<div class="panel-display panel-cruises2017_deal_tabs clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
			<?php if ($content['ship-tab_top']): ?>
				<div class="panel-col-top panel-panel col-lg-12 col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['ship-tab_top']; ?></div>
				</div>
			<?php endif; ?>
			<div class="center-wrapper">
				<?php if ($content['ship-tab_left']): ?>
					<div class="panel-col-first panel-panel col-md-6 col-sm-12 col-xs-12 col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['ship-tab_left']; ?></div>
					</div>
				<?php endif; ?>

				<?php if ($content['ship-tab_right']): ?>
					<div class="panel-col-last panel-panel col-md-6 col-sm-12 col-xs-12 col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['ship-tab_right']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($content['ship-tab_bottom']): ?>
				<div class="panel-col-bottom panel-panel col-lg-12 col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['ship-tab_bottom']; ?></div>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="tab-panel highlights-tab" id="highlights-tab">
		<div class="panel-display panel-cruises2017_deal_tabs clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
			<?php if ($content['highlights-tab_top']): ?>
				<div class="panel-col-top panel-panel col-lg-12 col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['highlights-tab_top']; ?></div>
				</div>
			<?php endif; ?>
			<div class="center-wrapper">
				<?php if ($content['highlights-tab_left']): ?>
					<div class="panel-col-first panel-panel col-md-6 col-sm-12 col-xs-12 col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['highlights-tab_left']; ?></div>
					</div>
				<?php endif; ?>

				<?php if ($content['highlights-tab_right']): ?>
					<div class="panel-col-last panel-panel col-md-6 col-sm-12 col-xs-12 col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['highlights-tab_right']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($content['highlights-tab_bottom']): ?>
				<div class="panel-col-bottom panel-panel col-lg-12 col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['highlights-tab_bottom']; ?></div>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="tab-panel gallery-tab" id="gallery-tab">
		<div class="panel-display panel-cruises2017_deal_tabs clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
			<?php if ($content['gallery-tab_top']): ?>
				<div class="panel-col-top panel-panel col-lg-12 col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['gallery-tab_top']; ?></div>
				</div>
			<?php endif; ?>
			<div class="center-wrapper">
				<?php if ($content['gallery-tab_left']): ?>
					<div class="panel-col-first panel-panel col-md-6 col-sm-12 col-xs-12 col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['gallery-tab_left']; ?></div>
					</div>
				<?php endif; ?>

				<?php if ($content['gallery-tab_right']): ?>
					<div class="panel-col-last panel-panel col-md-6 col-sm-12 col-xs-12 col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['gallery-tab_right']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($content['gallery-tab_bottom']): ?>
				<div class="panel-col-bottom panel-panel col-lg-12 col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['gallery-tab_bottom']; ?></div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
	<?php if ($content['below-tabs-block']): ?>
		<div class="panel-col-top panel-panel col-lg-12 col-md-12 below-tabs-block col-sm-no-padding col-xs-no-padding">
			<div class="inside">
				<?php print $content['below-tabs-block']; ?>
			</div>
		</div>
	<?php endif; ?>

