<?php

/**
 * @file
 * Implementation for the cruises2017_deal_tabs panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Cruises 2017 Tabbed Deal Page'),
  'category' => t('Benchmark Digital'),
  'icon' => 'cruises2017_deal_tabs.png',
  'theme' => 'panels_cruises2017_deal_tabs',
  'css' => 'cruises2017_deal_tabs.css',
  'regions' => array(

    'intro-top-block' => t('Intro Top Block'),

    'overview-tab_top' => t('Overview Tab: Top'),
    'overview-tab_left' => t('Overview Tab: Left side'),
    'overview-tab_right' => t('Overview Tab: Right side'),
    'overview-tab_bottom' => t('Overview Tab: Bottom'),

    'staterooms-rates-tab_top' => t('Staterooms & Rates Tab: Top'),
    'staterooms-rates-tab_left' => t('Staterooms & Rates Tab: Left side'),
    'staterooms-rates-tab_right' => t('Staterooms & Rates Tab: Right side'),
    'staterooms-rates-tab_bottom' => t('Staterooms & Rates Tab: Bottom'),

    'ship-tab_top' => t('Ship Tab: Top'),
    'ship-tab_left' => t('Ship Tab: Left side'),
    'ship-tab_right' => t('Ship Tab: Right side'),
    'ship-tab_bottom' => t('Ship Tab: Bottom'),

    'highlights-tab_top' => t('Highlights Tab: Top'),
    'highlights-tab_left' => t('Highlights Tab: Left side'),
    'highlights-tab_right' => t('Highlights Tab: Right side'),
    'highlights-tab_bottom' => t('Highlights Tab: Bottom'),

    'gallery-tab_top' => t('Gallery Tab: Top'),
    'gallery-tab_left' => t('Gallery Tab: Left side'),
    'gallery-tab_right' => t('Gallery Tab: Right side'),
    'gallery-tab_bottom' => t('Gallery Tab: Bottom'),

    'below-tabs-block' => t('Below Tabs Block Content')

  ),
);
