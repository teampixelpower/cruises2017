<?php

/**
 * @file
 * Implementation for the seventy_thirty_split panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Seventy Thirty Split'),
  'category' => t('Benchmark Digital'),
  'icon' => 'seventy_thirty_split.png',
  'theme' => 'panels_seventy_thirty_split',
  'css' => 'seventy_thirty_split.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
