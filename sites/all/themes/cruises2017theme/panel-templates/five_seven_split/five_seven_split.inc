<?php

/**
 * @file
 * Implementation for the five_seven_split panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Five Seven Split'),
  'category' => t('Benchmark Digital'),
  'icon' => 'five_seven_split.png',
  'theme' => 'panels_five_seven_split',
  'css' => 'five_seven_split.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
