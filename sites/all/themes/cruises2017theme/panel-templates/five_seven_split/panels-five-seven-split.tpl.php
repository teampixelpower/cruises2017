<?php
/**
 * @file
 * Template for the seventy_fifty_split panel layout.
 */
?>
<div class="panel-display panel-five_seven_split clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <?php if ($content['top']): ?>
    <div class="panel-col-top panel-panel col-md-12">
      <div class="inside"><?php print $content['top']; ?></div>
    </div>
  <?php endif; ?>

  <div class="center-wrapper">
    <?php if ($content['left']): ?>
      <div class="panel-col-first panel-panel col-lg-5 col-md-5 col-sm-6 col-xs-12">
        <div class="inside"><?php print $content['left']; ?></div>
      </div>
    <?php endif; ?>
    <?php if ($content['right']): ?>
      <div class="panel-col-last panel-panel col-lg-7 col-md-7 col-sm-6 col-xs-12">
        <div class="inside"><?php print $content['right']; ?></div>
      </div>
    <?php endif; ?>
  </div>

  <?php if ($content['bottom']): ?>
    <div class="panel-col-bottom panel-panel col-lg-12 col-md-12 col-sm-6 col-xs-12">
      <div class="inside"><?php print $content['bottom']; ?></div>
    </div>
  <?php endif; ?>
</div>
