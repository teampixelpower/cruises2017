<?php

/**
 * @file
 * Implementation for the six_six_split panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Six Six Split'),
  'category' => t('Benchmark Digital'),
  'icon' => 'six_six_split.png',
  'theme' => 'panels_six_six_split',
  'css' => 'six_six_split.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
