<?php
/**
 * @file
 * Template for the seventy_fifty_split panel layout.
 */
?>
<script src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 -->
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
jQuery(document).ready(function($) {
	$( "#cruises2017_destination_tabs" ).tabs();
});
</script>
 -->
<div id="cruises2017_destination_tabs">
<?php if ($content['intro-top-block']) : ?>
	<div class="panel-col-top panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
		<div class="inside">
			<?php print $content['intro-top-block']; ?>
		</div>
	</div>
<?php endif; ?>
<?php 
	// this is the method to strip tags to check for actual content to hide the tabs that are empty
	$cruises_tab = $content['cruises-tab_top'] . $content['cruises-tab_left'] . $content['cruises-tab_right'] . $content['cruises-tab_bottom'];
	$cruises_tab_actual_content = strip_tags($cruises_tab, '<img>');
	$cruises_tab_actual_content = str_replace(array("	"," ","\n","\r","&#8203;","​"), "", $cruises_tab_actual_content);
// dpm("cruises_tab");
// dpm($cruises_tab_actual_content);
	if ($cruises_tab_actual_content) {
		$cruises_tab_actual_content_yes = TRUE;
		$total_tabs = $total_tabs + 1;
	}

	$gallery_tab = $content['gallery-tab_top'] . $content['gallery-tab_left'] . $content['gallery-tab_right'] . $content['gallery-tab_bottom'];
	$gallery_tab_actual_content = strip_tags($gallery_tab, '<img>');
	$gallery_tab_actual_content = str_replace(array("	"," ","\n","\r","&#8203;","​"), "", $gallery_tab_actual_content);
// dpm("gallery_tab");
// dpm($gallery_tab_actual_content);

	if ($gallery_tab_actual_content) {
		$gallery_tab_actual_content_yes = TRUE;
		$total_tabs = $total_tabs + 1;
	}

	$map_tab = $content['map-tab_top'] . $content['map-tab_left'] . $content['map-tab_right'] . $content['map-tab_bottom'];

	// $map_tab_actual_content = strip_tags($map_tab, '<span> <div>');
	// $map_tab_actual_content = str_replace(array("	"," ","\n","\r","&#8203;","​"), "", $map_tab_actual_content);
	$map_tab_actual_content = 'always show';
// dpm("map_tab");
// dpm($map_tab_actual_content);
	// if ($map_tab_actual_content) {
		$map_tab_actual_content_yes = TRUE;
		$total_tabs = $total_tabs + 1;
	// }

// dpm('total_tabs: ' . $total_tabs);
// dpm('cruises_tab_actual_content_yes: ' . $cruises_tab_actual_content_yes);
// dpm('gallery_tab_actual_content_yes: ' . $gallery_tab_actual_content_yes);
// dpm('map_tab_actual_content_yes: ' . $map_tab_actual_content_yes);


	?>
<?php if ($total_tabs > 1) : ?>
	<ul class="top-tab-links flexbox-wrapper">
		<?php if ($cruises_tab_actual_content_yes) : ?>
			<li class="flexbox-child"><a href="#cruises-tab" class="tab-icon deals">Cruise Deals</a></li>
		<?php endif; ?>
		<?php if ($gallery_tab_actual_content_yes) : ?>
			<li class="flexbox-child"><a href="#gallery-tab" class="tab-icon gallery">Gallery</a></li>
		<?php endif; ?>
		<?php if ($map_tab_actual_content_yes) : ?>
			<li class="flexbox-child"><a href="#map-tab" class="tab-icon map">Map</a></li>
		<?php endif; ?>
	</ul>
<?php endif; ?>
	<?php if ($cruises_tab_actual_content_yes) : ?>
	<div class="tab-panel cruises-tab" id="cruises-tab">
		<div class="panel-display panel-cruises2017_destination_tabs clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
			<?php if ($content['cruises-tab_top']) : ?>
				<div class="panel-col-top panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['cruises-tab_top']; ?></div>
				</div>
			<?php endif; ?>
			<div class="center-wrapper flexbox-wrapper">
				<?php if ($content['cruises-tab_left']) : ?>
					<div class="panel-col-first panel-panel col-md-3 flexbox-child col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['cruises-tab_left']; ?></div>
					</div>
				<?php endif; ?>
				<?php if ($content['cruises-tab_right']) : ?>
					<div class="panel-col-last panel-panel col-md-9 flexbox-child cruiseline-full-width-image col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['cruises-tab_right']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($content['cruises-tab_bottom']) : ?>
				<div class="panel-col-bottom panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['cruises-tab_bottom']; ?></div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
<?php if ($gallery_tab_actual_content_yes) : ?>	
	<div class="tab-panel gallery-tab" id="gallery-tab">
		<div class="panel-display panel-cruises2017_destination_tabs clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
			<?php if ($content['gallery-tab_top']) : ?>
				<div class="panel-col-top panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['gallery-tab_top']; ?></div>
				</div>
			<?php endif; ?>
			<div class="center-wrapper">
				<?php if ($content['gallery-tab_left']) : ?>
					<div class="panel-col-first panel-panel col-md-6 col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['gallery-tab_left']; ?></div>
					</div>
				<?php endif; ?>

				<?php if ($content['gallery-tab_right']) : ?>
					<div class="panel-col-last panel-panel col-md-6 col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['gallery-tab_right']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($content['gallery-tab_bottom']) : ?>
				<div class="panel-col-bottom panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['gallery-tab_bottom']; ?></div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
<?php if ($map_tab_actual_content_yes) : ?>
	<div class="tab-panel map-tab" id="map-tab">
		<div class="panel-display panel-cruises2017_destination_tabs clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
			<?php if ($content['map-tab_top']) : ?>
				<div class="panel-col-top panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['map-tab_top']; ?></div>
				</div>
			<?php endif; ?>
			<div class="center-wrapper">
				<?php if ($content['map-tab_left']) : ?>
					<div class="panel-col-first panel-panel col-md-6 col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['map-tab_left']; ?></div>
					</div>
				<?php endif; ?>

				<?php if ($content['map-tab_right']) : ?>
					<div class="panel-col-last panel-panel col-md-6 col-sm-no-padding col-xs-no-padding">
						<div class="inside"><?php print $content['map-tab_right']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($content['map-tab_bottom']) : ?>
				<div class="panel-col-bottom panel-panel col-md-12 col-sm-no-padding col-xs-no-padding">
					<div class="inside"><?php print $content['map-tab_bottom']; ?></div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>

</div>
	<?php if ($content['below-tabs-block']) : ?>
		<div class="panel-col-top panel-panel col-md-12 below-tabs-block col-sm-no-padding col-xs-no-padding">
			<div class="inside">
				<?php print $content['below-tabs-block']; ?>
			</div>
		</div>
	<?php endif; ?>

