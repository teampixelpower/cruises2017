<?php

/**
 * @file
 * Implementation for the cruises2017_destination_tabs panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Cruises 2017 Tabbed Destination Page'),
  'category' => t('Benchmark Digital'),
  'icon' => 'cruises2017_destination_tabs.png',
  'theme' => 'panels_cruises2017_destination_tabs',
  'css' => 'cruises2017_destination_tabs.css',
  'regions' => array(

    'intro-top-block' => t('Intro Top Block'),

    'cruises-tab_top' => t('Cruises Tab: Top'),
    'cruises-tab_left' => t('Cruises Tab: Left side'),
    'cruises-tab_right' => t('Cruises Tab: Right side'),
    'cruises-tab_bottom' => t('Cruises Tab: Bottom'),

    'gallery-tab_top' => t('Gallery Tab: Top'),
    'gallery-tab_left' => t('Gallery Tab: Left side'),
    'gallery-tab_right' => t('Gallery Tab: Right side'),
    'gallery-tab_bottom' => t('Gallery Tab: Bottom'),

    'map-tab_top' => t('Map Tab: Top'),
    'map-tab_left' => t('Map Tab: Left side'),
    'map-tab_right' => t('Map Tab: Right side'),
    'map-tab_bottom' => t('Map Tab: Bottom'),

    'below-tabs-block' => t('Below Tabs Block Content')

  ),
);
