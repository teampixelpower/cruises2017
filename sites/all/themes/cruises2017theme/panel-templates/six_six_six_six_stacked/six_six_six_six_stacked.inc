<?php

/**
 * @file
 * Implementation for a Six-Six Column Row, 12-col, then second Six-Six Column Row
 */

// Plugin definition
$plugin = array(
  'title' => t('Six-Six Column Row, 12-col, then second Six-Six Column Row.'),
  'category' => t('Benchmark Digital'),
  'icon' => 'six_six_six_six_stacked.png',
  'theme' => 'panels_six_six_six_six_stacked',
  'css' => 'six_six_six_six_stacked.css',
  'regions' => array(
    'top' => t('Top'),
    'left_above' => t('Left above'),
    'right_above' => t('Right above'),
    'middle' => t('Middle'),
    'left_below' => t('Left below'),
    'right_below' => t('Right below'),
    'bottom' => t('Bottom'),
  ),
);

