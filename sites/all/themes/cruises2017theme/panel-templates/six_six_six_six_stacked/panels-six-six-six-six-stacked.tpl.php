<?php
/**
 * @file
 * Template for a Six-Six Column Row, 12-col, then second Six-Six Column Row
 */
?>
<div class="panel-display panel-six_six_six_six_stacked clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="panel-panel panel-col-top col-lg-12 col-md-12  col-sm-6 col-xs-12">
    <div class="inside"><?php print $content['top']; ?></div>
  </div>
  <div class="center-wrapper two-column-top">
    <div class="panel-panel panel-col-first col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="inside"><?php print $content['left_above']; ?></div>
    </div>

    <div class="panel-panel panel-col-last col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="inside"><?php print $content['right_above']; ?></div>
    </div>
  </div>
  <div class="panel-panel panel-col-middle col-lg-12 col-md-12 col-sm-6 col-xs-12">
    <div class="inside"><?php print $content['middle']; ?></div>
  </div>
  <div class="center-wrapper two-column-lower">
    <div class="panel-panel panel-col-first col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="inside"><?php print $content['left_below']; ?></div>
    </div>

    <div class="panel-panel panel-col-last col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="inside"><?php print $content['right_below']; ?></div>
    </div>
  </div>
  <div class="panel-panel panel-col-bottom col-lg-6 col-md-12 col-sm-6 col-xs-12">
    <div class="inside"><?php print $content['bottom']; ?></div>
  </div>
</div>
