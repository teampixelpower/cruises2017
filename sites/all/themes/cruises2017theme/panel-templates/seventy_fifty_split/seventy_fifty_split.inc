<?php

/**
 * @file
 * Implementation for the seventy_fifty_split panel layout.
 */

// Plugin definition
$plugin = array(
  'title' => t('Seventy Fifty Split'),
  'category' => t('Benchmark Digital'),
  'icon' => 'seventy_fifty_split.png',
  'theme' => 'panels_seventy_fifty_split',
  'css' => 'seventy_fifty_split.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
