<?php

/**
 * @file
 * Implementation for a WLH Full page content 2 column bricks
 */

// Plugin definition
$plugin = array(
  'title' => t('Two column bricks for Full World Leisure Holidays Pages'),
  'category' => t('Benchmark Digital'),
  'icon' => 'twocol_bricks_wlh_fullpage.png',
  'theme' => 'panels_twocol_bricks_wlh_fullpage',
  'css' => 'twocol_bricks_wlh_fullpage.css',
  'regions' => array(
    'top' => t('Top'),
    'left_above' => t('Left above'),
    'right_above' => t('Right above'),
    'middle' => t('Middle'),
    'left_below' => t('Left below'),
    'right_below' => t('Right below'),
    'bottom' => t('Bottom'),
  ),
);

