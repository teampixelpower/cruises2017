<?php
/**
 * @file
 * cruises2017theme Template
 */


// function cruises2017theme_html_head_alter(&$head_elements) {
//   $this_page = $head_elements['metatag_shortlink']['#value'];
//   $deal_page = 'https://www.cruises.co.za/cruise_factory_entity/cruise_factory_complete_deal/';
//   if (strpos($this_page, $deal_page) !== false) {
//     // delete other weird canonical
//     $this_page = $head_elements['metatag_shortlink']['#value'];
//     $this_page = str_replace('https://www.cruises.co.za/','',$this_page);
//     $this_page_path = db_query("SELECT * FROM {url_alias} WHERE source = :source", array(":source" => $this_page))->fetchAll();
//     $this_page_final_path = "https://www.cruises.co.za/" . $this_page_path[0]->alias;
//     $this_page_initial_path = "cruise_factory_entity/cruise_factory_complete_cruise_deal/" . $this_page_final_id;
//     $removetagname = "drupal_add_html_head_link:canonical:" . $this_page_final_path;
//     unset($head_elements[$removetagname]);

//     $url_title = $head_elements['metatag_canonical']['#value'];
//     $final_id = db_query("SELECT id FROM {eck_cruise_factory_entity} WHERE type = 'cruise_factory_complete_cruise' AND title = :title", array(":title" => $url_title))->fetchAll();
//     $final_id = $final_id[0]->id;
//     $initial_path = "cruise_factory_entity/cruise_factory_complete_cruise/" . $final_id;
//     $path = db_query("SELECT * FROM {url_alias} WHERE source = :source", array(":source" => $initial_path))->fetchAll();
//     $final_path = "https://www.cruises.co.za/" . $path[0]->alias;
//     $head_elements['metatag_canonical']['#value'] = $final_path;
//   }

//   if (isset($head_elements['metatags_quick_description']['#attributes']['content'])) {
//     $head_elements['metatags_quick_description']['#attributes']['content'] = truncate_utf8($head_elements['metatags_quick_description']['#attributes']['content'], 140, TRUE);    
//   }
//   if (isset($head_elements['metatag_description_0']['#value'])) {
//     $head_elements['metatag_description_0']['#value'] = truncate_utf8($head_elements['metatag_description_0']['#value'], 140, TRUE);
//   }

// }

/**
 * Add body classes if certain regions have content.
 */
function cruises2017theme_preprocess_html(&$variables) {
	if (!empty($variables['page']['sidebar'])) { //	if ($variables['page']['sidebar_first']) {
		$variables['classes_array'][] = 'sidebars';
	} else  {
		$variables['classes_array'][] = 'no-sidebar';
	}
  if (!empty($variables['page']['trackingcode_zone_head'])) {
    $variables['trackingcode_zone_head'] = block_get_blocks_by_region('trackingcode_zone_head');
  }
  if (!empty($variables['page']['trackingcode_zone_footer'])) {
    $variables['trackingcode_zone_footer'] = block_get_blocks_by_region('trackingcode_zone_footer');
  }
  foreach($variables['user']->roles as $role){
    $variables['classes_array'][] = 'user-role-' . drupal_html_class($role);
  }
}
/**
 * Implements hook_metatag_metatags_view_alter().
 *
 * Shorten all meta description tags to 150 characters.
 */
function cruises2017theme_metatag_metatags_view_alter(&$output, $instance, $options) {
  if (isset($output['description']['#attached']['drupal_add_html_head'][0][0]['#value'])) {
    $output['description']['#attached']['drupal_add_html_head'][0][0]['#value'] = truncate_utf8($output['description']['#attached']['drupal_add_html_head'][0][0]['#value'], 140, TRUE);
  }
}

// Add additional template suggestions
function cruises2017theme_preprocess_page(&$variables) {

  // drupal_add_js(drupal_get_path('theme', 'cruises2017theme') . '/js/Lettering.js/jquery.lettering.js',array('scope' => 'footer'));
  drupal_add_js(drupal_get_path('theme', 'cruises2017theme') . '/js/sticky/jquery.sticky.js',array('scope' => 'footer'));

  // drupal_add_js('https://code.jquery.com/ui/1.12.1/jquery-ui.js', array('scope' => 'header'));
  drupal_add_css('https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', 'external');

  drupal_add_css('https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.6/animate.min.css', 'external');
  drupal_add_js('https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', 'external');

  drupal_add_js('https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.min.js', 'external');
  drupal_add_js('https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/shortcuts/sticky.min.js', 'external');

  drupal_add_js(drupal_get_path('theme', 'cruises2017theme') . '/js/cruises2017theme-header.min.js',array('scope' => 'header'));
  drupal_add_js(drupal_get_path('theme', 'cruises2017theme') . '/js/cruises2017theme-footer.js',array('scope' => 'footer'));

	if (!empty($variables['node'])) {
		$variables['theme_hook_suggestions'][] = 'page--node-' . $variables['node']->type;
	}
  if($panel_page = page_manager_get_current_page()){
    // dpm($panel_page['name']);
    // $variables['template_files'] = "page__" . $panel_page['name'];
    $edited_name = str_replace ("-", "_", $panel_page['name']);
    $edited_name = str_replace ("entity_panels_cruise_factory_entity_", "", $edited_name);
    $variables['theme_hook_suggestions'][] = "page__" . $edited_name;
    // dpm($variables['theme_hook_suggestions']);
  }
	if (module_exists('path')) {
		//allow template suggestions based on url paths.
		$alias = drupal_get_path_alias(str_replace('/edit','',$_GET['q']));
		if ($alias != $_GET['q']) {
			$suggestions = array();
			$template_filename = 'page';
			foreach (explode('/', $alias) as $path_part) {
				$template_filename = $template_filename . '--' . $path_part;
				$suggestions[] = $template_filename;
		  	}
			$alias_array =  explode('/', $alias);
      $variables['template_files'] = $suggestions;
			$variables['theme_hook_suggestions'][] = $suggestions;
		}
	}
}

function cruises2017theme_menu_link(&$variables) {
  //get path alias of current page
  $current_path = drupal_get_path_alias();
  //get path alias of menu item
  $menu_path = drupal_get_path_alias($variables['element']['#href']);
  //if the href of the menu item is found in the current path
  if (strstr($current_path, $menu_path)){
    //add active-trail class to li and a tags for that item
    $variables['element']['#attributes']['class'][] = 'active-trail';
    $variables['element']['#localized_options']['attributes']['class'][] = 'active-trail';
  }
  return theme_menu_link($variables);
}
function cruises2017theme_preprocess_menu_link(&$variables) {
  //get path alias of current page
  $current_path = drupal_get_path_alias();
  //get path alias of menu item
  $menu_path = drupal_get_path_alias($variables['element']['#href']);
  //if the href of the menu item is found in the current path
  if (strstr($current_path, $menu_path)){
    //add active-trail class to li and a tags for that item
    $variables['element']['#attributes']['class'][] = 'active-trail';
    $variables['element']['#localized_options']['attributes']['class'][] = 'active-trail';
  }
}

/*function cruises2017theme_search_api_query_alter(SearchApiQueryInterface $query) {
  $original_keys = $query->getOriginalKeys();
  // dpm($original_keys);
  // dpm($query);
  if ($original_keys == 'final_destination_group') {
      $query->keys(NULL);
  }
  //  else if ($original_keys == 'resort-specials') {
  //     $query->keys(NULL);
  // }
}
*/




