<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">

  <?php 
  if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php 
      // $dom = new DOMDocument;
      // @$dom->loadHTML( $header );
      // $xPath = new DOMXpath($dom);
      // $xPathQuery = "//div";
      // $elements = $xPath -> query( $xPathQuery );
      // if( !is_null( $elements ) ){
      //   $results = array();
      //   foreach( $elements as $index => $element ){
      //     $nodes = $element -> childNodes;
      //     foreach( $nodes as $subindex => $node ){
      //       /* Each table row is assigned in new level in array using $index 
      //       if( $node->nodeType == XML_ELEMENT_NODE ) $results[ $index ][] = $node->nodeValue;
      //     }
      //   }
      // }
/*      if($rows) {
        $rows_dom = new DOMDocument;
        @$rows_dom->loadHTML( $rows );
        $rows_xPath = new DOMXpath($rows_dom);
        // $rows_xPathQuery = "//div";
        $rows_xPathQuery = '//div[contains(@class,"views-field-final-sailingdate-1")]';
        $rows_elements = $rows_xPath -> query( $rows_xPathQuery );
        if( !is_null( $rows_elements ) ){
          $rows_results = array();
          foreach( $rows_elements as $rows_index => $rows_element ){
            $rows_nodes = $rows_element -> childNodes;
            foreach( $rows_nodes as $rows_subindex => $rows_node ){
              /* Each table row is assigned in new level in array using $index * /
              if( $rows_node->nodeType == XML_ELEMENT_NODE ) $rows_results[ $rows_index ][] = $rows_node->nodeValue;
            }
          }
        }
        $first_date = date('Y', strtotime($rows_results[0][0]));
        $first_date_heading = date('F Y', strtotime($rows_results[0][0]));
        $current_month = date('F', strtotime($rows_results[0][0]));
        $current_month_number = date('m', strtotime($rows_results[0][0]));
        // dpm("first_date: " . $first_date);
        // $current_date = $current_month; // date_parse($results[1][0]);
        // dpm($current_date);
        // $current_month = $current_date['month'];
        // $current_month = date("F", mktime(0, 0, 0, $current_month, 10));

        $previous_month = $current_month_number - 1;
        $previous_month = date("F", mktime(0, 0, 0, $previous_month, 10));

        $next_month = $current_month_number + 1;
        $next_month = date("F", mktime(0, 0, 0, $next_month, 10));

        $header = str_replace("&laquo; Prev",$previous_month,$header);
        $header = str_replace("Next &raquo;",$next_month,$header);
      }
*/
      print $header;

      ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

    <script>
    jQuery(document).ready(function($) {
      $("#pricing-accordion").accordion({active: 0, autoHeight: false,collapsible: true });

      $('.ui-accordion-header').on('click', function(event) {
        // event.preventDefault();
        this_top = $("#pricing-accordion .ui-accordion-content-active").offset().top - 250;
        console.log("this_top: " + this_top);
        scroll_top_duration = 700;
        $('body,html').animate({
          scrollTop: this_top,
        }, scroll_top_duration);
      });



      });
    </script>

  <?php if ($rows): ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 departing-text-line"><p>Departing</p></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 view-content">
      <div id="pricing-accordion" class="pricing-accordion clearfix">
        <?php print $rows; ?>
      </div>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>


  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>
