# royalcaribbean2016theme
## Version 1.0

Based on Benchmark Clean Theme.



### Changelog

#### Feb 2017
- Added fields and styling for new importer fields


#### Oct 2016
- Added Liquid Slider theming

#### April 2016

- 15 April 2016 - Go Live Day

#### March 2016

- Initial commit
- hi-res logos


drush mi Cruiselines; drush mi DestinationGroup; drush mi Destinations; drush mi Ships; drush mi CruisesSiteDeals; drush mi Ship_Facts_; drush mi ShipImages; 
