<?php
// $Id: block.tpl.php,v 1.3 2007/08/07 08:39:36 goba Exp $
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clearfix block block-<?php print $block->module ?> <?php if ($block->css_class) { print $block->css_class; } ?>">
	<?php if (!empty($block->subject)): ?>
		<h2><?php print $block->subject ?></h2>
		<?php if (!empty($block->subtitle)): ?><div class="blocksubtitle subtitle"><?php print $block->subtitle; ?></div><?php endif; ?>
	<?php endif;?>
	<div class="content"><?php print $content ?></div>
</div>
