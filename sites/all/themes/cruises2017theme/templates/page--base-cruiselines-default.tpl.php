<header class="header row">

	<div class="container header_container">
		<?php if ($page['topzone']): ?>
			<div class="row">
				<div id="topzone">
					<div class="container">
						<?php print render($page['topzone']); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="row flexbox-wrapper">
			<div id="logozone" class="col-md-3 flexbox-child">
				<a href="<?php print base_path(); ?>" title="<?php print $site_name; ?>">
						<img
							src="<?php print check_url($logo); ?>"
							srcset="<?php print check_url($logo); ?> 1x, <?php
							print base_path() . path_to_theme(); ?>/logo@2x.png 2x"
							alt="<?php print $site_name; ?>"
							title="<?php print $site_name; ?>"
							itemprop="logo"
							border="0"
							id="logo"
						/>
					</a>
			</div>
			<div class="header_right col-md-9 flexbox-child">
				<?php if ($page['header_right']): ?>
					<div class="region-header_right-wrapper col-md-12">
						<?php print render($page['header_right']); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="navigationzone col-md-12">
		<div class="row">
			<div class="container header_container flexbox-wrapper">
					<?php if ($page['navigationzone']): ?>
						<nav class="flexbox-child">
							<input id="mobile-menu-icon" type="checkbox" />
							<label for="mobile-menu-icon">☰</label>
							<div class="region-navigationzone-wrapper col-lg-11 col-md-11 col-sm-12 col-xs-12">
								<?php print render($page['navigationzone']); ?>
							</div>
						</nav>
						<?php if ($page['nav_right']): ?>
							<div class="region-nav_right-wrapper col-md-1 flexbox-child">
								<div class="flexbox-wrapper nav_right-flex-blocks-wrapper">
									<?php print render($page['nav_right']); ?>
								</div>
							</div>
						<?php endif; ?>
					<?php endif; ?>
			</div>
		</div>
	</div>
</header>


<?php /*

<?php if ($page['headerzone']): ?>
	<div id="headerzone">
		<?php print render($page['headerzone']);?>
		<div class="container">
			<div id="logozone-headerzone" class="logozone-headerzone col-md-12">
				<a href="<?php print base_path() ?>" title="<?php print $site_name ?>">
						<img
							src="<?php print base_path() . path_to_theme() ?>/images/logo-white.png"
							srcset="<?php print base_path() . path_to_theme() ?>/images/logo-white.png 1x, <?php
							print base_path() . path_to_theme() ?>/images/logo-white@2x.png 2x"
							alt="<?php print $site_name ?>"
							title="<?php print $site_name ?>"
							itemprop="logo"
							border="0"
							id="logo-headerzone"
						/>
					</a>
			</div>
		</div>
	</div>
<?php endif; */ ?>
<div class="wrapper-wrapper">
	<div id="wrapper-<?php if (module_exists('path')) {
		$alias = drupal_get_path_alias($_GET['q']); $class =
			explode('/', $alias);
			print $class[0]; } ?>" class="container wrapper <?php
			if (module_exists('path')) {
				$alias = drupal_get_path_alias($_GET['q']);
				$class = explode('/', $alias);
				print $class[0];
			} ?>">
		<div id="container" class="row clearfix">
			<?php /* if ($page['sidebar']): ?>
				<div id="sidebar" class="sidebar col-md-4">
					<?php print render($page['sidebar']); ?>
				</div>
			<?php endif; */ ?>
			<div id="contentblock" class="contentblock col-md-12">
			<?php /* if ($page['sidebar']) {
					print 'content-with-sidebars col-md-8';
				} else {
					print 'col-md-12'; } ?>"> */ ?>
				
				<div class="clearfix">
					
					<?php print $messages; ?>
					<?php print render($page['help']); ?>
					<?php if ($page['slideshow_zone']): ?>
						<div id="slideshow_zone" class="slideshow_zone col-md-12">
							<?php print render($page['slideshow_zone']); ?>
						</div>
					<?php endif; ?>
					<?php /* if ($page['facet_filters']): ?>
						<div id="facet_filters" class="col-md-12 clearfix">
							<?php print render($page['facet_filters']);?>
							<script>
								/*jQuery(document).ready(function($) {
									$(".how-many-nights-facet").find('select option').text(function(value) {
										if (value != 0) {
											if (value == 1) {
												return value + " night";
											} else {
												return value + " nights";
											}
										}
									});
								});*\/
							</script>
						</div>
					<?php endif; */ ?>



<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<script>
jQuery(document).ready(function($) {
  $("#cruises2017_cruiseline_tabs").tabs();
  $('#cruises2017_cruiseline_tabs ul.top-tab-links li a').click(function(e){
    e.preventDefault();
    $(this).tabs('show');
  });

	$('.open-tab').click(function (e) {
		e.preventDefault();
		var tab = $(this).attr('href');
	  var index = $('#cruises2017_cruiseline_tabs a[href="'+tab+'"]').attr('id');
	  index = index.replace('ui-id-','') - 1;
	  $('#cruises2017_cruiseline_tabs').tabs("option", "active", index);
	});

});
</script>

					<a id="main-content"></a>
					
					<div class="clearfix">
						<?php  if ($page): ?>
							<?php  if ($title): ?>
								<?php print render($title_prefix); ?>
									<h1 class="page-title"><?php print $title ?></h1>
								<?php print render($title_suffix); ?>
							<?php endif;  ?>
						<?php endif;  ?>
						<?php if ($tabs): ?>
							<div class="tabs clearfix">
								<?php print render($tabs); ?>
							</div>
						<?php endif; ?>
						<?php if ($action_links): ?>
							<ul class="action-links clearfix">
								<?php print render($action_links); ?>
							</ul>
						<?php endif; ?>
					</div>

					<div class="content">
						<?php print render($page['content']); ?>
					</div>
				</div>
			</div>

		</div>
	</div>
	<?php if ($page['bottom_call_to_action_one']): ?>
		<div id="bottom_call_to_action_one">
			<div class="container">
				<?php print render($page['bottom_call_to_action_one']);?>
			</div>
		</div>
	<?php endif; ?>
	<div id="bottom_call_to_action_two" class="col-md-12">
		<div class="container">
			<?php if ($page['bottom_call_to_action_two']) : ?>
				<div class="col-md-6">
					<?php print render($page['bottom_call_to_action_two']); ?>
				</div>
			<?php endif; ?>
			<?php if ($page['socialzone']) : ?>
				<div id="socialzone" class="col-md-6">
					<?php print render($page['socialzone']); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if ($page['pre_footer']): ?>
		<div id="pre_footer">
			<div class="container">
				<?php print render($page['pre_footer']);?>
			</div>
		</div>
	<?php endif; ?>
	<footer id="footerwrapper" class="row">
		<?php if ($page['footer']): ?>
			<div id="footer">
				<div class="container">
					<?php print render($page['footer']);?>
				</div>
			</div>
		<?php endif; ?>
	</footer>
	<?php if ($page['post_footer']): ?>
		<div id="post_footer">
			<div class="container">
				<?php print render($page['post_footer']);?>
			</div>
		</div>
	<?php endif; ?>
	<?php if ($page['pagebottom']): ?>
		<div id="pagebottom">
			<div class="container">
				<?php print render($page['pagebottom']);?>
			</div>
		</div>
	<?php endif; ?>
<!-- 	<div id="designlink">
		<div class="container">
			<a href="http://benchmark.digital" target="_blank">
				Website by Benchmark Digital
			</a>
		</div>
	</div> -->
	<div class="go-top">
		<strong>&#8963;</strong>
		<!-- <i class="fa fa-angle-up"></i> -->
	</div>
</div>
