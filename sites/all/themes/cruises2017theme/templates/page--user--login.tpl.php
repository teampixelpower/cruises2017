<header class="header row">

	<div class="container header_container">
		<?php if ($page['topzone']): ?>
			<div class="row">
				<div id="topzone">
					<div class="container">
						<?php print render($page['topzone']); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="row flexbox-wrapper">
			<div id="logozone" class="col-md-3 flexbox-child">
				<a href="<?php print base_path(); ?>" title="<?php print $site_name; ?>">
						<img
							src="<?php print check_url($logo); ?>"
							srcset="<?php print check_url($logo); ?> 1x, <?php
							print base_path() . path_to_theme(); ?>/logo@2x.png 2x"
							alt="<?php print $site_name; ?>"
							title="<?php print $site_name; ?>"
							itemprop="logo"
							border="0"
							id="logo"
						/>
					</a>
			</div>
			<div class="header_right col-md-9 flexbox-child">
				<?php if ($page['header_right']): ?>
					<div class="region-header_right-wrapper col-md-12">
						<?php print render($page['header_right']); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</header>
<div class="wrapper-wrapper">
	<div id="wrapper-<?php if (module_exists('path')) {
		$alias = drupal_get_path_alias($_GET['q']); $class =
			explode('/', $alias);
			print $class[0]; } ?>" class="container wrapper <?php
			if (module_exists('path')) {
				$alias = drupal_get_path_alias($_GET['q']);
				$class = explode('/', $alias);
				print $class[0];
			} ?>">
		<div id="container" class="row clearfix">
			<div id="contentblock" class="contentblock col-md-12">
				<a id="main-content"></a>
				<div class="clearfix">
					<div class="row">
						<div class="main-content-block col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1 col-xs-12">
							<?php print $messages; ?>
							<?php print render($page['help']); ?>
							<?php print render($page["content"]); ?>
						</div>
						<div class="col-lg-8 col-md-8 col-sm-8 hidden-xs" id="login-page-img-col">
							<?php print render($page["login_page_img"])?>
						</div>
					</div>
				</div>
			</div>
			<script>
				// jQuery(document).ready(function($) {
					// $('#user-login > div').text().css("display", "none");
					// alert($('#user-login > div').text());
				// });
			</script>
		</div>
	</div>
</div>

<?php if ($page['post_footer']): ?>
	<div id="post_footer">
		<div class="container">
			<?php print render($page['post_footer']);?>
		</div>
	</div>
<?php endif; ?>
<?php if ($page['pagebottom']): ?>
	<div id="pagebottom">
		<div class="container">
			<?php print render($page['pagebottom']);?>
		</div>
	</div>
<?php endif; ?>
	<div class="go-top">
		<strong>&#8963;</strong>
		<!-- <i class="fa fa-angle-up"></i> -->
	</div>
</div>
