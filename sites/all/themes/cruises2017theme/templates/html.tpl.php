<!DOCTYPE html>
<html lang="<?php if ($language->language) { print $language->language; } ?>" dir="<?php if ($language->dir) { print $language->dir; } ?>" <?php if ($rdf_namespaces) { print $rdf_namespaces; } ?>>
<meta charset="utf-8" />
<meta name="viewport" content="initial-scale=1.0, user-scalable=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<head profile="<?php if ($grddl_profile) { print $grddl_profile; } ?>">
	<?php print $head; ?>
	<title><?php 
		$head_title = truncate_utf8($head_title, 60, TRUE);
	  $head_title = str_replace(" 00:00:00","", $head_title);
		print $head_title; ?></title>

	<?php print $styles; ?>
	<!--[if IE]>
		<link href="<?php print base_path() . path_to_theme() ?>/css/fix-ie.min.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

	<!--[if lt IE 9]>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/livingston-css3-mediaqueries-js/1.0.0/css3-mediaqueries.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script>window.html5 || document.write('<script src="js/libs/html5.js"><\/script>')</script>
	<![endif]-->

	<!--[if lt IE 10]>
		<link href="<?php print base_path() . path_to_theme() ?>/css/fix-ie.min.css" rel="stylesheet" type="text/css">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/livingston-css3-mediaqueries-js/1.0.0/css3-mediaqueries.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script>window.html5 || document.write('<script src="js/libs/html5.js"><\/script>')</script>
	<![endif]-->
	<?php print $scripts; ?>

	<link rel="apple-touch-icon" href="<?php print base_path() . path_to_theme() ?>/apple-touch-icon.png" />
	<?php
	if (isset($trackingcode_zone_head)) {
	if ($trackingcode_zone_head) :
	?>
		<script id="trackingcode_zone_head_begin"></script>
			<?php print render($trackingcode_zone_head); ?>
		<script id="trackingcode_zone_head_end"></script>
	<?php
	endif;
	}
	?>

</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
	<div id="skip-link"><a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a></div>
	<?php print $page_top; ?>
	<?php print $page; ?>
	<?php print $page_bottom; ?>
	<?php if (isset($trackingcode_zone_footer)) : ?>
		<script id="trackingcode_zone_footer_begin"></script>
			<?php print render($trackingcode_zone_footer); ?>
		<script id="trackingcode_zone_footer_end"></script>
	<?php endif; ?>
</body>
</html>
